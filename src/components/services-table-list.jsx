import React from "react";
import { List, ListItem, Button, Link } from "framework7-react";

import "./services-table-list.less";

const ServicesTableList = ({ services, style }) => {
  return (
    <>
      <List className="services-table-list my-3" noChevron noHairlines style={style}>
        {services.map((service) => (
          <ListItem link={`/service/${service.refId}`} key={service.refId}>
            <div className="services-table-list-title" slot="title">
              {service.title}
            </div>
            <div
              className="services-table-list-subtitle item-text"
              slot="title"
            >
              {service.subtitle}
            </div>
            <div className="services-table-list-image" slot="media">
              <img loading="lazy" src={service.icon} alt={service.title} />
            </div>

            <div className="services-table-list-button" slot="inner">
              <Button
                className="prevent-active-state-propagation"
                type="button"
              >
                Đặt ngay
              </Button>
            </div>
          </ListItem>
        ))}
      </List>
    </>
  );
};

export default ServicesTableList;

import React, { useState } from "react";

import {
  f7,
  f7ready,
  App,
  View
} from "framework7-react";

import routes from "../js/routes";
import store from '../js/store';

import baseStyles from "../styles/base";
import MaxApi from "@momo-platform/max-api";
import axios from "axios";

import { SWRConfig } from "swr";

const MyApp = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const f7params = {
    name: "momo-webapp", // App name
    theme: "ios", // Automatic theme detection
    // App routes
    routes: routes,
    store: store,
    // Register service worker
    // serviceWorker: {
    //   path: "/service-worker.js",
    // },
    sheet: {
      backdrop: true,
    },
    dialog: {
      title: "Thông báo",
      buttonOk: "Đồng ý",
    },
    view: {
      browserHistory: true,
      browserHistorySeparator: '',
      iosDynamicNavbar: false,
      xhrCache: true,
      pushStateAnimate: false,
      preloadPreviousPage: false
    },
    webView: true,
  };

  MaxApi.init({
    appId: "vn.momo.web.quyengop",
    name: "vn.momo.web.quyengop",
    displayName: "Quyên Góp",
    client: {
      web: {
        hostId: "vn.momo.web.quyengop",
        accessToken:
          "U2FsdGVkX1/GbSqeoCZwUA4RlSDjsqVFWWtqL6Re9Rxc1LEGiZHMg7VgjxY89CBf3KsQngY3gW0fU7fQMJsmiNK2fNu60l6rxnNrGtAuE3s=",
      },
    },
    configuration_version: 1,
  });



  const alertLoginData = () => {
    f7.dialog.alert(
      "Username: " + username + "<br>Password: " + password,
      () => {
        f7.loginScreen.close();
      }
    );
  };
  f7ready(() => {
    // Call F7 APIs here
  });

  return (
    <>
      <SWRConfig
        value={{
          refreshInterval: 0,
          revalidateOnFocus: false,
          fetcher: (url) => axios.get(url).then((res) => res.data),
        }}
      >
        <App {...f7params} >
          <View main className="safe-areas" url="/" />
        </App>
      </SWRConfig>
      <style jsx global>
        {baseStyles}
      </style>
    </>
  );
}

export default MyApp;
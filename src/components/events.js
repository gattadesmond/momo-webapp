import Framework7 from "framework7";

const events = new Framework7.Events();

window.f7Events = events;

export default events;

//HOW TO USE

// import events from "@/components/events";

// f7Events.emit('somethingHappened', 'dwdwdw');

// events.on('somethingHappened', (data2) => {
//     console.log(`${data2}`);
//   });

//   events.on('somethingHappened2', (data2) => {
//     console.log(`${data2}`);
//   });

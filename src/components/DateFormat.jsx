import React from 'react';
import { format as Format, differenceInHours, differenceInMinutes } from 'date-fns';
import { vi } from 'date-fns/locale';

function DateFormat(props) {
  const { date, format } = props;

  const date1 = Date.parse(date);
  const date2 = new Date();

  var minuteBetween = differenceInMinutes(date2, date1);
  var hourBetween = differenceInHours(date2, date1);

  if (hourBetween < 1 && hourBetween > 0) {
    return (`${minuteBetween} phút trước`);
  }
  if (hourBetween <= 24 && hourBetween >= 1) {
    return (`${hourBetween} giờ trước`);
  }
  // return Format(date1, format ? format : 'k:mm | dd/MM/yyyy');
  return Format(date1, format ? format : 'dd/MM/yyyy');

}

export default DateFormat;
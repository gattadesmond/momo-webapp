import React, { useState, useEffect } from "react";
import MaxApi from "@momo-platform/max-api";
import $ from "dom7";
import {
  f7,
  Page,
  PageContent,
  Navbar,
  NavLeft,
  NavTitle,
  NavRight,
  Link,
  Toolbar,
  Block,
  Toggle,
  Sheet,
  Row,
  Col,
  Button,
  useStore
} from "framework7-react";

const NavbarMoMoFixed = ({ title, isBack }) => {
  const [scrollActive, setScrollActive] = useState(false);
  const [pageBack, setPageBack] = useState(false);
  useEffect(() => {

    setPageBack(isBack);

    function handleNavbarMoMoScroll(e) {
      if (e.target.scrollTop > 30) {
        setScrollActive(true);
      } else {
        setScrollActive(false);
      }
    }
    $(".page-content").on("scroll", function (e) {
      handleNavbarMoMoScroll(e);
    });
    // Specify how to clean up after this effect:
    return function cleanup() {
      $(".page-content").off("scroll", function (e) {
        handleNavbarMoMoScroll(e);
      });
    };


  }, [isBack]);

  const onBack = () => {
    if (!pageBack) {
      MaxApi.goBack();
    }
  };
  return (
    <div className={`momo-navbar-fixed ${scrollActive ? "is-active" : ""} ${isBack ? "is-top" : ""}`}>
      <Link className="navbar-fixed-btn " back={pageBack} onClick={(x) => onBack()} >
        <svg
          width={33}
          height={33}
          viewBox="0 0 33 33"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <rect
            width="32.9317"
            height="32.9317"
            rx="16.4659"
            fill="black"
            fillOpacity="0.25"
          />
          <path
            d="M24 17H10"
            stroke="white"
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round"
          />
          <path
            d="M17 24L10 17L17 10"
            stroke="white"
            strokeWidth={2}
            strokeLinecap="round"
            strokeLinejoin="round"
          />
        </svg>
      </Link>
      <div className="navbar-fixed-title">{title}</div>
      <div className="navbar-fixed-bg"></div>
    </div>
  );
};

export default NavbarMoMoFixed;

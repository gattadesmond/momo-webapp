import React, { useEffect, useState } from "react";
import { Swiper, SwiperSlide, Link } from "framework7-react";

import Image from "@/components/image";

import axios from "@/libs/axios";
import { System } from "@/libs/constants";
const MainSlider = () => {
  const [items, setItems] = useState(null);

  useEffect(() => {
    getData();
  }, []);

  const getData = () => {
    axios.get(`${System.apihost}/__get/Donation/HomeBannerV2`).then((res) => {
      if (!res.Result) {
        return;
      }

      setItems(res.Data);
    });
  };
  return (
    <>
      <div className="mainSlider">
        {items && (
          <Swiper
            pagination
            speed={500}
            slidesPerView={"auto"}
            spaceBetween={10}
            centeredSlides={true}
            loop={true}
          >
            {items.map((item, index) => (
              <SwiperSlide key={index} className="mainSlider-item">
                <div className="soju-img">
                  {item.Link ?
                    <Link href={item.Link} external className="d-block">
                      <Image
                        src={item.Avatar}
                        alt={item.Name}
                        width="700"
                        height="350"
                        round="5px"
                      />
                    </Link>
                    :
                    <Image
                      src={item.Avatar}
                      alt={item.Name}
                      width="700"
                      height="350"
                      round="5px"
                    />}

                </div>
              </SwiperSlide>
            ))}
          </Swiper>
        )}

        <style jsx>{`
          .mainSlider {
            padding: 12px 0;
            user-select: none;
          }

          .mainSlider :global(.mainSlider-item) {
            width: calc(100% - 32px);
          }
          .mainSlider :global(.swiper-container) {
            padding-bottom: 20px;
          }
          .mainSlider :global(.swiper-pagination) {
            bottom: 0;
          }
        `}</style>
      </div>
    </>
  );
};

export default MainSlider;

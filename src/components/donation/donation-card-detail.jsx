import React, { useState, useEffect, Fragment, useRef } from "react";

import {
  f7,
  Progressbar,
  Button,
  Link,
  Preloader,
  Popup,
  Page,
  Swiper,
  SwiperSlide,
  Navbar,
  NavLeft,
  NavTitle,
  Toolbar,
  Block,
} from "framework7-react";

import Image from "@/components/image";
import { System, Track } from "@/libs/constants";
import ShareBtn from "@/components/share-btn";
import DonationUserList from "@/components/donation/donation-user-list";
import DonationUserListRank from "@/components/donation/donation-user-list-rank";
import DonationTimeline from "@/components/donation/donation-timeline";
// import { isEmptyObject } from "@/utils/utils";
import { numberWithCommas } from "../../../utils/utils";
import axios from "@/libs/axios";

const DonationCaseDetail = ({ dona, ConfigData }) => {
  const [gallery, setGallery] = useState([]);
  const [popupArticle, setPopupArticle] = useState(false);
  const pb = useRef(null);

  const getGallery = (fnc) => {
    axios
      .get(
        `${System.apihost}/__get/Donation/ListDonationDetailAlbum?id=${dona.Id}`
      )
      .then((res) => {
        if (res.Error) {
          f7.preloader.hide();
          f7.dialog.alert(ApiResponseErrorCodeMessage[res.Error.Code]);
          fnc && fnc();
          return;
        }

        f7.preloader.hide();
        fnc && fnc(res.Data);
      })
      .catch((ex) => {
        f7.preloader.hide();
        Track({ Name: "DonationCaseDetail-getGallery", Info: ex.message });
        fnc && fnc();
      });
  };
  const bindGallery = (data) => {
    if (data) {
      var lstGalery = data.map((item) => ({
        url: item.AvatarUrl,
        caption: item.Description,
      }));
      lstGalery.unshift({
        url: dona.ContentAvatar,
        caption: "",
      });

      setGallery(lstGalery);

      pb.current = f7.photoBrowser.create({
        photos: lstGalery,
        navbarShowCount: true,
        theme: "dark",
        routableModals: false,
        popupCloseLinkText: "Đóng",
      });
    }
  };

  const openPhotoBrowser = (index) => {
    if (!pb || !pb.current) return;
    pb.current.open(index);
  };

  useEffect(() => {
    if (dona.Id)
      getGallery((data) => {
        bindGallery(data);
      });
  }, [dona.Id]);

  return (
    <>
      {!dona || !dona.Id ? (
        <div className="text-center py-5 mt-5">
          <Preloader size={24}></Preloader>
        </div>
      ) : (
        <>
          <ShareBtn data={dona} />
          <div className="dona-header">
            <div className="dona-slider">
              {gallery && gallery.length > 0 && (
                <Swiper
                  pagination
                  speed={500}
                  slidesPerView={"auto"}
                  spaceBetween={0}
                  centeredSlides={true}
                  loop={true}
                  pagination={{ type: "fraction" }}
                >
                  {gallery.map((item, index) => (
                    <SwiperSlide key={index} className="mainSlider-item">
                      <div className="soju-img">
                        <Link
                          href=""
                          className="d-block"
                          onClick={() => openPhotoBrowser(index)}
                        >
                          <Image
                            src={item.url}
                            alt={item.caption}
                            width="75"
                            height="40"
                          />
                        </Link>
                      </div>
                    </SwiperSlide>
                  ))}
                </Swiper>
              )}
            </div>
          </div>

          <div className="block mt-4 mb-4 no-select">
            <div className="dona-title">
              <h1>{dona.Title}</h1>
            </div>

            <div className="soju_sapo">{dona.Short}</div>

            <div className="soju_progress">
              <div className="soju_progress_title">
                {dona.Type == 1 ? (
                  <>
                    {" "}
                    <strong className="soju_progress_num">
                      {dona.TotalMoney}đ{" "}
                    </strong>{" "}
                    / {dona.ExpectedValueFormat}đ
                  </>
                ) : (
                  <>
                    Đã quyên góp{" "}
                    <strong className="soju_progress_num">
                      {dona.TotalMoney}
                    </strong>{" "}
                    / {dona.ExpectedValueFormat} <b>Heo Vàng</b>
                  </>
                )}
              </div>
              <div className="soju_progress_bar">
                <Progressbar
                  color={
                    dona.TotalOrder >= dona.ExpectedValue
                      ? "green"
                      : dona.IsEndCampaign
                      ? "gray"
                      : "pink"
                  }
                  progress={dona.FinishPercent > 100 ? 100 : dona.FinishPercent}
                ></Progressbar>
              </div>
            </div>

            <div className="soju_info">
              <div className="soju_info_item">
                <div className="soju_info_label">Lượt quyên góp</div>
                <div className="soju_info_value">
                  {numberWithCommas(parseInt(dona.TotalTrans))}
                </div>
              </div>

              <div className="soju_info_item">
                <div className="soju_info_label">Đạt được</div>
                <div className="soju_info_value">{dona.FinishPercent}%</div>
              </div>

              <div className="soju_info_item btn_soju">
                {dona.ExpectedValue != null &&
                dona.TotalOrder >= dona.ExpectedValue &&
                (!dona.IsContDonate || dona.IsEndCampaign) ? (
                  <div className="soju_info_label">Đạt mục tiêu</div>
                ) : dona.IsEndCampaign ? (
                  <div className="soju_info_label">Hết thời hạn</div>
                ) : (
                  <>
                    <div className="soju_info_label">Thời hạn còn</div>
                    <div className="soju_info_value">{dona.TimeToEndDate}</div>
                  </>
                )}
              </div>
            </div>
            {dona.PartnerImage && (
              <Fragment>
                <div className="splash no-select"></div>

                <div className="section_title no-select">
                  Đồng hành cùng MoMo
                </div>

                <div className="soju_sponser no-select">
                  <div className="soju_sponser_avatar">
                    <img
                      src={dona.PartnerImage}
                      className="img-fluid"
                      loading="lazy"
                      alt=""
                    />
                  </div>
                  <div className="soju_sponser_txt">
                    {dona.RootCategoryName != "" ? (
                      <Fragment>
                        <div className="soju-sponser-name">
                          {dona.RootCategoryName}
                        </div>
                        <div>{dona.CategoryName}</div>
                      </Fragment>
                    ) : (
                      <div className="soju-sponser-name">
                        {dona.CategoryName}
                      </div>
                    )}
                  </div>
                </div>
              </Fragment>
            )}
            <div className="splash no-select"></div>

            <div className="section_title no-select">
              Thông tin chương trình
            </div>

            <div
              className="article_content dona_article_more no-select"
              dangerouslySetInnerHTML={{ __html: dona.Content }}
            />

            <div className="mt-3">
              <Button
                outline
                className="soju-btn-gray"
                onClick={(x) => setPopupArticle(true)}
              >
                Xem chi tiết
              </Button>
            </div>

            <Popup
              className="demo-popup"
              opened={popupArticle}
              onPopupClosed={() => setPopupArticle(false)}
            >
              <Page className="no-select">
                <Navbar>
                  <NavLeft>
                    <Link
                      popupClose
                      className="text-dark"
                      iconIos="f7:chevron_left"
                    />
                  </NavLeft>
                  <NavTitle>Thông tin chương trình</NavTitle>
                </Navbar>

                <Toolbar position="bottom" className="soju-popup-toolbar ">
                  <div className="px-1 w-100">
                    <Button
                      outline
                      className="soju-btn-gray bg-white"
                      popupClose
                    >
                      Đóng
                    </Button>
                  </div>
                </Toolbar>

                <Block>
                  <div
                    className="article_content no-select"
                    dangerouslySetInnerHTML={{ __html: dona.Content }}
                  />
                </Block>
              </Page>
            </Popup>

            <DonationTimeline caseId={dona.Id} />
          </div>

          <DonationUserListRank
            Title="Nhà hảo tâm hàng đầu"
            Description="Danh sách được cập nhập sau 1 phút"
            ConfigData={ConfigData}
            DataDonation={dona}
          />

          <DonationUserList
            Title="Danh sách nhà hảo tâm"
            Description="Danh sách được cập nhập sau 1 phút"
            DataDonation={dona}
          />

          <style jsx>{`
            .dona-slider {
              position: relative;
              background-color: var(--gray-200);
            }

            .dona-slider:after {
              content: "";
              position: relative;
              padding-top: 53.3333%;
              display: block;
            }

            .dona-slider :global(.swiper-container) {
              position: absolute;
              top: 0;
              left: 0;
              width: 100%;
              height: 100%;
            }

            .dona-slider :global(.swiper-pagination-fraction) {
              width: auto;
              padding: 3px 11px;
              background-color: rgba(0, 0, 0, 0.6);
              border-radius: 5px;
              color: white;
              font-size: 12px;
              font-weight: 600;
              right: 10px;
              left: auto;
            }

            .dona-title h1 {
              font-size: 21px;
              line-height: 26px;
              font-weight: 600;
              margin-top: 0;
              margin-bottom: 8px;
            }

            .dona_article_more {
              max-height: 250px;
              overflow: hidden;
            }

            .soju_sponser {
              display: flex;
              flex-flow: row nowrap;
              align-items: center;
            }

            .soju_sponser_avatar {
            }
            .soju_sponser_avatar img {
              max-height: 34px;
              width: auto;
            }

            .soju-sponser-name {
              font-size: 15px;
              margin-bottom: 4px;
              font-weight: 600;
              color: var(--gray-800);
            }

            .soju_sponser_txt {
              padding-left: 10px;
              flex: 1;
              font-size: 13px;
              color: var(--gray-600);
            }

            .soju_sapo {
              margin-top: 10px;
              color: var(--gray-600);
              font-size: 14px;
              line-height: 20px;
            }

            .soju-tab :global(.mainSlider-item) {
            }

            .soju_info {
              margin-top: 15px;
              display: flex;
              flex-flow: row nowrap;
              align-items: center;
            }
            .soju_info_item {
              flex: 1 0 auto;
            }
            .soju_info_label {
              font-size: 15px;
              margin-bottom: 4px;
              line-height: 20px;
              color: var(--gray-600);
            }
            .soju_info_value {
            font-size: 15px;
            line-height: 20px;
            font-weight: bold;
          }

            .soju_progress {
              padding-top: 24px;
            }
            .soju_progress_title {
              margin-bottom: 8px;
              font-size: 14px;
              line-height: 18px;
              vertical-align: bottom;
              color: var(--gray-700);
            }

            .soju_endtime {
              padding-left: 5px;
            }
            .soju_endtime span {
              font-size: 11px;
              display: inline-block;
              padding: 3px 7px;
              border-radius: 20px;
              color: var(--orange);
              background-color: rgba(252, 100, 45, 0.15);
            }
            .soju_progress_num {
              color: var(--gray-900);
              font-size: 18px;
              line-height: 22px;
              font-weight: bold;
            }
            .soju_progress_bar :global(.progressbar) {
              height: 8px;
              border-radius: 10px;
            }
          `}</style>
        </>
      )}
    </>
  );
};

export default DonationCaseDetail;

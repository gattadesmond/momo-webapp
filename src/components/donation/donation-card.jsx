import React, { Fragment } from "react";

import { Progressbar, Button, Link } from "framework7-react";

import Image from "@/components/image";

// import { isEmptyObject } from "@/utils/utils";
import { numberWithCommas } from "../../../utils/utils";

const DonationCase = ({ data, donationType }) => {
  const getLinkDetail = (data, openPopup) => {
    var rs =
      (data.Type == 1 ? "/quyen-gop/" : "/quyen-gop-heovang/") +
      data.UrlRewrite;
    if (openPopup) {
      rs += `${
        !data.IsEndCampaign &&
        (data.IsContDonate || data.TotalOrder < data.ExpectedValue)
          ? "#action=openpopup"
          : ""
      }`;
    }
    return rs;
  };

  return (
    <>
      <div className="soju">
        <a
          href={getLinkDetail(data)}
          className="soju-absolute-link link-absolute"
        ></a>
        <div className="soju_header no-select">
          <Image
            src={data.Avatar}
            alt={data.Title}
            width="750"
            height="400"
            round="0"
          />
        </div>

        <div className="soju_body">
          <h2 className="soju_title">{data.Title}</h2>
          {data.PartnerImage && (
            <div className="soju_sponser">
              <div className="soju_sponser_avatar">
                <img
                  src={data.PartnerImage}
                  className="img-fluid"
                  loading="lazy"
                  alt=""
                />
              </div>
              <div className="soju_sponser_txt">{data.CategoryName}</div>
              {data.TimeToEndDate != "" && (
                <div className="soju_endtime">
                  <span>Còn {data.TimeToEndDate}</span>
                </div>
              )}
            </div>
          )}

          <div className="soju_progress">
            <div className="soju_progress_title">
              <strong className="soju_progress_num">
                {data.TotalMoney}
                {donationType == "traitimmomo" ? "đ" : ""}{" "}
              </strong>{" "}
              / {data.ExpectedValueFormat}
              {donationType == "traitimmomo" ? (
                "đ"
              ) : (
                <>
                  {" "}
                  <b>Heo vàng</b>
                </>
              )}
            </div>
            <div className="soju_progress_bar">
              <Progressbar
                color={
                  data.TotalOrder >= data.ExpectedValue
                    ? "green"
                    : data.IsEndCampaign
                    ? "gray"
                    : "pink"
                }
                progress={data.FinishPercent > 100 ? 100 : data.FinishPercent}
              ></Progressbar>
            </div>
          </div>

          <div className="soju_info">
            <div className="soju_info_item">
              <div className="soju_info_label">Lượt quyên góp</div>
              <div className="soju_info_value">
                {numberWithCommas(parseInt(data.TotalTrans))}
              </div>
            </div>

            <div className="soju_info_item">
              <div className="soju_info_label">Đạt được</div>
              <div className="soju_info_value">{data.FinishPercent}%</div>
            </div>

            <div className="soju_info_item btn_soju">
              <Button
                color={
                  data.ExpectedValue != null &&
                  data.TotalOrder >= data.ExpectedValue &&
                  !data.IsContDonate
                    ? "gray"
                    : data.IsEndCampaign
                    ? "gray"
                    : "pink"
                }
                className="btn_soju_qg"
                href={getLinkDetail(data, true)}
                small
                outline
              >
                {data.ExpectedValue != null &&
                data.TotalOrder >= data.ExpectedValue &&
                (!data.IsContDonate || data.IsEndCampaign)
                  ? "Đạt mục tiêu"
                  : data.IsEndCampaign
                  ? "Hết thời hạn"
                  : "Quyên góp"}
              </Button>
            </div>
          </div>
        </div>

        {/* <footer className="soju_footer">
          <div className="soju_actions">
            <button className="button">Button</button>
          </div>
        </footer> */}

        <style jsx>{`
          .soju_info {
            margin-top: 14px;
            display: flex;
            flex-flow: row nowrap;
            align-items: center;
          }
          .soju-absolute-link {
          }
          .soju_info_item {
            flex: 1 0 auto;
          }
          .soju_info_label {
              font-size: 15px;
              margin-bottom: 4px;
              line-height: 20px;
              color: var(--gray-600);
            }
            .soju_info_value {
            font-size: 15px;
            line-height: 20px;
            font-weight: bold;
          }
          .soju {
            background-color: white;
            border-radius: 10px;
            overflow: hidden;
            box-shadow: 0px 1px 9px rgba(0, 0, 0, 0.16);
            border-radius: 10px;
            margin-bottom: 15px;
            position: relative;
          }
          .soju_figure {
            padding: 0;
            margin: 0;
          }

          .soju_body {
            padding: 15px 20px;
          }

          .soju_title {
            font-weight: bold;
            font-size: 16px;
            line-height: 22px;
            margin: 0 0 0 0;
          }

          .soju_sponser {
            margin-top: 5px;
            display: flex;
            flex-flow: row nowrap;
            align-items: center;
          }

          .soju_sponser_avatar {
            width: 22px;
            height: 22px;
            border-radius: 50%;
            padding: 3px;
            border: 1px solid var(--gray-200);
            border-radius: 50%;
          }
          .soju_sponser_txt {
            flex: 1;
            padding-left: 8px;
            font-size: 12px;
          }
          .soju_copy {
            font-size: 13px;
            line-height: 19px;
            margin-top: 7px;
            color: var(--gray-700);
            display: -webkit-box;
            -webkit-line-clamp: 2;
            overflow: hidden;
            -webkit-box-orient: vertical;
          }

          .soju_progress {
            padding-top: 5px;
          }
          .soju_progress_title {
            margin-bottom: 7px;
            font-size: 12px;
            line-height: 18px;
            vertical-align: bottom;
            color: var(--gray-700);
          }
          .soju_endtime {
            padding-left: 5px;
          }
          .soju_endtime span {
            font-size: 11px;
            display: inline-block;
            padding: 3px 7px;
            border-radius: 20px;
            color: var(--orange);
            background-color: rgba(252, 100, 45, 0.15);
          }

          .soju_progress_num {
              color: var(--gray-900);
              font-size: 18px;
              line-height: 22px;
              font-weight: bold;
            }
          .soju_progress_bar :global(.progressbar) {
            height: var(--f7-progressbar-height);
            border-radius: var(--f7-progressbar-border-radius);
          }

          .btn_soju {
            z-index: 5;
            position: relative;
          }

          .btn_soju :global(.btn_soju_qg) {
            border-width: 1px;
            padding: 2px 0px;
            height: auto;
            font-size: 12px;
            width: auto;
          }
        `}</style>
      </div>
    </>
  );
};

export default DonationCase;

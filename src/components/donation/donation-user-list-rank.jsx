import React, { useState, useEffect, Fragment } from "react";

import { Preloader, Button } from "framework7-react";

//Lib import

import axios from "@/libs/axios";
import { ApiResponseErrorCodeMessage, System } from "@/libs/constants";
import { OriginalNumber } from "@/libs/helper"

import Image from "@/components/image";

import format from "number-format.js";

const DonationUserListRank = ({ ConfigData, DataDonation, Title, Description }) => {
  const PAGE_SIZE = 3;

  const [data, setData] = useState({
    Items: [],
    ItemsAll: [],
    PageRank: 0,
    ItemPerPageRank: 5,

  });

  const listData = (fnc) => {

    if (data.ItemsAll.length < 1) {
      var url = `${System.apiAppHost}/medalwall/v1/ranking/agent/${DataDonation.AppServiceId}?medal_types=${DataDonation.Type == 1 ? 'cb_donation_momo' : 'cb_donation_heovang'}`;
      axios.get(url, {
        headers: {
          Authorization: 'Bearer ' + ConfigData.FirebaseToken
        },
        contentType: 'application/json',
        crossDomain: true,
        dataType: 'json'
      }).then((res) => {
        if (res && res.response_info.error_code == 0 && res.response_info.error_message == "success") {
          fnc && fnc(res.items);
        }
      });
    } else {
      fnc && fnc(data.ItemsAll);
    }

  };

  const ListDonationRankUser = (fnc) => {
    listData((items) => {
      data.ItemsAll = items;
      var rs = [];
      for (var i = (data.PageRank * data.ItemPerPageRank); i < ((data.PageRank + 1) * data.ItemPerPageRank) && i < data.ItemsAll.length; i++) {
        rs.push(bindItemUserRank(data.ItemsAll[i]));
      }
      fnc && fnc(rs);
    })
  };

  const LoadDonationRankUserPaging = () => {
    setData({ ...data, IsLoading: true })
    ListDonationRankUser((res) => {
      data.Total = data.ItemsAll.length;
      data.Items = data.Items.concat(res);

      setData({ ...data, IsLoading: false })
    });
  };


  const bindItemUserRank = (item) => {
    var insertItem = {};
    insertItem.amount = item.medals[0].value;
    if (item.userId && item.userId.length > 9) {
      insertItem.phoneNumber = 'xxxxxxx' + item.userId.substr(item.userId.length - 3);
    }
    if (item.isAnonymous) {
      insertItem.cusName = 'Nhà hảo tâm';
      insertItem.avatar = 'https://static.mservice.io/images/s/momo-upload-api-200520135933-637255799731362042.png';
    }
    else {
      insertItem.cusName = item.name;
      insertItem.avatar = 'http://avatars.mservice.io.s3-ap-southeast-1.amazonaws.com/avatar/' + OriginalNumber(item.userId) + '.png';
    }
    return insertItem;
  };

  const LoadMore = () => {
    data.PageRank = data.PageRank + 1;
    setData({ ...data });
    LoadDonationRankUserPaging();
  }
  useEffect(() => {
    if (ConfigData && ConfigData.FirebaseToken) {

      LoadDonationRankUserPaging();
    }


  }, [ConfigData.FirebaseToken, DataDonation]);

  if (!data.Items || data.Items < 1) return <></>;
  return (

    <div className="section no-select">
      <div className="section-splash"></div>
      <div className="block mt-4 mb-4">
        <div className="section_title">{Title}</div>
        <div className="section_subTitle">{Description}</div>

        <div className="people-list">
          {data.Items &&
            data.Items.map((item, index) => (
              <div className="people-list-item" key={index}>
                <div className="people-item">
                  <div className="people-avatar">
                    <Image
                      src={item.avatar}
                      width="35"
                      height="35"
                      round="50%"
                      background="transparent"
                      onError={x => {
                        x.target.src = 'https://static.mservice.io/images/s/momo-upload-api-200520135933-637255799731362042.png'
                      }}
                    />
                  </div>

                  <div className="people-info">
                    <div className="people-name">{item.cusName}</div>
                    <div className="people-phone">{item.phoneNumber}</div>
                  </div>

                  <div className="people-money">
                    {format("#,###.", item.amount)}
                    {DataDonation.Type == 1 ? 'đ' : <img
                      src="https://static.mservice.io/pwa/images/donation/pig-coin-new.svg"
                      className="icon-heovang"
                      width="24"
                      alt=""
                    />}
                  </div>
                </div>

                {item.message && (
                  <div className="people-message">{item.message}</div>
                )}
              </div>
            ))}
        </div>

        <div className="mt-3">
          {(data.ItemPerPageRank <= data.Items.length && data.Items.length < data.Total) &&
            <Button
              outline
              className="soju-btn-gray"
              disabled={data.IsLoading}
              onClick={() => LoadMore()}
            >
              {data.IsLoading && <Preloader></Preloader>} &nbsp;
              {data.IsLoading ? "Đang chờ..." : "Xem thêm "}
            </Button>
          }
        </div>


      </div>

      <style jsx>{`
        .people-list .people-list-item:not(:last-child) {
          border-bottom: 1px solid var(--gray-200);
        }
        .people-item {
          padding: 8px 0 8px;
          position: relative;
          display: flex;
          flex-flow: row nowrap;
          align-items: center;
          font-size: 14px;
          color: var(--gray-900);
        }
        .people-avatar {
          width: 35px;
          height: 35px;
          flex: 0 0 35px;
        }

        .people-avatar + .people-info {
          padding-left: 10px;
        }

        .people-info {
          padding: 0 15px 0 0;
          flex-grow: 1;
        }
        .people-name {
          font-weight: 600;
          margin-bottom: 3px;
          padding-top: 3px;
        }
        .people-phone {
          color: var(--gray-500);
          font-size: 12px;
        }

        .people-money {
          white-space: nowrap;
          font-weight: bold;
          font-size: 13px;
        }

        .people-message {
          border-radius: 15px;
          background-color: var(--gray-100);
          padding: 7px 12px;
          font-size: 11px;
          position: relative;
          margin-bottom: 12px;
          display: inline-block;
        }
      `}</style>
    </div>
  );
};

export default DonationUserListRank;


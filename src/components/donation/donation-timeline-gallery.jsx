import React, { useState, useEffect, Fragment, useRef } from "react";

import { Preloader, Button, PhotoBrowser } from "framework7-react";

import Image from "@/components/image";

const DonationTimelineGallery = ({ gallerys }) => {
  const galleryEl = useRef(null);

  const openGallery = (index) => {
    galleryEl.current.open(index);
  };

  const galleryPhoto = gallerys.map((item) => ({
    url: item.Avatar,
    caption: item.Description,
  }));

  return (
    <>
      <div className="soju-timeline-gallery">
        {galleryPhoto.map((item, idx2) => {
          return (
            <div
              className="gallery-col"
              key={idx2}
              onClick={() => openGallery(idx2)}
            >
              
              <Image src={item.url}  alt={item.caption} width="50" height="50" round="5px" />
            </div>
          )
        })}
      </div>

      <PhotoBrowser
        photos={galleryPhoto}
        navbarShowCount={true}
        theme={"dark"}
        routableModals={false}
        popupCloseLinkText={"Đóng"}
        ref={galleryEl}
      />

      <style jsx>{`
        .soju-timeline-gallery {
          margin-top: 10px;
          display: flex;
          flex-flow: row wrap;
          margin-left: -5px;
          margin-right: -5px;
        }

        .soju-timeline-gallery .gallery-col {
          flex: 0 0 25%;
          box-sizing: border-box;
          padding : 6px;
        }
   
      `}</style>
    </>
  );
};

export default DonationTimelineGallery;

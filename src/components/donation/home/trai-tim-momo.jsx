import React, { useState, useEffect } from "react";

import { f7, Preloader, Button } from "framework7-react";

import DonationCase from "@/components/donation/donation-card.jsx";

//Lib import
import { ApiResponseErrorCodeMessage, System, Track } from "@/libs/constants";

import axios from "@/libs/axios";

const TraiTimMoMo = (props) => {
  const {
    donationType = "traitimmomo",
    ...rest
  } = props;

  const [Model, setModel] = useState({
    Items: [],
    PageCount: 0,
    Count: 5,
    Idx: 0,
    Type: 2,
    Url: '',
    IsLoading: false
  });

  useEffect(() => {

    if (donationType == "traitimmomo") {
      Model.Type = 1;
    }

    getData(true, (data) => {
      bindData(true, data, () => {
        setModel({ ...Model });
      });
    });



  }, [donationType]);

  const getData = (isReset, fnc) => {

    if (Model.IsLoading)
      return;

    Model.IsLoading = true;
    setModel({ ...Model });

    if (isReset) {
      Model.Idx = 0;
    }

    axios
      .get(`${System.apihost}/__get/Donation/LoadMoreDonationHome?count=${Model.Count}&idx=${Model.Idx}&type=${Model.Type}`)
      .then((res) => {
        if (res.Error) {
          f7.preloader.hide();
          f7.dialog.alert(ApiResponseErrorCodeMessage[res.Error.Code]);
          fnc && fnc();
          return;
        }

        f7.preloader.hide();
        fnc && fnc(res.Data);
      })
      .catch((ex) => {
        f7.preloader.hide();
        Track({ Name: 'TraiTimMoMo-getData', Info: ex.message });
        fnc && fnc();
      });
  }

  const bindData = (isReset, data, fnc) => {

    if (!data)
      return;
    Model.Idx = data.LastIndex;
    Model.PageCount = data.PageCount;
    Model.Count = data.Count;
    Model.TotalItems = data.TotalItems;


    if (isReset) {
      Model.Items = data.Items;
    } else {
      data.Items.forEach(x => {
        Model.Items.push(x);
      });
    }

    Model.IsLoading = false;

    fnc && fnc(Model);
  }

  const handleLoadMore = () => {
    getData(false, (data) => {
      bindData(false, data, () => {
        setModel({ ...Model });
      });
    });
  }

  return (
    <>
      {
        Model.Items.map((item, index) => (
          <DonationCase data={item} key={index} donationType={donationType} />
        ))}

      {Model.Idx < Model.PageCount && <div className="">

        <Button
          color="gray"
          className="text-dark font-weight-bold"
          large
          outline
          disabled={Model.IsLoading}
          onClick={() => handleLoadMore()}
        >
          {Model.IsLoading && <Preloader></Preloader>} &nbsp;
            {Model.IsLoading ? "Đang tải..." : "Xem thêm "}
        </Button>

      </div>}


      <style jsx>{`
         {
          /* .soju_info {
            margin-top: 14px;
            display: flex;
            flex-flow: row nowrap;
          } */
        }
      `}</style>
    </>
  );
};

export default TraiTimMoMo;


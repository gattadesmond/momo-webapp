import React, { useState, useEffect } from "react";

import { Preloader, Button, Block, Searchbar } from "framework7-react";

import Image from "@/components/image";

//Lib import
import { ApiResponseErrorCodeMessage, System } from "@/libs/constants";

import { useSWRInfinite } from "swr";
import axios from "axios";

const CaseKhac = () => {
  function naylagi(i) {}

  return (
    <>
      <div className="soju-search mt-3">
        <Searchbar
          disableButtonText="Đóng"
          placeholder="Nhập chương trình cần tìm"
          clearButton={true}
          searchContainer=".search-list"
          searchIn=".item-title"
          backdrop={false}
        ></Searchbar>
      </div>
      <div className="list media-list chevron-center sjc search-list searchbar-found">
        <ul>
          <li>
            <a href="#" className="item-link item-content align-items-center">
              <div className="item-media align-self-center">
                <div className="sjc-icon">
                  <img
                    src="https://static.mservice.io/img/momo-upload-api-201130105739-637423306590907551.png"
                    loading="lazy"
                  />
                </div>
              </div>
              <div className="item-inner">
                <div className="item-title-row">
                  <div className="item-title">Như Chưa Hề Có Cuộc Chia Ly</div>
                </div>
                <div className="item-text">
                  Như Chưa Hề Có Cuộc Chia Ly' là hoạt động thiện nguyện tìm
                  kiếm, đoàn tụ, hàn gắn những vết thương ly biệt và giúp những
                  thân nhân thất lạc sau nhiều năm được gặp lại nhau.
                </div>
              </div>
            </a>
          </li>

          {[
            "Cùng báo Tuổi Trẻ cứu trợ bà con vùng lũ miền Trung",
            "Uớc mơ của Thúy",
            "Bảo vệ chủ quyền biển đảo",
          ].map((item, index) => (
            <li key={index}>
              <a href="#" className="item-link item-content align-items-center">
                <div className="item-media align-self-center">
                  <div className="sjc-icon">
                    <img
                      src="https://static.mservice.io/img/momo-upload-api-201013185830-637382123101342424.png"
                      loading="lazy"
                    />
                  </div>
                </div>
                <div className="item-inner">
                  <div className="item-title-row">
                    <div className="item-title">{item}</div>
                  </div>
                  <div className="item-text">
                    Đại dịch COVID-19 vừa qua đi, bà con miền Trung lại phải
                    hứng thêm trận lũ lụt lớn nhất trong 10 năm trở lại đây.
                    Cùng chung tay cứu trợ bà con vùng lũ!
                  </div>
                </div>
              </a>
            </li>
          ))}
        </ul>
      </div>

      <div className="searchbar-not-found py-4">
        <img src="/static/images/not-found.svg" className="d-block mx-auto img-fluid" />
        <div className="text-center">
          <div><strong>Không tìm thấy hoàn cảnh</strong></div>
          <div>Hãy thử từ khoá khác để tìm kiếm tốt hơn</div>
        </div>
      </div>

      <style jsx>{`
         {
          .sjc {
            --f7-list-item-text-font-size: 12px;
            --f7-list-item-text-line-height: 16px;
            --f7-list-item-title-font-size: 15px;
            --f7-list-item-padding-horizontal: 15px;
            --f7-list-item-media-margin: 12px;
            margin-top: 0;
          }

          .sjc :global(ul:before) {
            display: none;
          }

          .sjc :global(ul:after) {
            display: none;
          }

          .sjc-icon {
            width: 45px;
            height: 45px;
            border-radius: 5px;
            border: 1px solid var(--gray-300);
            padding: 5px;
            box-sizing: border-box;
          }
          .sjc-icon img {
            width: 100%;
          }
          .soju-search {
            --f7-searchbar-input-font-size: 15px;
            --f7-searchbar-input-bg-color: var(--gray-200);
            --f7-searchbar-input-padding-horizontal: 30px;
            --f7-searchbar-border-color: transparent;
            --f7-searchbar-bg-color-rgb: white;
            --f7-searchbar-link-color: var(--pinkmomo);
            --f7-searchbar-input-height: 38px;
            --f7-searchbar-inner-padding-right: 12px;
            --f7-searchbar-inner-padding-left: 12px;
            overflow: hidden;
          }

          .soju-search :global(.searchbar) {
            background-color: white;
            padding: 8px 5px;

            border-radius: 0;
          }
        }
      `}</style>
    </>
  );
};

export default CaseKhac;

import React, { useState, useEffect, Fragment } from "react";

import { Preloader, Button } from "framework7-react";

//Lib import
import { ApiResponseErrorCodeMessage, System } from "@/libs/constants";

import Image from "@/components/image";

import { useSWRInfinite } from "swr";
import axios from "@/libs/axios";
import format from "number-format.js";

const DonationUserList = ({ DataDonation, Title, Description }) => {
  const PAGE_SIZE = 3;

  const fetcher = (url) =>
    axios.get(url).then(function (res) {
      return res.Data.Items;
    });

  const { data, error, mutate, size, setSize, isValidating } = useSWRInfinite(
    (index) =>
      `${System.apihost
      }/__get/Donation/ListDonationUser?donationId=${DataDonation.Id}&itemPerPage=${PAGE_SIZE}&page=${(index + 1) * PAGE_SIZE - PAGE_SIZE
      }`,
    fetcher
  );

  const donationUser = data ? [].concat(...data) : [];
  const isLoadingInitialData = !data && !error;

  const isLoadingMore =
    isLoadingInitialData ||
    (size > 0 && data && typeof data[size - 1] === "undefined");

  const isEmpty = data?.[0]?.length === 0;
  const isReachingEnd =
    isEmpty || (data && data[data.length - 1]?.length < PAGE_SIZE);


  function loadMoreUser(i) {
    if (isReachingEnd) {
      return;
    }
    setSize(i);
  }
  if (!DataDonation) return <></>;
  return (

    <div className="section no-select">
      <div className="section-splash"></div>
      <div className="block mt-4 mb-4">
        <div className="section_title">{Title}</div>
        <div className="section_subTitle">{Description}</div>

        <div className="people-list">
          {donationUser &&
            donationUser.map((item, index) => (
              <div className="people-list-item" key={index}>
                <div className="people-item">
                  <div className="people-avatar">
                    <Image
                      src="https://static.mservice.io/images/s/momo-upload-api-200520135933-637255799731362042.png"
                      alt="fwe"
                      width="35"
                      height="35"
                      round="50%"
                      background="transparent"
                    />
                  </div>

                  <div className="people-info">
                    <div className="people-name">{item.cusName}</div>
                    <div className="people-phone">{item.phoneNumber}</div>
                  </div>

                  <div className="people-money">
                    {format("#,###.", item.amount)}
                    {DataDonation.Type == 1 ? 'đ' : <img
                      src="https://static.mservice.io/pwa/images/donation/pig-coin-new.svg"
                      className="icon-heovang"
                      width="24"
                      alt=""
                    />}
                  </div>
                </div>

                {item.message && (
                  <div className="people-message">{item.message}</div>
                )}
              </div>
            ))}
        </div>

        <div className="mt-3">
          {!isReachingEnd && (
            <Button
              outline
              className="soju-btn-gray"
              disabled={isLoadingMore || isReachingEnd}
              onClick={() => loadMoreUser(size + 1)}
            >
              {isLoadingMore && <Preloader></Preloader>} &nbsp;
              {isLoadingMore ? "Đang chờ..." : "Xem thêm "}
            </Button>
          )}
        </div>

      </div>

      <style jsx>{`
        .people-list .people-list-item:not(:last-child) {
          border-bottom: 1px solid var(--gray-200);
        }
        .people-item {
          padding: 8px 0 8px;
          position: relative;
          display: flex;
          flex-flow: row nowrap;
          align-items: center;
          font-size: 14px;
          color: var(--gray-900);
        }
        .people-avatar {
          width: 35px;
          height: 35px;
          flex: 0 0 35px;
        }

        .people-avatar + .people-info {
          padding-left: 10px;
        }

        .people-info {
          padding: 0 15px 0 0;
          flex-grow: 1;
        }
        .people-name {
          font-weight: 600;
          margin-bottom: 3px;
          padding-top: 3px;
        }
        .people-phone {
          color: var(--gray-500);
          font-size: 12px;
        }

        .people-money {
          white-space: nowrap;
          font-weight: bold;
          font-size: 13px;
        }

        .people-message {
          border-radius: 15px;
          background-color: var(--gray-100);
          padding: 7px 12px;
          font-size: 11px;
          position: relative;
          margin-bottom: 12px;
          display: inline-block;
        }
      `}</style>
    </div>
  );
};

export default DonationUserList;


import React, { useState, useEffect } from "react";

//Lib import
import { ApiResponseErrorCodeMessage, System } from "@/libs/constants";

import DonationTimelineGallery from "./donation-timeline-gallery";
import DateFormat from "@/components/DateFormat";

import axios from "@/libs/axios";
import {
  f7,
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  Link,
  Toolbar,
  Block,
  Popup,
  Button,
} from "framework7-react";

const DonationTimeline = ({ caseId }) => {
  const [data, setData] = useState({
    DonationId: caseId,
    Count: 100,
    Items: [],
    LastIndex: 0,
    PageCount: 0,
    TotalItems: 0,
    SortType: 1,
    SortDir: 1
  });
  const [isShowPopup, setIsShowPopup] = useState(false);
  const [dataSche, setDataSche] = useState({});

  const getData = () => {
    axios
      .get(`${System.apihost}/__get/DonationSchedule/List?donationId=${data.DonationId}&count=${data.Count}&sortType=${data.SortType}&sortDir=${data.SortDir}`)
      .then((res) => {
        if (!res.Result) {
          return;
        }
        res = res.Data;

        res.Items.forEach((x) => {
          data.Items.push(x);
        });
        data.TotalItems = res.TotalItems;
        data.Count = res.Count;
        data.LastIndex = res.LastIndex;
        data.PageCount = res.PageCount;

        setData({ ...data });
      });
  };

  useEffect(() => {
    getData();
  }, []);


  const ShowPopup = (sche) => {
    if (sche.Items.length < 1 && sche.Content) {
      setDataSche({ ...sche });
      setIsShowPopup(true);
    }

  }

  if (data.Items.length < 1) return <></>;

  return (
    <>
      <div className="splash no-select"></div>

      <div className="section_title no-select">Cập nhập tình trạng</div>

      <div className="timeline soju-timeline no-select">
        {data.Items.map((sche, idx1) => (
          <div key={idx1} className="timeline-item">
            <div className="timeline-item-divider"></div>
            <div className="timeline-item-content">
              <div className="timeline-item-time">
                <DateFormat date={sche.CreatedOn}></DateFormat>
              </div>
              <div
                onClick={(x) => ShowPopup(sche)}
                className="timeline-item-text"
                dangerouslySetInnerHTML={{
                  __html: sche.Title,
                }}
              ></div>

              {sche.Items && sche.Items.length > 0 && (
                <DonationTimelineGallery gallerys={sche.Items} />
              )}
            </div>
          </div>
        ))}
      </div>

      <Popup
        className="demo-popup"
        opened={isShowPopup}
        onPopupClosed={() => setIsShowPopup(false)}
      >
        <Page>
          <Navbar>
            <NavLeft>
              <Link
                popupClose
                className="text-dark"
                iconIos="f7:chevron_left"
              />
            </NavLeft>
            <NavTitle>Tình trạng</NavTitle>
          </Navbar>

          <Toolbar position="bottom" className="soju-popup-toolbar ">
            <div className="px-1 w-100">
              <Button
                outline
                className="soju-btn-gray bg-white"
                popupClose
              >
                Đóng
                    </Button>
            </div>
          </Toolbar>
          <Block className="my-3">
            <div className="section_title " style={{ 'margin': 0 }}>{dataSche.Title}</div>
            <div className="timeline-item-time mt-2" style={{ 'margin': '0 0 5px 0' }}>
              {dataSche.CreatedOn && <DateFormat date={dataSche.CreatedOn}></DateFormat>}

            </div>
            <div
              className="article_content mt-3"
              dangerouslySetInnerHTML={{ __html: dataSche.Content }}
            />
          </Block>

        </Page>
      </Popup>

      <style jsx>{`
        .soju-timeline {
          // --f7-timeline-padding-horizontal: 0;
          padding-left: 0;
          padding-right: 0;
          margin-top: 0;
          margin-bottom: 0;
        }

        .soju-timeline :global(.timeline-item-content) {
          width: 100%;
        }
        
        .soju-timeline .timeline-item-time {
          margin-bottom: 5px;
        }
        .timeline-item-date {
          flex-shrink: 0;
          width: 70px;
          text-align: right;
          box-sizing: border-box;
        }

        .timeline-item-divider {
          margin-left: 5px;
        }

        .timeline-item-divider {
          background-color: var(--pink);
        }
        .timeline-item-divider:after,
        .timeline-item-divider:before {
          background-color: var(--pinklight);
        }
      `}</style>
    </>
  );
};

export default DonationTimeline;

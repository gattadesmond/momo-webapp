import React, { useState, useEffect } from "react";

import Avatar from "../../components/avatar";
import { OriginalNumber } from "@/libs/helper";
import {
  Block
} from "framework7-react";
const UserInfo = (props) => {
  const { userInfo } = props;

  const [UserInfo, setUserInfo] = useState({});
  useEffect(() => {
    if (userInfo && userInfo.phone) {
      var avatar =
        "//avatars.mservice.io.s3-ap-southeast-1.amazonaws.com/avatar/" +
        OriginalNumber(userInfo.phone) +
        ".png";
      setUserInfo({
        ...userInfo,
        Avatar: avatar,
      });
    }
  }, [userInfo]);
 
  return (
    <>
      <Block className="my-0">
        <div className="user-info mt-3 no-select">
          <div className="user-avatar">
            <Avatar img={UserInfo.Avatar} />
          </div>

          <div className="user-content">
            <div className="name">{UserInfo.userName}</div>
            {/* <div className="title">Cứu tinh trái đất</div> */}
          </div>

          <a href="/quyen-gop-cap-nhat" className="user-noti">
            <i className="icon icon-fill f7-icons if-not-md">
              bell
              <span className="badge color-red"></span>
            </i>
          </a>

          <style jsx>{`
          .user-info {
            display: flex;
            flex-flow: row nowrap;
            align-items: center;
            padding: 12px 8px 12px 15px;
            background-color: white;
            border: 1px solid var(--gray-200);
            border-radius: 5px;
          }
          .user-noti{
            flex: 0 0 auto;
            cursor: pointer;
            padding: 5px 10px;
            color: var(--gray-700);
            display: block;
          }

          .user-content {
            flex: 1;
            padding-left: 15px;
          }
          .name {
            font-size: 16px;
            font-weight: bold;
          }
          .title {
            margin-top: 3px;
            background-color: var(--green);
            color: white;
            display: inline-block;
            border-radius: 20px;
            padding: 3px 10px;
            font-size: 12px;
            font-weight: 600;
          }
        `}</style>
        </div>
      </Block>
    </>
  );
};

export default UserInfo;

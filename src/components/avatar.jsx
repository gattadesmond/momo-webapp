import React from "react";

const Avatar = ({ img, char }) => {
 
  return (
    <>
      <div className="avatar">
        {img != null ? (
          <img
            src={img}
            loading="lazy"
            alt=""
            className="img"
          />
        ) : (
          <span className="char">A</span>
        )}
      </div>
      <style jsx>{`
        .avatar {
          width: 45px;
          height: 45px;
          border-radius: 50%;
          overflow: hidden;
          position: relative;
          background-color: var(--gray-300);
        }
        .img {
          width: 100%;
          height: 100%;
          position: absolute;
          top: 0;
          left: 0;
          object-fit: cover;
        }
        .char {
          width: 100%;
          height: 100%;
          position: absolute;
          top: 0;
          left: 0;
          display: flex;
          align-items: center;
          justify-content: center;
          font-size: 26px;
          font-weight: 500;
          color: var(--gray-800);
        }
      `}</style>
    </>
  );
};

export default Avatar;

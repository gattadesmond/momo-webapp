import React from "react";

import cl from "classnames";

import { f7, Icon, Popover, List, ListItem, Link } from "framework7-react";
import {
  ProcessAppShareFacebook,
  ProcessAppShareOther
} from "@/libs/constants";
const ShareBtn = ({ data }) => {

  const shareCopyLink = () => {
    if (!data || !data.ShareUrl)
      return;

    var toastCopy = f7.toast.create({
      text: 'Sao chép liên kết thành công',
      closeTimeout: 2000,
    });

    const textInput = document.createElement("input");
    textInput.setAttribute('readonly', true);
    textInput.value = data.ShareUrl.trim();

    document.body.appendChild(textInput);

    textInput.select();
    document.execCommand("Copy");
    f7.sheet.close(".soju-sheet");
    toastCopy.open();
  }
  const shareFacebook = () => {
    try {
      ProcessAppShareFacebook(data.ShareUrl.trim());
    } catch (e) {

    }

    f7.sheet.close(".soju-sheet");
  }
  const shareOther = () => {
    var contentShare = "Hãy chia sẻ hoàn cảnh này đến bạn bè để điều kỳ diệu sớm trở thành hiện thực! " + data.ShareUrl.trim();
    try {
      ProcessAppShareOther(contentShare);
    } catch (e) {

    }
    f7.sheet.close(".soju-sheet");
  }

  return (
    <>
      <div className="soju-sharebtn-container">
        <Link className="soju-sharebtn" popoverOpen=".popover-share">
          Chia sẽ{" "}
          <Icon
            f7="arrowshape_turn_up_right_fill"
            size="12px"
            color="black"
          ></Icon>
        </Link>

        <Popover className="popover-share">
          <div className="list my-0 no-hairlines dona-share-list">
            <ul>
              <li>
                <a onClick={x => shareCopyLink()} className="item-link item-content ">
                  <div className="item-media">
                    <i className="f7-icons text-color-gray">link</i>
                  </div>
                  <div className="item-inner">
                    <div className="item-title">Sao chép liên kết</div>
                  </div>
                </a>
              </li>
              <li>
                <a onClick={x => shareFacebook()} className="item-link item-content">
                  <div className="item-media">
                    <i className="f7-icons" style={{ color: "#385898" }}>
                      logo_facebook
                    </i>
                  </div>
                  <div className="item-inner">
                    <div className="item-title">Chia sẻ qua Facebook</div>
                  </div>
                </a>
              </li>
              <li>
                <a onClick={x => shareOther()} className="item-link item-content">
                  <div className="item-media">
                    <img src="https://static.mservice.io/pwa/images/social/share.svg" width="34" alt="" />
                  </div>
                  <div className="item-inner">
                    <div className="item-title">Chia sẻ khác</div>
                  </div>
                </a>
              </li>
            </ul>
          </div>
        </Popover>
      </div>

      <style jsx>{`
        .soju-sharebtn-container :global(.soju-sharebtn) {
          cursor: pointer;
          position: fixed;
          right: 15px;
          top: 20px;
          z-index: 15;
          background-color: white;
          border-radius: 20px;
          display: block;
          padding: 4px 13px;
          font-size: 14px;
          color: var(--primary-2);
          box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0.2),
            0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
        }

        .soju-sharebtn-container :global(.icon) {
          vertical-align: 5%;
          color: var(--primary-2);
        }
      `}</style>
    </>
  );
};

export default ShareBtn;

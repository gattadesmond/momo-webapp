import React from "react";
import cl from "classnames";

const Image = (props) => {
  const {
    caption = null,
    alt = null,
    src,
    width = null,
    height = null,
    round = 0,
    background = "#fafafa",
    ...rest
  } = props;

  let aspectRatio = null;
  if (width && height) {
    aspectRatio = `${String((height / width) * 100)}%`;
  }
  return (
    <>
      <figure className="figure">
        <div
          className={cl("figure-div", {
            "figure-embed": aspectRatio != null,
          })}
        >
          <img
            {...rest}
            src={src}
            alt={alt}
            className="figure-img img-fluid d-block"
            loading="lazy"
          />
        </div>
        {caption && (
          <figcaption className="figure-caption">{caption}</figcaption>
        )}
        <style jsx>{`
          figure {
            display: block;
            margin: 0;
            padding: 0;
          }


          .figure-div {
            background-color: ${background};
            border-radius: ${round};
            overflow: hidden;
            
          }
          .figure-embed {
            position: relative;
            display: block;
            width: 100%;
            padding: 0;
            overflow: hidden;
          }
          .figure-embed::before {
            display: block;
            content: "";
            padding-top: ${aspectRatio};
          }

          .figure-embed .figure-img {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 100%;
            object-fit: cover;
            
            border: 0;
          }
        `}</style>
      </figure>
    </>
  );
};

export default Image;

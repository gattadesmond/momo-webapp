import React, { useState, useEffect } from "react";
import {
  ApiResponseErrorCodeMessage,
  System
} from "@/libs/constants";
import axios from "@/libs/axios";
import {
  f7
} from "framework7-react";
import $ from "dom7";
import { decodeHTML } from "@/libs/helper";
import { Single, Group } from "@/components/guide/Template"

const GuideArticle = (props) => {
  const { Html } = props;

  const [Model, setModel] = useState({
    Items: []
  });

  const init = () => {
    if (!Html)
      return;

    var html = decodeHTML(Html);
    var htmlInlines = html.match(/<help-article.+?>/g);
    if (!htmlInlines)
      return;
    htmlInlines.forEach((htmlHA, idx) => {
      var p = document.createElement('p');
      p.innerHTML = htmlHA;
      var ha = $(p).find('help-article');


      var id = ha.attr("data-helpid");
      var title = ha.attr("data-title");
      var type = ha.attr("data-type");
      Model.Items.push({
        Id: id,
        Title: title,
        Type: type,
        Target: ha
      });

    });

    setModel({ ...Model });
  }

  const handleBind = (data) => {
    var haFrame = $(`#ha-${data.Id}`);
    var Target = $(`help-article[data-helpid="${data.Id}"]`);
    Target = Target.filter(x => {
      if ($(x).data('type') == data.Type)
        return x;
      return null;
    });
    var parent = Target.parent();
    parent.append(haFrame.children());
    Target.remove();
    haFrame.remove();
  }

  useEffect(() => {
    init();
  }, [Html])
  return (
    <>
      {Model.Items.map((item, idx) => {
        switch (item.Type) {
          case '2':
            return <Single key={`ha-s-${idx}`} {...item} OnBind={handleBind} />;
          case '1':
            return <Group key={`ha-g-${idx}`} {...item} OnBind={handleBind} />;
        }
      })}
    </>
  );
};

export default GuideArticle;

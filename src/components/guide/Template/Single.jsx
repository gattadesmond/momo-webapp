import React, { useState, useEffect } from "react";
import {
  ApiResponseErrorCodeMessage,
  System
} from "@/libs/constants";
import axios from "@/libs/axios";
import {
  f7
} from "framework7-react";
import $ from "dom7";

const Single = (props) => {
  const { Id, Title, Type, OnBind } = props;

  const [Model, setModel] = useState({
    Blocks: []
  });

  const getData = (id, fnc) => {
    var url = `${System.apihost}/__get/Guide/DetailSingle?id=${id}`;
    axios
      .get(url)
      .then((res) => {
        if (res.Error) {
          f7.preloader.hide();
          f7.dialog.alert(ApiResponseErrorCodeMessage[res.Error.Code]);
          if (res.Error.Code === 'notfound') {
            f7.views.main.router.navigate({
              name: '404'
            });
          }
          return;
        }

        f7.preloader.hide();
        fnc && fnc(res.Data);
      })
      .catch((error) => {
        debugger
        f7.preloader.hide();
        f7.dialog.alert(ApiResponseErrorCodeMessage["system_error"]);
        return;
      });

  };

  const bindData = (data) => {
    setModel({ ...data });

    var casHtuSingle = f7.popup.create({
      el: '.pop-huong-dan-single-' + Id,
      swipeToClose: "to-bottom",
      swipeHandler: '.close-handler',
      dataId: Id
    });

    var swiperSingle = f7.swiper.create('.pop-huong-dan-sw-single-' + Id, {
      pagination: {
        el: '.swiper-pagination-single-' + Id,
        type: 'fraction',
        hideOnClick: true
      },
      navigation: {
        nextEl: '.swiper-button-next-single-' + Id,
        prevEl: '.swiper-button-prev-single-' + Id
      }
    })

    casHtuSingle.on('open', function (popup) {
      var swiperSingle = f7.swiper.get('.pop-huong-dan-sw-single-' + Id);
      swiperSingle.slideTo(0);
      swiperSingle.update();
    });

    OnBind && OnBind(props);

  }

  useEffect(() => {
    if (!Id)
      return;
    getData(Id, (data) => {
      bindData(data);
    })
  }, [Id])

  return (<div style={{ 'display': 'none' }} id={`ha-${Id}`}>
    <div className="pron-container balance">

      <h3 className="mt-2 mb-2 lutie-section-title font-weight-bold">{Title}</h3>
    </div>
    <div className="card ">
      <div className="card-content pt-1">
        <div className="htu-mockup-more  bg-transparent position-relative">

          <a href="" data-popup={`.pop-huong-dan-single-${Id}`} className="popup-open link-absolute"></a>
          {Model.Blocks.map((item, idx1) => {
            return idx1 < 3 && (
              <div key={`blocks1-${idx1}`} className={`htu-mockup-item i${idx1 + 1}`}
                style={{ 'backgroundImage': `url('${item.Avatar}')` }}>
              </div>
            );
          })}
        </div>


        <div className="mt-2 pb-2 text-center ">
          <a href="" data-popup={`.pop-huong-dan-single-${Id}`}
            className="popup-open  button button-outline button-round d-inline-block">
            Xem
            hướng dẫn
            </a>
        </div>
      </div>
    </div>

    <div className={`popup cas-htu-popup popup-swipe-round pop-huong-dan-single-${Id}`}>
      <div className="view">
        <div className="page">
          <div className="navbar close-handler">
            <div className="navbar-inner sliding">
              <div className="left">
                <a className="link popup-close">
                  <img src="https://static.mservice.io/pwa/images/icons/icon-x.svg" width="28" alt="" />
                </a>
              </div>
              <div className="title">{Title}</div>
            </div>
          </div>
          <div className="page-content" style={{ 'paddingTop': '48px' }}>
            <div className={`swiper-container htu-swiper pop-huong-dan-sw-single-${Id}`}>
              <div className={`swiper-pagination swiper-pagination-single-${Id}`}></div>
              <div className={`swiper-button-next  htu-navigation-btn swiper-button-next-single-${Id}`}></div>
              <div className={`swiper-button-prev  htu-navigation-btn swiper-button-prev-single-${Id}`}></div>
              <div className="swiper-wrapper">


                {Model.Blocks.map((item, idx2) => {
                  return (
                    <div key={`blocks2-${idx2}`} className="swiper-slide">

                      <div className="htu-content">
                        <div className="htu-mockup">
                          <div className="htu-mockup-slider" style={{ 'backgroundImage': `url('https://static.mservice.io/pwa/images/phone-mockup.png')` }}>
                            <img src={item.Avatar} className="img-fluid d-block mx-auto swiper-lazy" alt="" />
                          </div>

                          <div className="htu-mockup-txt">
                            <h3 className="htu-mockup-title mt-0 mb-2" dangerouslySetInnerHTML={{
                              __html: item.Title,
                            }}></h3>
                            <div className="htu-mockup-body" dangerouslySetInnerHTML={{
                              __html: item.Content,
                            }}>
                            </div>
                          </div>

                        </div>
                      </div>

                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <style jsx>{``}
    </style>

  </div>
  );
};

export default Single;

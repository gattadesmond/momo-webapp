import React, { useState, useEffect } from "react";
import {
  ApiResponseErrorCodeMessage,
  System
} from "@/libs/constants";
import axios from "@/libs/axios";
import {
  f7
} from "framework7-react";

const Group = (props) => {
  const { Id, Title, Type, OnBind } = props;

  const [Model, setModel] = useState({
    Groups: []
  });

  const getData = (id, fnc) => {
    var url = `${System.apihost}/__get/Guide/DetailGroup?id=${id}`;
    axios
      .get(url)
      .then((res) => {
        if (res.Error) {
          f7.preloader.hide();
          f7.dialog.alert(ApiResponseErrorCodeMessage[res.Error.Code]);
          if (res.Error.Code === 'notfound') {
            f7.views.main.router.navigate({
              name: '404'
            });
          }
          return;
        }

        f7.preloader.hide();
        fnc && fnc(res.Data);
      })
    // .catch((error) => {
    //   debugger
    //   f7.preloader.hide();
    //   f7.dialog.alert(ApiResponseErrorCodeMessage["system_error"]);
    //   return;
    // });

  };

  const bindData = (data) => {

    setModel({ ...data });

    for (var j = 0; j < data.Groups.length; j++) {
      var id = data.Groups[j].Id;

      var casHtuGroup = f7.popup.create({
        el: '.pop-huong-dan-group-' + id,
        swipeToClose: "to-bottom",
        swipeHandler: '.close-handler',
        dataId: id
      });

      var swiperGroup = f7.swiper.create('.pop-huong-dan-sw-' + id, {
        pagination: {
          el: '.swiper-pagination-ct-' + id,
          type: 'fraction',
          hideOnClick: true
        },
        navigation: {
          nextEl: '.swiper-button-next-ct-' + id,
          prevEl: '.swiper-button-prev-ct-' + id
        }
      })

      casHtuGroup.on('opened', function (popup) {
        var swiperGroup = f7.swiper.get('.pop-huong-dan-sw-' + popup.params.dataId);
        swiperGroup.slideTo(0);
        swiperGroup.update();
      });
    }

    OnBind && OnBind(props);

  }

  useEffect(() => {
    if (!Id)
      return;
    getData(Id, (data) => {
      bindData(data);
    })
  }, [Id])

  return (
    <>
      <div style={{ 'display': 'none' }} id={`ha-${Id}`}>

        <div className="sale-cluster">
          <div className="sale-cluster-title">
            {Title}
          </div>
          <div className="list accordion-list my-4 p-0">
            <ul>
              {Model.Groups.map((group, idx1) => {
                return (
                  <li key={`group1-${idx1}`} className={`accordion-item ${idx1 == 0 ? 'accordion-item-opened' : ''}`}>
                    <a href="" className="item-content item-link pl-0">
                      <div className="item-inner">
                        <div className="item-title  font-weight-bold">{group.Name}</div>
                      </div>
                    </a>
                    <div className="accordion-item-content">
                      <div className="htu-mockup-more  bg-transparent position-relative">

                        <a href="" data-popup={`.pop-huong-dan-group-${group.Id}`} className="popup-open link-absolute"></a>
                        {group.Items.map((item, idx2) => {
                          return (
                            item.Blocks.map((block, idx3) => {
                              return idx3 < 3 && (<div key={`block1-${idx3}`} className={`htu-mockup-item i${idx3 + 1}`}
                                style={{ 'backgroundImage': `url('${block.Avatar}')` }}>
                              </div>);

                            }));
                        })}
                      </div>


                      <div className="mt-2 pb-5 text-center ">
                        <a href="" data-popup={`.pop-huong-dan-group-${group.Id}`}
                          className="popup-open  button button-outline button-round d-inline-block">
                          Xem
                          hướng dẫn
              </a>
                      </div>

                    </div>
                  </li>);
              })}

            </ul>
          </div>
        </div>

        <style jsx>{``}
        </style>

      </div>
      {Model.Groups.map((group, idx1) => {
        return (
          <div key={`group2-${idx1}`} className={`popup cas-htu-popup popup-swipe-round pop-huong-dan-group-${group.Id}`}>
            <div className="view">
              <div className="page">
                <div className="navbar close-handler">
                  <div className="navbar-inner sliding">
                    <div className="left">
                      <a className="link popup-close">
                        <img src="https://static.mservice.io/pwa/images/icons/icon-x.svg" width="28" alt="" />
                      </a>
                    </div>
                    <div className="title">{group.Name}</div>
                  </div>
                </div>
                <div className="page-content">
                  <div className={`swiper-container htu-swiper pop-huong-dan-sw-${group.Id}`}>
                    <div className={`swiper-pagination swiper-pagination-ct-${group.Id}`}></div>
                    <div className={`swiper-button-next htu-navigation-btn swiper-button-next-ct-${group.Id}`}></div>
                    <div className={`swiper-button-prev htu-navigation-btn swiper-button-prev-ct-${group.Id}`}></div>
                    <div className="swiper-wrapper">
                      {group.Items.map((item, idx2) => {
                        return (
                          item.Blocks.map((block, idx3) => {
                            return (
                              <div key={`block2-${idx3}`} className="swiper-slide">
                                <div className="htu-content">
                                  <div className="htu-mockup">
                                    <div className="htu-mockup-slider" style={{ 'backgroundImage': `url('https://static.mservice.io/pwa/images/phone-mockup.png')` }}>
                                      <img src={block.Avatar} className="img-fluid d-block mx-auto swiper-lazy" alt="" />
                                    </div>
                                    <div className="htu-mockup-txt">
                                      <h3 className="htu-mockup-title mt-0 mb-2"
                                        dangerouslySetInnerHTML={{
                                          __html: block.Title,
                                        }}></h3>
                                      <div className="htu-mockup-body" dangerouslySetInnerHTML={{
                                        __html: block.Content,
                                      }}>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            );
                          })
                        );
                      })}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      })}
    </>
  );
};

export default Group;

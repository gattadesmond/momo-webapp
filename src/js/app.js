// Import React and ReactDOM
import React from 'react';
import ReactDOM from 'react-dom';

// Import Framework7
import Framework7, { getDevice } from './framework7-custom.js';

// import Framework7Keypad from 'framework7-plugin-keypad';
import Framework7Keypad from 'framework7-plugin-keypad/src/framework7-keypad.js';

// import Framework7Keypad from 'framework7-plugin-keypad/src/framework7-keypad.js';


// install plugin
// Framework7.use(Framework7Keypad);

// Import Framework7-React Plugin
import Framework7React from 'framework7-react';

import baseStyles from '../styles/base';

// Import Framework7 Styles
import '../css/framework7-custom.less';
import 'framework7-plugin-keypad/src/framework7-keypad.less';

// Import Icons and App Custom Styles
import '../css/icons.css';
import '../css/app.less';
import '../css/old.less';
import '../css/bootstrap-util.css';



// Import App Component
import App from '../components/app.jsx';

// Init F7 React Plugin
Framework7.use(Framework7React);

Framework7.use(Framework7Keypad);


const events = new Framework7.Events();
const Device = getDevice();



// Fix viewport scale on mobiles
if ((Device.ios || Device.android) && Device.standalone) {
  const viewPortContent = document.querySelector('meta[name="viewport"]').getAttribute('content');
  document.querySelector('meta[name="viewport"]').setAttribute('content', `${viewPortContent}, maximum-scale=1, user-scalable=no`);
}


// Mount React App
ReactDOM.render(
  React.createElement(App),
  document.getElementById('app'),
);
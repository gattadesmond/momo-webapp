
import { createStore } from 'framework7/lite';

const store = createStore({
  state: {
    UserInfo: {

    },
    WebToken: ''
  },
  getters: {
    UserInfo({ state }) {
      return state.UserInfo;
    },
    WebToken({ state }) {
      return state.WebToken;
    }
  },
  actions: {
    UserInfo({ state }, value) {
      state.UserInfo = value;
    },
    WebToken({ state }, value) {
      state.WebToken = value;
    }
  },
})
export default store;

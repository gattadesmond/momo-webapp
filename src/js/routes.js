import DonationPage from "../pages/donation/donation.jsx";
import DonationUngHo from "../pages/donation/donation-ungho.jsx";
import DonationManage from "../pages/donation/donation-ungho-manage.jsx";
import DonationUngHoRule from "../pages/donation/donation-ungho-rule.jsx";
import DonationDetail from "../pages/donation/detail.jsx";
import DonationLatest from "../pages/donation/donation-latest.jsx";
import DonationHeoVangDetail from "../pages/donation/detailheovang.jsx";
import HDLKAgribankPage from "../pages/staticpage/agribank.jsx";
import HDLKShbPage from "../pages/staticpage/shb.jsx";
import Rule from "../pages/staticpage/cac-dieu-khoan-va-dieu-kien-ve-dich-vu.jsx";
import GuideSinglePage from "../pages/guide/Single.jsx";
import XacNhan from "../pages/donation/payment/xacnhan.jsx";
import DonationFail from "../pages/donation/payment/xacnhan-thatbai.jsx";
import DonationSuccess from "../pages/donation/payment/xacnhan-thanhcong.jsx";
import ChonNguonTien from "../pages/donation/payment/moneychoice.jsx";

import AboutPage from "../pages/about.jsx";
import FormPage from "../pages/form.jsx";
import HomePage from "../pages/home.jsx";
import HeoDiHoc from "../pages/heodihoc";
import ServicePage2 from "../pages/service-page2";
import Noti from "../pages/noti/index.jsx";
import NotiPreview from "../pages/noti/preview.jsx";
import Quiz from "../pages/quiz/index.jsx";
import ReferralPage from "../pages/referral";
import ReferralFriendPage from "../pages/referral/friend.jsx";
import ServicePage from "../pages/service";
import SucManh2kPage from "../pages/sucmanh2k/index.jsx";
import SucManh2kManagePage from "../pages/sucmanh2k/manage.jsx";

import NCHCCCLPage from "../pages/nchcccl/index.jsx";
import NCHCCCLManagePage from "../pages/nchcccl/manage.jsx";

import DynamicRoutePage from "../pages/dynamic-route.jsx";
import RequestAndLoad from "../pages/request-and-load.jsx";
import NotFoundPage from "../pages/404.jsx";

var routes = [
  {
    path: "/service-page",
    component: ServicePage,
  },
  {
    path: "/quyen-gop",
    component: DonationPage,
    options: {
      props: {
        activeTab: "tab-donation-traitim",
      },
    },
  },
  {
    path: "/quyen-gop-ungho",
    component: DonationUngHo,
    options: {

    },
  },
  {
    path: "/quyen-gop-manage",
    component: DonationManage,
    options: {

    },
  },
  {
    path: "/quyen-gop-rule",
    component: DonationUngHoRule,
    options: {
    },
  },
  {
    path: "/quyen-gop-heovang",
    component: DonationPage,
    options: {
      props: {
        activeTab: "tab-donation-heovang",
      },
    },
  },
  {
    path: "/quyen-gop-khac",
    component: DonationPage,
    options: {
      props: {
        activeTab: "tab-donation-khac",
      },
    },
  },
  {
    path: "/quyen-gop/:slug",
    component: DonationDetail,
  },
  {
    path: "/quyen-gop-heovang/:slug",
    component: DonationHeoVangDetail,
  },

  {
    path: "/quyen-gop-cap-nhat",
    component: DonationLatest,

  },
  {
    path: "/chon-nguon-tien/:token",
    component: ChonNguonTien,
  },
  {
    path: "/xac-nhan-dong-gop/:token",
    component: XacNhan,
  },
  {
    path: "/dong-gop-that-bai/:token",
    component: DonationFail,
  },
  {
    path: "/dong-gop-thanh-cong/:token",
    component: DonationSuccess,
  },
  {
    path: "/hdlk-agribank",
    component: HDLKAgribankPage,
  },
  {
    path: "/hdlk-shb",
    component: HDLKShbPage,
  },
  {
    path: "/cac-dieu-khoan-va-dieu-kien-ve-dich-vu",
    component: Rule,
  },
  {
    name: "guide-detail-single",
    path: "/guide/huong-dan-:id",
    component: GuideSinglePage,
  },
  {
    path: "/heodihoc",
    component: HeoDiHoc,
  },
  {
    path: "/service-page2",
    component: ServicePage2,
  },
  {
    name: 'article-detail',
    path: "/news/:category/:slug-:id(\\d+)",
    component: Noti,
  },
  {
    name: 'article-preview',
    path: "/news-preview/:category/:slug-:id(\\d+)",
    component: NotiPreview,
  },
  {
    name: 'momo-survey',
    path: "/momo-survey/:campaignCode",
    component: Quiz,
  },
  {
    name: "donation-2000",
    path: "/sucmanh2k",
    component: SucManh2kPage
  },
  {
    name: "donation-2000-manage",
    path: "/sucmanh2k-quanly",
    options: {
      history: true
    },
    component: SucManh2kManagePage
  },
  {
    name: "nchcccl",
    path: "/nhu-chua-he-co-cuoc-chia-ly",
    component: NCHCCCLPage
  },
  {
    name: "nchcccl-manage",
    path: "/nhu-chua-he-co-cuoc-chia-ly-quanly",
    options: {
      history: true
    },
    component: NCHCCCLManagePage
  },
  {
    path: "/gtbb",
    component: ReferralPage,
  },
  {
    path: "/referral-friend",
    component: ReferralFriendPage,
  },
  // {
  //   path: "/form/",
  //   component: FormPage,
  // },
  // {
  //   path: "/dynamic-route/blog/:blogId/post/:postId/",
  //   component: DynamicRoutePage,
  // },
  // {
  //   path: "/request-and-load/user/:userId/",
  //   async: function ({ router, to, resolve }) {
  //     // App instance
  //     var app = router.app;

  //     // Show Preloader
  //     app.preloader.show();

  //     // User ID from request
  //     var userId = to.params.userId;

  //     // Simulate Ajax Request
  //     setTimeout(function () {
  //       // We got user data from request
  //       var user = {
  //         firstName: 'Vladimir',
  //         lastName: 'Kharlampidi',
  //         about: 'Hello, i am creator of Framework7! Hope you like it!',
  //         links: [
  //           {
  //             title: 'Framework7 Website',
  //             url: 'http://framework7.io',
  //           },
  //           {
  //             title: 'Framework7 Forum',
  //             url: 'http://forum.framework7.io',
  //           },
  //         ]
  //       };
  //       // Hide Preloader
  //       app.preloader.hide();

  //       // Resolve route to load page
  //       resolve(
  //         {
  //           component: RequestAndLoad,
  //         },
  //         {
  //           props: {
  //             user: user,
  //           }
  //         }
  //       );
  //     }, 1000);
  //   },
  // },
  {
    path: "(.*)",
    component: NotFoundPage,
  },
];

export default routes;

import axios from 'axios';
//import {appSettings} from 'configs';
//import authenProvider from 'modules/authen/authenProvider'
const instanceAxios = axios.create({
  //baseURL: `${appSettings.AppConfig.HOST_API}`,
  timeout: 20000
});
//  export default axios.create();
const _fetchJson = (source, options = {}) => {
  //var userToken = _getUserToken();
  options = mergeConfig({
    headers: {
      //'x-user-token': userToken ? userToken : '',
      //'x-client': 4,
      'x-timestamp': _getTimestamp()
    }
  }, options);

  
  return instanceAxios
    .request(`${source}`, options)
    .then(res => {
      // if (res.status !== 200) {
      //   if (res.data && res.data.Error) {
      //     if (res.data.Error.Code === 1001) {
      //       authenProvider.RemoveAuthenInfo();
      //       global.AdminContext.Message.Error(`Authen failed: ${res.data.Error.Code}`);
      //       return { Authen: false };
      //     }
      //   }
      // }
      return res.data;
    })
    .catch(error => {
      // if (error.response && error.response.status === 401) {
      //   authenProvider.RemoveAuthenInfo();
      //   global.AdminContext.Message.Error(`Authen failed - status: ${error.response.status}`);
      //   return { Authen: false };
      // }
      // if (error.message) {
      //   global.AdminContext.Message.Error(error.message);
      // }
      // return { Error: { Message: error.message } };
    });

};

const _getUserToken = () => {
  // var userAuthen = localStorage.getItem("userAuthen");
  // if (!userAuthen) return null;
  // userAuthen = JSON.parse(userAuthen);
  // if (!userAuthen) return null;
  // return userAuthen.Token;
  return '';
};
const _getTimestamp = () => {
  var date = new Date();
  return date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
};

const mergeConfig = (config1, config2) => {
  // eslint-disable-next-line no-param-reassign
  config1 = config1 || {};
  config2 = config2 || {};
  var config = {};
  for (var prop1 in config1) {
    config[prop1] = config1[prop1];
  }
  for (var prop2 in config2) {
    config[prop2] = config2[prop2];
  }
  return config;
};

const _validationResponse = (response) => {
  // if (!response || response.Error || !response.Result) {
  //   if (response.Error)
  //     if (response.Error.Message)
  //       global.AdminContext.Message.Error(response.Error.Message);
  //     else if (response.Error.Code) {
  //     } else global.AdminContext.Message.Error('Error');
  //   return false;
  // } else {
  //   return true;
  // }
  return true;
}


const requestAxios = () => {

  var _get = (source, options) => {
    options = mergeConfig({
      method: 'get'
    }, options);

    return _fetchJson(source, options);
  };
  var _post = (source, options) => {
    options = mergeConfig({
      method: 'post'
    }, options);

    return _fetchJson(source, options);
  };
  var _put = (source, options) => {
    options = mergeConfig({
      method: 'put'
    }, options);

    return _fetchJson(source, options);
  };
  var _delete = (source, options) => {
    options = mergeConfig({
      method: 'delete'
    }, options);

    return _fetchJson(source, options);
  };
  return {
    get: _get,
    post: _post,
    put: _put,
    delete: _delete,
    validResponse: _validationResponse
  };
};

export default requestAxios();

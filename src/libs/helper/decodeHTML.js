var decodeHTML = function (html) {
  if (!window.document)
    return html;
  var txt = window.document.createElement('textarea');
  txt.innerHTML = html;
  return txt.value;
};
export default decodeHTML;


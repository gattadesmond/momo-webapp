var OriginalNumber = (input) => {
  try {
    if (!input) {
      return input;
    }
    var startNum = input.substring(0, 3);
    var lstOriginalNumber = ["0162", "0163", "0164", "0165", "0166", "0167", "0168", "0169", "0120", "0121", "0122", "0126", "0128", "0188", "0186", "0199"];
    var lstNewNumber = ["032", "033", "034", "035", "036", "037", "038", "039", "070", "079", "077", "076", "078", "056", "058", "059"];

    if (lstNewNumber.indexOf(startNum) >= 0) {
      startNum = lstOriginalNumber[lstNewNumber.indexOf(startNum)];
      input = startNum + input.substring(3, input.length);
    }

    return input;
  } catch (e) {
    return "";
  }

}
export default OriginalNumber;


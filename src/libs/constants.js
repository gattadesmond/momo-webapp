export const env = process.env.NODE_ENV || "development";
import axios from "@/libs/axios";
import Framework7 from "framework7";

const events = new Framework7.Events();
window.f7Events = events;
export default events;


// http://dev1.momo.vn:6969
export const System =
  env === "production"
    ? {
      apihost: "https://mapi.momo.vn",
      apiAppHost: "https://s.dev.mservice.io",
      apiQuiz: "https://helios.mservice.io/quiz/public",
      userInfo: {},
      HostUrl: location.origin,
    }
    : {
      apihost: "http://dev1.momo.vn:6969",//"http://localhost:51166",
      apiAppHost: "https://s.dev.mservice.io",
      apiQuiz: "https://helios.dev.mservice.io/quiz/public",
      userInfo: {
        phone: "0772532512",
        userName: "MarTech Test",
        balance: 300000,
        appVer: 21422,
        appCode: "2.1.42",
        deviceOS: "IOS",
      },
      HostUrl: location.origin,
    };

// export const System = env === 'production' ?
//     {
//         apihost: "https://mapi.momo.vn",
//         userInfo: {},
//         HostUrl: location.origin
//     } :
//     {
//         apihost: "http://localhost:51166",
//         userInfo: {
//             phone: "0772532512", userName: "MarTech Test", balance: 300000, appVer: 21422, appCode: "2.1.42", deviceOS: "IOS"
//         },
//         HostUrl: location.origin
//     };

export const ApiResponseErrorCodeMessage = {
  request_error: "Hệ thống đang cập nhật. Vui lòng quay lại sau ít phút!!!",
  system_error: "Hệ thống đang cập nhật. Vui lòng quay lại sau ít phút!!!",
  token_error: "Hệ thống đang cập nhật. Vui lòng quay lại sau ít phút!!!",
  notfound: "404! Không tìm thấy dữ liệu",
  donate_error_1014:
    "Giao dịch thất bại vì mật khẩu không chính xác. Vui lòng thử lại.",
  donate_error_1000: "Vui lòng nhập mật khẩu 6 mã PIN",
  donation_createtrans_errortotal: "Vui lòng nhập số tiền muốn Quyên góp",
  session_error: "Về màn hình chính",
  session_error_content:
    "Hệ thống đang cập nhật. Vui lòng quay lại sau ít phút!!!",
  cashback_game_missing:
    "Chương trình chưa bắt đầu. Bạn vui lòng thử lại lần sau!",
  cashback_game_submit_error: "Lỗi xử lý kết quả câu hỏi",
  heodihoc_error: "Về màn hình nhiệm vụ",
  heodihoc_session_error:
    "Hệ thống đang cập nhật. Vui lòng quay lại sau ít phút!!!",
  heodihoc_noquiz: "Lớp học chưa mở. Vui lòng thử lại sau ít phút!",
  heodihoc_submiterror:
    "Hệ thống đang cập nhật. Vui lòng quay lại sau ít phút!!!",
};

export const Track = (dataTrack) => {
  var url = `${System.apihost}/__post/Ajax/TrackInfo`


  url = `${url}?data=${JSON.stringify(dataTrack)}`
  axios.post(url);
};

export const FormatNumber = (input) => {
  if (window.isNaN(input)) {
    return "";
  }
  return input.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "";
};
export const ShortName = (input) => {
  try {
    if (!input) {
        return input;
    }
    var buff = input.split(' ');
    if (buff.length == 1)
        return buff[0][0].toUpperCase();
    if (buff.length > 1) {
        var first = buff[buff.length - 1][0].toUpperCase();
        var second = buff[buff.length - 2][0].toUpperCase();
        return second + first;
    }
    return input;
} catch (e) {
    return input[0];
}
};
export const FM_Date1 = (date, patternStr) => {
  if (!date || typeof date != 'object')
    return date;

  if (!patternStr) {
    patternStr = 'yyyy/MM/dd';
  }
  var day = date.getDate(),
    month = date.getMonth(),
    year = date.getFullYear(),
    hour = date.getHours(),
    minute = date.getMinutes(),
    second = date.getSeconds(),
    miliseconds = date.getMilliseconds(),
    h = hour % 12,
    hh = twoDigitPad(h),
    HH = twoDigitPad(hour),
    mm = twoDigitPad(minute),
    ss = twoDigitPad(second),
    aaa = hour < 12 ? 'AM' : 'PM',
    EEEE = dayOfWeekNames[date.getDay()],
    EEE = EEEE.substr(0, 3),
    dd = twoDigitPad(day),
    M = month + 1,
    MM = twoDigitPad(M),
    MMMM = monthNames[month],
    MMM = MMMM.substr(0, 3),
    yyyy = year + "",
    yy = yyyy.substr(2, 2)
    ;
  // checks to see if month name will be used
  patternStr = patternStr
    .replace('hh', hh).replace('h', h)
    .replace('HH', HH).replace('H', hour)
    .replace('mm', mm).replace('m', minute)
    .replace('ss', ss).replace('s', second)
    .replace('S', miliseconds)
    .replace('dd', dd).replace('d', day)

    .replace('EEEE', EEEE).replace('EEE', EEE)
    .replace('yyyy', yyyy)
    .replace('yy', yy)
    .replace('aaa', aaa);
  if (patternStr.indexOf('MMM') > -1) {
    patternStr = patternStr
      .replace('MMMM', MMMM)
      .replace('MMM', MMM);
  }
  else {
    patternStr = patternStr
      .replace('MM', MM)
      .replace('M', M);
  }
  return patternStr;
};

export const ValidateAccessToken = (webToken, fnc) => {
  var url =
    System.apihost +
    "/__get/Common/ValidateAccessToken?token=" +
    encodeURIComponent(webToken);

  axios.get(url).then((res) => {
    fnc && fnc(res);
  });
};

export const UpdateCurrentLocation = (location) => {
  alert("Lấy Location Thành Công " + location);
};

export const ProcessShareFacebook = (url, isIos) => {
  var now = new Date().valueOf();
  setTimeout(function () {
    if (new Date().valueOf() - now > 500) return;
    window.location =
      "momo://?refId=browser|https://facebook.com/sharer/sharer.php%3Fu%3D" +
      encodeURIComponent(url);
  }, 200);

  if (isIos == undefined) {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;

    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
      isIos = true;
    } else {
      isIos = false;
    }
  }

  if (isIos) {
    window.location =
      "fbapi://dialog/share?app_id=966242223397117&amp;version=20130410&amp;method_args=%7B%22name%22%3Anull%2C%22description%22%3Anull%2C%22link%22%3A%22" +
      encodeURIComponent(url) +
      "%22%2C%22quote%22%3Anull%2C%22hashtag%22%3Anull%2C%22dataFailuresFatal%22%3A%22false%22%7D";
  } else {
    window.location =
      "fb://faceweb/f?href=https://facebook.com/sharer/sharer.php?u=" +
      encodeURIComponent(url);
  }
};

export const ProcessAppTracking = (event, action) => {
  var objTrack = {
    action: "trackingEvent",
    value: { event: event, param: { action: action } },
  };
  window.ReactNativeWebView.postMessage(JSON.stringify(objTrack));
};

export const ProcessAppTrackingCustomParam = (event, param) => {
  var objTrack = {
    action: "trackingEvent",
    value: { event: event, param: param },
  };
  window.ReactNativeWebView.postMessage(JSON.stringify(objTrack));
};

export const ProcessAppShareFacebook = (url) => {
  var objShare = { action: "shareFacebookUrl", value: url };
  window.ReactNativeWebView.postMessage(JSON.stringify(objShare));
};

export const ProcessAppShareImageFacebook = () => {
  var objShare = { action: "shareScreenShot" };
  window.ReactNativeWebView.postMessage(JSON.stringify(objShare));
};

export const ProcessAppShareOther = (url) => {
  var objShare = { action: "shareContent", value: url };
  window.ReactNativeWebView.postMessage(JSON.stringify(objShare));
};

window.CallbackPasswordInfo = function (userData) {
  f7Events.emit("CallbackPasswordInfo", userData);
};

window.CallbackAppUserInfo = function (userData) {
  f7Events.emit("initPageData", userData);
};

window.CallbackPasswordInfo = function (userData) {
  f7Events.emit("CallbackPasswordInfo", userData);
};
window.CallbackAppFwFunc = function (func, data) {
  f7Events.emit(func, data);
};

window.CallbackGetCloudToken = function (userData) {
  f7Events.emit("CallbackGetCloudToken", userData);
};

import React, { useState } from "react";
import events from "@/components/events";
import {
  f7,
  Page,
  Preloader
} from "framework7-react";

//Lib import
import {
  FM_Date1,
  FormatNumber,
  Track,
} from "@/libs/constants";


const SucManh2kManage = (props) => {

  const [Model, setModel] = useState({
    IsLoading: true,
    PageNumber: 1,
    PageSize: 10,
    IsLoadMore: false,
    UserInfo: {},
    DonationInfo: {
      listTokens: [
        {
          paymentSchedule: ""
        }
      ],
      listHistory: []
    },
  });

  const GetUserInfo = () => {
    try {
      window.ReactNativeWebView.postMessage('GetUserInfo');
    } catch (ex) {
      Track({ Name: '2kManage-GetUserInfo', Info: ex.message });
    }

  };

  const GetDonationInfo = (fnc) => {

    try {
      var param = {
        msgType: "GET_TOKENIZE_SERVICE",
        momoMsg: {
          _class: "mservice.backend.entity.msg.ListMomoTokenize"
        },
        pageNumber: Model.PageNumber,
        pageSize: Model.PageSize,
        clientId: "",
        serviceId: "power_2000",
        resFunc: "CallBackDonationInfo"
      };

      window.ReactNativeWebView.postMessage(JSON.stringify({ action: "requestBackend", value: param }));

    } catch (ex) {
      Track({ Name: '2kManage-GetDonationInfo-ex', Info: ex.message });
    }

  };
  const CallbackPasswordInfo = (data) => {

    try {
      var param = {
        msgType: "CANCEL_TOKENIZE_SERVICE",
        momoMsg: {
          _class: "mservice.backend.entity.msg.ListMomoTokenize",
          requestId: Date.now(),
          clientId: "",
          serviceId: "power_2000"
        },
        requestId: "",
        clientId: "",
        serviceId: "power_2000",
        resFunc: "CallBackCancelSubcriber"
      };

      window.ReactNativeWebView.postMessage(JSON.stringify({ action: "requestBackend", value: param }));
    } catch (ex) {
      Track({ Name: '2kManage-CallbackPasswordInfo', Info: ex.message });
    }
    f7.preloader.show();
  };

  const CallBackDonationInfo = (data) => {

    try {

      var jsonString = data.replace(/\n/g, "").replace(/"{/g, "{").replace(/}"/g, "}").replace(/]"/g, "]").replace(/"\[/g, "[");
      var rs = JSON.parse(jsonString);
      if (!rs.result) {
        f7.preloader.hide();
        f7.dialog.alert(rs.errorDesc);
        return;
      }
      if (rs.momoMsg.listTokens.length <= 0 || rs.momoMsg.listTokens[0].paymentSchedule == undefined) {
        location.href = '/sucmanh2k'
        return;
      }
      if (rs.momoMsg.listHistory.length == 10) {
        Model.IsLoadMore = true;
      }
      else {
        Model.IsLoadMore = false;
      }
      for (var i = 0; i < rs.momoMsg.listHistory.length; i++) {
        Model.DonationInfo.listHistory.push(rs.momoMsg.listHistory[i]);
      }
      Model.DonationInfo.listTokens = rs.momoMsg.listTokens;
      Model.IsLoading = false;
      setModel({ ...Model });
      f7.preloader.hide();
    } catch (ex) {
      Track({ Name: '2kManage-CallBackDonationInfo-ex', Info: ex.message });
    }
  };

  const CallBackCancelSubcriber = (data) => {
    var rs = JSON.parse(data);
    if (rs.result) {
      f7.preloader.hide();
      location.href = '/sucmanh2k'
      return;
    }
    else {
      f7.preloader.hide();
      f7.dialog.alert(rs.errorDesc);
      return;
    }
  };

  const InitPageData = (res) => {
    res = JSON.parse(res);
    Model.UserInfo = res;
    setModel({
      ...Model
    });
    GetDonationInfo();
  }

  const BtnSubcriberConfirmPassClick = () => {
    try {
      window.ReactNativeWebView.postMessage(JSON.stringify({ action: "requirePassword" }));
    } catch (ex) {

    }
  };

  const LoadMoreHistory = () => {

    f7.preloader.show();
    Model.PageNumber = Model.PageNumber + 1;
    setModel({ ...Model });

    try {
      var param = {
        msgType: "GET_TOKENIZE_SERVICE",
        momoMsg: {
          _class: "mservice.backend.entity.msg.ListMomoTokenize"
        },
        pageNumber: Model.PageNumber,
        pageSize: Model.PageSize,
        clientId: "",
        serviceId: "power_2000",
        resFunc: "CallBackDonationInfo"
      };
      window.ReactNativeWebView.postMessage(JSON.stringify({ action: "requestBackend", value: param }));
    } catch (e) {

    }
  };
  const BtnCancelClick = () => {

    var dialog = f7.dialog.create({
      title: '',
      text: '',
      cssClass: "pron-popup-container",
      content: '<div class="pron-popup">' +
        '  <div class="pron-popup-content text-left">' +
        '     <h3 class="mt-0 mb-4 lutie-22 " >Hủy liên kết</h3> ' +
        '     <div class=" lutie-13" >Hủy liên kết cho dịch vụ Sức mạnh 2000 ?</div> ' +
        '   </div>' +
        '</div>',
      buttons: [
        {
          text: 'KHÔNG',
          close: true,
          color: "gray"
        },
        {
          text: 'CÓ',
          onClick: function () {
            f7.dialog.close(".pron-popup-container");
            BtnSubcriberConfirmPassClick();
          }
        }],
      on: {
        open: function () {
          console.log("OPEN");
        }
      }
    }).open();

    f7.sheet.close(".dona-sheet-cancel");
  };

  const pageInit = () => {

    events.on("initPageData", InitPageData);
    events.on("CallBackDonationInfo", CallBackDonationInfo);
    events.on("CallBackCancelSubcriber", CallBackCancelSubcriber);
    events.on("CallbackPasswordInfo", CallbackPasswordInfo);

    GetUserInfo();
    /*
         InitPageData(`"UserInfo": {
          "phone": "01678964081",
          "userName": "Đặng Hoài Anh",
          "balance": "56806100",
          "deviceOS": "ios"
        }`);
        */
  };

  const pageOut = () => {
    f7.sheet.close();
  };

  const pageDestroy = () => {
    if (f7.sheet) f7.sheet.destroy();
  };

  return (
    <Page
      name="home"
      onPageInit={pageInit}
      onPageBeforeRemove={pageDestroy}
      onPageBeforeOut={pageOut}
    >
      <>
        {(!Model || Model.IsLoading) ? (
          <div className="text-center py-5 mt-5">
            <Preloader size={24}></Preloader>
          </div>
        ) : (
            <>
              <div className="pron-container balance pt-4 mb-1">
                <div className="pron-row align-items-center">
                  <div className="pron-col">
                    <h2 className="mt-0 mb-0 mdona-title">Thông tin dịch vụ</h2>
                  </div>

                  <div className="pron-col auto">
                    <a href="#" className="setting__more link sheet-open" data-sheet=".dona-sheet-cancel">
                      <i className="f7-icons">ellipsis</i>
                    </a>
                  </div>
                </div>
              </div>

              <div className="list m-0 list-style1 no-border-outside">
                <ul className="dona-list-detail">
                  <li>
                    <div className="item-content">
                      <div className="item-inner">
                        <div className="item-title ">Kỳ thanh toán tiếp theo</div>
                        <div className="item-after ">{Model.DonationInfo.listTokens[0].paymentSchedule}</div>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>

              <div className="pron-container balance pt-4">

                <h2 className="mt-0 mb-1 mdona-title">Lịch sử giao dịch</h2>
                {Model.DonationInfo.listHistory.length > 0 ? <>
                  {Model.DonationInfo.listHistory.map((item, idx) => (
                    <div className="dona-history position-relative">
                      <a href={`momo://?action=marketing&refId=DetailTrans&tranId=${item.tranId}`} className="link-absolute external"></a>
                      <div className="dona-history-item">

                        <div className="dona-history-time lutie-10">
                          {FM_Date1(item.finishTime, 'dd/MM/yyyy HH:mm')}
                        </div>
                        <div className="d-flex align-items-center mt-1">
                          <div className="dona-history-status text-orange flex-grow-1 font-weight-bold lutie-11">{(item.status == 999 || item.status == 0) ? 'THÀNH CÔNG' : 'THẤT BẠI'}</div>
                          <div className="dona-history-money font-weight-bold lutie-12">- {FormatNumber(item.amount)}</div>
                        </div>
                      </div>
                    </div>
                  ))}
                  {Model.IsLoadMore &&
                    <div className="mt-3 pb-5 text-center">
                      <a href="javascript:void(0)" onClick={x => LoadMoreHistory()} className="button button-outline button-round d-inline-block">
                        Xem  thêm
                    </a>
                    </div>}</>
                  :
                  <>
                    <div className="text-center mt-3">
                      <img src="https://static.mservice.io/images/s/momo-upload-api-200312133020-637196166203949468.png" className="img-fluid d-block mx-auto" width="200" alt="" />
                    </div>
                    <div className="text-center text-gray">
                      Bạn chưa có lịch sử giao dịch nào
            </div>
                  </>
                }
              </div>

              <div className="sheet-modal sheet-pop-round dona-sheet-cancel" style={{ 'height': 'auto' }}>
                <div className="sheet-modal-inner">
                  <div className="page-content">
                    <a className="link sheet-close " style={{ 'position': 'absolute', 'top': '9px', 'right': '7px', 'zIndex': '10', 'color': 'gray' }}>
                      <i className="f7-icons opacity-5 ">close</i>
                    </a>
                    <div className="list my-0 no-hairlines dona-share-list">
                      <ul>
                        <li>
                          <a onClick={x => BtnCancelClick()} className="list-button item-link">Hủy đăng ký</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </>
          )}


        <style jsx>{`
         .dona-history {
          padding: 0;
        }
      `}</style>
      </>
    </Page >
  );
};

export default SucManh2kManage;

import React from "react";

const Step6 = () => {
  return (
    <>
      <div className="dona-status ">
        <div className="container">
          <div className="dona-status-icon">
            <img src="/static/images/donation/warning.svg" width="55" className="img-fluid mx-auto d-block" alt="" />
          </div>

          <div className="dona-status-content text-center">
            <div className="lutie-16 mb-0">Thanh toán thất bại</div>
          </div>
        </div>
      </div>

      <div className="block block-strong mt-0">
        <div className="text-center">
          <div className="lutie-16 mb-0 lh-reset" id="errorDesc">
          </div>
        </div>
      </div>
    </>
  );
};

export default Step6;

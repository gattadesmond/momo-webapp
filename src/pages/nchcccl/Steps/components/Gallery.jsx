import React, { useState, useEffect } from "react";

//Lib import
import { ApiResponseErrorCodeMessage, System } from "@/libs/constants";

import DateFormat from "@/components/DateFormat";
import DonationTimelineGallery from "@/components/donation/donation-timeline-gallery.jsx";

import axios from "@/libs/axios";
import {
  f7,
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  Link,
  Toolbar,
  Block,
  Popup,
  Button,
} from "framework7-react";

const Gallery = ({ gallerys }) => {

  if (gallerys.length < 1) return <></>;

  return (
    <>
      <div className="splash no-select"></div>

      <div className="section_title no-select">Hình ảnh</div>

      <div className="timeline soju-timeline no-select">

        <DonationTimelineGallery gallerys={gallerys} />
      </div>

      <style jsx>{`
        .soju-timeline {
          // --f7-timeline-padding-horizontal: 0;
          padding-left: 0;
          padding-right: 0;
          margin-top: 0;
          margin-bottom: 0;
        }

        .soju-timeline :global(.timeline-item-content) {
          width: 100%;
        }
        
        .soju-timeline .timeline-item-time {
          margin-bottom: 5px;
        }
        .timeline-item-date {
          flex-shrink: 0;
          width: 70px;
          text-align: right;
          box-sizing: border-box;
        }

        .timeline-item-divider {
          margin-left: 5px;
        }

        .timeline-item-divider {
          background-color: var(--pink);
        }
        .timeline-item-divider:after,
        .timeline-item-divider:before {
          background-color: var(--pinklight);
        }
      `}</style>
    </>
  );
};

export default Gallery;

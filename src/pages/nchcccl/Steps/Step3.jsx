import React, { useState } from "react";
import { getDevice } from "@/js/framework7-custom.js";

import $ from "dom7";

const Device = getDevice();

const Step3 = ({ model }) => {
  const [Model, setModel] = useState(model);


  var $chkAgree = $('#chkAgree');
  if ($chkAgree.length > 0) {
    if (!$chkAgree[0]._change) {

      $('#chkAgree').change(function () {
        if (this.checked) {
          $('#btnDonationStep3').removeClass("color-gray");
          $('#btnDonationStep3').addClass("color-pink");
          $(this).prop("checked", true);
        }
        else {
          $('#btnDonationStep3').removeClass("color-pink");
          $('#btnDonationStep3').addClass("color-gray");
          $(this).prop("checked", false);
        }
      });
      $chkAgree[0]._change = true;
    }
  }

  return (
    <>
      <div className="dona-hero-img">
        <img
          src="https://static.mservice.io/images/s/momo-upload-api-200312114228-637196101485592238.png"
          className="d-block pr-4"
          width={220}
          alt=""
        />
      </div>
      <div className="pron-container flex-grow-1 balance pt-4">
        <h3 className="mt-0 mb-1 ">Khi bạn ủng hộ định kỳ, MoMo sẽ:</h3>
        <ul className="list-unstyled mt-2 pl-2">
          <li className="mb-2">
            <i className="f7-icons lutie-18 text-success">checkmark_alt</i> Thu tiền định kỳ như đã đăng ký
                            </li>
          <li className="mb-2">
            <i className="f7-icons lutie-18 text-success">checkmark_alt</i>  Nếu ví không đủ số dư, MoMo tự động ngưng gói ủng hộ và không tiếp tục truy thu
                            </li>
          <li className="mb-2">
            <i className="f7-icons lutie-18 text-success">checkmark_alt</i>  Gói ủng hộ có thời hạn trong 365 ngày
                            </li>
          <li className="mb-2">
            <i className="f7-icons lutie-18 text-success">checkmark_alt</i> Bạn có thể hủy mọi lúc tại mục quản lý gói ủng hộ trong tính năng Sức mạnh 2000
                            </li>
          <li className="mb-2">
            <i className="f7-icons lutie-18 text-success">checkmark_alt</i> Thu phí 0.5% trên mỗi giao dịch
                            </li>
        </ul>
      </div>
      <div className="dona-rule-check">
        <div className="form-check">
          <input
            className="form-check-input"
            type="checkbox"
            id="chkAgree"
          />
          <label className="form-check-label" htmlFor="chkAgree">
            Tôi đồng ý với các <a href="momo://?refId=https://app.momo.vn:82/momo_app/html/dieukhoan.html" className="external">điều khoản</a> của MoMo và chính sách bảo mật của MoMo
                </label>
        </div>
      </div>



    </>
  );
};

export default Step3;

import React, { useState } from "react";
import { getDevice } from "@/js/framework7-custom.js";

import $ from "dom7";
import {
  FormatNumber,
} from "@/libs/constants";
const Device = getDevice();

const Step2 = ({ model }) => {
  const [Model, setModel] = useState(model);

  const handleChange = (name, val) => {
    Model.DonationInfo[name] = val;
    setModel({ ...Model });
  }
  const handleMoneyClick = (evt) => {
    $('.mdona-item').removeClass('is-active');
    $(evt.currentTarget).addClass('is-active');

    var type = $(evt.currentTarget).attr('data-type');
    var value = $(evt.currentTarget).attr('data-value');
    var text = $(evt.currentTarget).attr('data-text');

    Model.DonationSubcriberInfo.Type = type;
    Model.DonationSubcriberInfo.Amount = value;
    if (type == 2) {
      $("#lblNote3").hide();
      $("#lblNote2").show();
      $("#num2").text(text);
    }
    else {
      $("#lblNote2").hide();
      $("#lblNote3").show();
      $("#num3").text(text);
    }
    $("#TotalMoneyError").hide();
    Model.DonationInfo.TotalMoneyFormat = "";
    setModel({ ...Model });
  }

  var $inputNum = $('#mdona-input-value');
  if ($inputNum.length > 0) {
    if (!$inputNum[0]._keyup) {
      $inputNum.keyup(function (e) {
        var value = $inputNum.val().replace(/\./g, '').replace(/\-/g, '').replace(/\ /g, '');
        var valMoney = parseInt(value);
        Model.DonationInfo.TotalMoneyFormat = FormatNumber(valMoney);
        setModel({ ...Model });
        $inputNum.val(FormatNumber(valMoney));
        $("#TotalMoneyError").hide();
      });
      $inputNum[0]._keyup = true;
    }
    if (!$inputNum._inputClear) {
      $inputNum.on('input:clear', function (e) {
        Model.DonationInfo.TotalMoneyFormat = "";
        setModel({ ...Model });
      });
      $inputNum[0]._inputClear = true;
    }
    if (!$inputNum._inputFocus) {
      $inputNum.on('focus', function (e) {
        $('.mdona-item').removeClass('is-active');
        Model.DonationSubcriberInfo.Type = 1;
        Model.DonationSubcriberInfo.Amount = 0;
        $("#lblNote3").hide();
        $("#lblNote2").hide();
        setModel({ ...Model });
      });
      $inputNum[0]._inputFocus = true;
    }
  }

  return (
    <>
      <div className="pron-container balance pt-4">
        <h2 className="mt-0 mb-1 mdona-title">Ủng hộ mỗi ngày</h2>
        <div className="mb-0 text-gray lutie-12" id="lblNote2">
          Trừ mỗi ngày <span id="num2">2.000đ</span> trong 1 năm. Bạn có thể hủy mọi lúc.
              </div>
        <div className="mdona-check mt-2">
          <div className="mdona-col">
            <div onClick={x => handleMoneyClick(x)} className="mdona-item is-active" data-type="2" data-value="2000" data-text="2.000đ">
              <div className="mdona-check-icon">
                <i className="f7-icons size-14">icon-checkmark_alt</i>
              </div>
              <div className="mdona-money">2.000đ</div>
              <div className="mdona-money-label">Mỗi ngày</div>
            </div>
          </div>
          <div className="mdona-col" >
            <div onClick={x => handleMoneyClick(x)} className="mdona-item" data-type="2" data-value="3000" data-text="3.000đ">
              <div className="mdona-check-icon">
                <i className="f7-icons size-14">icon-checkmark_alt</i>
              </div>
              <div className="mdona-money">3.000đ</div>
              <div className="mdona-money-label">Mỗi ngày</div>
            </div>
          </div>
          <div className="mdona-col">
            <div onClick={x => handleMoneyClick(x)} className="mdona-item" data-type="2" data-value="5000" data-text="5.000đ">
              <div className="mdona-check-icon">
                <i className="f7-icons size-14">icon-checkmark_alt</i>
              </div>
              <div className="mdona-money">5.000đ</div>
              <div className="mdona-money-label">Mỗi ngày</div>
            </div>
          </div>
        </div>
      </div>
      <div className="pron-container balance pt-4">
        <h2 className="mt-0 mb-1  mdona-title">Ủng hộ mỗi Tuần</h2>
        <div className="mb-0 text-gray lutie-12 " id="lblNote3" style={{ 'display': 'none' }}>
          Trừ mỗi tuần <span id="num3">15.000đ</span> trong 1 năm. Bạn có thể hủy mọi lúc.
              </div>
        <div className="mdona-check mt-2">
          <div className="mdona-col">
            <div onClick={x => handleMoneyClick(x)} className="mdona-item " data-type="3" data-value="15000" data-text="15.000đ">
              <div className="mdona-check-icon">
                <i className="f7-icons size-14">icon-checkmark_alt</i>
              </div>
              <div className="mdona-money">15.000đ</div>
              <div className="mdona-money-label">Mỗi</div>
            </div>
          </div>
          <div className="mdona-col">
            <div onClick={x => handleMoneyClick(x)} className="mdona-item" data-type="3" data-value="25000" data-text="25.000đ">
              <div className="mdona-check-icon">
                <i className="f7-icons size-14">icon-checkmark_alt</i>
              </div>
              <div className="mdona-money">25.000đ</div>
              <div className="mdona-money-label">Mỗi tuần</div>
            </div>
          </div>
          <div className="mdona-col">
            <div onClick={x => handleMoneyClick(x)} className="mdona-item" data-type="3" data-value="35000" data-text="35.000đ">
              <div className="mdona-check-icon">
                <i className="f7-icons size-14">icon-checkmark_alt</i>
              </div>
              <div className="mdona-money">35.000đ</div>
              <div className="mdona-money-label">Mỗi tuần</div>
            </div>
          </div>
        </div>
      </div>
      <div className="pron-container balance pt-4">
        <h2 className="mt-0 mb-1 mdona-title ">Ủng hộ ngay 1 lần</h2>
        <div className="mdona-input-money mt-2 is-wrong">
          <input
            inputMode={!Device.ios ? "numeric" : ""}
            type="text"
            autoComplete="off"
            id="mdona-input-value"
            name="TotalMoneyFormat"
            placeholder="Nhập số tiền bạn muốn ủng hộ"
            pattern="[0-9]*"
            value={Model.DonationInfo.TotalMoneyFormat || ''}
            onChange={(x) =>
              handleChange("TotalMoneyFormat", x.target.value)
            }
          />
          <span className="input-clear-button"></span>

        </div>
        <div id="TotalMoneyError" style={{ 'display': 'none' }} className="lutie-12 text-danger mt-1">
          Số tiền tối thiểu là 1.000đ
            </div>
        {/* <div className="apd-payment mb-2 hide-scrollbar mt-2">
                <div className="apd-payment-item">
                  <input
                    className="apd-payment-check"
                    type="radio"
                    id="apd1"
                    name="payDonation"
                    defaultValue={1}
                    defaultChecked
                  />
                  <label htmlFor="apd1" className="apd-payment-label">
                    <span className="apd-payment-mo">10.000 ₫</span>
                  </label>
                </div>
                <div className="apd-payment-item">
                  <input
                    className="apd-payment-check"
                    type="radio"
                    id="apd2"
                    name="payDonation"
                    defaultValue={2}
                  />
                  <label htmlFor="apd2" className="apd-payment-label">
                    <span className="apd-payment-mo">20.000 ₫</span>
                  </label>
                </div>
                <div className="apd-payment-item">
                  <input
                    className="apd-payment-check"
                    type="radio"
                    id="apd3"
                    name="payDonation"
                    defaultValue={3}
                  />
                  <label htmlFor="apd3" className="apd-payment-label">
                    <span className="apd-payment-mo">50.000 ₫</span>
                  </label>
                </div>
                <div className="apd-payment-item">
                  <input
                    className="apd-payment-check"
                    type="radio"
                    id="apd4"
                    name="payDonation"
                    defaultValue={4}
                  />
                  <label htmlFor="apd4" className="apd-payment-label">
                    <span className="apd-payment-mo">100.000 ₫</span>
                  </label>
                </div>
                <div className="apd-payment-item">
                  <input
                    className="apd-payment-check"
                    type="radio"
                    id="apd5"
                    name="payDonation"
                    defaultValue={5}
                  />
                  <label htmlFor="apd5" className="apd-payment-label">
                    <span className="apd-payment-mo">200.000 ₫</span>
                  </label>
                </div>
                <div className="apd-payment-item">
                  <input
                    className="apd-payment-check"
                    type="radio"
                    id="apd6"
                    name="payDonation"
                    defaultValue={6}
                  />
                  <label htmlFor="apd6" className="apd-payment-label">
                    <span className="apd-payment-mo">500.000 ₫</span>
                  </label>
                </div>
              </div>
         */}
      </div>
      <style jsx>
        {
          ` 
.mdona-input-money{position: relative;}
.input-clear-button {
  right: 10px;
}`
        }
      </style>
    </>
  );
};

export default Step2;

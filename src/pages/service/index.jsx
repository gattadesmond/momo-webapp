import React, { useState, useEffect } from "react";
import {
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  NavRight,
  Link,
  List,
  Sheet,
  Icon,
  ListInput,
  ListItem,
  Toggle,
  BlockTitle,
  Row,
  Button,
  Range,
  Block,
} from "framework7-react";

import Image from "@/components/image";
import ServicesTableList from "@/components/services-table-list";

import "./service.less";

const ServicePage = (props) => {
  const pageInit = () => {};

  const pageDestroy = () => {};

  const services = [
    {
      refId: "airline_landing_page",
      title: "Mua vé máy bay",
      subtitle: "Đặt là bay, giá tốt ngất ngây",
      icon:
        "https://static.mservice.io/fileuploads/svg/momo-file-201124155541.svg",
    },
    {
      refId: "vetauhoa_landing_page",
      title: "Mua vé tàu hỏa",
      subtitle: "Đặt mau lẹ, trải nghiệm khỏe re",
      icon:
        "https://static.mservice.io/fileuploads/svg/momo-file-201124155620.svg",
    },
    {
      refId: "bus_landing_page",
      title: "Mua vé xe khách",
      subtitle: "Đặt dễ dàng, ưu đãi ngập tràn",
      icon:
        "https://static.mservice.io/fileuploads/svg/momo-file-201124155610.svg",
    }
  ];

  const services2 = [
    {
      refId: "traveloka_landing_page",
      title: "Đặt phòng khách sạn Traveloka",
      subtitle: "Đặt phòng & thanh toán ngay trên Ví",
      icon:
        "/static/images/service/logo_traveloka@3x.png",
    }
  ];

  return (
    <>
      <Page onPageInit={pageInit} onPageBeforeRemove={pageDestroy}>
        <Navbar className="momo-navbar">
          <NavLeft backLink>{/* <Link iconIos="f7:chevron_left" /> */}</NavLeft>

          <NavTitle sliding> Khám phá dịch vụ</NavTitle>
        </Navbar>

        <div className="service__banner">
          <Image
            src="/static/images/service/2021-DuXuan-1080x540.png"
            alt=""
            width="108"
            height="54"
          />
        </div>
        <Block className="my-4 service-article">
          <h1 className="tt1">Du xuân hay về nhà, MoMo chiều tất cả</h1>
          <p>
            <strong>Ví MoMo sẽ giúp bạn chuẩn bị chu đáo cho những chuyến đi mùa lễ hội.</strong>
            Nhập địa danh, đặt <strong>vé máy bay, vé tàu hỏa và vé xe khách</strong> thật dễ
            dàng - tiết kiệm cùng các hãng hàng không, vận tải uy tín nhất Việt
            Nam.
          </p>

          <p>
            Bạn muốn nghỉ chân khám phá các vùng đất xinh đẹp? Đừng quên <strong>đặt
            phòng khách sạn</strong> với cả triệu lựa chọn trên Ví MoMo.
          </p>

          <h2 className="tt2">Khởi động mùa lễ hội bằng tấm vé ưng ý </h2>

          <p>
            Vi vu bằng <strong>máy bay</strong> để mọi vùng đất trở nên gần hơn và bản thân có
            thêm thời gian nghỉ ngơi, du lịch. Lựa chọn đi <strong>tàu hỏa</strong> nếu bạn muốn
            ngắm cảnh đẹp lễ hội lướt ngoài cửa sổ. Đặt trước chỗ ngồi cho
            chuyến đi thoải mái ngay trong dịp cao điểm - hãy đặt <strong>xe khách</strong> trên
            Ví MoMo.
          </p>
        </Block>

        <ServicesTableList services={services} />

        <Block className="my-4 service-article">


          <h2 className="tt2">Nơi lưu trú như mơ, cho mùa vui rực rỡ </h2>

          <p>
          Một nơi lưu trú phù hợp sẽ giúp bạn tận hưởng chuyến đi đầu năm trọn vẹn hơn. Vì thế, Ví MoMo mang tới các lựa chọn linh hoạt cho bạn: từ khách sạn cao cấp tới phòng nghỉ tiết kiệm, từ căn hộ cho thuê ngắn ngày tới biệt thự nghỉ dưỡng.
          </p>
        </Block>

        <ServicesTableList services={services2} />



      </Page>

      <style jsx>{``}</style>
    </>
  );
};

export default ServicePage;

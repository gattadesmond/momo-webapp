import React, { useState, useEffect } from "react";
import {
  f7,
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  Sheet,
  Icon,
  Block,
} from "framework7-react";
import $ from "dom7";

import "./style.less";

const ServicePage2 = (props) => {
  const pageInit = () => {};

  const pageDestroy = () => {};

  return (
    <>
      <Page onPageInit={pageInit} onPageBeforeRemove={pageDestroy}>
        <Navbar className="momo-navbar">
          <NavLeft backLink></NavLeft>

          <NavTitle sliding> Khám phá dịch vụ</NavTitle>
        </Navbar>

        <div className="service__banner">
          <figure className="figure">
            <div className="figure-embed">
              <img
                src="https://static.mservice.io/images/s/momo-upload-api-210120113054-637467390542745363.png"
                alt=""
                className="figure-img img-fluid d-block"
                loading="lazy"
              />
            </div>
          </figure>
        </div>

        <Block className="py-0 my-0">
          <div className="my-3 service-article">
            <h1 className="tt1">Du xuân hay về nhà, MoMo chiều tất cả</h1>
            <p className="">
              <strong className="">
                Ví MoMo sẽ giúp bạn chuẩn bị chu đáo cho những chuyến đi mùa lễ
                hội.
              </strong>
              Nhập địa danh, đặt
              <strong className="">
                vé máy bay, vé tàu hỏa và vé xe khách
              </strong>
              thật dễ dàng - tiết kiệm cùng các hãng hàng không, vận tải uy tín
              nhất Việt Nam.
            </p>
            <p className="">
              Bạn muốn nghỉ chân khám phá các vùng đất xinh đẹp? Đừng quên
              <strong className="">đặt phòng khách sạn</strong> với cả triệu lựa
              chọn trên Ví MoMo.
            </p>
            <h2 className="tt2">Khởi động mùa lễ hội bằng tấm vé ưng ý</h2>
            <p className="">
              Vi vu bằng <strong className="">máy bay</strong> để mọi vùng đất
              trở nên gần hơn và bản thân có thêm thời gian nghỉ ngơi, du lịch.
              Lựa chọn đi
              <strong className="">tàu hỏa</strong> nếu bạn muốn ngắm cảnh đẹp
              lễ hội lướt ngoài cửa sổ. Đặt trước chỗ ngồi cho chuyến đi thoải
              mái ngay trong dịp cao điểm - hãy đặt{" "}
              <strong className="">xe khách</strong> trên Ví MoMo.
            </p>
          </div>
          <div className="services-table-list my-3 list no-hairlines no-chevron">
            <ul>
              <li className="">
                <a className="item-link" href="/service/airline_landing_page">
                  <div className="item-content">
                    <div className="item-media">
                      <div className="services-table-list-image" slot="media">
                        <img
                          loading="lazy"
                          src="https://static.mservice.io/fileuploads/svg/momo-file-201124155541.svg"
                          alt="Mua vé máy bay"
                        />
                      </div>
                    </div>
                    <div className="item-inner">
                      <div className="item-title">
                        <div className="services-table-list-title" slot="title">
                          Mua vé máy bay
                        </div>
                        <div
                          className="services-table-list-subtitle item-text"
                          slot="title"
                        >
                          Đặt là bay, giá tốt ngất ngây
                        </div>
                      </div>
                      <div className="services-table-list-button" slot="inner">
                        <button
                          className="prevent-active-state-propagation button"
                          type="button"
                        >
                          Đặt ngay
                        </button>
                      </div>
                    </div>
                  </div>
                </a>
              </li>
              <li className="">
                <a className="item-link" href="/service/vetauhoa_landing_page">
                  <div className="item-content">
                    <div className="item-media">
                      <div className="services-table-list-image" slot="media">
                        <img
                          loading="lazy"
                          src="https://static.mservice.io/fileuploads/svg/momo-file-201124155620.svg"
                          alt="Mua vé tàu hỏa"
                        />
                      </div>
                    </div>
                    <div className="item-inner">
                      <div className="item-title">
                        <div className="services-table-list-title" slot="title">
                          Mua vé tàu hỏa
                        </div>
                        <div
                          className="services-table-list-subtitle item-text"
                          slot="title"
                        >
                          Đặt mau lẹ, trải nghiệm khỏe re
                        </div>
                      </div>
                      <div className="services-table-list-button" slot="inner">
                        <button
                          className="prevent-active-state-propagation button"
                          type="button"
                        >
                          Đặt ngay
                        </button>
                      </div>
                    </div>
                  </div>
                </a>
              </li>
              <li className="">
                <a className="item-link" href="/service/bus_landing_page">
                  <div className="item-content">
                    <div className="item-media">
                      <div className="services-table-list-image" slot="media">
                        <img
                          loading="lazy"
                          src="https://static.mservice.io/fileuploads/svg/momo-file-201124155610.svg"
                          alt="Mua vé xe khách"
                        />
                      </div>
                    </div>
                    <div className="item-inner">
                      <div className="item-title">
                        <div className="services-table-list-title" slot="title">
                          Mua vé xe khách
                        </div>
                        <div
                          className="services-table-list-subtitle item-text"
                          slot="title"
                        >
                          Đặt dễ dàng, ưu đãi ngập tràn
                        </div>
                      </div>
                      <div className="services-table-list-button" slot="inner">
                        <button
                          className="prevent-active-state-propagation button"
                          type="button"
                        >
                          Đặt ngay
                        </button>
                      </div>
                    </div>
                  </div>
                </a>
              </li>
            </ul>
          </div>
          <div className="my-3 service-article">
            <h2 className="tt2">Nơi lưu trú như mơ, cho mùa vui rực rỡ</h2>
            <p className="">
              Một nơi lưu trú phù hợp sẽ giúp bạn tận hưởng chuyến đi đầu năm
              trọn vẹn hơn. Vì thế, Ví MoMo mang tới các lựa chọn linh hoạt cho
              bạn: từ khách sạn cao cấp tới phòng nghỉ tiết kiệm, từ căn hộ cho
              thuê ngắn ngày tới biệt thự nghỉ dưỡng.
            </p>
          </div>
          <div className="services-table-list my-3 list no-hairlines no-chevron">
            <ul>
              <li className="">
                <a className="item-link" href="/service/traveloka_landing_page">
                  <div className="item-content">
                    <div className="item-media">
                      <div className="services-table-list-image" slot="media">
                        <img
                          loading="lazy"
                          src="https://static.mservice.io/images/s/momo-upload-api-210120113110-637467390703942163.png"
                          alt="Đặt phòng khách sạn Traveloka"
                        />
                      </div>
                    </div>
                    <div className="item-inner">
                      <div className="item-title">
                        <div className="services-table-list-title" slot="title">
                          Đặt phòng khách sạn Traveloka
                        </div>
                        <div
                          className="services-table-list-subtitle item-text"
                          slot="title"
                        >
                          Đặt phòng &amp; thanh toán ngay trên Ví
                        </div>
                      </div>
                      <div className="services-table-list-button" slot="inner">
                        <button
                          className="prevent-active-state-propagation button"
                          type="button"
                        >
                          Đặt ngay
                        </button>
                      </div>
                    </div>
                  </div>
                </a>
              </li>
            </ul>
          </div>
        </Block>
      </Page>

      <style jsx>{`
        .is-error {
          color: #58151c;
          background-color: #f8d7da;
          border-color: #f1909b;
        }
      `}</style>
    </>
  );
};

export default ServicePage2;

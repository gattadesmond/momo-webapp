import React, { useState, useEffect } from "react";
import {
  f7,
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  Toolbar,
  Link,
  Block,
  Tabs,
  Tab,
  useStore,
} from "framework7-react";

import $ from "dom7";

import "./donation.less";

const DonationUngHo = (props) => {
  const pageInit = () => {
    $(".mdona-item").each(function (e, i) {
      $(e).on("click", function (e) {
        $(".mdona-item").removeClass("is-active");
        $(this).addClass("is-active");
      });
    });
  };

  const pageOut = () => {};

  const pageDestroy = () => {};

  return (
    <Page
      name="home"
      onPageInit={pageInit}
      onPageBeforeRemove={pageDestroy}
      onPageBeforeOut={pageOut}
    >
      <Navbar className="momo-navbar">
        <NavLeft backLink>{/* <Link iconIos="f7:chevron_left" /> */}</NavLeft>

        <NavTitle sliding> Gói ủng hộ xây dựng</NavTitle>
      </Navbar>

      <Toolbar position="bottom" className=" soju-toolbar ">
        <div className=" w-100">
          <p className="row mb-0 ">
            <a className="col button button-fill button-large color-gray  back ">
              Quay lại
            </a>
            <a
              href="/donation-ungho-rule/"
              className="col button button-fill button-large color-pink "
            >
              Tiếp tục&nbsp;
            </a>
          </p>
        </div>
      </Toolbar>

      <div>
        <div className="pron-container balance pt-4">
          <h2 className="mt-0 mb-1 mdona-title">Ủng hộ mỗi ngày</h2>
          <div className="mb-0 text-gray lutie-12 ">
            Số tiền chọn sẽ tự động quyên góp mỗi ngày trong 1 năm. Bạn có thể
            huỷ mọi lúc
          </div>
          <div className="mdona-check mt-2">
            <div className="mdona-col">
              <div className="mdona-item is-active">
                <div className="mdona-check-icon">
                  <i className="f7-icons size-14">icon-checkmark_alt</i>
                </div>
                <div className="mdona-money">2.000đ</div>
                <div className="mdona-money-label">Mỗi ngày</div>
              </div>
            </div>
            <div className="mdona-col">
              <div className="mdona-item">
                <div className="mdona-check-icon">
                  <i className="f7-icons size-14">icon-checkmark_alt</i>
                </div>
                <div className="mdona-money">3.000đ</div>
                <div className="mdona-money-label">Mỗi ngày</div>
              </div>
            </div>
            <div className="mdona-col">
              <div className="mdona-item">
                <div className="mdona-check-icon">
                  <i className="f7-icons size-14">icon-checkmark_alt</i>
                </div>
                <div className="mdona-money">5.000đ</div>
                <div className="mdona-money-label">Mỗi ngày</div>
              </div>
            </div>
          </div>
        </div>
        <div className="pron-container balance pt-4">
          <h2 className="mt-0 mb-1  mdona-title">Ủng hộ mỗi Tuần</h2>
          <div className="mb-0 text-gray lutie-12 ">
            Số tiền chọn sẽ tự động quyên góp mỗi ngày trong 1 năm. Bạn có thể
            huỷ mọi lúc
          </div>
          <div className="mdona-check mt-2">
            <div className="mdona-col">
              <div className="mdona-item ">
                <div className="mdona-check-icon">
                  <i className="f7-icons size-14">icon-checkmark_alt</i>
                </div>
                <div className="mdona-money">15.000đ</div>
                <div className="mdona-money-label">Mỗi</div>
              </div>
            </div>
            <div className="mdona-col">
              <div className="mdona-item">
                <div className="mdona-check-icon">
                  <i className="f7-icons size-14">icon-checkmark_alt</i>
                </div>
                <div className="mdona-money">25.000đ</div>
                <div className="mdona-money-label">Mỗi tuần</div>
              </div>
            </div>
            <div className="mdona-col">
              <div className="mdona-item">
                <div className="mdona-check-icon">
                  <i className="f7-icons size-14">icon-checkmark_alt</i>
                </div>
                <div className="mdona-money">35.000đ</div>
                <div className="mdona-money-label">Mỗi tuần</div>
              </div>
            </div>
          </div>
        </div>
        <div className="pron-container balance pt-4">
          <h2 className="mt-0 mb-1 mdona-title ">Ủng hộ ngay 1 lần</h2>
          <div className="mdona-input-money mt-2 is-wrong">
            <input
              type="text"
              id="mdona-input-value"
              placeholder="Nhập số tiền bạn muốn ủng hộ"
              pattern="\d*"
            />
            <div className="lutie-12 text-danger mt-1">
              “Số tiền tối đa là 20.000.000đ”
            </div>
          </div>
          <div className="apd-payment mb-2 hide-scrollbar mt-2">
            <div className="apd-payment-item">
              <input
                className="apd-payment-check"
                type="radio"
                id="apd1"
                name="payDonation"
                defaultValue={1}
                defaultChecked
              />
              <label htmlFor="apd1" className="apd-payment-label">
                <span className="apd-payment-mo">10.000 ₫</span>
              </label>
            </div>
            <div className="apd-payment-item">
              <input
                className="apd-payment-check"
                type="radio"
                id="apd2"
                name="payDonation"
                defaultValue={2}
              />
              <label htmlFor="apd2" className="apd-payment-label">
                <span className="apd-payment-mo">20.000 ₫</span>
              </label>
            </div>
            <div className="apd-payment-item">
              <input
                className="apd-payment-check"
                type="radio"
                id="apd3"
                name="payDonation"
                defaultValue={3}
              />
              <label htmlFor="apd3" className="apd-payment-label">
                <span className="apd-payment-mo">50.000 ₫</span>
              </label>
            </div>
            <div className="apd-payment-item">
              <input
                className="apd-payment-check"
                type="radio"
                id="apd4"
                name="payDonation"
                defaultValue={4}
              />
              <label htmlFor="apd4" className="apd-payment-label">
                <span className="apd-payment-mo">100.000 ₫</span>
              </label>
            </div>
            <div className="apd-payment-item">
              <input
                className="apd-payment-check"
                type="radio"
                id="apd5"
                name="payDonation"
                defaultValue={5}
              />
              <label htmlFor="apd5" className="apd-payment-label">
                <span className="apd-payment-mo">200.000 ₫</span>
              </label>
            </div>
            <div className="apd-payment-item">
              <input
                className="apd-payment-check"
                type="radio"
                id="apd6"
                name="payDonation"
                defaultValue={6}
              />
              <label htmlFor="apd6" className="apd-payment-label">
                <span className="apd-payment-mo">500.000 ₫</span>
              </label>
            </div>
          </div>
        </div>
      </div>

      <style jsx>{`
        .soju-tab {
        }
        .soju-tab :global(.mainSlider-item) {
        }
      `}</style>
    </Page>
  );
};

export default DonationUngHo;

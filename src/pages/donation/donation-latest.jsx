import React, { useState, useEffect } from "react";
import {
  f7,
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  NavRight,
  Link,
  Block,
  Popup,
  Toolbar,
  Button,
} from "framework7-react";
import { ApiResponseErrorCodeMessage, System } from "@/libs/constants";
import Image from "@/components/image";
import axios from "@/libs/axios";
import DonationTimelineGallery from "@/components/donation/donation-timeline-gallery";
import DateFormat from "@/components/DateFormat";
import MaxApi from "@momo-platform/max-api";

import NavbarMoMoFixed from "@/components/navbar-momo-fixed";

const DonationLatest = (props) => {

  const [ConfigData, setConfigData] = useState({
    UserInfo: {},
    FirebaseToken: "",
    IsFromNoti: false,
  });

  const [data, setData] = useState({
    Count: 10,
    Idx: 0,
    SortType: 1,
    SortDir: 2,
    Items: [],
  });
  const [isShowPopup, setIsShowPopup] = useState(false);
  const [dataSche, setDataSche] = useState({});
  const ShowPopup = (sche) => {
    if (sche.Items.length < 1 && sche.Content) {
      setDataSche({ ...sche });
      setIsShowPopup(true);
    }
  };
  const listData = (fnc) => {
    var url = `${System.apihost}/__get/DonationSchedule/List?count=${data.Count}&idx=${data.Idx}&sortType=${data.SortType}&SortDir=${data.SortDir}`;
    axios.get(url).then((res) => {
      if (!res.Result && res.Data == null) {
        f7.dialog.alert(ApiResponseErrorCodeMessage[res.ErrorCode]);
        f7.preloader.hide();
        return;
      }
      fnc && fnc(res.Data);
    });
  };

  const onBack = () => {
    MaxApi.goBack();
  };

  const pageInit = () => {
    // events.on("CallbackGetCloudToken", (res) => {
    //   setConfigData({
    //     ...ConfigData,
    //     FirebaseToken: res,
    //   });
    // });

    // events.on('initPageData', (res) => {
    //   res = JSON.parse(res);
    //   setConfigData({
    //     ...ConfigData,
    //     UserInfo: res,
    //   });
    // });

    //Device.ios
    // if (props.f7route.query.webToken) {
    //   //Di truc tiep

    //   // try {
    //   //   window.ReactNativeWebView.postMessage("GetUserInfo");
    //   //   window.ReactNativeWebView.postMessage(
    //   //     JSON.stringify({ action: "getCloudToken" })
    //   //   );
    //   // } catch (e) { }

    //   // ConfigData.IsFromNoti = true;
    //   // setConfigData({
    //   //   ...ConfigData
    //   // });


    // } else {
    //   //di bang Miniapp
    //   // MaxApi.getProfile(function (res) {
    //   //   ConfigData.UserInfo = {
    //   //     phone: res.user,
    //   //     userName: res.userName,
    //   //   };
    //   //   MaxApi.getDeviceInfo(function (res) {
    //   //     ConfigData.FirebaseToken = res.firebaseToken;
    //   //     setConfigData({ ...ConfigData });
    //   //   });
    //   // });
    // }
  };


  function pageDestroy() { }

  useEffect(() => {
    f7.preloader.show();
    listData((res) => {
      res.Items.forEach((x) => {
        data.Items.push(x);
      });
      data.TotalItems = res.TotalItems;
      data.Count = res.Count;
      data.LastIndex = res.LastIndex;
      data.PageCount = res.PageCount;
      setData({ ...data });

      f7.preloader.hide();
    });
  }, []);
  return (
    <Page name="home" onPageInit={pageInit} onPageBeforeRemove={pageDestroy}>
      {/* Top Navbar */}

      <NavbarMoMoFixed title={'Cập nhật hoàn cảnh'} isBack /> 
      
      <Block className="my-0 px-0">
        {data.Items.map((sche, idx) => {
          return (
            <div key={idx} className="sojuBox no-select" onClick={(x) => ShowPopup(sche)}>
              <div className="sojuBox-sidebar">
                {sche.PartnerImage && (
                  <div className="sojuBox-avatar">
                    <img src={sche.PartnerImage} alt="" className="img-fluid" />
                  </div>
                )}
              </div>
              <div className="sojuBox-main">
                <div className="sojuBox-header">
                  <div className="content">
                    {sche.RootCategoryName ? (
                      <>
                        <div className="title">{sche.RootCategoryName}</div>
                        <div className="sub">{sche.CategoryName}</div>
                      </>
                    ) : (
                        <div className="title">{sche.CategoryName}</div>
                      )}
                  </div>
                  <div className="date">
                    <DateFormat date={sche.CreatedOn}></DateFormat>
                  </div>
                </div>
              </div>

              <div className="sojuBox-content">{sche.Title}</div>

              <div className="sojuBox-gallery embed-responsive embed-responsive-16by9">
                <img
                  src={sche.Avatar}
                  className="img-fluid d-block embed-responsive-item"
                />
              </div>

              {sche.Items && sche.Items.length > 0 && (
                <DonationTimelineGallery gallerys={sche.Items} />
              )}

              <div className="d-none">
                <Image
                  src={sche.Avatar}
                  alt=""
                  className="embed-responsive-img"
                />
              </div>
            </div>
          );
        })}
      </Block>

      <Popup
        className="demo-popup"
        opened={isShowPopup}
        onPopupClosed={() => setIsShowPopup(false)}
      >
        <Page className="no-select">
          <Navbar>
            <NavLeft>
              <Link
                popupClose
                className="text-dark"
                iconIos="f7:chevron_left"
              />
            </NavLeft>
            <NavTitle>Tình trạng</NavTitle>
          </Navbar>

          <Toolbar position="bottom" className="soju-popup-toolbar ">
            <div className="px-1 w-100">
              <Button outline className="soju-btn-gray bg-white" popupClose>
                Đóng
              </Button>
            </div>
          </Toolbar>
          <Block>
            <div className="section_title" style={{ margin: 0 }}>
              {dataSche.Title}
            </div>
            <div className="timeline-item-time" style={{ margin: "0 0 5px 0" }}>
              {dataSche.CreatedOn && (
                <DateFormat date={dataSche.CreatedOn}></DateFormat>
              )}
            </div>
            <div
              className="article_content"
              dangerouslySetInnerHTML={{ __html: dataSche.Content }}
            />
          </Block>
        </Page>
      </Popup>

      <style jsx>{`
        .sojuBox {
          background-color: white;
          overflow: hidden;
          padding: 15px;
          position: relative;
          border-bottom: 1px solid var(--gray-200);
        }

        .sojuBox-gallery {
        }

        .sojuBox-sidebar {
          position: absolute;
          left: 12px;
          top: 12px;
        }

        .sojuBox-avatar {
          width: 50px;
          height: 50px;
          border: 1px solid var(--gray-300);
          padding: 5px;
          box-sizing: border-box;
          border-radius: 50%;
          overflow: hidden;
          display: flex;
          align-items: center;
          justify-content: center;
        }

        .sojuBox-main {
          flex: 1;
        }

        .sojuBox-header {
          position: relative;
          display: flex;
          flex-flow: row nowrap;
          padding-left: 60px;
          margin-bottom: 10px;
          max-width: 100%;
        }

        .sojuBox-header .content {
          flex: 1;
          padding: 0;
          min-width: 0;
        }

        .sojuBox-header .title {
          font-size: 16px;
          font-weight: bold;
          margin-bottom: 3px;
          white-space: nowrap;
          text-overflow: ellipsis;
          overflow: hidden;
        }

        .sojuBox-header .date {
          padding-top: 2px;
          z-index: 1;
          font-size: 14px;
          background: white;
          padding-left: 8px;

          color: var(--gray-600);
          white-space: nowrap;
        }

        .sojuBox-header .sub {
          font-size: 14px;
          color: var(--gray-600);
        }

        .sojuBox-content {
          padding: 5px 0 15px;
        }

        .sojuBox-gallery {
          border-radius: 5px;
          overflow: hidden;
          background-color: var(--gray-200);
        }

        .soju-tab :global(.mainSlider-item) {
        }
      `}</style>
    </Page>
  );
};

export default DonationLatest;

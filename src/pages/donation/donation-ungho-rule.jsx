import React, { useState, useEffect } from "react";
import {
  f7,
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  Toolbar,
  Link,
  Block,
  Tabs,
  Tab,
  useStore,
} from "framework7-react";

import $ from "dom7";

import "./donation.less";

const DonationUngHo = (props) => {
  const pageInit = () => {
    $(".mdona-item").each(function (e, i) {
      $(e).on("click", function (e) {
        $(".mdona-item").removeClass("is-active");
        $(this).addClass("is-active");
      });
    });
  };

  const pageOut = () => {};

  const pageDestroy = () => {};

  return (
    <Page
      name="home"
      onPageInit={pageInit}
      onPageBeforeRemove={pageDestroy}
      onPageBeforeOut={pageOut}
      pageContent={false}
    >
      <Navbar className="momo-navbar">
        <NavLeft backLink>{/* <Link iconIos="f7:chevron_left" /> */}</NavLeft>

        <NavTitle sliding> Đăng ký ủng hộ định kỳ</NavTitle>
      </Navbar>

      <Toolbar position="bottom" className=" soju-toolbar ">
        <div className=" w-100">
          <p className="row mb-0 ">
            <a
              href="/donation-ungho-rule/"
              className="col button button-fill button-large color-pink "
            >
              Quản lý gói ủng hộ
            </a>
          </p>
        </div>
      </Toolbar>

      <div className="page-content bg-white d-flex flex-column">
        <div className="dona-hero-img">
          <img
            src="https://static.mservice.io/images/s/momo-upload-api-200312114228-637196101485592238.png"
            className="d-block pr-4"
            width={220}
            alt=""
          />
        </div>
        <div className="pron-container flex-grow-1 balance pt-4">
          <h3 className="mt-0 mb-1 ">Khi bạn ủng hộ định kỳ, MoMo sẽ:</h3>
          <ul className="list-unstyled mt-2 pl-2">
            <li className="mb-2">
              <i className="f7-icons lutie-18 text-success">checkmark_alt</i>{" "}
              Thu tiền định kỳ như đã đăng ký
            </li>
            <li className="mb-2">
              <i className="f7-icons lutie-18 text-success">checkmark_alt</i>{" "}
              Nếu ví không đủ số dư, MoMo tự động ngưng gói ủng hộ và không tiếp
              tục truy thu
            </li>
            <li className="mb-2">
              <i className="f7-icons lutie-18 text-success">checkmark_alt</i>{" "}
              Gói ủng hộ có thời hạn trong 365 ngày
            </li>
            <li className="mb-2">
              <i className="f7-icons lutie-18 text-success">checkmark_alt</i>{" "}
              Bạn có thể ngừng ủng hộ mọi lúc tại mục Dịch vụ liên kết hoặc tại
              tính năng Sức mạnh 2000
            </li>
          </ul>
        </div>
        <div className="dona-rule-check">
          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              defaultValue
              id="defaultCheck1"
            />
            <label className="form-check-label" htmlFor="defaultCheck1">
              Tôi đồng ý với các điều khoản của MoMo và chính sách bảo mật của
              MoMo
            </label>
          </div>
        </div>
      </div>

      <style jsx>{`
        .soju-tab {
        }
        .soju-tab :global(.mainSlider-item) {
        }
      `}</style>
    </Page>
  );
};

export default DonationUngHo;

import React, { useState, useEffect, useRef, Fragment } from "react";
import { getDevice } from "@/js/framework7-custom.js";
import events from "@/components/events";

import {
  f7,
  Page,
  PageContent,
  Navbar,
  NavLeft,
  NavTitle,
  NavRight,
  Link,
  Toolbar,
  Block,
  Toggle,
  Sheet,
  Row,
  Col,
  Button,
  useStore
} from "framework7-react";

//Lib import
import {
  ApiResponseErrorCodeMessage,
  System,
  ValidateAccessToken,
  FormatNumber,
} from "@/libs/constants";
import store from '@/js/store';


import useSWR from "swr";

import axios from "@/libs/axios";
import MaxApi from "@momo-platform/max-api";
import DonationCaseDetail from "@/components/donation/donation-card-detail.jsx";

const Device = getDevice();

const MoneyChoice = (props) => {

  const WebToken = useStore('WebToken');

  const [dona, setDona] = useState({});
  const [donaModel, setdonaModel] = useState({ Open: false });
  const [ConfigData, setConfigData] = useState({
    UserInfo: {
      phone: ''
    },
    IsFromNoti: false,
    FirebaseToken: "",
    ListSuggestions: [],
    ListSuggestionsText: [
      "Cảm ơn những lá chắn vững vàng!",
      "Vững tâm nhé các Chiến sĩ áo trắng.",
      "Chúng tôi sẽ ở nhà!",
      "Chúng ta sẽ thành công!",
    ],
  });

  const CreateTransByToken = (param, fnc) => {
    var url = `${System.apihost}/__post/Donation/CreateTransByTokenV2`;
    axios
      .post(url, {
        data: param,
      })
      .then((res) => {
        if (!res.Result) {
          f7.preloader.hide();
          if (res.ErrorCode == "session_error") {
            f7.dialog.alert(
              ApiResponseErrorCodeMessage[res.ErrorCode],
              ApiResponseErrorCodeMessage[res.ErrorCode + "_content"],
              function () {
                location.href = "momo://?refId=Cashback";
              }
            );
            return;
          }
          f7.dialog.alert(ApiResponseErrorCodeMessage[res.ErrorCode]);

          return;
        }
        fnc && fnc(res.Data);
      });
  };

  const ListCashBackInfoUserByToken = (fnc) => {
    var url = `${System.apihost}/__get/Donation/ListCashBackInfoUserByToken?token=${props.token}`;

    axios
      .get(url)
      .then((res) => {
        if (!res.Result && res.Data == null) {
          f7.preloader.hide();
          if (res.ErrorCode == "session_error") {
            f7.dialog.alert(
              ApiResponseErrorCodeMessage[res.ErrorCode],
              ApiResponseErrorCodeMessage[res.ErrorCode + "_content"]
            );
            return;
          }
          f7.dialog.alert(ApiResponseErrorCodeMessage[res.ErrorCode]);

          return;
        }
        f7.preloader.hide();

        fnc && fnc(res.Data);
      })
      .catch((error) => {
        //alert(error);
        f7.preloader.hide();
        f7.dialog.alert(ApiResponseErrorCodeMessage["system_error"]);

        return;
      });
  };



  const pb = useRef(null);

  const DonationClick = () => {
    if (!ConfigData.UserInfo.phone)
      return;

    f7.preloader.show();
debugger;
    if (dona.Uid == "") {
      app.dialog.alert(ApiResponseErrorCodeMessage["system_error"]);
      return;
    }
    if (Device.ios) {
      location.href = 'momo://?refId=browser|' + window.HostUrl + "/chon-nguon-tien/" + dona.Uid;
    }
    else {
      location.href = "/chon-nguon-tien/" + dona.Uid;
    }
    return;
    // ListCashBackInfoUser(function (res) {

    //   if (res.totalMoMoMoney > 1000) {
    //     res.TotalMoneyFormat = FormatNumber(20000);
    //     res.MoneySource = 1;
    //     DonationSuggestions(res.totalMoMoMoney, 1);
    //     res.CurrentMoney = 20000;
    //   } else if (res.totalCashBackMoney > 1000) {
    //     res.TotalMoneyFormat = "";
    //     res.MoneySource = 6;
    //     res.CurrentMoney = 0;

    //     DonationSuggestions(res.totalCashBackMoney, 6);
    //   }
    //   res.Anonymous = false;

    //   setdonaModel({
    //     ...res,
    //     Open: true
    //   });

    //   f7.preloader.hide();


    // });
  };

  const SubmitTrans = () => {

    if (donaModel.TotalMoneyFormat != "") {

      if (donaModel.CurrentMoney < 1000) {
        donaModel.ErrorMes = "Số tiền tối thiểu là 1.000đ.";
        setdonaModel({
          ...donaModel,
        });
        return;
      }

      if (donaModel.MoneySource == 1) {
        if (donaModel.CurrentMoney > 20000000) {
          donaModel.ErrorMes = "Số tiền tối đa là 20.000.000đ";
          setdonaModel({
            ...donaModel,
          });
          return;
        }
        if (donaModel.CurrentMoney > donaModel.totalMoMoMoney) {
          var dialog = f7.dialog
            .create({
              title: "",
              text: "",
              closeByBackdropClick: true,
              cssClass: "pron-popup-container",
              content:
                '<div class="pron-popup">' +
                ' <div class="pron-popup-close" ><img src="https://static.mservice.io/pwa/images/donation/popup-close.svg"/></div> ' +
                '  <div class="pron-popup-content text-left">' +
                '     <h3 class="mt-0 mb-4 lutie-22 " >Số dư trong ví không đủ</h3> ' +
                "     <div >Số tiền trong Ví MoMo không đủ. Vui lòng nạp thêm để thực hiện giao dịch.</div> " +
                "   </div>" +
                "</div>",
              buttons: [
                {
                  text: "Nạp tiền vào Ví",
                  onClick: function () {
                    if (ConfigData.IsFromNoti || WebToken) {
                      location.href = "momo://?refId=CashInOutMomo";
                    } else {
                      MaxApi.startFeature("vn.momo.billpay", "", function () {
                        console.log("ass");
                      });
                    }
                  },
                },
              ],

              on: {
                open: function (d) {
                  d.$el.find(".pron-popup-close").on("click", function (e) {
                    f7.dialog.close();
                  });
                },
              },
            })
            .open();

          //$("#TotalMoneyError").text("Số tiền vượt quá số dư Ví MoMo.");
          //$("#TotalMoneyError").show();
          return;
        }
      } else {
        if (donaModel.CurrentMoney > donaModel.totalCashBackMoney) {
          donaModel.ErrorMes = "Số tiền vượt quá số dư Heo đất.";
          setdonaModel({
            ...donaModel,
          });
          return;
        }
      }
      donaModel.ErrorMes = "";
      setdonaModel({
        ...donaModel,
      });

    } else {
      donaModel.ErrorMes = "Vui lòng nhập số tiền muốn quyên góp.";
      setdonaModel({
        ...donaModel,
      });

      return;
    }

    f7.preloader.show();
    var param = {
      donationId: dona.Id,
      totalOrder: donaModel.CurrentMoney,
      moneySource: donaModel.MoneySource,
      anonymous: donaModel.Anonymous,
      reason: donaModel.Message,
      userPhone: ConfigData.UserInfo.phone,
      userName: ConfigData.UserInfo.userName,
      token: props.token,
    };

    CreateTransByToken(param, function (res) {

      f7.preloader.hide();

        location.href = "/xac-nhan-dong-gop/" + res;
    });
  };

  const SuggesionChange = (total) => {
    donaModel.TotalMoneyFormat = FormatNumber(total);
    donaModel.CurrentMoney = total;
    donaModel.ErrorMes = "";

    //$$('#donation-money').trigger("change");
    setdonaModel({
      ...donaModel,
    });


  };

  const MoneySourceChange = (src) => {
    donaModel.MoneySource = src;

    if (src == 1) {
      donaModel.TotalMoneyFormat = FormatNumber(20000);
      DonationSuggestions(donaModel.totalMoMoMoney, src);
      donaModel.CurrentMoney = 20000;
    } else {
      donaModel.TotalMoneyFormat = "";
      donaModel.CurrentMoney = 0;

      DonationSuggestions(donaModel.totalCashBackMoney, src);
    }
    donaModel.ErrorMes = "";

    setdonaModel({
      ...donaModel,
    });


  };

  const InputChange = (inputName, val) => {
    if (inputName == "TotalMoneyFormat") {
      var value = val.replace(/\./g, "").replace(/\-/g, "").replace(/\ /g, "");
      var valMoney = parseInt(value);
      donaModel.TotalMoneyFormat = FormatNumber(valMoney);
      donaModel.CurrentMoney = valMoney;
    }

    if (inputName == "chkAnonymous") {
      donaModel.Anonymous = val;
    }
    if (inputName == "Message") {
      var max = 70;
      if (val.length > max) {
        donaModel.Message = val.substr(0, max);
      } else {
        donaModel.Message = val;
      }
    }

    setdonaModel({
      ...donaModel,
    });


  };

  const DonationSuggestions = (total, src) => {

    ConfigData.ListSuggestions = [];
    if (dona.Id == 172) {
      ConfigData.ListSuggestions.push(20000);
      ConfigData.ListSuggestions.push(50000);
      ConfigData.ListSuggestions.push(100000);
      ConfigData.ListSuggestions.push(200000);
    } else {
      if (src == 1) {
        if (total > 20000) {
          ConfigData.ListSuggestions.push(20000);
        }
        if (total > 50000) {
          ConfigData.ListSuggestions.push(50000);
        }
        if (total > 100000) {
          ConfigData.ListSuggestions.push(100000);
        }
        if (total > 200000) {
          ConfigData.ListSuggestions.push(200000);
        }
        if (total > 500000) {
          ConfigData.ListSuggestions.push(500000);
        }
      } else if (total > 1000) {
        var totalTemp = 0;
        ConfigData.ListSuggestions.push(total);
        totalTemp = Math.ceil(total / 2 / 1000) * 1000;

        if (totalTemp > 1000) {
          ConfigData.ListSuggestions.push(totalTemp);
          totalTemp = Math.ceil(total / 4 / 1000) * 1000;
        }
        if (totalTemp > 1000) {
          ConfigData.ListSuggestions.push(totalTemp);
          totalTemp = Math.ceil(total / 8 / 1000) * 1000;
        }
        if (totalTemp > 1000) {
          ConfigData.ListSuggestions.push(totalTemp);
          ConfigData.ListSuggestions.push(1000);
        } else {
          ConfigData.ListSuggestions.push(1000);
        }
      }
    }

    setConfigData({
      ...ConfigData,
    });
  };
  
  const SheetClose = () => {
    donaModel.TotalMoneyFormat = "";
    donaModel.Message = "";
    donaModel.Open = false;
    donaModel.MoneySource = 1;

    setdonaModel({
      ...donaModel,
    });
  };

  const pageInit = () => {
    f7.preloader.show();
    ListCashBackInfoUserByToken(function (res) {

        if (res.totalMoMoMoney > 1000) {
          res.TotalMoneyFormat = FormatNumber(20000);
          res.MoneySource = 1;
          DonationSuggestions(res.totalMoMoMoney, 1);
          res.CurrentMoney = 20000;
        } else if (res.totalCashBackMoney > 1000) {
          res.TotalMoneyFormat = "";
          res.MoneySource = 6;
          res.CurrentMoney = 0;
  
          DonationSuggestions(res.totalCashBackMoney, 6);
        }
        res.Anonymous = false;
  
        setdonaModel({
          ...res,
          Open: true
        });
  
        f7.preloader.hide();
      });
  };

  const pageOut = () => {
    f7.sheet.close();
  };

  const pageDestroy = () => {
    if (pb.current) pb.current.destroy();
    if (f7.sheet) f7.sheet.destroy();
  };

  return (
    <Page
      name="home"
      onPageInit={pageInit}
      onPageBeforeRemove={pageDestroy}
      onPageBeforeOut={pageOut}
    >
        <Toolbar position="bottom" className=" soju-toolbar ">
              {!Device.ios && (
                <div className="px-1 w-50">
                  <Button large color="gray" back>
                    Quay lại
                  </Button>
                </div>
              )}
              <div className="px-1 w-100">
                <Button large fill className="soju-btn-cta" onClick={SubmitTrans}>
                  Quyên góp
                </Button>
              </div>
        </Toolbar>

        {donaModel && (
          <PageContent>
            <Block className="my-3">
              <div>
                <div className="mt-4">
                  <div className="mb-3 font-weight-bold mt-3">CHỌN NGUỒN TIỀN</div>

                  <div className="apd-source">
                    <div className="apd-source-item ">
                      <input
                        type="radio"
                        name="checkbox"
                        defaultValue="value"
                        id="apd-source-1"
                        defaultChecked={donaModel.MoneySource == 1}
                        onChange={() => MoneySourceChange(1)}
                      />
                      <label
                        htmlFor="apd-source-1"
                        className="apd-source-label"
                      >
                        <div className="apd-source-icon">
                          <img
                            src="/static/images/logo-momo.svg"
                            width={36}
                            alt=""
                          />
                        </div>
                        <div className="font-weight-bold lutie-14">Ví MoMo</div>
                        <div className="lutie-12 text-gray">
                          {FormatNumber(donaModel.totalMoMoMoney)}đ
                        </div>
                      </label>
                    </div>
                    <div className="apd-source-item ">
                      <input
                        type="radio"
                        name="checkbox"
                        defaultValue="value"
                        id="apd-source-2"
                        defaultChecked={donaModel.MoneySource == 6}
                        onChange={() => MoneySourceChange(6)}
                      />
                      <label
                        htmlFor="apd-source-2"
                        className="apd-source-label"
                      >
                        <div className="apd-source-icon">
                          <img
                            src="/static/images/heoicon.jpg"
                            width={38}
                            alt=""
                          />
                        </div>
                        <div className="font-weight-bold lutie-14 ">
                          Heo đất
                        </div>
                        <div className="lutie-12 text-gray">
                          {FormatNumber(donaModel.totalCashBackMoney)}đ
                        </div>
                      </label>
                    </div>
                  </div>
                </div>

                <Row
                  noGap
                  className="align-items-center py-3 mt-4 border-top border-bottom "
                >
                  <Col width="66">
                    <div className="py-1 font-weight-bold">QUYÊN GÓP ẨN DANH</div>
                  </Col>
                  <Col width="33">
                    <div className="toggle--small text-right">
                      <Toggle
                        color="green"
                        id="chkAnonymous"
                        name="chkAnonymous"
                        onChange={(x) =>
                          InputChange("chkAnonymous", x.target.checked)
                        }
                      />
                    </div>
                  </Col>
                </Row>

                <div className="mt-4 ">
                  {donaModel.TotalMoneyFormat != undefined && (
                    <div className="text-uppercase lutie-15 font-weight-bold">
                      {donaModel.TotalMoneyFormat == "" || donaModel.TotalMoneyFormat.length > 0
                        ? "SỐ TIỀN QUYÊN GÓP"
                        : "NHẬP SỐ TIỀN MUỐN QUYÊN GÓP"}
                    </div>
                  )}

                  <div className="list no-hairlines m-0  ">
                    <ul>
                      <div className="list-form list-form-outline  ">
                        <div className="item-inner p-0">
                          <div className="item-input-wrap have-lab mt-1">
                            <input
                              inputMode={!Device.ios ? "numeric" : ""}
                              pattern="[0-9]*"
                              type="text"
                              autoComplete="off"
                              id="donation-money"
                              value={donaModel.TotalMoneyFormat || ''}
                              onChange={(x) =>
                                InputChange("TotalMoneyFormat", x.target.value)
                              }
                            />
                            <div className="item-input-lab">đ</div>
                            <div
                              className={
                                donaModel.ErrorMes != ""
                                  ? "item-input-error-message d-block pl-0 mt-2"
                                  : "item-input-error-message d-none pl-0 mt-2"
                              }
                              id="TotalMoneyError"
                            >
                              {donaModel.ErrorMes}
                            </div>
                            <span
                              className="input-clear-button"
                              onClick={() =>
                                InputChange("TotalMoneyFormat", "0")
                              }
                            />
                          </div>
                        </div>
                      </div>
                    </ul>
                  </div>

                  <div className="apd-payment mb-2 mt-3 hide-scrollbar">
                    {ConfigData.ListSuggestions &&
                      ConfigData.ListSuggestions.map((item, index) => (
                        <div key={index} className="apd-payment-item">
                          <input
                            className="apd-payment-check"
                            type="radio"
                            id={`dnsg${item}`}
                            name="payDonation"
                            value={item || 0}
                            checked={donaModel.CurrentMoney == item}
                            onChange={() => SuggesionChange(item)}
                          />
                          <label
                            htmlFor={`dnsg${item}`}
                            className="apd-payment-label"
                          >
                            <span className="apd-payment-mo">
                              {FormatNumber(item)}₫
                            </span>
                          </label>
                        </div>
                      ))}
                  </div>
                </div>

                <div className="mt-4 ">
                  <div className="text-uppercase lutie-15 font-weight-bold">
                    Lời nhắn
                    {donaModel.Message && donaModel.Message.length > 0
                      ? `(${donaModel.Message.length}/70)`
                      : ""}
                  </div>
                  <div className="position-relative">
                    <textarea
                      name="Message"
                      id="donation-content"
                      maxLength="70"
                      className="resizable soju-form"
                      placeholder="Nhập lời nhắn"
                      value={donaModel.Message || ''}
                      onChange={(x) => InputChange("Message", x.target.value)}
                      style={{ paddingRight: "30px" }}
                    ></textarea>
                    <span
                      className="input-clear-button" style={{ right: "10px" }}
                      onClick={() => InputChange("Message", "")}
                    />
                  </div>
                </div>

                {/* <div className="apd-payment apd-payment-message mb-3 mt-3 hide-scrollbar">
                <div className="apd-payment-item">
                  <input
                    className="apd-payment-check"
                    type="radio"
                    id="apd21"
                    name="payDonation"
                    defaultValue={1}
                    defaultChecked
                  />
                  <label htmlFor="apd21" className="apd-payment-label">
                    <span className="apd-payment-mo">
                      Cảm ơn vì đã là lá chắn vững vàng cho quyết thắng đại dịch
                      ₫
                    </span>
                  </label>
                </div>
                <div className="apd-payment-item">
                  <input
                    className="apd-payment-check"
                    type="radio"
                    id="apd22"
                    name="payDonation"
                    defaultValue={2}
                  />
                  <label htmlFor="apd22" className="apd-payment-label">
                    <span className="apd-payment-mo">Việt Nam Vô Địch! ₫</span>
                  </label>
                </div>
                <div className="apd-payment-item">
                  <input
                    className="apd-payment-check"
                    type="radio"
                    id="apd23"
                    name="payDonation"
                    defaultValue={3}
                  />
                  <label htmlFor="apd23" className="apd-payment-label">
                    <span className="apd-payment-mo">
                      Quyết thắng đại dịch ₫
                    </span>
                  </label>
                </div>
                <div className="apd-payment-item">
                  <input
                    className="apd-payment-check"
                    type="radio"
                    id="apd24"
                    name="payDonation"
                    defaultValue={4}
                  />
                  <label htmlFor="apd24" className="apd-payment-label">
                    <span className="apd-payment-mo">
                      Quyết thắng đại dịch ₫
                    </span>
                  </label>
                </div>
                <div className="apd-payment-item">
                  <input
                    className="apd-payment-check"
                    type="radio"
                    id="apd25"
                    name="payDonation"
                    defaultValue={5}
                  />
                  <label htmlFor="apd25" className="apd-payment-label">
                    <span className="apd-payment-mo">
                      Quyết thắng đại dịch ₫
                    </span>
                  </label>
                </div>
                <div className="apd-payment-item">
                  <input
                    className="apd-payment-check"
                    type="radio"
                    id="apd26"
                    name="payDonation"
                    defaultValue={6}
                  />
                  <label htmlFor="apd26" className="apd-payment-label">
                    <span className="apd-payment-mo">
                      Quyết thắng đại dịch ₫
                    </span>
                  </label>
                </div>
              </div> */}
              </div>
            </Block>
          </PageContent>
        )}

    </Page>
  );
};

export default MoneyChoice;

import React, { useState, useEffect, Fragment } from "react";
import { ApiResponseErrorCodeMessage, System, ValidateAccessToken, FormatNumber } from "@/libs/constants";

import {
  f7,
  Page,
  PageContent,
  Navbar,
  NavLeft,
  NavTitle,
  NavTitleLarge,
  NavRight,
  Link,
  Toolbar,
  Sheet,
  Block,
  Preloader,
  BlockTitle,
  List,
  ListItem,
  Tabs,
  Tab,
  Row,
  Col,
  Button,
} from "framework7-react";

// import Framework7Keypad from 'framework7-plugin-keypad';

// Import App Component
// import UserInfo from "../components/donation/user-info.jsx";
import axios from "@/libs/axios";
import Framework7 from "framework7";

const XacNhan = (props) => {
  const [sheetOpened, setSheetOpened] = useState(false);
  const [transData, settransData] = useState(null);
  const [transResult, settransResult] = useState({
    FailCountMax: 3,
    FailCountLeft: 3,
    FailCount: 0,
    ErrorCode: ''
  });
  const [isSubmit, setIsSubmit] = useState(false);

  const getTransactionInfo = (fnc) => {
    var url = `${System.apihost}/__get/Donation/GetTrans?token=${props.token}`
    axios.get(url).then((res) => {
      if (!res.Result) {
        f7.preloader.hide();
        if (res.ErrorCode === 'token_error') {
          location.href = '/404';
        } else {
          f7.dialog.alert(ApiResponseErrorCodeMessage[res.ErrorCode]);
        }
        return;
      }
      fnc && fnc(res.Data);
    });
  };


  const SubmitTrans = (passCode) => {
    if (isSubmit)
      return;
    f7.preloader.show();
    var param = {
      token: props.token,
      passCode: passCode
    };
    var url = `${System.apihost}/__post/Donation/SubmitTransV2`;
    axios.post(url, {
      data: param
    }).then((res) => {
      if (res.Result !== true) {
        setIsSubmit(false);
        f7.preloader.hide();
        if (res.ErrorCode === 'donate_error_1014') {
          transResult.ErrorCode = res.ErrorCode;
          transResult.FailCountLeft = transResult.FailCountMax - res.Data.FailCount;
          transResult.FailCount = res.Data.FailCount;

          settransResult({
            ...transResult
          });

          numpadInline.setValue('');
          if (transResult.FailCountLeft === 0) {
            f7.sheet.close('.sheetKeypad');
            location.href = "/dong-gop-that-bai/" + props.token;
            return;
          }
        }
        else {
          f7.sheet.close('.sheetKeypad');
          location.href = "/dong-gop-that-bai/" + props.token;
          return;
        }
        return;
      }
      f7.sheet.close('.sheetKeypad');
      f7.preloader.hide();
      location.href = "/dong-gop-thanh-cong/" + props.token;
    });
  };

  function pageInit() {
    getTransactionInfo(function (res) {
      settransData(res);
      f7.preloader.hide();
    });
  }

  function pageDestroy() {
    if (pb.current) pb.current.destroy();
    if (f7.sheet) f7.sheet.destroy();
  }

  function momoPassSym(value) {
    var sym = Array.from(document.querySelectorAll(".momo-pass-sym .sym"));
    sym.forEach((item) => item.classList.remove("is-active"));
    sym.forEach(function (item, index) {
      if (index <= value - 1) {
        item.classList.add("is-active");
      }
    });
  }

  var numpadInline = null;

  function initKeyboardF7() {

    // if (numpadInline  != null) return;
    numpadInline = f7.keypad.create({
      inputEl: "#dona-numpad-inline",
      containerEl: "#dona-inline-container",
      toolbar: false,
      valueMaxLength: 6,
      dotButton: false,
      formatValue: function (value) {
        value = value.toString();
        momoPassSym(value.length);
        return (
          "******".substring(0, value.length) +
          "______".substring(0, 6 - value.length)
        );
      },
      on: {
        change(keypad, value) {
          console.log(keypad, value);


          value = value.toString();
          if (value.length === 6) {
            setIsSubmit(true);
            SubmitTrans(value);
          }
        },
      },
    });

  }

  function removeKeyboardF7() {
    numpadInline.$el[0].remove();
    numpadInline.destroy();

  }

  return (
    <Page
      name=""
      className="bg-light"
      onPageInit={pageInit}
      onPageBeforeRemove={pageDestroy}
    >
      {/* Top Navbar */}

      <Navbar className="momo-navbar">
        <NavLeft>
          <div className="logo-momo link back ml-2">
            <img
              src="https://static.mservice.io/img/logo-momo.png"
              className="img-fluid"
              width={40}
              alt=""
            />
          </div>
        </NavLeft>
        <NavTitle sliding>Xác nhận thanh toán</NavTitle>
        <NavRight>&nbsp;</NavRight>
      </Navbar>

      <Toolbar bottom position="bottom" className=" soju-toolbar ">
        <div className="px-1 w-100">
          <Button
            large
            fill
            className="font-weight-bold"
            color="pink"
            sheetOpen=".sheetKeypad"
          >
            <i className="f7-icons lutie-18">lock_fill</i> Xác nhận
          </Button>
        </div>
      </Toolbar>
      {transData && (
        <Fragment>
          <div className="block-title">NGUỒN TIỀN</div>
          <div className="block block-strong ">
            <div className="pron-row no-gutters align-items-center">
              <div className="pron-col auto">
                <div className="dona-momo-icon mr-4">
                  {transData.MoneySource == 1 ? (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      className="svg-inline svg"
                      viewBox="0 0 29.29 27.32"
                      fill="#fff"
                    >
                      <g data-name="Layer 2">
                        <g data-name="Layer 1">
                          <path d="M22.94 12.68a6.34 6.34 0 1 0-6.35-6.34 6.34 6.34 0 0 0 6.35 6.34zm0-9.1a2.76 2.76 0 1 1-2.77 2.76 2.77 2.77 0 0 1 2.77-2.76zM10.57 0a5.1 5.1 0 0 0-2.88.89A5.1 5.1 0 0 0 4.81 0 4.65 4.65 0 0 0 0 4.46v8.22h3.86V4.46a.93.93 0 0 1 .95-.88.93.93 0 0 1 1 .84v8.22h3.82V4.46a1 1 0 0 1 1.26-.63 1 1 0 0 1 .64.63v8.22h3.86V4.46A4.65 4.65 0 0 0 10.57 0zM22.94 27.32A6.35 6.35 0 1 0 16.59 21a6.32 6.32 0 0 0 6.34 6.32zm0-9.1a2.75 2.75 0 1 1-.05 0zM10.57 14.64a5.1 5.1 0 0 0-2.88.89 5.1 5.1 0 0 0-2.88-.89A4.65 4.65 0 0 0 0 19.1v8.22h3.86V19.1a.93.93 0 0 1 .95-.88.93.93 0 0 1 1 .84v8.22h3.82V19.1a1 1 0 0 1 1.26-.63 1 1 0 0 1 .64.63v8.22h3.86V19.1a4.65 4.65 0 0 0-4.82-4.46z" />
                        </g>
                      </g>
                    </svg>
                  ) : (
                      <img src="https://static.mservice.io/pwa/images/donation/heoicon.jpg" width="38" alt=""></img>
                    )}

                </div>
              </div>
              {transData.MoneySource == 1 ? (
                <div className="pron-col">
                  <div className="lutie-16 font-weight-normal mb-1">Ví MoMo</div>
                  <div className="opacity-5 lutie-12">{FormatNumber(transData.TotalMomoMoney)}đ</div>
                </div>
              ) : (
                  <div className="pron-col">
                    <div className="lutie-16 font-weight-normal mb-1">Heo đất</div>
                    <div className="opacity-5 lutie-12">{FormatNumber(transData.TotalCashBackMoney)}đ</div>
                  </div>
                )}
            </div>
          </div>
          <div className="block-title">Chi tiết giao dịch</div>
          <div className="list list-style1">
            <ul>
              <li>
                <div className="item-content">
                  <div className="item-inner">
                    <div className="item-title">Tên chương trình</div>
                    <div className="item-after">{transData.Description}</div>
                  </div>
                </div>
              </li>
              <li>
                <div className="item-content">
                  <div className="item-inner">
                    <div className="item-title">Số tiền</div>
                    <div className="item-after">{FormatNumber(transData.TotalOrder)}đ</div>
                  </div>
                </div>
              </li>
              <li>
                <div className="item-content">
                  <div className="item-inner">
                    <div className="item-title">Phí giao dịch</div>
                    <div className="item-after"> Miễn phí</div>
                  </div>
                </div>
              </li>
              <li>
                <div className="item-content">
                  <div className="item-inner">
                    <div className="item-title">Tổng tiền</div>
                    <div className="item-after">
                      <strong>{FormatNumber(transData.TotalOrder)}đ</strong>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </div>
          <div className="block">


            <div className="pron-row no-gutters">
              <div className="pron-col auto">
                <img
                  src="/static/images/dv-image-seal.png"
                  width={80}
                  className="d-block mr-4"
                  alt=""
                />
              </div>
              <div className="pron-col">
                <div className="lutie-12 text-gray">
                  Bảo mật chuẩn SSL/TLS, mọi thông tin giao dịch đều được mã hoá an
                  toàn.
            </div>
              </div>
            </div>
          </div>
        </Fragment>
      )}
      {/* Sheet Heovang */}
      <Sheet
        className="sheetKeypad soju-sheet"
        opened={sheetOpened}
        style={{ height: "auto", "--f7-sheet-bg-color": "#fff" }}
        backdrop
        onSheetClosed={() => {
          setSheetOpened(false);
          removeKeyboardF7();
        }}
        onSheetOpened={() => {
          initKeyboardF7();
        }}
      >
        <Navbar className="soju-navbar-sheet">
          <NavLeft> &nbsp;</NavLeft>

          <NavTitle sliding>Nhập mật khẩu</NavTitle>

          <NavRight>
            <Link sheetClose iconIos="f7:multiply" />
          </NavRight>
        </Navbar>

        <form>
          <div className="block passcode-input d-none">
            <input type="text" defaultValue="____" id="dona-numpad-inline" />
          </div>
          <div className="momo-pass">
            <div className="momo-pass-sym">
              <div className="sym" />
              <div className="sym" />
              <div className="sym" />
              <div className="sym" />
              <div className="sym" />
              <div className="sym" />
            </div>
            {transResult.ErrorCode && (
               <div className="momo-pass-error text-danger">
                  Giao dịch thất bại vì mật khẩu không chính xác. Vui lòng thử lại.
                  <br/>(Bạn còn {transResult.FailCountLeft} lần thử)
              </div>
            )}
           
          </div>
          <div className="block block-strong no-padding no-margin passcode-numpad mt-4">
            <div id="dona-inline-container" />
          </div>
        </form>
      </Sheet>

      <style jsx>{`
        .block-title {
          font-size: 14px;
          text-transform: uppercase;
          font-weight: normal;
          color: var(--gray-500);
        }
        #dona-inline-container{
          min-height: 200px;
        }
      `}</style>
    </Page>
  );
};

export default XacNhan;

import React, { useState, useEffect, Fragment } from "react";
import {
  f7,
  Page,
  PageContent,
  Navbar,
  NavLeft,
  NavTitle,
  NavTitleLarge,
  NavRight,
  Link,
  Toolbar,
  Sheet,
  Block,
  Preloader,
  BlockTitle,
  List,
  ListItem,
  Tabs,
  Tab,
  Row,
  Col,
  Button,
} from "framework7-react";

// import Framework7Keypad from 'framework7-plugin-keypad';

// Import App Component
// import UserInfo from "../components/donation/user-info.jsx";
import axios from "@/libs/axios";
import Framework7 from "framework7";
import { ApiResponseErrorCodeMessage, System, ValidateAccessToken, FormatNumber } from "@/libs/constants";

const XacNhanThatBai = (props) => {
  const [transData, settransData] = useState(null);

  const getTransactionInfo = (fnc) => {
    var url = `${System.apihost}/__get/Donation/GetTransStatus?status=4&token=${props.token}`
    axios.get(url).then((res) => {
      if (!res.Result) {
        f7.preloader.hide();
        if (res.ErrorCode === 'token_error') {
          location.href = '/404';
        } else {
          f7.dialog.alert(ApiResponseErrorCodeMessage[res.ErrorCode]);
        }
        return;
      }
      fnc && fnc(res.Data);
    });
  };

  function pageInit() {
    f7.preloader.show();
    getTransactionInfo(function (res) {
      settransData(res);
      f7.preloader.hide();
    });

  }

  function pageDestroy() { }

  return (
    <Page
      name=""
      className="bg-light"
      onPageInit={pageInit}
      onPageBeforeRemove={pageDestroy}
    >


      <Toolbar bottom position="bottom" className=" soju-toolbar ">
        <div className="px-1 w-100">
          <Button
            external
            large
            fill
            className="font-weight-bold"
            color="pink"
            href="momo://?refId=TAB0"
          >
            Đóng
          </Button>
        </div>
      </Toolbar>
      {transData && (
        <Fragment>
          <div className="dona-status ">
            <div className="container">
              <div className="dona-status-icon">
                <img
                  src="/static/images/donation/warning.svg"
                  width={55}
                  className="img-fluid mx-auto d-block"
                  alt=""
                />
              </div>
              <div className="dona-status-content text-center">
                <div className="lutie-16 mb-0"> Giao dịch không thành công !</div>
              </div>
            </div>
          </div>



          <div className="block block-strong mt-0">
            <div className="text-center">
              <div className="lutie-16 mb-0 lh-reset">
                {transData.Description}
              </div>
            </div>
          </div>

        </Fragment>
      )}
      <style jsx>{`
        .block-title {
          font-size: 14px;
          text-transform: uppercase;
          font-weight: normal;
          color: var(--gray-500);
        }
        #dona-inline-container {
          min-height: 200px;
        }
      `}</style>
    </Page>
  );
};

export default XacNhanThatBai;

import React, { useState, useEffect, Fragment, useRef } from "react";
import {
  f7,
  Page,
  PageContent,
  Navbar,
  NavLeft,
  NavTitle,
  NavTitleLarge,
  NavRight,
  Link,
  Toolbar,
  Sheet,
  Block,
  Preloader,
  BlockTitle,
  List,
  ListItem,
  Tabs,
  Tab,
  Row,
  Col,
  Button,
} from "framework7-react";

import axios from "@/libs/axios";
import Framework7 from "framework7";
import { ApiResponseErrorCodeMessage, System, ValidateAccessToken, FormatNumber } from "@/libs/constants";

import ShareBtnSheet from "@/components/share-btn-sheet";

const XacNhanThanhCong = (props) => {
  const [transData, settransData] = useState(null);
  const refSheetShare = useRef();
  const getTransactionInfo = (fnc) => {
    var url = `${System.apihost}/__get/Donation/GetTransStatus?status=3&token=${props.token}`
    axios.get(url).then((res) => {
      if (!res.Result) {
        f7.preloader.hide();
        if (res.ErrorCode === 'token_error') {
          location.href = '/404';
        } else {
          f7.dialog.alert(ApiResponseErrorCodeMessage[res.ErrorCode]);
        }
        return;
      }
      fnc && fnc(res.Data);
    });
  };

  function pageInit() {
    f7.preloader.show();
    getTransactionInfo(function (res) {
      settransData(res);


      var donationThanksTemplate = '';

      if (res.AvatarFacebookUrl != 'https://static.mservice.io/default/s/no-image.png') {
        var btnShare = '  <div class="px-2 text-center">' +
          '   <div class="py-2"> ' +
          '       <a href="javascript:void(0);" id="BtnSharePopup" class="button button-fill color-pink py-1 h-auto">CHIA SẺ NGAY</a>' +
          '   </div>' +
          '   </div>';

        if (res.ShareUrl == null) {
          btnShare = "";
        }

        donationThanksTemplate = '<div class="pron-popup">' +
          ' <div class="pron-popup-close" ><img src="https://static.mservice.io/pwa/images/donation/bigclose.png" width="38" /></div> ' +
          '   <div class="pron-popup-image">' +
          '     <img src="' + res.AvatarFacebookUrl + '" class="img-fluid">' +
          '   </div>' +
          btnShare +
          '</div>';
      }
      else {
        var donationThanksInfo = '<div class="dona-thanks-info pb-4">' +

          '        <div class="dona-thanks-col">' +
          '          <div class="dona-thanks-val">' +
          res.TotalSuccess +
          '            <img src="https://static.mservice.io/pwa/images/donation/thanks-heart.svg" width="20" alt="">' +
          '          </div>' +
          '          <div class="dona-thanks-la">' +
          '            Lượt quyên góp' +
          '          </div>' +
          '        </div>' +
          '      </div>';
        var btnShare = '  <div class="px-2 text-center">' +
          '   <div class="py-2"> ' +
          '       <a href="javascript:void(0);" id="BtnSharePopup" class="button button-fill color-pink py-1 h-auto">CHIA SẺ NGAY</a>' +
          '   </div>' +
          '   </div>'
        if (res.ShareUrl == null) {
          btnShare = "";
        }

        donationThanksTemplate = '<div class="pron-popup">' +
          ' <div class="pron-popup-close" ><img src="https://static.mservice.io/pwa/images/donation/popup-close.svg"/></div> ' +
          '   <div class="pron-popup-image" style="position: relative;">' +
          '     <img src="' + res.Avatar + '" class="img-fluid">' +
          '     <div style="background-image: url(https://static.mservice.io/pwa/images/tim.png);position: absolute;width: 100%;height: 100%;bottom: 0;left: 0;background-size: 100% auto;background-repeat: no-repeat;background-position: center bottom;"></div>' +
          '   </div>' +
          '  <div class="pron-popup-content text-center">' +
          '     <h3 class="mt-0 mb-2 lutie-18 " >Cảm ơn ' + res.UserName + ' </h3> ' +
          '     <div >Điều kỳ diệu này sẽ không thành hiện thực nếu thiếu đi sự giúp sức của bạn. Hãy tiếp tục đồng hành cùng nhau trên hành trình lan toả yêu thương nhé!</div> '
          + donationThanksInfo +
          '   </div>' +
          btnShare +
          '</div>';
      }

      f7.dialog.create({
        title: '',
        closeByBackdropClick: true,
        text: '',
        content: donationThanksTemplate,
        cssClass: "pron-popup-container",
        on: {
          open: function (d) {
            d.$el.find(".pron-popup-close").on('click', function (e) {
              f7.dialog.close();
            });

            d.$el.find("#BtnSharePopup").on('click', function (e) {
              f7.dialog.close();
              BtnShare(f7);
            });
          }
        }
      }).open();

      f7.preloader.hide();
    });
  }
  const BtnShare = () => {
    refSheetShare.current.Show();
  }
  function pageDestroy() { }

  return (
    <Page
      name=""
      className="bg-light"
      onPageInit={pageInit}
      onPageBeforeRemove={pageDestroy}
    >

      <ShareBtnSheet data={transData} ref={refSheetShare} />
      <Toolbar bottom position="bottom" className=" soju-toolbar ">
        <div className="px-1 w-100">
          <Button
            external
            large
            fill
            className="font-weight-bold"
            color="pink"
            href="momo://?refId=TAB0"
          >
            Hoàn tất
          </Button>
        </div>
      </Toolbar>
      {transData && (
        <Fragment>
          <div className="dona-status ">
            <div className="container">
              <div className="dona-status-icon">
                <img
                  src="/static/images/donation/success.svg"
                  width={55}
                  className="img-fluid mx-auto d-block"
                  alt=""
                />
              </div>
              <div className="dona-status-content text-center">
                <div className="lutie-16 mb-0"> Cảm ơn tấm lòng cao đẹp của bạn!</div>
              </div>
            </div>
          </div>

          <div className="block block-strong mt-0">
            <div className="text-center">
              <div className="lutie-16 mb-0 lh-reset">
                Hành động tương thân tương ái của bạn là món quà vô cùng ý nghĩa và thiết thực để chung tay xây dựng Việt Nam thêm vững mạnh! Hãy tiếp tục lan tỏa yêu thương để kết nối những tấm lòng thiện nguyện!
          </div>
            </div>
          </div>
          <div className="list list-style1">
            <ul>
              <li>
                <div className="item-content">
                  {transData.MoneySource == 1 ? (
                    <div className="item-inner">
                      <div className="item-title">Ví MoMo</div>
                      <div className="item-after">{FormatNumber(transData.TotalMomoMoney)}đ</div>
                    </div>
                  ) : (
                      <div className="item-inner">
                        <div className="item-title">Heo đất</div>
                        <div className="item-after">{FormatNumber(transData.TotalCashBackMoney)}đ</div>
                      </div>
                    )}
                </div>
              </li>
              <li>
                <div className="item-link item-content">
                  <div className="item-inner">
                    <div className="item-title">Số tiền quyên góp</div>
                    <div className="item-after">
                      <span className="color-cyan">{FormatNumber(transData.TotalOrder)}đ</span>{" "}
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </Fragment>
      )}
      <style jsx>{`
        .block-title {
          font-size: 14px;
          text-transform: uppercase;
          font-weight: normal;
          color: var(--gray-500);
        }
        #dona-inline-container {
          min-height: 200px;
        }
      `}</style>
    </Page>
  );
};

export default XacNhanThanhCong;

import React, { Fragment, useState, useEffect, useRef } from "react";

import events from "@/components/events";
import {
  f7,
  Page,
  PageContent,
  Navbar,
  NavLeft,
  NavTitle,
  NavRight,
  Link,
  Toolbar,
  Block,
  Toggle,
  Sheet,
  Row,
  Col,
  Stepper,
  Button,
  useStore,
} from "framework7-react";

import $ from "dom7";

//Lib import
import { ApiResponseErrorCodeMessage, System } from "@/libs/constants";


import axios from "@/libs/axios";
import MaxApi from "@momo-platform/max-api";
import format from "number-format.js";
import ShareBtnSheet from "@/components/share-btn-sheet";
import DonationCaseDetail from "@/components/donation/donation-card-detail.jsx";
import store from "@/js/store";

const DonationDetail = (props) => {
  const refSheetShare = useRef();

  const refSheetStepper = useRef();

  const WebToken = useStore("WebToken");
  const [dona, setDona] = useState({});
  const [donaModel, setdonaModel] = useState({});
  const [ConfigData, setConfigData] = useState({
    UserInfo: { phone: "" },
    IsFromNoti: false,
    FirebaseToken: "",
    ListSuggestions: [],
    ListSuggestionsText: [
      "Cảm ơn những lá chắn vững vàng!",
      "Vững tâm nhé các Chiến sĩ áo trắng.",
      "Chúng tôi sẽ ở nhà!",
      "Chúng ta sẽ thành công!",
    ],
    ListImage: [],
    DonationUser: [],
    DonationRankUserAll: [],
    DonationRankUser: [],
    TotalDonationUser: 0,
    TotalDonationRankUser: 50,
    Page: 0,
    PageRank: 0,
    ItemPerPage: 10,
    ItemPerPageRank: 5,
    IsIos: 0,
    TokenPayment: "",
    FailCountMax: 3,
    FailCountLeft: 3,
    FailCount: 0,
    CloudToken: "123",
    ErrorCode: "",
  });


  useEffect(()=> {
    
  }, [])

  const ListCashBackInfoUser = (fnc) => {
    var url = `${System.apihost}/__get/Donation/ListHeoVangInfoUserV2?userPhone=${ConfigData.UserInfo.phone}`;
    axios
      .get(url)
      .then((res) => {
        if (!res.Result && res.Data == null) {
          f7.preloader.hide();
          if (res.ErrorCode == "session_error") {
            f7.dialog.alert(
              ApiResponseErrorCodeMessage[res.ErrorCode],
              ApiResponseErrorCodeMessage[res.ErrorCode + "_content"]
            );
            return;
          }
          f7.dialog.alert(ApiResponseErrorCodeMessage[res.ErrorCode]);
          return;
        }
        fnc && fnc(res.Data);
      })
      .catch((error) => {
        //alert(error);
        f7.preloader.hide();
        f7.dialog.alert(ApiResponseErrorCodeMessage["system_error"]);

        return;
      });
  };

  const [sheetOpened, setSheetOpened] = useState(false);
  const [sheetOpened2, setSheetOpened2] = useState(false);

  const pb = useRef(null);

  const InputChange = (inputName, val) => {
    if (inputName == "Message") {
      var max = 70;
      if (val.length > max) {
        donaModel.Message = val.substr(0, max);
      } else {
        donaModel.Message = val;
      }
    } else {
      donaModel[inputName] = val;
    }

    setdonaModel({
      ...donaModel,
    });
  };

  const DonationClick = () => {
    if (!ConfigData.UserInfo.phone) return;

    f7.preloader.show();
    ListCashBackInfoUser(function (res) {
      res.TotalMoneyFormat = res.totalCashBackMoney;
      res.Message = "";

      if (res.totalCashBackMoney <= 0) {
        f7.preloader.hide();

        var dialog = f7.dialog
          .create({
            title: "",
            closeByBackdropClick: true,
            text: "",
            content:
              '<div class="pron-popup">' +
              '   <div class="pron-popup-image">' +
              '     <img src="https://static.mservice.io/pwa/images/donation/banner-heo-vang.jpg" class="img-fluid">' +
              "   </div>" +
              '  <div class="pron-popup-content text-left">' +
              '     <h3 class="mt-0 mb-2 lutie-18 " >Bạn chưa có Heo Vàng</h3> ' +
              "     <div >Hãy vào tính năng Heo Đất MoMo và cho heo ăn ngay để thu thập Heo Vàng.</div> " +
              '<div class="py-3 position-relative"> <a href="javascript:void(0);" id="btnNoHeoVang" class="external button button-fill color-green py-1 h-auto">Nuôi heo</a> <img src="https://static.mservice.io/pwa/images/donation/t-i-th-c-n.svg" class="button-coin-icon"/>  </div>' +
              "   </div>" +
              "</div>",
            cssClass: "pron-popup-container",
            on: {
              open: function (ent) {
                ent.$el.find("#btnNoHeoVang").on("click", function (e) {
                  window.ReactNativeWebView.postMessage("FinishActivity");
                });
              },
            },
          })
          .open();

        return;
      }

      if (res.totalCashBackMoney < 1) {
        f7.preloader.hide();
        f7.dialog.alert("Số Heo Vàng của bạn dưới 1.");
        return;
      }

      $(refSheetStepper.current.el.children[1].children[0]).attr({
        "pattern" : "[0-9]*",
        "inputmode" : "numeric"
      });

      setdonaModel({ ...res });

      f7.preloader.hide();
      setSheetOpened(true);
    });
  };

  const SubmitTrans = () => {
    donaModel.ErrorMsg = "";
    if (donaModel.TotalMoneyFormat) {
      donaModel.TotalMoney = donaModel.TotalMoneyFormat; //parseInt(value);
      if (donaModel.TotalMoney < 1) {
        donaModel.ErrorMsg = "Số Heo Vàng tối thiểu là 1.";
        setdonaModel({ ...donaModel });
        return;
      }

      if (donaModel.TotalMoney > donaModel.totalCashBackMoney) {
        donaModel.ErrorMsg =
          "Số Heo Vàng đóng góp vượt quá số Heo Vàng bạn đang có.";
        setdonaModel({ ...donaModel });
        return;
      }
    } else {
      donaModel.ErrorMsg = "Vui lòng nhập số Heo Vàng muốn quyên góp.";
      setdonaModel({ ...donaModel });
      return;
    }

    f7.preloader.show();

    var param = {
      donationId: dona.Id,
      totalOrder: donaModel.TotalMoney,
      anonymous: donaModel.Anonymous,
      reason: donaModel.Message,
      userPhone: ConfigData.UserInfo.phone,
      userName: ConfigData.UserInfo.userName,
    };

    CreateTrans(param, function (res) {
      setSheetOpened(false);

      f7.preloader.hide();

      ConfigData.TokenPayment = res;

      var dialog = f7.dialog
        .create({
          title: "",
          text: "",
          cssClass: "pron-popup-container",
          content:
            '<div class="pron-popup">' +
            '  <div class="pron-popup-content text-left">' +
            '     <h3 class="mt-0 mb-4 lutie-22 " >Xác nhận</h3> ' +
            "     <div >Xác nhận quyên góp " +
            donaModel.TotalMoneyFormat +
            ' Heo Vàng cho chương trình "' +
            dona.Title +
            '" </div> ' +
            "   </div>" +
            "</div>",
          buttons: [
            {
              text: "HỦY",
              close: true,
              color: "gray",
            },
            {
              text: "XÁC NHẬN",
              onClick: function () {
                getTransactionInfo(function (res) {
                  submitTransConfirm(donaModel.TotalMoneyFormat);
                });
              },
            },
          ],

          on: {
            open: function () {
              console.log("OPEN");
            },
          },
        })
        .open();
    });
  };

  function LoadDonationUserPaging() {
    window.isLoadingDonationUser = true;
    ListDonationUser(dona.Id, function (res) {
      ConfigData.TotalDonationUser = res.TotalItems;
      ConfigData.DonationUser = ConfigData.DonationUser.concat(res.Items);
      setConfigData({ ...ConfigData });
      window.isLoadingDonationUser = false;
    });
  }
  function ListDonationUser(donationId, fnc) {
    var url = `${System.apihost}/__get/Donation/ListDonationUser?donationId=${donationId}&page=${ConfigData.Page}&itemPerPage=${ConfigData.ItemPerPage}`;

    axios.get(url).then((res) => {
      if (!res.Result || res.Data == null) {
        f7.dialog.alert(ApiResponseErrorCodeMessage[res.ErrorCode]);
        f7.preloader.hide();
        return;
      }
      fnc && fnc(res.Data);
    });
  }
  function GetDonationDetail(slug, fnc) {
    var url = `${System.apihost}/__get/Donation/GetDonationDetail?slug=${slug}`;
    axios
      .get(url)
      .then((res) => {
        if (!res.Result || res.Data == null) {
          f7.dialog.alert(ApiResponseErrorCodeMessage[res.ErrorCode]);
          f7.preloader.hide();
          return;
        }
        fnc && fnc(res.Data);
      })
      .catch((error) => {
        f7.preloader.hide();
        f7.dialog.alert(ApiResponseErrorCodeMessage["system_error"]);
        return;
      });
  }

  const BindDonationDetail = () => {
    GetDonationDetail(props.f7route.params.slug, (data) => {
      if (data) {
        setDona(data);
        if (data.Type != 2) {
          location.href = "/quyen-gop-heovang";
        }
        var action = props.f7route.hash;
        if (action == "action=openpopup") {
          DonationClick();
        }
      }
    });
  };

  function getTransactionInfo(fnc) {
    var url = `${System.apihost}/__get/Donation/GetTransHeoVang?token=${ConfigData.TokenPayment}`;
    axios.get(url).then((res) => {
      if (!res.Result && res.Data == null) {
        f7.dialog.alert(ApiResponseErrorCodeMessage[res.ErrorCode]);
        f7.preloader.hide();
        return;
      }
      fnc && fnc(res.Data);
    });
  }

  function submitTransConfirm(totalHeo, fnc) {
    if (window._isSubmiting) return;
    window._isSubmiting = true;
    f7.preloader.show();

    var url = `${System.apihost}/__post/Donation/SubmitTransHeoVangNoPassV2`;
    var param = {
      token: ConfigData.TokenPayment,
    };
    axios
      .post(url, {
        data: param,
      })
      .then((res) => {
        if (!res.Result) {
          window._isSubmiting = false;
          f7.preloader.hide();

          if (res.ErrorCode === "donate_error_1014") {
            ConfigData.ErrorCode = res.ErrorCode;
            ConfigData.FailCountLeft =
              ConfigData.FailCountMax - res.Data.FailCount;
            ConfigData.FailCount = res.Data.FailCount;
            setConfigData({ ...ConfigData });
            if (ConfigData.FailCountLeft === 0) {
              var dialog = f7.dialog
                .create({
                  title: "",
                  text: "",
                  cssClass: "pron-popup-container",
                  content:
                    '<div class="pron-popup">' +
                    '  <div class="pron-popup-content text-left">' +
                    "     <div >Giao dịch thất bại vì " +
                    res.Data.Message +
                    "</div> " +
                    "   </div>" +
                    "</div>",
                  buttons: [{ text: "ĐÓNG", close: true }],
                  on: {
                    open: function () {
                      console.log("OPEN");
                    },
                  },
                })
                .open();
              //f7.dialog.alert('Giao dịch thất bại vì ' + res.Data.Message);
              return;
            }
          } else {
            f7.dialog.alert(res.Data.Message);
            return;
          }
          return;
        }
        window._isSubmiting = false;
        f7.preloader.hide();

        var donationThanksTemplate = "";

        if (
          dona.AvatarFacebookUrl !=
          "https://static.mservice.io/default/s/no-image.png"
        ) {
          var btnShare =
            '  <div class="px-2 text-center">' +
            '   <div class="py-2"> ' +
            '       <a href="javascript:void(0);" id="BtnSharePopup" class="button button-fill color-pink py-1 h-auto">CHIA SẺ NGAY</a>' +
            "   </div>" +
            "   </div>";

          if (!dona.ShareUrl) {
            btnShare = "";
          }

          donationThanksTemplate =
            '<div class="pron-popup">' +
            ' <div class="pron-popup-close" ><img src="https://static.mservice.io/pwa/images/donation/bigclose.png" width="38" /></div> ' +
            '   <div class="pron-popup-image">' +
            '     <img src="' +
            dona.AvatarFacebookUrl +
            '" class="img-fluid">' +
            "   </div>" +
            btnShare +
            "</div>";
        } else {
          var donationThanksInfo =
            '<div class="dona-thanks-info">' +
            '        <div class="dona-thanks-col">' +
            '          <div class="dona-thanks-val">' +
            "           <span>+" +
            totalHeo +
            "</span>" +
            '            <img src="https://static.mservice.io/pwa/images/donation/heo-v-ng.svg" width="20" alt="">' +
            "          </div>" +
            '          <div class="dona-thanks-la">' +
            "            Đã quyên góp" +
            "          </div>" +
            "        </div>" +
            '        <div class="dona-thanks-col">' +
            '          <div class="dona-thanks-val">' +
            "            " +
            format(
              "#,###.",
              parseInt(DetailItemModel.DonationItem.TotalTransNumber) + 1
            ) +
            '            <img src="https://static.mservice.io/pwa/images/donation/thanks-heart.svg" width="20" alt="">' +
            "          </div>" +
            '          <div class="dona-thanks-la">' +
            "            Lượt quyên góp" +
            "          </div>" +
            "        </div>" +
            "      </div>";
          //var btnTemplate = '<a href="javascript:void(0);" id="BtnFeed" class="mt-1 button button-fill color-pink py-1 h-auto">Nuôi thêm Heo Vàng</a>';
          var btnTemplate =
            '<a href="javascript:void(0);" id="BtnSharePopupClose" class="mt-1 button button-fill color-gray py-1 h-auto">Đóng</a>';
          if (dona.ShareUrl) {
            btnTemplate =
              '<a href="javascript:void(0);" id="BtnSharePopup" class="button button-fill color-pink py-1 h-auto">Chia sẻ</a> </div>';
          }

          donationThanksTemplate =
            '<div class="pron-popup">' +
            '   <div class="pron-popup-image">' +
            '     <img src="' +
            dona.Avatar +
            '" class="img-fluid">' +
            "   </div>" +
            '  <div class="pron-popup-content text-left">' +
            '     <h3 class="mt-0 mb-2 lutie-18 " >Cảm ơn bạn - Bạn vừa làm một điều tốt</h3> ' +
            "     <div >Heo vàng mà bạn quyên góp sẽ được các nhà tài trợ chuyển thành tiền mặt và gửi tới dự án. Chúng tôi sẽ cập nhật tới bạn khi dự án được hoàn thành. Mong bạn hãy tiếp tục nuôi heo và quyên góp.</div> " +
            donationThanksInfo +
            '<div class="py-3"> ' +
            btnTemplate +
            "   </div>" +
            "</div>";
        }

        var dialog = f7.dialog
          .create({
            title: "",
            closeByBackdropClick: true,
            text: "",
            content: donationThanksTemplate,
            cssClass: "pron-popup-container",
            on: {
              open: function (ent) {
                ent.$el.find("#BtnSharePopup").on("click", function (e) {
                  f7.dialog.close(".pron-popup-container");
                  f7.preloader.show();

                  GetDonationDetail(props.f7route.params.slug, function (res) {
                    setDona({ ...res });
                    f7.preloader.hide();
                  });
                  refSheetShare.current.Show();
                });
                ent.$el.find("#BtnFeed").on("click", function (e) {
                  var objSms = {
                    action: "startService",
                    value: {
                      serviceId: "Cashback",
                      shouldFinishActivity: true,
                    },
                  };
                  window.ReactNativeWebView.postMessage(JSON.stringify(objSms));
                });
                ent.$el.find("#BtnSharePopupClose").on("click", function (e) {
                  f7.dialog.close(".pron-popup-container");
                  f7.preloader.show();
                  GetDonationDetail(props.f7route.params.slug, function (res) {
                    setDona({ ...res });
                    f7.preloader.hide();
                  });
                  ConfigData.DonationUser = [];
                  LoadDonationUserPaging();
                });
                ent.$el.find(".pron-popup-close").on("click", function (e) {
                  f7.dialog.close();
                });
              },
              close: function () {
                f7.preloader.show();
                GetDonationDetail(props.f7route.params.slug, function (res) {
                  setDona({ ...res });
                  f7.preloader.hide();
                });
                ConfigData.DonationUser = [];
                LoadDonationUserPaging();
              },
            },
          })
          .open();

        fnc && fnc(res.Data);
      });
  }

  function CreateTrans(param, fnc) {
    var url = `${System.apihost}/__post/Donation/CreateTransV2`;
    axios
      .post(url, {
        data: param,
      })
      .then((res) => {
        if (!res.Result) {
          f7.preloader.hide();
          if (res.ErrorCode == "session_error") {
            f7.dialog.alert(
              ApiResponseErrorCodeMessage[res.ErrorCode],
              ApiResponseErrorCodeMessage[res.ErrorCode + "_content"],
              function () {
                location.href = "momo://?refId=Cashback";
              }
            );
            return;
          }
          f7.dialog.alert(ApiResponseErrorCodeMessage[res.ErrorCode]);

          return;
        }
        fnc && fnc(res.Data);
      });
  }
  const pageInit = () => {
    events.on("CallbackGetCloudToken", (res) => {
      ConfigData.FirebaseToken = res;
      setConfigData({
        ...ConfigData,
      });
    });

    events.on("initPageData", (res) => {
      res = JSON.parse(res);
      ConfigData.UserInfo = res;
      setConfigData({
        ...ConfigData,
      });

      BindDonationDetail();
    });

    //Device.ios
    if (WebToken || props.f7route.query.webToken) {
      //Di truc tiep
      store.dispatch("WebToken", props.f7route.query.webToken || WebToken);
      if (props.f7route.query.webToken) {
        ConfigData.IsFromNoti = true;
        setConfigData({
          ...ConfigData,
        });
      }
      try {
        window.ReactNativeWebView.postMessage("GetUserInfo");
        window.ReactNativeWebView.postMessage(
          JSON.stringify({ action: "getCloudToken" })
        );
      } catch (e) {}

      //ValidateAccessToken(props.f7route.query.webToken, function (res) {
      // setConfigData({
      //   ...ConfigData,
      //   IsFromNoti: true,
      //   UserInfo: {
      //     phone: "0772532512",
      //     userName: "MarTech Test",
      //     balance: 300000,
      //     appVer: 21422,
      //     appCode: "2.1.42",
      //     deviceOS: "IOS",
      //   },
      //   // UserInfo: { phone: "0789654123", userName: "MarTech Test", balance: 300000, appVer: 21422, appCode: "2.1.42", deviceOS: "IOS" }
      // });
      //});
    } else {
      //di bang Miniapp
      MaxApi.getProfile(function (res) {
        ConfigData.UserInfo = {
          phone: res.user,
          userName: res.userName,
        };
        MaxApi.getDeviceInfo(function (res) {
          ConfigData.FirebaseToken = res.firebaseToken;
          setConfigData({ ...ConfigData });

          BindDonationDetail();
        });
      });
    }
  };

  function pageOut() {
    f7.sheet.close();
  }

  function pageDestroy() {
    if (pb.current) pb.current.destroy();
    if (f7.sheet) f7.sheet.destroy();
  }

  return (
    <Page
      name="home"
      onPageInit={pageInit}
      onPageBeforeRemove={pageDestroy}
      onPageBeforeOut={pageOut}
    >
      {/* Top Navbar */}
      <ShareBtnSheet data={dona} ref={refSheetShare} />

      {dona.Id &&
        ((!dona.IsEndCampaign &&
          !(
            dona.ExpectedValue != null && dona.TotalOrder >= dona.ExpectedValue
          )) ||
          !ConfigData.IsFromNoti ||
          dona.IsContDonate) && (
          <Toolbar position="bottom" className=" soju-toolbar ">
            {!dona.IsEndCampaign &&
            (!(
              dona.ExpectedValue != null &&
              dona.TotalOrder >= dona.ExpectedValue
            ) ||
              dona.IsContDonate) ? (
              <>
                {!ConfigData.IsFromNoti && (
                  <div className="px-1 w-50">
                    <Button large color="gray" back>
                      Quay lại
                    </Button>
                  </div>
                )}
                <div className="px-1 w-100">
                  <Button
                    large
                    fill
                    className="soju-btn-cta"
                    onClick={DonationClick}
                  >
                    Quyên góp
                  </Button>
                </div>
              </>
            ) : (
              <div className="px-1 w-100">
                <Button large color="gray" back>
                  Quay lại
                </Button>
              </div>
            )}
          </Toolbar>
        )}

      <DonationCaseDetail dona={dona} ConfigData={ConfigData} />

      {/* Sheet Heovang */}
      <Sheet
        className="sheetMoney soju-sheet"
        opened={sheetOpened}
        style={{ height: "450px", "--f7-sheet-bg-color": "#fff" }}
        backdrop
        onSheetClosed={() => {
          setSheetOpened(false);
        }}
      >
        <Navbar className="soju-navbar-sheet">
          <NavLeft> &nbsp;</NavLeft>

          <NavTitle sliding> Quyên góp Heo Vàng </NavTitle>

          <NavRight>
            <Link sheetClose iconIos="f7:multiply" />
          </NavRight>
        </Navbar>

        <Toolbar position="bottom" className=" soju-toolbar ">
          {/* <div className="px-1 w-50">
            <Button large color="gray" backLink>
              Quay lại
            </Button>
          </div> */}
          <div className="px-1 w-100">
            <Button
              large
              fill
              className="soju-btn-cta"
              onClick={() => SubmitTrans()}
            >
              Quyên góp
            </Button>
          </div>
        </Toolbar>
        {donaModel && (
          <PageContent>
            <Block>
              <div>
                <div className="lutie-16 text-center mb-3">
                  Bạn có{" "}
                  <div className="pig-coin">
                    <img
                      src="https://static.mservice.io/pwa/images/donation/pig-coin-new.svg"
                      width={28}
                      alt=""
                    />
                    <span className="pig-coin-num">
                      {donaModel.totalCashBackMoney}
                    </span>
                  </div>
                </div>

                <div className="text-center text-gray">
                  Chọn số lượng Heo muốn quyên góp:
                </div>

                <div className="text-center mb-3 mt-4">
                  <div className="dona-stepper">
                    <Stepper
                      large
                      max={donaModel.totalCashBackMoney}
                      value={donaModel.TotalMoneyFormat}
                      onStepperChange={(x) =>
                        InputChange("TotalMoneyFormat", x)
                      }
                      ref={refSheetStepper}
                    >
                    </Stepper>
                  </div>
                  {donaModel.ErrorMsg && (
                    <div
                      style={{ display: "block" }}
                      className="mt-2 item-input-error-message"
                    >
                      {donaModel.ErrorMsg}
                    </div>
                  )}
                </div>

                <Row
                  noGap
                  className="align-items-center py-3 mt-5 border-top border-bottom "
                >
                  <Col width="66">
                    <div className="">Quyên góp ẩn danh</div>
                  </Col>
                  <Col width="33">
                    <div className="toggle--small text-right">
                      <Toggle
                        color="green"
                        onChange={(x) =>
                          InputChange("Anonymous", x.target.checked)
                        }
                      />
                    </div>
                  </Col>
                </Row>

                <div className="mt-4">
                  <div className="mb-3">
                    Lời nhắn
                    {donaModel.Message && donaModel.Message.length > 0
                      ? `(${donaModel.Message.length}/70)`
                      : ""}
                  </div>
                  <div className="position-relative">
                    <textarea
                      name="Message"
                      id="donation-content"
                      maxLength="70"
                      className="resizable soju-form"
                      placeholder="Nhập lời nhắn"
                      value={donaModel.Message}
                      onChange={(x) => InputChange("Message", x.target.value)}
                    ></textarea>
                    <span
                      className="input-clear-button"
                      style={{ right: "10px" }}
                      onClick={() => InputChange("Message", "")}
                    />
                  </div>
                </div>
              </div>
            </Block>
          </PageContent>
        )}
      </Sheet>

      {/* Sheet Tien */}
      <Sheet
        className="sheetMoney2 soju-sheet"
        opened={sheetOpened2}
        style={{ height: "450px", "--f7-sheet-bg-color": "#fff" }}
        backdrop
        onSheetClosed={() => {
          setSheetOpened2(false);
        }}
      >
        <Navbar className="soju-navbar-sheet">
          <NavLeft> &nbsp;</NavLeft>

          <NavTitle sliding> Quyên góp Tiền </NavTitle>

          <NavRight>
            <Link sheetClose iconIos="f7:multiply" />
          </NavRight>
        </Navbar>

        <Toolbar position="bottom" className=" soju-toolbar ">
          <div className="px-1 w-100">
            <Button large fill className="soju-btn-cta" href="/xac-nhan/">
              Quyên góp
            </Button>
          </div>
        </Toolbar>

        <PageContent>
          <Block className="my-3">
            <div>
              <div className="mt-4">
                <div className="mb-3">Chọn nguồn tiền</div>

                <div className="apd-source">
                  <div className="apd-source-item ">
                    <input
                      type="radio"
                      name="checkbox"
                      defaultValue="value"
                      id="apd-source-1"
                      defaultChecked
                    />
                    <label htmlFor="apd-source-1" className="apd-source-label">
                      <div className="apd-source-icon">
                        <img
                          src="/static/images/logo-momo.svg"
                          width={36}
                          alt=""
                        />
                      </div>
                      <div className="font-weight-bold lutie-14">Ví MoMo</div>
                      <div className="lutie-12 text-gray">20.000.000đ</div>
                    </label>
                  </div>
                  <div className="apd-source-item ">
                    <input
                      type="radio"
                      name="checkbox"
                      defaultValue="value"
                      id="apd-source-2"
                    />
                    <label htmlFor="apd-source-2" className="apd-source-label">
                      <div className="apd-source-icon">
                        <img
                          src="/static/images/heoicon.jpg"
                          width={38}
                          alt=""
                        />
                      </div>
                      <div className="font-weight-bold lutie-14 ">Heo đất</div>
                      <div className="lutie-12 text-gray">10 heo vàng</div>
                    </label>
                  </div>
                </div>
              </div>

              <Row
                noGap
                className="align-items-center py-3 mt-4 border-top border-bottom "
              >
                <Col width="66">
                  <div className="">Quyên góp ẩn danh</div>
                </Col>
                <Col width="33">
                  <div className="toggle--small text-right">
                    <Toggle color="green" />
                  </div>
                </Col>
              </Row>

              <div className="mt-4 ">
                <div className="mb-2">Nhập số tiền bạn muốn quyên góp</div>

                <div className="list no-hairlines m-0  ">
                  <ul>
                    <div className="list-form list-form-outline  ">
                      <div className="item-inner p-0">
                        <div className="item-input-wrap have-lab mt-1">
                          <input
                            inputMode="numeric"
                            pattern="[0-9]*"
                            type="text"
                            placeholder="Nhập số tiền"
                          />
                          <div className="item-input-lab">đ</div>
                          <div className="item-input-error-message d-block pl-0 mt-2">
                            Số tiền tối thiểu là 1.000 đ
                          </div>
                          <span className="input-clear-button" />
                        </div>
                      </div>
                    </div>
                  </ul>
                </div>

                <div className="apd-payment mb-2 mt-3 hide-scrollbar">
                  <div className="apd-payment-item">
                    <input
                      className="apd-payment-check"
                      type="radio"
                      id="apd1"
                      name="payDonation"
                      defaultValue={1}
                      defaultChecked
                    />
                    <label htmlFor="apd1" className="apd-payment-label">
                      <span className="apd-payment-mo">10.000 ₫</span>
                    </label>
                  </div>
                  <div className="apd-payment-item">
                    <input
                      className="apd-payment-check"
                      type="radio"
                      id="apd2"
                      name="payDonation"
                      defaultValue={2}
                    />
                    <label htmlFor="apd2" className="apd-payment-label">
                      <span className="apd-payment-mo">20.000 ₫</span>
                    </label>
                  </div>
                  <div className="apd-payment-item">
                    <input
                      className="apd-payment-check"
                      type="radio"
                      id="apd3"
                      name="payDonation"
                      defaultValue={3}
                    />
                    <label htmlFor="apd3" className="apd-payment-label">
                      <span className="apd-payment-mo">50.000 ₫</span>
                    </label>
                  </div>
                  <div className="apd-payment-item">
                    <input
                      className="apd-payment-check"
                      type="radio"
                      id="apd4"
                      name="payDonation"
                      defaultValue={4}
                    />
                    <label htmlFor="apd4" className="apd-payment-label">
                      <span className="apd-payment-mo">100.000 ₫</span>
                    </label>
                  </div>
                  <div className="apd-payment-item">
                    <input
                      className="apd-payment-check"
                      type="radio"
                      id="apd5"
                      name="payDonation"
                      defaultValue={5}
                    />
                    <label htmlFor="apd5" className="apd-payment-label">
                      <span className="apd-payment-mo">200.000 ₫</span>
                    </label>
                  </div>
                  <div className="apd-payment-item">
                    <input
                      className="apd-payment-check"
                      type="radio"
                      id="apd6"
                      name="payDonation"
                      defaultValue={6}
                    />
                    <label htmlFor="apd6" className="apd-payment-label">
                      <span className="apd-payment-mo">500.000 ₫</span>
                    </label>
                  </div>
                </div>
              </div>

              <div className="mt-4 ">
                <div className="mb-2">Lời nhắn</div>
                <textarea
                  name="Message"
                  maxLength="70"
                  className="resizable soju-form"
                  placeholder="Nhập lời nhắn"
                ></textarea>
              </div>

              <div className="apd-payment apd-payment-message mb-3 mt-3 hide-scrollbar">
                <div className="apd-payment-item">
                  <input
                    className="apd-payment-check"
                    type="radio"
                    id="apd21"
                    name="payDonation"
                    defaultValue={1}
                    defaultChecked
                  />
                  <label htmlFor="apd21" className="apd-payment-label">
                    <span className="apd-payment-mo">
                      Cảm ơn vì đã là lá chắn vững vàng cho quyết thắng đại dịch
                      ₫
                    </span>
                  </label>
                </div>
                <div className="apd-payment-item">
                  <input
                    className="apd-payment-check"
                    type="radio"
                    id="apd22"
                    name="payDonation"
                    defaultValue={2}
                  />
                  <label htmlFor="apd22" className="apd-payment-label">
                    <span className="apd-payment-mo">Việt Nam Vô Địch! ₫</span>
                  </label>
                </div>
                <div className="apd-payment-item">
                  <input
                    className="apd-payment-check"
                    type="radio"
                    id="apd23"
                    name="payDonation"
                    defaultValue={3}
                  />
                  <label htmlFor="apd23" className="apd-payment-label">
                    <span className="apd-payment-mo">
                      Quyết thắng đại dịch ₫
                    </span>
                  </label>
                </div>
                <div className="apd-payment-item">
                  <input
                    className="apd-payment-check"
                    type="radio"
                    id="apd24"
                    name="payDonation"
                    defaultValue={4}
                  />
                  <label htmlFor="apd24" className="apd-payment-label">
                    <span className="apd-payment-mo">
                      Quyết thắng đại dịch ₫
                    </span>
                  </label>
                </div>
                <div className="apd-payment-item">
                  <input
                    className="apd-payment-check"
                    type="radio"
                    id="apd25"
                    name="payDonation"
                    defaultValue={5}
                  />
                  <label htmlFor="apd25" className="apd-payment-label">
                    <span className="apd-payment-mo">
                      Quyết thắng đại dịch ₫
                    </span>
                  </label>
                </div>
                <div className="apd-payment-item">
                  <input
                    className="apd-payment-check"
                    type="radio"
                    id="apd26"
                    name="payDonation"
                    defaultValue={6}
                  />
                  <label htmlFor="apd26" className="apd-payment-label">
                    <span className="apd-payment-mo">
                      Quyết thắng đại dịch ₫
                    </span>
                  </label>
                </div>
              </div>
            </div>
          </Block>
        </PageContent>
      </Sheet>

      <style jsx>{``}</style>
    </Page>
  );
};

export default DonationDetail;

import React, { useState, useEffect } from "react";
import {
  f7,
  Page,
  Navbar,
  Link,
  Block,
  Tabs,
  Tab,
  useStore,
} from "framework7-react";

// Import App Component
import MainSlider from "@/components/donation/main-slider.jsx";
import UserInfo from "@/components/donation/user-info.jsx";
import TraiTimMoMo from "@/components/donation/home/trai-tim-momo";
import CaseKhac from "@/components/donation/home/case-khac";

import NavbarMoMoFixed from "@/components/navbar-momo-fixed";

import MaxApi from "@momo-platform/max-api";
import events from "@/components/events";
import store from "@/js/store";
import $ from "dom7";

const DonationPage = (props) => {
  const { activeTab, ...rest } = props;

  const WebToken = useStore("WebToken");

  const [ConfigData, setConfigData] = useState({
    UserInfo: {},
    FirebaseToken: "",
    IsFromNoti: false,
  });

  const [ActiveTab, setActiveTab] = useState(activeTab);
  useEffect(() => {
    setActiveTab(activeTab);
  }, [activeTab]);


  useEffect(() => {
 
  }, [ActiveTab]);

  const pageInit = () => {
    events.on("CallbackGetCloudToken", (res) => {
      ConfigData.FirebaseToken = res;
      setConfigData({
        ...ConfigData,
      });
    });

    events.on("initPageData", (res) => {
      res = JSON.parse(res);
      ConfigData.UserInfo = res;
      setConfigData({
        ...ConfigData,
      });
    });

    //Device.ios
    if (WebToken || props.f7route.query.webToken) {
      //Di truc tiep
      store.dispatch("WebToken", props.f7route.query.webToken);
      try {
        window.ReactNativeWebView.postMessage("GetUserInfo");
        window.ReactNativeWebView.postMessage(
          JSON.stringify({ action: "getCloudToken" })
        );
      } catch (e) {}

      ConfigData.IsFromNoti = true;
      setConfigData({
        ...ConfigData,
      });

      //ValidateAccessToken(props.f7route.query.webToken, function (res) {
      // setConfigData({
      //   ...ConfigData,
      //   UserInfo: { phone: "0789654123", userId: "0378964081", userName: "MarTech Test", balance: 300000, appVer: 21422, appCode: "2.1.42", deviceOS: "IOS" }
      // });
      //});
    } else {
      // setConfigData({
      //   ...ConfigData,
      //   UserInfo: { phone: "0789654123", userId: "0378964081", userName: "MarTech Test", balance: 300000, appVer: 21422, appCode: "2.1.42", deviceOS: "IOS" }
      // });
      //di bang Miniapp
      MaxApi.getProfile(function (res) {
        ConfigData.UserInfo = {
          phone: res.user,
          userName: res.displayName,
        };
        MaxApi.getDeviceInfo(function (res) {
          ConfigData.FirebaseToken = res.firebaseToken;
          setConfigData({ ...ConfigData });
        });
      });
    }
  };

  const pageOut = () => {
    f7.sheet.close();
  };

  const pageDestroy = () => {
    if (f7.sheet) f7.sheet.destroy();
  };

  const onBack = () => {
    MaxApi.goBack();
  };

  return (
    <Page
      name="home"
      onPageInit={pageInit}
      onPageBeforeRemove={pageDestroy}
      onPageBeforeOut={pageOut}
      className="no-swipeback"
    >
      {!ConfigData.IsFromNoti && <NavbarMoMoFixed title={"Quyên góp"} />}

      {/* Toolbar */}
      {/* <Toolbar bottom>
        <Link>Left Link</Link>
        <Link>Right Link</Link>
      </Toolbar> */}
      {/* Page content */}
      {/* {ConfigData.UserInfo && ConfigData.UserInfo.userName && (
        <Block className="my-0"> */}
      <UserInfo userInfo={ConfigData.UserInfo} />
      {/* </Block>
      )} */}
      <MainSlider />
      {/* 
    <BlockTitle>Danh sách hoàn cảnh</BlockTitle> */}

      <nav className="soju-tab">
        <Link
          className="soju-tab-item"
          tabLink="#tab-donation-traitim"
          tabLinkActive={ActiveTab == "tab-donation-traitim"}
        >
          Trái tim MoMo
        </Link>
        <Link
          className="soju-tab-item"
          tabLink="#tab-donation-heovang"
          tabLinkActive={ActiveTab == "tab-donation-heovang"}
        >
          Heo đất MoMo
        </Link>
        <Link
          className="soju-tab-item"
          tabLink="#tab-donation-khac"
          tabLinkActive={ActiveTab == "tab-donation-khac"}
        >
          Khác
        </Link>
      </nav>

      <Tabs>
        <Tab
          id="tab-donation-traitim"
          onTabShow={() => setActiveTab("tab-donation-traitim")}
          tabActive={ActiveTab == "tab-donation-traitim"}
        >
          <Block className="mt-3">
            <TraiTimMoMo donationType={"traitimmomo"} />
          </Block>
        </Tab>
        <Tab
          id="tab-donation-heovang"
          tabActive={ActiveTab == "tab-donation-heovang"}
          onTabShow={() => setActiveTab("tab-donation-heovang")}
        >
          <Block className="mt-3">
            {/* <HeoVangMoMo /> */}
            <TraiTimMoMo donationType={"heovangmomo"} />
          </Block>
        </Tab>
        <Tab
          id="tab-donation-khac"
          tabActive={ActiveTab == "tab-donation-khac"}
          onTabShow={() => setActiveTab("tab-donation-khac")}
        >
          <CaseKhac />
        </Tab>
      </Tabs>
      <style jsx>{`
    
        .soju-tab { 
          position: sticky;
          top: 0;
          background: white;
          z-index: 15;
        }
    
        .soju-tab :global(.mainSlider-item) {
        }
      `}</style>
    </Page>
  );
};

export default DonationPage;

import React, { useState, useEffect } from "react";
import {
  f7,
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  Toolbar,
  Link,
  Block,
  Tabs,
  Tab,
  Sheet,
  NavRight,
  PageContent,
  useStore,
} from "framework7-react";

import $ from "dom7";

import "./donation.less";

const DonationManage = (props) => {
  const [sheetOpened, setSheetOpened] = useState(false);

  const pageInit = () => {
    $(".mdona-item").each(function (e, i) {
      $(e).on("click", function (e) {
        $(".mdona-item").removeClass("is-active");
        $(this).addClass("is-active");
      });
    });
  };

  const pageOut = () => {};

  const pageDestroy = () => {};

  return (
    <Page
      name="home"
      onPageInit={pageInit}
      onPageBeforeRemove={pageDestroy}
      onPageBeforeOut={pageOut}
      pageContent={false}
    >
      <Navbar className="momo-navbar">
        <NavLeft backLink>{/* <Link iconIos="f7:chevron_left" /> */}</NavLeft>

        <NavTitle sliding> Đăng ký ủng hộ định kỳ</NavTitle>
      </Navbar>

      <Toolbar position="bottom" className=" soju-toolbar ">
        <div className=" w-100">
          <p className="row mb-0 ">
            <a
              href="/donation-ungho-rule/"
              className="col button button-fill button-large color-pink "
            >
              Quản lý gói ủng hộ
            </a>
          </p>
        </div>
      </Toolbar>

      <div className="page-content bg-white d-flex flex-column">
        <div className="pron-container balance pt-4 mb-1">
          <div className="pron-row align-items-center">
            <div className="pron-col">
              <h2 className="mt-0 mb-0 mdona-title">Thông tin dịch vụ</h2>
            </div>
            <div className="pron-col auto">
              <a
                href="#"
                className="setting__more link popover-open"
                data-popover=".popover-more"
              >
                <i className="f7-icons">ellipsis</i>
              </a>
            </div>
          </div>
        </div>
        <div className="list m-0 list-style1 no-border-outside">
          <ul className="dona-list-detail">
            <li>
              <div className="item-content">
                <div className="item-inner">
                  <div className="item-title ">Tài khoản liên kết</div>
                  <div className="item-after ">124253455</div>
                </div>
              </div>
            </li>
            <li>
              <div className="item-content">
                <div className="item-inner">
                  <div className="item-title ">Hạn mức thanh toán</div>
                  <div className="item-after ">
                    <div
                      className="item-list-select" onClick={() => setSheetOpened(true)}
                    >
                      5.000đ/ngày <span className="arrow-down ml-1" />
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div className="item-content">
                <div className="item-inner">
                  <div className="item-title ">Gói ủng hộ</div>
                  <div className="item-after ">
                  <div
                      className="item-list-select" onClick={() => setSheetOpened(true)}
                    >
                      5.000đ/ngày <span className="arrow-down ml-1" />
                    </div>
                  </div>
                </div>
              </div>
            </li>
            <li>
              <div className="item-content">
                <div className="item-inner">
                  <div className="item-title ">Kỳ thanh toán tiếp theo</div>
                  <div className="item-after ">18:00 - 12/12/2020</div>
                </div>
              </div>
            </li>
          </ul>
        </div>
        <div className="pron-container balance pt-4">
          <h2 className="mt-0 mb-1 mdona-title">Lịch sử giao dịch</h2>
          <div className="text-center mt-3">
            <img
              src="https://static.mservice.io/images/s/momo-upload-api-200312133020-637196166203949468.png"
              className="img-fluid d-block mx-auto"
              width={200}
              alt=""
            />
          </div>
          <div className="text-center text-gray">
            Bạn chưa có lịch sử giao dịch nào
          </div>
          <div className="dona-history">
            <div className="dona-history-item">
              <div className="dona-history-title h5 font-weight-bold">
                Gói Premium &nbsp;Google Driver
              </div>
              <div className="dona-history-time lutie-10">
                15/7/2019 - 21 :30
              </div>
              <div className="d-flex align-items-center mt-1">
                <div className="dona-history-status text-orange flex-grow-1 font-weight-bold lutie-11">
                  {" "}
                  THÀNH CÔNG{" "}
                </div>
                <div className="dona-history-money font-weight-bold lutie-12">
                  -250.000đ
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="popover popover-more">
        <div className="popover-inner">
          <div className="list">
            <ul>
              <li>
                <a className="list-button item-link" href="#">
                  Hủy đăng ký
                </a>
              </li>
              <li>
                <a className="list-button item-link" href="#">
                  Link 2
                </a>
              </li>
              <li>
                <a className="list-button item-link" href="#">
                  Link 3
                </a>
              </li>
              <li>
                <a className="list-button item-link" href="#">
                  Link 4
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <Sheet
        className="dona-sheet-money soju-sheet"
        style={{ height: "350px", "--f7-sheet-bg-color": "#fff" }}
        backdrop
        opened={sheetOpened}
        onSheetClosed={() => {
          setSheetOpened(false);
        }}
      >
        <Navbar className="soju-navbar-sheet">
          <NavLeft> &nbsp;</NavLeft>

          <NavTitle sliding> Đổi gói ủng hộ </NavTitle>

          <NavRight>
            <Link sheetClose iconIos="f7:multiply" />
          </NavRight>
        </Navbar>

        <PageContent>
          <div className="dona-change-select">
            <div className="dona-change-item is-active">
              Mỗi ngày 2000đ
              <div className="mdona-change-icon">
                <i className="f7-icons size-14">icon-checkmark_alt</i>
              </div>
            </div>
            <div className="dona-change-item">
              Mỗi ngày 3000đ
              <div className="mdona-change-icon">
                <i className="f7-icons size-14">icon-checkmark_alt</i>
              </div>
            </div>
            <div className="dona-change-item">
              Mỗi ngày 5000đ
              <div className="mdona-change-icon">
                <i className="f7-icons size-14">icon-checkmark_alt</i>
              </div>
            </div>
            <div className="dona-change-item">
              Mỗi ngày 14000đ
              <div className="mdona-change-icon">
                <i className="f7-icons size-14">icon-checkmark_alt</i>
              </div>
            </div>
            <div className="dona-change-item">
              Mỗi ngày 21000đ
              <div className="mdona-change-icon">
                <i className="f7-icons size-14">icon-checkmark_alt</i>
              </div>
            </div>
            <div className="dona-change-item">
              Mỗi ngày 35000đ
              <div className="mdona-change-icon">
                <i className="f7-icons ">icon-checkmark_alt</i>
              </div>
            </div>
            {/* <div class="dona-change-item text-danger">
              Ngưng ủng hộ
          </div> */}
          </div>
        </PageContent>
      </Sheet>

      <style jsx>{``}</style>
    </Page>
  );
};

export default DonationManage;

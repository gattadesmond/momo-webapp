import React, { useState, useEffect } from "react";
import {
  Swiper,
  SwiperSlide,
  Navbar,
  NavLeft,
  NavTitle,
  Link,
  Page,
  Tabs,
  Tab,
  f7,
} from "framework7-react";

import Image from "@/components/image";

const ShbPage = (props) => {
  const [ActiveTab, setActiveTab] = useState("tab-smartotp");

  return (
    <Page name="home" pageContent={false}>
      <Navbar className="momo-navbar">
        <NavLeft backLink>{/* <Link iconIos="f7:chevron_left" /> */}</NavLeft>

        <NavTitle sliding> SHB</NavTitle>
      </Navbar>

      <div className="tab-pri">
        <div className="tab-pri__body">
          <div className="tab-pri__item">
            <Link
              className="tab-link"
              tabLink="#tab-smartotp"
              tabLinkActive={ActiveTab == "tab-smartotp"}
            >
              Smart OTP
            </Link>
          </div>

          <div className="tab-pri__item">
            <Link
              className="tab-link"
              tabLink="#tab-smsotp"
              tabLinkActive={ActiveTab == "tab-smsotp"}
            >
              SMS OTP
            </Link>
          </div>
        </div>
      </div>

      <Tabs className="tab-pri__htuBank">
        <Tab
          id="tab-smartotp"
          className="page-content "
          onTabShow={() => setActiveTab("tab-smartotp")}
          tabActive={ActiveTab == "tab-smartotp"}
        >
          <div className="tab-pri__wrap">
            <div className="tab-pri__content">
              <div className="mb-2">
                Bạn sử dụng Smart OTP của SHB, vui lòng thực hiện theo hướng dẫn
                sau.
              </div>

              <div className="mainSlider">
                <Swiper
                  className="swiper-htuBank"
                  pagination
                  speed={400}
                  spaceBetween={10}
                >
                  {[
                    {
                      img:
                        "https://static.mservice.io/images/s/momo-upload-api-200716152902-637305101422615437.png",
                      text:
                        "<b>Bước 1:</b> Lấy <b>Mã giao dịch</b> trên Ví MoMo",
                    },
                    {
                      img:
                        "https://static.mservice.io/images/s/momo-upload-api-200716152908-637305101489477507.png",
                      text:
                        "<b>Bước 2:</b> Mở ứng dụng SHB Mobile, chọn <b>Tạo mã xác thực</b> Smart OTP",
                    },
                    {
                      img:
                        "https://static.mservice.io/images/s/momo-upload-api-200716152915-637305101554716864.png",
                      text:
                        "<b>Bước 3:</b> Nhập Mã <b>Smart OTP</b>​ bạn đã tạo với ngân hàng",
                    },
                    {
                      img:
                        "https://static.mservice.io/images/s/momo-upload-api-200716152922-637305101625265725.png",
                      text:
                        "<b>Bước 4:</b> Nhập <b>Mã giao dịch</b> vừa lấy ở MoMo",
                    },
                    {
                      img:
                        "https://static.mservice.io/images/s/momo-upload-api-200716152929-637305101692772607.png",
                      text:
                        "<b>Bước 5:</b> Sao chép <b>​Mã xác thực</b> nhận được",
                    },
                    {
                      img:
                        "https://static.mservice.io/images/s/momo-upload-api-200716152936-637305101769843227.png",
                      text:
                        "<b>Bước 6:</b> Quay lại Ví MoMo và nhập <b>Mã xác thực</b> vừa sao chép",
                    },
                  ].map((item, index) => (
                    <SwiperSlide key={index} className="mainSlider-item">
                      <div className="soju-img">
                        <Image
                          className="heobong-imgSlide"
                          src={item.img}
                          round="5px"
                        />
                      </div>
                      <div
                        className="my-2"
                        dangerouslySetInnerHTML={{ __html: item.text }}
                      ></div>
                    </SwiperSlide>
                  ))}
                </Swiper>
              </div>
            </div>
          </div>
        </Tab>
        <Tab
          id="tab-smsotp"
          className="page-content "
          tabActive={ActiveTab == "tab-smsotp"}
          onTabShow={() => setActiveTab("tab-smsotp")}
        >
          <div className="tab-pri__wrap">
            <div className="tab-pri__content">
              <div className="mb-2">
                Nếu bạn dùng SMS OTP, hãy thực hiện theo hướng dẫn sau:
              </div>

              <div>
                <ul className="steps steps-vertical ml-0">
                  <li className="step-item ">
                    <div className="step-link">
                      <span className="step-number">1</span>
                      <span className="step-title">
                        <b>Bước 1:</b> Lấy <b>Mã OTP</b> ​từ tin nhắn được gửi
                        tới SĐT bạn đã đăng ký tại ngân hàng
                      </span>
                    </div>
                  </li>

                  <li className="step-item ">
                    <div className="step-link">
                      <span className="step-number">2</span>
                      <span className="step-title">
                        <b>Bước 2:</b> Nhập <b>Mã OTP </b>​vào Ví MoMo
                      </span>
                    </div>
                  </li>

                  <li className="step-item ">
                    <div className="step-link">
                      <span className="step-number">3</span>
                      <span className="step-title">
                        <b>Bước 3:</b> Chọn <b>Tiếp tục</b> để liên kết
                      </span>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </Tab>
      </Tabs>
      <style jsx>{`

        
      `}</style>
    </Page>
  );
};

export default ShbPage;

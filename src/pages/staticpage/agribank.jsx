import React, { useState, useEffect } from "react";
import {
  Swiper,
  SwiperSlide,
  Navbar,
  NavLeft,
  NavTitle,
  Link,
  Page,
  Tabs,
  Tab,
  f7,
} from "framework7-react";

import Image from "@/components/image";

const AgribankPage = (props) => {
  const [ActiveTab, setActiveTab] = useState("tab-atm");

  const [swiperSlide1, setSwiperSlide1] = useState(null);

  const handleTabShow = (tabName) => {
    setActiveTab(tabName);
    if (swiperSlide1 != null) {
      swiperSlide1.update();
    }
  };

  return (
    <Page name="home" pageContent={false}>
      <Navbar className="momo-navbar">
        <NavLeft backLink>{/* <Link iconIos="f7:chevron_left" /> */}</NavLeft>

        <NavTitle sliding> Agribank</NavTitle>
      </Navbar>

      <div className="tab-pri">
        <div className="tab-pri__body">
          <div className="tab-pri__item">
            <Link
              className="tab-link"
              tabLink="#tab-atm"
              tabLinkActive={ActiveTab == "tab-atm"}
            >
              Dùng ATM
            </Link>
          </div>

          <div className="tab-pri__item">
            <Link
              className="tab-link"
              tabLink="#tab-app"
              tabLinkActive={ActiveTab == "tab-app"}
            >
              App Agribank
            </Link>
          </div>

          <div className="tab-pri__item">
            <Link
              className="tab-link"
              tabLink="#tab-bank"
              tabLinkActive={ActiveTab == "tab-bank"}
            >
              Ra ngân hàng
            </Link>
          </div>
        </div>
      </div>

      <Tabs className="tab-pri__htuBank">
        <Tab
          id="tab-atm"
          className="page-content "
          onTabShow={() => handleTabShow("tab-atm")}
          tabActive={ActiveTab == "tab-atm"}
        >
          <div className="tab-pri__wrap">
            <div className="tab-pri__content">
              <Swiper
                className="swiper-htuBank"
                pagination
                speed={400}
                spaceBetween={10}
              >
                {[
                  "https://static.mservice.io/images/s/momo-upload-api-200415163141-637225651010351397.jpg",
                  "https://static.mservice.io/images/s/momo-upload-api-200415163251-637225651710272595.jpg",
                  "https://static.mservice.io/images/s/momo-upload-api-200415163306-637225651866637347.jpg",
                  "https://static.mservice.io/images/s/momo-upload-api-200415163324-637225652048389984.jpg",
                  "https://static.mservice.io/images/s/momo-upload-api-200415163342-637225652220997717.jpg",
                ].map((item, index) => (
                  <SwiperSlide key={index} className="mainSlider-item">
                    <div className="soju-img">
                      <Image src={item} round="5px" />
                    </div>
                  </SwiperSlide>
                ))}
              </Swiper>
            </div>

            <div className="toolbar toolbar-bottom toolbar-large">
              <div className="toolbar-inner">
                <div className=" w-100">
                  <p className="row mb-0 ">
                    <a
                      href="#"
                      className="col button button-fill  sheet-close button-large color-pink"
                    >
                      Tìm ATM gần nhất
                    </a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </Tab>
        <Tab
          id="tab-app"
          className="page-content "
          tabActive={ActiveTab == "tab-app"}
          onTabShow={() => handleTabShow("tab-app")}
        >
          <div className="tab-pri__wrap">
            <div className="tab-pri__content">
              <Swiper
                className="swiper-htuBank"
                pagination
                speed={400}
                spaceBetween={10}
                onSwiper={setSwiperSlide1}
              >
                {[
                  "https://static.mservice.io/images/s/momo-upload-api-200415163141-637225651010351397.jpg",
                  "https://static.mservice.io/images/s/momo-upload-api-200415163251-637225651710272595.jpg",
                  "https://static.mservice.io/images/s/momo-upload-api-200415163306-637225651866637347.jpg",
                  "https://static.mservice.io/images/s/momo-upload-api-200415163324-637225652048389984.jpg",
                  "https://static.mservice.io/images/s/momo-upload-api-200415163342-637225652220997717.jpg",
                ].map((item, index) => (
                  <SwiperSlide key={index} className="mainSlider-item">
                    <div className="soju-img">
                      <Image src={item} round="5px" />
                    </div>
                  </SwiperSlide>
                ))}
              </Swiper>

              <h2 className="mt-0 mb-2">Giới thiệu</h2>
              <p>
                Agribank E-Mobile Banking là ứng dụng di động thông minh do Ngân
                hàng Agribank phát triển, cho phép khách hàng cá nhân dễ dàng
                thực hiện các giao dịch tài chính, trải nghiệm các tiện ích
                thanh toán mọi lúc mọi nơi ngay trên điện thoại di động/máy tính
                bảng.
              </p>
              <p>
                Với Agribank E-Mobile Banking, khách hàng có thể giao dịch nhanh
                chóng và an toàn tại mọi thời điểm, kể cả thời gian ngoài giờ
                hành chính, ngày nghỉ, Lễ Tết.
              </p>
            </div>

            <div className="toolbar toolbar-bottom toolbar-large">
              <div className="toolbar-inner">
                <div className=" w-100">
                  <p className="row mb-0 ">
                    <a
                      href="#"
                      className="col button button-fill  sheet-close button-large color-pink"
                    >
                      Tìm ATM gần nhất
                    </a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </Tab>
        <Tab
          id="tab-bank"
          className="page-content "
          tabActive={ActiveTab == "tab-bank"}
          onTabShow={() => handleTabShow("tab-bank")}
        >
          <div className="tab-pri__wrap">
            <div className="tab-pri__content">
              <h2 className="mt-0 mb-2">Giới thiệu</h2>
              <p>
                Quý khách có thể dễ dàng tìm thấy chi nhánh Agribank gần mình
                nhất bên dưới và yêu cầu nhân viên ngân hàng hỗ trợ đăng kí dịch
                vụ giao dịch Internet - Ecommerce (hoàn toàn miễn phí).
              </p>
              <p>
                Khi đến đăng kí, vui lòng mang theo CMND/CCCD/Hộ chiếu trùng với
                giấy tờ đã dùng để mở thẻ tại ngân hàng Agribank.
              </p>
              <p>
                Quý khách có thể dễ dàng tìm thấy chi nhánh Agribank gần mình
                nhất bên dưới và yêu cầu nhân viên ngân hàng hỗ trợ đăng kí dịch
                vụ giao dịch Internet - Ecommerce (hoàn toàn miễn phí).
              </p>
              <p>
                Khi đến đăng kí, vui lòng mang theo CMND/CCCD/Hộ chiếu trùng với
                giấy tờ đã dùng để mở thẻ tại ngân hàng Agribank.
              </p>

              <p>
                Quý khách có thể dễ dàng tìm thấy chi nhánh Agribank gần mình
                nhất bên dưới và yêu cầu nhân viên ngân hàng hỗ trợ đăng kí dịch
                vụ giao dịch Internet - Ecommerce (hoàn toàn miễn phí).
              </p>
              <p>
                Khi đến đăng kí, vui lòng mang theo CMND/CCCD/Hộ chiếu trùng với
                giấy tờ đã dùng để mở thẻ tại ngân hàng Agribank.
              </p>

              <p>
                Quý khách có thể dễ dàng tìm thấy chi nhánh Agribank gần mình
                nhất bên dưới và yêu cầu nhân viên ngân hàng hỗ trợ đăng kí dịch
                vụ giao dịch Internet - Ecommerce (hoàn toàn miễn phí).
              </p>
              <p>
                Khi đến đăng kí, vui lòng mang theo CMND/CCCD/Hộ chiếu trùng với
                giấy tờ đã dùng để mở thẻ tại ngân hàng Agribank.
              </p>

              <p>
                Quý khách có thể dễ dàng tìm thấy chi nhánh Agribank gần mình
                nhất bên dưới và yêu cầu nhân viên ngân hàng hỗ trợ đăng kí dịch
                vụ giao dịch Internet - Ecommerce (hoàn toàn miễn phí).
              </p>
              <p>
                Khi đến đăng kí, vui lòng mang theo CMND/CCCD/Hộ chiếu trùng với
                giấy tờ đã dùng để mở thẻ tại ngân hàng Agribank.
              </p>
            </div>

            <div className="toolbar toolbar-bottom toolbar-large">
              <div className="toolbar-inner">
                <div className=" w-100">
                  <p className="row mb-0 ">
                    <a
                      href="#"
                      className="col button button-fill  sheet-close button-large color-pink"
                    >
                      Tìm ATM gần nhất
                    </a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </Tab>
      </Tabs>
      <style jsx>{``}</style>
    </Page>
  );
};

export default AgribankPage;

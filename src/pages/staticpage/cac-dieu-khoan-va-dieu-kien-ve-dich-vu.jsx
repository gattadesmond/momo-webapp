import React, { useState, useEffect } from "react";
import {
  f7ready,
  Swiper,
  SwiperSlide,
  Navbar,
  NavLeft,
  NavTitle,
  Link,
  Page,
  Toolbar,
  Button,
  Tabs,
  Tab,
  f7,
} from "framework7-react";

import Image from "@/components/image";
import $ from "dom7";

const contentLoadMore = function (e) {
  const saleMore = e.querySelector(".sale-cluster-content");
  const saleMoreBtn = e.querySelector(".js-sale-more-btn");
  const saleMoreHeight = saleMore.scrollHeight;
  if (saleMoreHeight > 200) {
    e.classList.add("active");

    saleMoreBtn.addEventListener("click", function () {
      saleMore.style.setProperty("max-height", saleMoreHeight + "px");
      e.classList.remove("active");
    });
  }
};

const pageInit = () => {
  f7ready((f7) => {
    var saleMoreCon = Array.from(document.querySelectorAll(".js-sale-more"));
    saleMoreCon.forEach(function (e) {
      contentLoadMore(e);
    });
  });
};

const Rule = (props) => {
  return (
    <Page name="home" onPageInit={pageInit} pageContent={false}>
      <Toolbar position="bottom" className="soju-popup-toolbar ">
        <div className="px-1 w-100">
          <Button large fill className="soju-btn-cta">
            Quyên góp
          </Button>
        </div>
      </Toolbar>
      <div className="page-content bg-white d-flex flex-column">
        <div className="pron-container flex-grow-1 balance pt-4">
          <div className="sale-cluster">
            <div className="js-sale-more active">
              <div className="sale-cluster-content  noti-content">
                <style
                  type="text/css"
                  dangerouslySetInnerHTML={{
                    __html:
                      ".js-sale-more .sale-cluster-content { max-height: calc(100vh - 160px);",
                  }}
                />
                <p style={{ textAlign: "justify" }}>
                  Người Sử Dụng cần đọc và đồng ý với những Điều Khoản và Điều
                  Kiện này trước khi sử dụng Sản Phẩm/Dịch Vụ.
                </p>
                <p style={{ textAlign: "justify" }}>
                  CÁC ĐIỀU KHOẢN VÀ ĐIỀU KIỆN VỀ DỊCH VỤ (sau đây gọi tắt là
                  “Điều Khoản Chung”) điều chỉnh các quyền và nghĩa vụ của Người
                  Sử Dụng, với tư cách là khách hàng, khi sử dụng Sản Phẩm/Dịch
                  Vụ do CÔNG TY CỔ PHẦN DỊCH VỤ DI ĐỘNG TRỰC TUYẾN cung cấp trên
                  Ví Điện Tử MoMo.&nbsp;
                </p>
                <h2 style={{ textAlign: "justify" }}>
                  <strong>1. Định nghĩa</strong>
                </h2>
                <p style={{ textAlign: "justify" }}>
                  Trong Điều Khoản Chung này, các từ và thuật ngữ sau đây sẽ có
                  nghĩa dưới đây trừ khi ngữ cảnh có yêu cầu khác:
                </p>
                <p style={{ textAlign: "justify" }}>
                  <strong>1.1 M_Service:</strong> là Công Ty Cổ Phần Dịch Vụ Di
                  Động Trực Tuyến, một công ty được thành lập hợp lệ và hoạt
                  động theo pháp luật của nước Cộng Hòa Xã Hội Chủ Nghĩa Việt
                  Nam, có Giấy Chứng Nhận Đăng Ký Doanh Nghiệp Số 0305289153 cấp
                  lần đầu ngày 26/10/2007.
                  <br />
                  <strong>1.2 Ứng Dụng MoMo:</strong>&nbsp;là Ứng dụng trên nền
                  tảng di động do M_Service phát triển và vận hành để cung cấp
                  Dịch vụ Ví điện tử và các dịch vụ trung gian thanh toán khác
                  được Ngân hàng Nhà nước Việt Nam cấp phép triển khai dưới
                  thương hiệu MoMo.
                  <br />
                  <strong>1.3 Tài Khoản MoMo</strong>: là tài khoản điện tử trên
                  hệ thống công nghệ thông tin của M_Service do Người Sử Dụng
                  tạo lập và quản lý thông qua nhiều hình thức khác nhau, bao
                  gồm nhưng không giới hạn bởi ứng dụng trên điện thoại di động,
                  website, SIMCARD và các hình thức khác để truy cập, sử dụng
                  Sản Phẩm/Dịch Vụ, bao gồm cả Dịch vụ Ví điện tử và các dịch vụ
                  trung gian thanh toán khác do M_Service cung cấp.
                  <br />
                  <strong>1.4 Người Sử Dụng</strong>:&nbsp;là các khách hàng có
                  nhu cầu mở và sử dụng Sản Phẩm/Dịch Vụ của M_Service.
                  <br />
                  <strong>1.5 Sản Phẩm/Dịch Vụ:</strong>&nbsp;bao gồm dịch vụ
                  được thực hiện Tại Điểm Giao Dịch và các dịch vụ trên Ứng Dụng
                  MoMo.
                  <br />
                  <strong>1.6 Giao Dịch: </strong>là bất kỳ giao dịch nào của
                  Người Sử Dụng liên quan đến việc sử dụng Sản Phẩm/Dịch Vụ được
                  cung cấp bởi M_Service.
                  <br />
                  <strong>1.7 Điểm Giao Dịch:</strong>&nbsp;là các địa điểm đối
                  tác được M_Service ủy quyền cung cấp các dịch vụ Nạp/Rút,
                  thanh toán tiền điện, nước, khoản vay để Người Sử Dụng sử
                  dụng; Danh sách Điểm Giao Dịch được liệt kê{" "}
                  <a
                    href="javascript:WebApi.openURL('https://momo.vn/scoretransaction/index1')"
                    target="_blank"
                    className="external"
                  >
                    tại đây
                  </a>
                  .<br />
                  <strong>1.8 Các Giới Hạn Giao Dịch: </strong>nghĩa là các giới
                  hạn sau đây:
                </p>
                <p style={{ textAlign: "justify", marginLeft: "40px" }}>
                  a. Người Sử Dụng là cá nhân sử dụng Tài khoản MoMo bị giới hạn
                  số dư tối đa của Tài Khoản MoMo là 50.000.000 đồng và tổng giá
                  trị Giao Dịch không quá 100.000.000 đồng/tháng. Hạn mức này
                  không áp dụng đối với Người Sử Dụng là tổ chức hoặc người ký
                  hợp đồng làm Đơn vị chấp nhận thanh toán của M_Service và có
                  thể được điều chỉnh, thay đổi theo Quy Định Pháp Luật và chính
                  sách của M_Service tại từng thời điểm;
                </p>
                <p style={{ textAlign: "justify", marginLeft: "40px" }}>
                  b. Dịch vụ chuyển tiền MoMo tại quầy (Cash-to-Cash) và Dịch vụ
                  chuyển tiền MoMo trên di động (Mobile-to-Mobile) và có thể
                  thay đổi theo Quy Định Pháp Luật và chính sách của M_Service
                  tại từng thời điểm;
                </p>
                <p style={{ textAlign: "justify", marginLeft: "40px" }}>
                  c. Người Sử Dụng sử dụng Dịch Vụ chuyển tiền tại quầy và
                  chuyển tiền MoMo trên di động bị giới hạn đối với các giao
                  dịch lên tới không quá 20.000.000 VND mỗi Người Sử Dụng một
                  ngày.
                </p>
                <p style={{ textAlign: "justify" }}>
                  <strong>​1.9&nbsp;</strong>
                  <strong>Biện Pháp Xác Thực: </strong>&nbsp;là các yếu tố xác
                  thực mà M_Service sử dụng để xác thực định danh Người Sử Dụng
                  bao gồm nhưng không giới hạn mật khẩu sử dụng một lần
                  (One-Time Password), mật khẩu, đặc điểm sinh trắc học và các
                  biện pháp xác thực khác được phép thực hiện theo Quy Định Pháp
                  Luật.
                  <br />
                  <strong>1.10 Dịch Vụ Khách Hàng: </strong>nghĩa là dịch vụ
                  chăm sóc khách hàng của M_Service, được cung cấp theo số điện
                  thoại (028) 39917199 hoặc 1900545441.
                  <br />
                  <strong>1.11 Hồ Sơ Mở Tài Khoản MoMo: </strong>là các giấy tờ,
                  thông tin của cá nhân phải cung cấp theo quy định của pháp
                  luật và yêu cầu của M_Service, bao gồm nhưng không giới hạn:
                </p>
                <p style={{ textAlign: "justify", marginLeft: "40px" }}>
                  a. Đối với cá nhân: họ và tên; ngày, tháng, năm sinh; quốc
                  tịch; số điện thoại; căn cước công dân/chứng minh nhân dân/hộ
                  chiếu còn thời hạn;
                </p>
                <p style={{ textAlign: "justify", marginLeft: "40px" }}>
                  b. Đối với tổ chức:&nbsp; tên giao dịch viết tắt và đầy đủ; mã
                  số doanh nghiệp; mã số thuế; trụ sở chính; địa chỉ giao dịch;
                  số điện thoại; người đại diện hợp pháp;
                </p>
                <p style={{ textAlign: "justify", marginLeft: "40px" }}>
                  c. Hồ Sơ Mở Tài Khoản MoMo sẽ được M_Service cập nhật theo quy
                  định của pháp luật và yêu cầu phù hợp của M_Service theo từng
                  thời điểm và được đăng tải trên Ứng Dụng MoMo.
                </p>
                <p style={{ textAlign: "justify" }}>
                  <strong>1.12 Quy Định Pháp Luật: </strong>bao gồm toàn bộ các
                  quy định pháp luật của nước Cộng Hòa Xã Hội Chủ Nghĩa Việt Nam
                  như Luật, Bộ luật, Pháp lệnh, Nghị định, Thông tư, quy chuẩn,
                  quy tắc, quyết định hành chính của cơ quan nhà nước có thẩm
                  quyền và các quy định có hiệu lực pháp luật khác tại từng thời
                  điểm.
                  <br />
                  <strong>1.13 Ngày Làm Việc</strong>: là các ngày từ Thứ Hai
                  đến Thứ Sáu, không bao gồm ngày nghỉ, lễ, Tết theo Quy Định
                  Pháp Luật.
                </p>
                <h2 style={{ textAlign: "justify" }}>
                  <strong>2. Các Quy Tắc Chung</strong>
                </h2>
                <div style={{ textAlign: "justify" }}>
                  <strong>2.1</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Bằng việc truy
                  cập, tải về Ứng Dụng MoMo, Người Sử Dụng xác nhận đã hiểu rõ
                  các Điều Khoản Chung và hoàn toàn đồng ý với từng phần cũng
                  như toàn bộ các điều khoản và điều kiện được quy định tại đây,
                  cũng như bất kỳ các điều chỉnh liên quan và chấp nhận rằng
                  việc sử dụng Sản Phẩm/Dịch Vụ sẽ chịu sự điều chỉnh của những
                  Điều Khoản Chung này.&nbsp;
                </div>
                <p style={{ textAlign: "justify" }}>
                  &nbsp;<strong>2.2</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Bằng việc truy
                  cập, tải về và sử dụng Ứng Dụng MoMo, Người Sử Dụng thừa nhận
                  và đồng ý rằng đã chấp thuận với các phương thức, yêu cầu,
                  và/hoặc chính sách được quy định trong Điều Khoản Chung này,
                  và rằng Người Sử Dụng theo đây đồng ý cho M_Service thu thập,
                  sử dụng, tiết lộ và/hoặc xử lý dữ liệu cá nhân của Người Sử
                  Dụng như được mô tả trong Điều Khoản Chung này.
                </p>
                <p style={{ textAlign: "justify" }}>
                  <strong>&nbsp;2.3</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Người Sử Dụng sẽ
                  được xem là đương nhiên chấp nhận và chịu sự ràng buộc của
                  những Điều Khoản Chung này và việc Người Sử Dụng dùng một phần
                  hoặc toàn bộ các Sản Phẩm/Dịch Vụ trên Ứng Dụng MoMo được xem
                  là giữa Người Sử Dụng và M_Service đã ký kết và thực hiện một
                  Hợp đồng dịch vụ.&nbsp;
                </p>
                <div style={{ textAlign: "justify" }}>
                  <strong>2.4</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>M_Service có quyền
                  thay đổi những Điều Khoản Chung này hoặc bất kỳ tính năng nào
                  của Sản Phẩm/Dịch Vụ vào bất kỳ thời điểm nào. Thay đổi đó sẽ
                  có hiệu lực ngay lập tức sau khi công bố thay đổi của các Điều
                  Khoản Chung hoặc tính năng tại Ứng Dụng MoMo.
                </div>
                <div style={{ textAlign: "justify" }}>&nbsp;</div>
                <div style={{ textAlign: "justify" }}>
                  <strong>2.5</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Người Sử Dụng đồng
                  ý đánh giá những Điều Khoản Chung này định kỳ để đảm bảo rằng
                  Người Sử Dụng đã được cập nhật đối với bất kỳ các thay đổi
                  hoặc sửa đổi đối với những Điều Khoản Chung này. Việc Người Sử
                  Dụng tiếp tục sử dụng Sản Phẩm/Dịch Vụ sẽ được xem là Người Sử
                  Dụng chấp nhận hoàn toàn các Điều Khoản Chung được thay đổi.
                </div>
                <div style={{ textAlign: "justify" }}>&nbsp;</div>
                <div style={{ textAlign: "justify" }}>
                  <strong>2.6</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Người Sử Dụng đảm
                  bảo rằng Người Sử Dụng đã hiểu rõ các hướng dẫn và quy trình
                  sử dụng Sản Phẩm/Dịch Vụ của M_Service và những thay đổi, bổ
                  sung (nếu có) của M_Service.
                </div>
                <h2 style={{ textAlign: "justify" }}>
                  <strong>
                    3.<span style={{ whiteSpace: "pre" }}> </span>Đăng Ký/Ngưng
                    sử dụng Sản Phẩm/Dịch Vụ
                  </strong>
                </h2>
                <h3 style={{ textAlign: "justify" }}>
                  3.1<span style={{ whiteSpace: "pre" }}> </span>Đăng Ký và sử
                  dụng Sản Phẩm/Dịch Vụ
                </h3>
                <p style={{ marginLeft: "40px", textAlign: "justify" }}>
                  <strong>a.</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Để sử dụng Sản
                  Phẩm/Dịch Vụ, trước hết Người Sử Dụng cần tải Ứng Dụng MoMo,
                  cung cấp Hồ Sơ Mở Tài Khoản MoMo và làm theo hướng dẫn.
                </p>
                <p style={{ marginLeft: "40px", textAlign: "justify" }}>
                  <strong>b.</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Người Sử Dụng đồng
                  ý cung cấp cho M_Service hoặc các Điểm Giao Dịch của M_Service
                  các thông tin đầy đủ, cập nhật và chính xác liên quan đến
                  Người Sử Dụng mà M_Service sẽ yêu cầu vào từng thời điểm để sử
                  dụng Sản Phẩm/Dịch Vụ. Người Sử Dụng đồng ý thông báo ngay cho
                  M_Service hoặc các Điểm Giao Dịch của M_Service bất kỳ thay
                  đổi nào về Hồ Sơ Mở Tài Khoản MoMo và các thông tin đã được
                  cung cấp cho M_Service. Người Sử Dụng tuyên bố và bảo đảm rằng
                  các thông tin của Người Sử Dụng và các thông tin khác được
                  cung cấp cho M_Service là trung thực và chính xác và chịu
                  trách nhiệm đối với các thông tin đã cung cấp trên Ứng Dụng
                  MoMo.&nbsp;
                </p>
                <p style={{ marginLeft: "40px", textAlign: "justify" }}>
                  <strong>c.</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Theo yêu cầu của
                  M_Service, Người Sử Dụng sẽ cung cấp cho M_Service các thông
                  tin liên quan đến việc sử dụng Sản Phẩm/Dịch Vụ mà M_Service
                  có thể yêu cầu một cách hợp lý cho các mục đích sau đây:
                </p>
                <ul>
                  <li style={{ marginLeft: "40px", textAlign: "justify" }}>
                    Trợ giúp M_Service tuân thủ các nghĩa vụ của mình theo Quy
                    Định Pháp Luật;
                  </li>
                  <li style={{ marginLeft: "40px", textAlign: "justify" }}>
                    Báo cáo các cơ quan hữu quan hoặc các cơ quan chính phủ về
                    việc tuân thủ những nghĩa vụ đó;
                  </li>
                  <li style={{ marginLeft: "40px", textAlign: "justify" }}>
                    Đánh giá việc Người Sử Dụng đã tuân thủ, đang tuân thủ và có
                    thể tiếp tục tuân thủ tất cả các nghĩa vụ của mình theo
                    những Điều Khoản Chung này hay không.
                  </li>
                </ul>
                <p style={{ marginLeft: "40px", textAlign: "justify" }}>
                  <strong>d.</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Trừ khi pháp luật
                  có quy định khác, Người Sử Dụng buộc phải hoàn thành việc liên
                  kết Tài Khoản MoMo với tài khoản thanh toán hoặc thẻ ghi nợ
                  của Người Sử Dụng mở tại ngân hàng liên kết để được kích hoạt
                  tính năng Ví điện tử MoMo. Người Sử Dụng được liên kết Tài
                  Khoản MoMo với một hoặc nhiều tài khoản thanh toán hoặc thẻ
                  ghi nợ của Người Sử Dụng là chủ Tài Khoản MoMo mở tại một hoặc
                  một số ngân hàng liên kết.
                </p>
                <p style={{ marginLeft: "40px", textAlign: "justify" }}>
                  <strong>e.</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Việc nạp tiền vào
                  Tài Khoản MoMo của Người Sử Dụng phải thực hiện từ:&nbsp;
                </p>
                <ul>
                  <li style={{ marginLeft: "40px", textAlign: "justify" }}>
                    Tài khoản thanh toán hoặc thẻ ghi nợ của Người Sử Dụng là
                    chủ Tài Khoản MoMo tại ngân hàng; hoặc/và
                  </li>
                  <li style={{ marginLeft: "40px", textAlign: "justify" }}>
                    Nhận tiền từ Tài Khoản MoMo của Người Sử Dụng khác cùng được
                    mở bởi M_Service.&nbsp;
                  </li>
                </ul>
                <p style={{ marginLeft: "40px", textAlign: "justify" }}>
                  <strong>f.</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Người Sử Dụng được
                  sử dụng Tài Khoản MoMo của mình để:&nbsp;
                </p>
                <ul>
                  <li style={{ marginLeft: "40px", textAlign: "justify" }}>
                    Thanh toán cho các hàng hóa, dịch vụ hợp pháp; hoặc/và
                  </li>
                  <li style={{ marginLeft: "40px", textAlign: "justify" }}>
                    Chuyển tiền cho Tài Khoản&nbsp; MoMo của Người Sử Dụng khác
                    do M_Service mở; hoặc/và
                  </li>
                  <li style={{ marginLeft: "40px", textAlign: "justify" }}>
                    Rút tiền ra khỏi Tài Khoản MoMo của Người Sử Dụng về tài
                    khoản thanh toán hoặc thẻ ghi nợ của Người Sử Dụng (là chủ
                    Tài Khoản MoMo) tại ngân hàng.
                  </li>
                </ul>
                <p style={{ marginLeft: "40px", textAlign: "justify" }}>
                  <strong>g.</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>M_Service sẽ có
                  quyền áp dụng phí dịch vụ và/hoặc lệ phí đối với Sản Phẩm/Dịch
                  Vụ; chi tiết biểu phí xem{" "}
                  <a
                    href="javascript:WebApi.openURL('https://momo.vn/ung-dung-momo/bieu-phi')"
                    className="external"
                  >
                    tại đây
                  </a>
                  . Người Sử Dụng đồng ý sẽ chịu trách nhiệm thanh toán đầy đủ
                  và đúng hạn mọi khoản phí dịch vụ và các lệ phí khác đến hạn
                  thanh toán liên quan đến bất kỳ Giao Dịch nào hoặc việc sử
                  dụng Sản Phẩm/Dịch Vụ mà M_Service tính phí, và đồng ý cho
                  phép M_Service khấu trừ bất kỳ khoản phí, lệ phí hay khoản
                  tiền khác mà Ngưởi Sử Dụng phải trả cho M_Service vào số dư
                  Tài Khoản MoMo của Người Sử Dụng. M_Service có quyền điều
                  chỉnh, thay đổi biểu phí tùy từng thời điểm theo quyết định
                  riêng của mình.
                </p>
                <p style={{ marginLeft: "40px", textAlign: "justify" }}>
                  <strong>h.</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Người Sử Dụng cam
                  kết không sử dụng Sản Phẩm/Dịch Vụ cho bất kỳ mục đích hoặc
                  liên quan đến bất kỳ hành động vi phạm các Quy Định Pháp Luật,
                  bao gồm, nhưng không giới hạn, các luật và quy định liên quan
                  đến phòng, chống rửa tiền, chống tài trợ khủng bố.&nbsp;
                </p>
                <p style={{ marginLeft: "40px", textAlign: "justify" }}>
                  <strong>i.</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Người Sử Dụng xác
                  nhận và công nhận rằng Người Sử Dụng có đầy đủ năng lực hành
                  vi, quyền hạn hoặc thẩm quyền để sử dụng Sản Phẩm/Dịch Vụ.
                </p>
                <p style={{ marginLeft: "40px", textAlign: "justify" }}>
                  <strong>j.</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Người Sử Dụng sẽ
                  chịu trách nhiệm quản lý tài khoản, mật khẩu tài khoản, các
                  thông tin liên quan đến tài khoản, Biện Pháp Xác Thực, thông
                  tin thiết bị… của mình. Nếu thông tin các thông tin trên của
                  Người Sử Dụng bị mất hoặc bị lấy cắp hoặc bị tiết lộ một cách
                  bất hợp pháp, thì Người Sử Dụng phải thay đổi thông tin tài
                  khoản bằng cách sử dụng các công cụ được cài đặt sẵn trong Ứng
                  Dụng MoMo hoặc thông báo ngay cho M_Service thông qua Dịch Vụ
                  Khách Hàng để tạm ngừng Tài Khoản MoMo. Người Sử Dụng sẽ hoàn
                  toàn chịu trách nhiệm về bất kỳ và tất cả yêu cầu Giao Dịch đã
                  xảy ra trước khi M_Service nhận được thông báo đó. Người Sử
                  Dụng lưu ý rằng Tài Khoản MoMo sẽ chỉ tạm thời ngừng khi Người
                  Sử Dụng đã cung cấp mọi thông tin được yêu cầu cho Dịch Vụ
                  Khách Hàng mà Dịch Vụ Khách Hàng có thể yêu cầu một cách hợp
                  lý.
                </p>
                <p style={{ marginLeft: "40px", textAlign: "justify" }}>
                  <strong>k.</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Khi sử dụng Sản
                  Phẩm/Dịch Vụ, Người Sử Dụng chịu trách nhiệm về bất kỳ và tất
                  cả hành động cũng như sai sót của mình trong việc vận hành Ứng
                  Dụng MoMo và/hoặc thực hiện Giao Dịch. Nếu bất kỳ một sai sót
                  hay sự cố nào xảy ra, Người Sử Dụng phải liên hệ ngay với Dịch
                  Vụ Khách Hàng để được hướng dẫn. M_Service sẽ nỗ lực hết sức
                  để tư vấn và trợ giúp Người Sử Dụng.
                </p>
                <p style={{ marginLeft: "40px", textAlign: "justify" }}>
                  <strong>l.</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Trong trường hợp
                  có sự cố về Sản Phẩm/Dịch Vụ hoặc nếu một Giao Dịch không được
                  thực hiện theo yêu cầu của Người Sử Dụng, Người Sử Dụng sẽ
                  thông báo ngay cho M_Service về sự cố đó và M_Service sẽ nỗ
                  lực hết sức để tư vấn và trợ giúp Người Sử Dụng.
                </p>
                <p style={{ marginLeft: "40px", textAlign: "justify" }}>
                  <strong>m.</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>M_Service đồng ý,
                  trên cơ sở toàn quyền quyết định bồi hoàn cho bất kỳ Giao Dịch
                  nào đã được thực hiện sai do lỗi của M_Service.​
                </p>
                <h3 style={{ textAlign: "justify" }}>
                  3.2 Ngưng sử dụng Sản Phẩm/Dịch Vụ
                </h3>
                <div style={{ textAlign: "justify" }}>
                  <strong>a.</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>M_Service ngừng,
                  chấm dứt và hủy bỏ Sản Phẩm/Dịch Vụ:
                </div>
                <ul>
                  <li style={{ textAlign: "justify" }}>
                    Người Sử Dụng đồng ý, xác nhận và chấp thuận rằng Sản
                    Phẩm/Dịch Vụ (hoặc bất kỳ phần nào của Sản Phẩm/Dịch Vụ) có
                    thể được M_Service hủy bỏ vì bất kỳ lý do nào mà M_Service
                    thấy là phù hợp và cần thiết vào bất kỳ thời điểm nào mà
                    không cần thông báo trước cho Người Sử Dụng. Người Sử Dụng
                    cũng đồng ý rằng bất kỳ lý do hủy bỏ nào mà M_Service đưa ra
                    sẽ được Người Sử Dụng xem là lý do hợp lý. Sau khi hủy bỏ,
                    Sản Phẩm/Dịch Vụ (hoặc bất kỳ phần nào của Sản Phẩm/Dịch Vụ)
                    có thể được cung cấp lại bởi M_Service trên cơ sở toàn quyền
                    quyết định thấy là phù hợp.
                  </li>
                  <li style={{ textAlign: "justify" }}>
                    Người Sử Dụng đồng ý, xác nhận và chấp thuận rằng vào mọi
                    thời điểm M_Service có quyền ngừng hoặc chấm dứt Tài Khoản
                    của Người Sử Dụng hoặc khả năng tiếp cận và sử dụng Sản
                    Phẩm/Dịch Vụ (hoặc bất kỳ phần nào của Sản Phẩm/Dịch Vụ) của
                    Người Sử Dụng vì bất kỳ lý do nào mà M_Service thấy là phù
                    hợp và cần thiết, bao gồm, nhưng không giới hạn trường hợp
                    Người Sử Dụng vi phạm bất kỳ quy định nào của những Điều
                    Khoản Chung này hoặc làm trái hoặc vi phạm bất kỳ quy định,
                    luật hiện hành liên quan đến việc sử dụng Sản Phẩm/Dịch Vụ.
                    Người Sử Dụng cũng đồng ý rằng bất kỳ lý do hủy bỏ nào do
                    M_Service đưa ra sẽ được Người Sử Dụng xem là hợp lý. Việc
                    tạm ngừng cung cấp Sản Phẩm/Dịch Vụ có thể được thực hiện
                    trong bất kỳ thời điểm nào và theo bất kỳ điều kiện nào mà
                    M_Service trên cơ sở toàn quyền quyết định thấy là phù hợp.
                  </li>
                  <li style={{ textAlign: "justify" }}>
                    "Nếu Người Sử Dụng vi phạm bất kỳ quy định nào của Điều
                    Khoản Chung này hoặc làm trái hoặc vi phạm bất kỳ quy định",
                    Quy Định Pháp Luật liên quan đến việc sử dụng Sản Phẩm/Dịch
                    Vụ, M_Service có quyền ngừng Sản Phẩm/Dịch Vụ thông báo cho
                    cơ quan nhà nước có thẩm quyền và/hoặc các cá nhân, tổ chức
                    liên quan về việc làm trái hoặc vi phạm theo cách thức phù
                    hợp. Sau khi hủy bỏ hoặc chấm dứt Sản Phẩm/Dịch Vụ (hoặc bất
                    kỳ phần nào của Sản Phẩm/Dịch Vụ):
                    <ul>
                      <li>
                        Tất cả các quyền đã được trao cho Người Sử Dụng theo
                        những Điều Khoản Chung này sẽ chấm dứt ngay lập tức;
                      </li>
                      <li>
                        Người Sử Dụng phải thanh toán ngay cho M_Service mọi
                        khoản phí và lệ phí chưa trả đến hạn và còn nợ M_Service
                        (nếu có);
                      </li>
                      <li>
                        <p>
                          Người Sử Dụng tại đây ủy quyền không hủy ngang và vô
                          điều kiện cho M_Service hoàn trả số dư có trong Tài
                          Khoản MoMo (nếu có) cho Người Sử Dụng, sau khi khấu
                          trừ mọi khoản tiền (bao gồm, nhưng không giới hạn các
                          khoản phí và lệ phí) đến hạn thanh toán và còn nợ
                          M_Service (nếu có) bởi Người Sử Dụng.
                        </p>
                      </li>
                    </ul>
                  </li>
                </ul>
                <p style={{ textAlign: "justify" }}>
                  <strong>b.</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Người Sử Dụng chấm
                  dứt sử dụng Sản Phẩm/Dịch Vụ:
                </p>
                <p style={{ textAlign: "justify" }}>
                  Người Sử Dụng có thể chấm dứt việc sử dụng Sản Phẩm/Dịch Vụ
                  của mình căn cứ theo những Điều Khoản Chung này vào bất kỳ
                  thời điểm nào bằng cách đến các Điểm Giao Dịch của M_Service
                  hoặc liên hệ với Dịch Vụ Khách Hàng để được hướng dẫn.
                </p>
                <ul>
                  <li style={{ textAlign: "justify" }}>
                    Sau khi hủy bỏ hoặc chấm dứt sử dụng Sản Phẩm/Dịch Vụ (hoặc
                    bất kỳ phần nào của Sản Phẩm/Dịch Vụ):
                    <ul>
                      <li style={{ textAlign: "justify" }}>
                        ​Tất cả các quyền đã được trao cho Người Sử Dụng theo
                        những Điều Khoản Chung này liên quan đến Sản Phẩm/Dịch
                        Vụ đã chấm dứt sẽ chấm dứt ngay lập tức;
                      </li>
                      <li style={{ textAlign: "justify" }}>
                        Người Sử Dụng phải thanh toán ngay cho M_Service mọi
                        khoản phí và lệ phí chưa trả đến hạn và còn nợ M_Service
                        liên quan đến Sản Phẩm/Dịch Vụ đã chấm dứt (nếu có);
                      </li>
                      <li style={{ textAlign: "justify" }}>
                        Trong trường hợp tất cả các Sản Phẩm/Dịch Vụ đều đã được
                        chấm dứt, Người Sử Dụng tại đây ủy quyền không hủy ngang
                        và vô điều kiện cho M_Service hoàn lại số dư có trong
                        Tài Khoản MoMo (nếu có) cho Người Sử Dụng, sau khi khấu
                        trừ mọi khoản tiền (bao gồm, nhưng không giới hạn các
                        khoản phí và lệ phí) đến hạn và còn nợ M_Service (nếu
                        có) bởi Người Sử Dụng.​
                      </li>
                    </ul>
                  </li>
                </ul>
                <h2 style={{ textAlign: "justify" }}>
                  <strong>
                    4.<span style={{ whiteSpace: "pre" }}> </span>Quyền sở hữu
                    trí tuệ
                  </strong>
                </h2>
                <p style={{ textAlign: "justify" }}>
                  <strong>4.1.</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Tất cả các nhãn
                  hiệu hàng hóa, logo, nhãn hiệu dịch vụ và tất cả các quyền sở
                  hữu trí tuệ khác thuộc bất kỳ loại nào (cho dù đã được đăng ký
                  hay chưa), và tất cả các nội dung thông tin, thiết kế, tài
                  liệu, đồ họa, phần mềm, hình ảnh, video, âm nhạc, âm thanh,
                  phức hợp phần mềm, mã nguồn và phần mềm cơ bản liên quan đến
                  M_Service (gọi chung là “Quyền Sở Hữu Trí Tuệ”) là tài sản và
                  luôn luôn là tài sản của M_Service và các tổ chức/đại lý khác
                  được ủy quyền bởi M_Service (nếu có). Tất cả các Quyền Sở Hữu
                  Trí Tuệ được bảo hộ bởi Quy Định Pháp Luật về bản quyền và các
                  công ước quốc tế. Tất cả các quyền đều được bảo lưu.
                </p>
                <p style={{ textAlign: "justify" }}>
                  <strong>4.2.</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Ngoại trừ được cho
                  phép rõ ràng trong những Điều Khoản Chung này, Người Sử Dụng
                  không được sử dụng, biên tập, công bố, mô phỏng, dịch, thực
                  hiện các sản phẩm phái sinh từ, phân phát hoặc bằng cách khác
                  sử dụng, tái sử dụng, sao chép, sửa đổi, hoặc công bố Quyền Sở
                  Hữu Trí Tuệ theo bất kỳ cách thức nào mà không có sự chấp
                  thuận trước bằng văn bản của M_Service. Người Sử Dụng không
                  được trợ giúp hoặc tạo điều kiện cho bất kỳ bên thứ ba nào sử
                  dụng Quyền Sở Hữu Trí Tuệ theo bất kỳ cách thức nào mà cấu
                  thành một vi phạm về sở hữu trí tuệ và/hoặc đối với các quyền
                  liên quan khác của M_Service.
                </p>
                <h2 style={{ textAlign: "justify" }}>
                  <strong>5. Bồi hoàn</strong>
                </h2>
                <div style={{ textAlign: "justify" }}>
                  Người Sử Dụng đồng ý bồi hoàn cho M_Service và các bên liên
                  quan của M_Service và đảm bảo cho họ không bị thiệt hại bởi
                  mọi tổn thất, khiếu nại, yêu cầu, khiếu kiện, thủ tục tố tụng,
                  chi phí (bao gồm, nhưng không giới hạn, các chi phí pháp lý)
                  và các trách nhiệm có thể phải gánh chịu hoặc đưa ra đối với
                  M_Service và/hoặc các nhân viên, cán bộ… của M_Service phát
                  sinh từ hoặc liên quan đến:
                </div>
                <ul>
                  <li style={{ textAlign: "justify" }}>
                    Việc Người Sử Dụng sử dụng Sản Phẩm/Dịch Vụ (hoặc bất kỳ
                    phần nào của Sản Phẩm/Dịch Vụ);
                  </li>
                  <li style={{ textAlign: "justify" }}>
                    Việc Người Sử Dụng vi phạm những Điều Khoản Chung này.
                  </li>
                </ul>
                <h2 style={{ textAlign: "justify" }}>
                  <strong>6. Tiết Lộ Thông Tin</strong>
                </h2>
                <div style={{ textAlign: "justify" }}>
                  <strong>6.1</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Người Sử Dụng đồng
                  ý rằng M_Service có thể thu thập, lưu trữ, sử dụng và xử lý
                  các thông tin về Hồ Sơ Mở Tài Khoản MoMo cũng như các thông
                  tin khác từ Người Sử Dụng hoặc các bên thứ ba để phục vụ cho
                  mục đích nhận biết khách hàng và xác thực theo Quy Định Pháp
                  Luật. M_Service cũng có thể thu thập, lưu trữ, sử dụng và xử
                  lý thông tin cá nhân của Người Sử Dụng cho mục đích nghiên cứu
                  và phân tích hoạt động và cải tiến Sản Phẩm/Dịch Vụ.
                </div>
                <div style={{ textAlign: "justify" }}>&nbsp;</div>
                <div style={{ textAlign: "justify" }}>
                  <strong>6.2</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Khi Người Sử Dụng
                  đăng ký một Tài Khoản MoMo để sử dụng Sản Phẩm/Dịch Vụ, Người
                  Sử Dụng hiểu và đồng ý cấp quyền cho M_Service thu thập, lưu
                  giữ, sử dụng và xử lý thông tin thông qua việc truy cập vào
                  các ứng dụng sau trên thiết bị di động theo những Điều Khoản
                  Chung này.
                </div>
                <ul>
                  <li style={{ textAlign: "justify" }}>
                    Vị trí: lấy thông tin về vị trí để hiện thị danh sách các
                    điểm giao dịch gần nhất.
                  </li>
                  <li style={{ textAlign: "justify" }}>
                    Trạng thái kết nối: để đảm bảo các tính năng trực tuyến của
                    Ứng Dụng MoMo hoạt động đúng.
                  </li>
                  <li style={{ textAlign: "justify" }}>
                    Máy chụp hình: cho phép Người Sử Dụng dùng để quét mã QR.
                  </li>
                  <li style={{ textAlign: "justify" }}>
                    Trạng thái kết nối: để đảm bảo các tính năng trực tuyến của
                    Ứng Dụng MoMo hoạt động đúng.
                  </li>
                </ul>
                <div style={{ textAlign: "justify" }}>
                  <strong>6.3</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Người Sử Dụng chấp
                  thuận, ủy quyền không hủy ngang và vô điều kiện cho M_Service
                  tiết lộ hoặc công bố các thông tin liên quan đến Người Sử Dụng
                  hoặc các Giao Dịch của Người Sử Dụng với các cá nhân hoặc tổ
                  chức mà M_Service có thể được yêu cầu tiết lộ theo bất kỳ Quy
                  Định Pháp Luật hoặc quy định nào áp dụng đối với M_Service
                  hoặc căn cứ theo bất kỳ yêu cầu hoặc lệnh nào của bất kỳ cơ
                  quan nhà nước có thẩm quyền nào hoặc lệnh của tòa
                  án.&nbsp;&nbsp;
                </div>
                <div style={{ textAlign: "justify" }}>&nbsp;</div>
                <p style={{ textAlign: "justify" }}>
                  <strong>6.4&nbsp;</strong>Người Sử Dụng đồng ý với&nbsp;Chính
                  Sách Quyền Riêng Tư&nbsp;của M_Service được quy định{" "}
                  <a
                    href="javascript:WebApi.openURL('https://app.momo.vn/icon/momo_app_v2/html/privacy_policy/index.html')"
                    target="_blank"
                    className="external"
                  >
                    tại đây
                  </a>
                  .
                </p>
                <h2 style={{ textAlign: "justify" }}>
                  <strong>7. Giới Hạn Trách Nhiệm</strong>
                </h2>
                <div style={{ textAlign: "justify" }}>
                  <strong>
                    7.1<span style={{ whiteSpace: "pre" }}> </span>
                  </strong>
                  Trong mọi trường hợp M_Service (bao gồm cả các nhân viên, Điểm
                  Giao Dịch, cán bộ hoặc các bên liên kết của M_Service) sẽ
                  không chịu trách nhiệm đối với Người Sử Dụng về bất kỳ tổn
                  thất, thiệt hại, trách nhiệm và chi phí nào theo bất kỳ nguyên
                  nhân hành động nào gây ra bởi việc sử dụng, hoặc không có khả
                  năng sử dụng Sản Phẩm/Dịch Vụ trừ khi M_Service (bao gồm cả
                  các nhân viên, Điểm Giao Dịch, cán bộ hoặc các bên liên kết
                  của M_Service) có lỗi trong việc để xảy ra tổn thất, thiệt
                  hai.
                </div>
                <div style={{ textAlign: "justify" }}>&nbsp;</div>
                <div style={{ textAlign: "justify" }}>
                  <strong>7.2</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Tuy nhiên, trong
                  trường hợp M_Service (bao gồm cả các nhân viên, Điểm Giao
                  Dịch, cán bộ hoặc các bên liên kết của M_Service) phải chịu
                  trách nhiệm về các tổn thất hoặc thiệt hại theo quy định nêu
                  trên đây, thì Người Sử Dụng đồng ý rằng toàn bộ trách nhiệm
                  của M_Service (bao gồm cả các nhân viên, Điểm Giao Dịch, cán
                  bộ hoặc các bên liên kết của M_Service) sẽ được giới hạn ở số
                  tiền thực tế của các thiệt hại trực tiếp phải gánh chịu bởi
                  Người Sử Dụng và trong bất kỳ trường hợp nào sẽ không vượt quá
                  tổng số tiền được chuyển vào và chuyển ra từ Tài Khoản MoMo
                  của Người Sử Dụng.
                </div>
                <div style={{ textAlign: "justify" }}>&nbsp;</div>
                <div style={{ textAlign: "justify" }}>
                  <strong>7.3</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Trong mọi trường
                  hợp M_Service sẽ không chịu trách nhiệm về bất kỳ thiệt hại
                  gián tiếp, đặc biệt, do hệ quả hoặc sự kiện ngẫu nhiên nào
                  phát sinh hoặc bắt nguồn từ việc sử dụng, hoặc không có khả
                  năng sử dụng Sản Phẩm/Dịch Vụ.
                </div>
                <div style={{ textAlign: "justify" }}>&nbsp;</div>
                <div style={{ textAlign: "justify" }}>
                  <strong>7.4</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Trong phạm vi mà
                  Quy Định Pháp Luật cho phép, Người Sử Dụng đồng ý rằng
                  M_Service (bao gồm cả các nhân viên, Điểm Giao Dịch, cán bộ
                  hoặc các bên liên kết của M_Service) sẽ không chịu trách nhiệm
                  về bất kỳ tổn thất, thiệt hại, trách nhiệm và/hoặc chi phí nào
                  mà Người Sử Dụng phải gánh chịu do việc Người Sử Dụng hoặc một
                  bên thứ ba khác truy cập trái phép vào máy chủ, giao diện của
                  M_Service, trang Web của M_Service, thiết bị và/hoặc dữ liệu
                  của Người Sử Dụng dù là vô tình hoặc bằng cách thức không hợp
                  pháp hoặc không được phép như xâm nhập trái phép hoặc các lý
                  do khác nằm ngoài tầm kiểm soát của M_Service (ví dụ như vi
                  rút máy tính).
                </div>
                <div style={{ textAlign: "justify" }}>&nbsp;</div>
                <div style={{ textAlign: "justify" }}>
                  <strong>7.5</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>M_Service sẽ không
                  chịu trách nhiệm về việc không thực hiện hoặc chậm thực hiện
                  các nghĩa vụ của mình theo những Điều Khoản Chung này do các
                  sự kiện bất khả kháng nằm ngoài tầm kiểm soát hợp lý của
                  M_Service, bao gồm, nhưng không giới hạn, thiên tai, bão tố,
                  mưa dông, bùng nổ vi rút, các hạn chế của chính phủ, đình
                  công, chiến tranh, hỏng mạng hoặc hỏng mạng viễn thông hoặc
                  các sự kiện bất khả kháng khác theo quy định của pháp luật
                  hoặc được công nhận bởi hai bên.
                </div>
                <h2 style={{ textAlign: "justify" }}>
                  <strong>8. An Ninh</strong>
                </h2>
                <div style={{ textAlign: "justify" }}>
                  <strong>8.1</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Người Sử Dụng cam
                  kết sẽ chỉ sử dụng Ứng Dụng MoMo và Tài Khoản MoMo cho các
                  hoạt động hợp pháp, không thực hiện các giao dịch thanh toán,
                  chuyển tiền bất hợp pháp, bao gồm nhưng không giới hạn các
                  hành vi đánh bạc,thanh toán các hàng hóa, dịch vụ bất hợp pháp
                  hay rửa tiền, và sẽ thông báo ngay lập tức cho M_Service về
                  (các) giao dịch có dấu hiệu sử dụng trái phép Tài Khoản MoMo
                  của Người Sử Dụng và/hoặc Sản Phẩm/Dịch Vụ đã biết hoặc nghi
                  vấn, hoặc bất kỳ vi phạm an ninh nào đã biết hoặc nghi vấn, kể
                  cả việc mất thông tin, lấy cắp thông tin hoặc tiết lộ không
                  được phép về thông tin cá nhân hoặc Tài Khoản của Người Sử
                  Dụng.
                </div>
                <div style={{ textAlign: "justify" }}>&nbsp;</div>
                <div style={{ textAlign: "justify" }}>
                  <strong>8.2</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Người Sử Dụng cam
                  kết không sử dụng, và không cho bất kỳ bên thứ ba nào khác sử
                  dụng, Ứng Dụng MoMo và Tài Khoản MoMo để thực hiện các hành vi
                  xâm nhập trái phép, tấn công hệ thống, phát tán virus và phần
                  mềm độc hại và các hành vi vi phạm Quy Định Pháp Luật hoặc gây
                  gián đoạn, cản trở hoạt động bình thường đối với hệ thống của
                  M_Service và các đối tác của M_Service, hoặc đối với việc sử
                  dụng Ứng Dụng MoMo, Tài Khoản MoMo của người khác.
                </div>
                <div style={{ textAlign: "justify" }}>&nbsp;</div>
                <div style={{ textAlign: "justify" }}>
                  <strong>8.3</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Người Sử Dụng chịu
                  trách nhiệm và miễn trừ toàn bộ trách nhiệm cho M_Service về
                  việc sử dụng hoặc hoạt động trên Tài Khoản MoMo của Người Sử
                  Dụng trước pháp luật. Bất kỳ hoạt động gian lận, lừa gạt hoặc
                  hoạt động bất hợp pháp khác có thể là căn cứ cho phép
                  M_Service tạm ngừng Tài Khoản MoMo và/hoặc chấm dứt Sản
                  Phẩm/Dịch Vụ được cung cấp cho Người Sử Dụng, theo toàn quyền
                  quyết định của M_Service, và M_Service có thể trình báo về
                  hành vi của Người Sử Dụng với các cơ quan nhà nước có thẩm
                  quyền để xem xét xử lý.
                </div>
                <h2 style={{ textAlign: "justify" }}>
                  <strong>9. Thông Tin Liên Lạc và Thông Báo</strong>
                </h2>
                <div style={{ textAlign: "justify" }}>
                  <strong>9.1&nbsp;</strong>Người Sử Dụng đồng ý rằng M_Service
                  hoặc các bên liên kết của M_Service có thể gửi tin nhắn hoặc
                  gọi điện thoại cho Người Sử Dụng thông qua số điện thoại hoặc
                  thông báo qua Ứng Dụng MoMo về hoặc liên quan đến các thông
                  tin cập nhật về Sản Phẩm/Dịch Vụ và các sự kiện được đưa ra
                  hoặc cung cấp bởi M_Service hoặc các bên liên kết của
                  M_Service. Người Sử Dụng đồng ý rằng các thông báo được gửi
                  qua hình thức tin nhắn hoặc cuộc gọi hoặc qua Ứng Dụng MoMo
                  nêu tại điều này sẽ không bị giới hạn về số lượng và thời gian
                  và có giá trị đầy đủ như một thông báo chính thức của
                  M_Service đến Người Sử Dụng với điều kiện đảm bảo tuân thủ đầy
                  đủ các quy định của pháp luật.
                </div>
                <div style={{ textAlign: "justify" }}>&nbsp;</div>
                <div style={{ textAlign: "justify" }}>
                  9.2<span style={{ whiteSpace: "pre" }}> </span>Người Sử Dụng
                  đồng ý rằng M_Service không có bất kỳ nghĩa vụ nào trong việc
                  đưa ra thông báo định kỳ cho Người Sử Dụng về chi tiết các
                  Giao Dịch được tiến hành bởi Người Sử Dụng.
                </div>
                <div style={{ textAlign: "justify" }}>&nbsp;</div>
                <div style={{ textAlign: "justify" }}>
                  <strong>9.3</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Mọi thông báo và
                  tài liệu (nếu có) cần phải được đưa ra bởi Người Sử Dụng theo
                  những Điều Khoản Chung này cho M_Service sẽ được gửi cho
                  M_Service đến địa chỉ trụ sở hoặc thông qua Dịch Vụ Khách hàng
                  của M_Service.
                </div>
                <div style={{ textAlign: "justify" }}>&nbsp;</div>
                <div style={{ textAlign: "justify" }}>
                  <strong>9.4</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Tất cả các thông
                  báo và tài liệu (nếu có) cần gửi cho Người Sử Dụng bởi
                  M_Service theo những Điều Khoản Chung này sẽ được gửi bằng một
                  trong những phương thức sau đây:
                </div>
                <ul>
                  <li style={{ textAlign: "justify" }}>
                    Gửi thư thường hoặc thư bảo đảm đến địa chỉ mới nhất của
                    Người Sử Dụng theo Hồ Sơ Mở Tài Khoản MoMo của Người Sử Dụng
                    tại M_Service;
                  </li>
                  <li style={{ textAlign: "justify" }}>
                    Gửi thư điện tử đến địa chỉ thư điện tử mới nhất của Người
                    Sử Dụng theo ghi chép của M_Service;
                  </li>
                  <li style={{ textAlign: "justify" }}>
                    Công bố thông báo hoặc thông tin liên lạc trên trang web{" "}
                    <a
                      href="javascript:WebApi.openURL('https://momo.vn/')"
                      className="external"
                    >
                      momo.vn
                    </a>
                    .
                  </li>
                  <li style={{ textAlign: "justify" }}>
                    Gửi tin nhắn (SMS) hoặc gọi điện đến số điện thoại mới nhất
                    của Người Sử Dụng theo ghi chép của M_Service.
                  </li>
                </ul>
                <div style={{ textAlign: "justify" }}>
                  <strong>
                    9.5<span style={{ whiteSpace: "pre" }}> </span>
                  </strong>
                  Bất kỳ thông báo hoặc tài liệu hoặc thư từ liên lạc nào được
                  xem là đã được gửi và nhận:
                </div>
                <ul>
                  <li style={{ textAlign: "justify" }}>
                    Nếu được gửi bằng thư thường hoặc thư bảo đảm, trong vòng ba
                    (3) Ngày Làm Việc kể từ ngày gửi;
                  </li>
                  <li style={{ textAlign: "justify" }}>
                    Nếu được gửi bằng các hình thức khác được quy định tại điều
                    này, Ngày Làm Việc sau ngày gửi thông báo hoặc tài liệu đó.
                  </li>
                </ul>
                <h2 style={{ textAlign: "justify" }}>
                  <strong>
                    10.<span style={{ whiteSpace: "pre" }}> </span>Xử lý các
                    giao dịch có nhầm lẫn, sự cố kỹ thuật hoặc dấu hiệu vi phạm
                    pháp luật
                  </strong>
                </h2>
                <p style={{ textAlign: "justify" }}>
                  <strong>10.1</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Trong mọi trường
                  hợp giá trị tiền được nạp vào Tài Khoản MoMo của Người Sử Dụng
                  chênh lệch với giá trị thực tế mà Người Sử Dụng yêu cầu và đã
                  thực hiện nạp, Người Sử Dụng có nghĩa vụ phải hoàn trả lại cho
                  M_Service hoặc người gửi tiền theo cách thức và trong thời hạn
                  được yêu cầu.&nbsp;
                </p>
                <p style={{ textAlign: "justify" }}>
                  <strong>10.2</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Trong trường hợp
                  M_Service có cơ sở xác định một giao dịch được thực hiện do
                  nhầm lẫn, do sự cố kỹ thuật hoặc có dấu hiệu vi phạm pháp
                  luật, M_Service tùy theo quyết định của riêng mình có quyền
                  thực hiện các biện pháp phòng ngừa, ngăn chặn nhằm giảm thiểu
                  các thiệt hại có thể phát sinh, bao gồm nhưng không giới hạn
                  các biện pháp sau:
                </p>
                <ul>
                  <li style={{ textAlign: "justify" }}>
                    Khoanh giữ, đóng băng khoản tiền phát sinh từ hoặc liên quan
                    đến giao dịch trong Tài Khoản MoMo của Người Sử Dụng;
                    hoặc/và
                  </li>
                  <li style={{ textAlign: "justify" }}>
                    Tạm ngừng hoạt động Tài Khoản MoMo của Người Sử Dụng;
                    hoặc/và
                  </li>
                  <li style={{ textAlign: "justify" }}>
                    Khấu trừ, thu hồi từ Tài Khoản MoMo của Người Sử Dụng khoản
                    tiền phát sinh do nhầm lẫn và/hoặc sự cố kỹ thuật; hoặc/và
                  </li>
                  <li style={{ textAlign: "justify" }}>
                    Báo cáo cơ quan nhà nươc có thẩm quyền để được hướng dẫn xử
                    lý đối với giao dịch và/hoặc khoản tiền phát sinh từ hoặc có
                    liên quan đến nhầm lẫn, sự cố kỹ thuật hoặc vi phạm pháp
                    luật.
                  </li>
                </ul>
                <p style={{ textAlign: "justify" }}>
                  <strong>10.3</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Người Sử Dụng đồng
                  ý rằng M_Service có quyền thực hiện các biện pháp quy định tại
                  Điều 10.2 nêu trên và sẽ cam kết hợp tác đầy đủ với M_Service
                  trong việc xác minh, giải quyết các vấn đề liên quan đến giao
                  dịch có nhầm lẫn, sự cố kỹ thuật hoặc dấu hiệu vi phạm pháp
                  luật. M_Service có quyền tạm khóa, phong tỏa Tài Khoản MoMo
                  và/hoặc khoanh giữ, đóng băng khoản tiền liên quan đến giao
                  dịch để xác minh, làm rõ và phòng ngừa, ngăn chặn thiệt hại có
                  thể xảy ra.
                </p>
                <p style={{ textAlign: "justify" }}>
                  <strong>10.4</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Trong trường hợp
                  Người Sử Dụng không hợp tác với M_Service hoặc có biểu hiện
                  chiếm đoạt, tẩu tán khoản tiền phát sinh từ hoặc liên quan đến
                  nhầm lẫn, sự cố kỹ thuật hoặc vi phạm pháp luật, M_Service có
                  quyền duy trì các biện pháp phòng ngừa, ngăn chặn nêu tại Điều
                  10.2 và yêu cầu cơ quan có thẩm quyền xử lý, bao gồm cả xử lý
                  hình sự trong trường hợp có dấu hiệu tội phạm.&nbsp; Trong mọi
                  trường hợp, M_Service sẽ thực hiện theo kết luận, quyết định
                  hoặc phán quyết cuối cùng của cơ quan nhà nước có thẩm
                  quyền.&nbsp;
                </p>
                <p style={{ textAlign: "justify" }}>
                  <strong>10.5</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Trong trường hợp
                  M_Service có cơ sở xác định Người Sử Dụng có hành vi lạm dụng
                  các chương trình khuyến mại, chính sách ưu đãi, hỗ trợ người
                  dùng của M_Service, M_Service có quyền tạm ngừng hoặc chấm dứt
                  các chương trình, chính sách ưu đãi, hỗ trợ đó với một, hoặc
                  một số hoặc toàn bộ Người Sử Dụng có liên quan và thực hiện
                  các biện pháp cần thiết để thu hồi các ưu đãi, hỗ trợ và lợi
                  ích kinh tế mà Người Sử Dụng liên quan đã nhận được. Trong
                  trường hợp hành vi lạm dụng có yếu tố vi phạm Quy Định Pháp
                  Luật, M_Service sẽ thông báo với cơ quan nhà nước có thẩm
                  quyền để xem xét xử lý.
                </p>
                <h2 style={{ textAlign: "justify" }}>
                  <strong>11. Các quy định khác</strong>
                </h2>
                <p style={{ textAlign: "justify" }}>
                  <strong>11.1</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Việc M_Service
                  không thực hiện hoặc áp dụng bất kỳ quyền hoặc biện pháp nào
                  mà M_Service có theo quy định tại Điều Khoản Chung này hoặc
                  theo Quy Định Pháp Luật không bị xem là M_Service từ bỏ hoặc
                  hạn chế quyền hoặc biện pháp đó, và M_Service bảo lưu quyền
                  thực hiện quyền hoặc biện pháp đó vào bất kỳ thời điểm nào
                  M_Service nhận thấy thích hợp.
                </p>
                <p style={{ textAlign: "justify" }}>
                  <strong>11.2</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Trong trường hợp
                  bất kỳ quy định nào của những Điều Khoản Chung này được xác
                  định là bất hợp pháp hoặc không thể thực thi bằng cách khác
                  thì M_Service sẽ sửa đổi quy định đó, hoặc (theo toàn quyền
                  quyết định của mình) bỏ quy định đó ra khỏi những Điều Khoản
                  Chung này. Nếu bất kỳ quy định nào của những Điều Khoản Chung
                  này được xác định là bất hợp pháp hoặc không thể thực thi,
                  việc xác định như vậy sẽ không ảnh hưởng đến các quy định còn
                  lại của những Điều Khoản Chung này, và những Điều Khoản Chung
                  này sẽ vẫn có đầy đủ hiệu lực.
                </p>
                <p style={{ textAlign: "justify" }}>
                  <strong>11.3</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Người Sử Dụng xác
                  nhận rằng M_Service, theo các luật và quy định hiện hành hoặc
                  sau khi nhận được chỉ thị của các cơ quan hữu quan chính phủ,
                  có thể được yêu cầu thực hiện các hành động mà có thể vi phạm
                  các quy định của những Điều Khoản Chung này. Về vấn đề này,
                  Người Sử Dụng đồng ý không buộc M_Service phải chịu trách
                  nhiệm.
                </p>
                <p style={{ textAlign: "justify" }}>
                  <strong>11.4</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Người Sử Dụng
                  không được chuyển nhượng các quyền của mình theo những Điều
                  Khoản Chung này nếu không có sự chấp thuận trước bằng văn bản
                  của M_Service. M_Service có thể chuyển nhượng các quyền của
                  mình theo những Điều Khoản Chung này mà không cần có sự chấp
                  thuận trước bằng văn bản của Người Sử Dụng.
                </p>
                <p style={{ textAlign: "justify" }}>
                  <strong>11.5</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Những Điều Khoản
                  Chung này sẽ có giá trị ràng buộc đối với những người thừa kế,
                  các đại diện cá nhân và đại diện theo pháp luật, các bên kế
                  nhiệm quyền sở hữu và các bên nhận chuyển nhượng được phép về
                  tài sản (nếu có) của Người Sử Dụng.
                </p>
                <p style={{ textAlign: "justify" }}>
                  <strong>11.6</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Bất kỳ tranh chấp
                  hoặc bất đồng nào theo những Điều Khoản Chung này trước hết sẽ
                  được giải quyết thông qua thương lượng hòa giải. Nếu không đạt
                  được thỏa thuận thông qua thương lượng hòa giải như vậy, các
                  bên tại đây đồng ý sẽ đưa tranh chấp hoặc bất đồng lên tòa án
                  có thẩm quyền tại Thành Phố Hồ Chí Minh để giải quyết.
                </p>
                <p style={{ textAlign: "justify" }}>
                  <strong>11.7</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>M_Service không
                  thực hiện hoặc áp dụng bất kỳ quyền hoặc biện pháp nào mà
                  M_Service có theo quy định tại Điều Khoản Chung này hoặc theo
                  Quy Định Pháp Luật không bị xem là M_Service từ bỏ hoặc hạn
                  chế quyền hoặc biện pháp đó và M_Service bảo lưu việc thực
                  hiện quyền hoặc biện pháp đó vào bất kỳ thời điểm nào mà
                  M_Service nhận thấy phù hợp.
                </p>
                <p style={{ textAlign: "justify" }}>
                  <strong>11.8</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Tranh chấp giữa
                  Người Sử Dụng và bên thứ ba: M_Service không có bất cứ trách
                  nhiệm liên quan nào mà chỉ đóng vai trò hỗ trợ Người Sử Dụng,
                  cung cấp thông tin cần thiết để Người Sử Dụng và bên thứ ba
                  liên quan để cùng giải quyết. Người Sử Dụng và bên thứ ba phải
                  trực tiếp giải quyết mọi vấn đề liên quan đến giao dịch của
                  Người Sử Dụng và bên thứ ba. Trong trường hợp có khiếu nại,
                  tranh chấp, yêu cầu hoàn tiền… M_Service có toàn quyền tạm
                  giữ/đóng băng các khoản tiền trong tài khoản có liên quan cho
                  đến khi vấn đề được giải quyết hoặc có quyết định cuối cùng
                  của cơ quan nhà nước có thẩm quyền.
                </p>
                <p style={{ textAlign: "justify" }}>
                  <strong>11.9</strong>
                  <span style={{ whiteSpace: "pre" }}> </span>Việc M_Service
                  không thực hiện hoặc áp dụng bất kỳ quyền hoặc biện pháp nào
                  mà M_Service có theo quy định tại Điều Khoản Chung nay hoặc
                  theo Quy Định Pháp Luật không được xem là M_Service từ bỏ hoặc
                  hạn chế quyền hoặc biện pháp đó, và M_Service bảo lưu quyền
                  thực hiện quyền hoặc biện pháp đó vào bất kỳ thời điểm nào
                  M_Service nhận thấy thích hợp.
                </p>
                <p style={{ textAlign: "justify" }}>
                  Bằng việc xác nhận/đánh dấu vào mục dưới đây, Người Sử Dụng
                  xác nhận đã đọc, hiểu đầy đủ và đồng ý hoàn toàn với các nội
                  dung của Điều Khoản Chung này.
                </p>
              </div>
              <div className="js-sale-more-btn">+ XEM THÊM</div>
            </div>
          </div>
        </div>
        <div className="dona-rule-check">
          <div className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              defaultValue
              id="defaultCheck1"
            />
            <label className="form-check-label" htmlFor="defaultCheck1">
              Tôi đồng ý với các điều khoản của MoMo và chính sách bảo mật của
              MoMo
            </label>
          </div>
        </div>
      </div>

      <style jsx>{`
        .dona-rule-check {
          position: sticky;
          bottom: 0;
          background: #f7f7f8;
        }
      `}</style>
    </Page>
  );
};

export default Rule;

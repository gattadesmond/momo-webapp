import React, { useState, useEffect } from "react";
import {
  f7,
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  Sheet,
  Icon,
  Toolbar,
  Button,
  Block,
} from "framework7-react";
import $ from "dom7";
import format from "number-format.js";

import "./referral.less";

const ReferralFriendPage = (props) => {
  const pageInit = () => {};

  const pageDestroy = () => {};

  return (
    <>
      <Page
        onPageInit={pageInit}
        onPageBeforeRemove={pageDestroy}
        className="bg-light mm-type"
      >
        <div className="pron-container balance">
          <h2 className="text-left mt-3 mb-2">Nhiệm vụ giới thiệu MoMo</h2>
          <p>Bạn ơi! Cùng làm nhiệm vụ nhận quà cực cool nào</p>
        </div>

        <div className="refFeature-card">
          <div className="refFeature-content">
            <div className="pron-row mb-3 align-items-center">
              <div className="pron-col">
                <h2 className="mt-0 mb-0">Con người công chúng</h2>
              </div>
              <div className="pron-col auto">
                <div className="refFeature-step">0/3</div>
              </div>
            </div>
            <ul className="refFeature-list">
              <li>
                <div className="refFeature-list-icon green">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={24}
                    height={24}
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-star"
                  >
                    <polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2" />
                  </svg>
                </div>
                <span>Mục tiêu: </span>
                Hướng dẫn 02 người bạn liên kết ngân hàng
              </li>
              <li>
                <div className="refFeature-list-icon red">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={24}
                    height={24}
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-gift"
                  >
                    <polyline points="20 12 20 22 4 22 4 12" />
                    <rect x={2} y={7} width={20} height={5} />
                    <line x1={12} y1={22} x2={12} y2={7} />
                    <path d="M12 7H7.5a2.5 2.5 0 0 1 0-5C11 2 12 7 12 7z" />
                    <path d="M12 7h4.5a2.5 2.5 0 0 0 0-5C13 2 12 7 12 7z" />
                  </svg>
                </div>
                <span>Phần thưởng: </span>
                01 mã giảm 20.000đ mua thẻ điện thoại
              </li>
            </ul>
            <div className="accordion-item">
              <div className="accordion-item-content">
                <div className="mt-1 mb-4">
                  <h4 className="mt-0">Gợi ý nhiệm vụ :</h4>
                  <img
                    src="https://static.mservice.io/img/s500x240/momo-upload-api-201105135734-637401814546973263.jpg"
                    className="img-fluid d-block mx-auto mb-3 rounded"
                    alt=""
                  />
                  <div className="mm-space-title">
                    <h3 className="h5 mb-1 mt-0 mm-space-name">
                      Tiến trình giới thiệu bạn bè
                    </h3>
                  </div>
                  <div className="lutie-14 text-gray mb-3">
                    Hiện đang có <strong className="text-momo">3</strong> người
                    đang nhập mã giới thiệu của bạn
                  </div>
                  <div className="contact__list">
                    <div className="contact__item shadow-1">
                      <div className="contact__grid">
                        <div className="contact__avatar">K</div>
                        <div className="contact__info">
                          <div className="contact__name text-truncate">
                            Soju Gatta
                          </div>
                          <div className="contact__phone">0468 455 234</div>
                        </div>
                        <div className="contact__function">
                          <a href>
                            <i className="f7-icons size-14">chat_bubble</i>
                          </a>
                          <a href>
                            <i className="f7-icons size-14">phone</i>
                          </a>
                        </div>
                      </div>
                      <div className="text-primary mt-2 mb-1 lutie-14">
                        Đã nhập mã thành công, còn 2 bước
                      </div>
                      <div className="process-step">
                        <div className="process-step__item is-active" />
                        <div className="process-step__item" />
                        <div className="process-step__item" />
                      </div>
                      <div className="lutie-13 mt-2">
                        <span className="text-gray">Bước tiếp theo : </span>{" "}
                        Liên kết tài khoản ngân hàng Ví MoMo
                      </div>
                    </div>
                    <div className="contact__item shadow-1">
                      <div className="contact__grid">
                        <div className="contact__avatar">MD</div>
                        <div className="contact__info">
                          <div className="contact__name text-truncate">
                            Soju Gatta
                          </div>
                          <div className="contact__phone">0468 455 234</div>
                        </div>
                        <div className="contact__function">
                          <a href>
                            <i className="f7-icons size-14">chat_bubble</i>
                          </a>
                          <a href>
                            <i className="f7-icons size-14">phone</i>
                          </a>
                        </div>
                      </div>
                      <div className="text-primary mt-2 mb-1 lutie-14">
                        Đã nhập mã thành công, còn 2 bước
                      </div>
                      <div className="process-step">
                        <div className="process-step__item is-active" />
                        <div className="process-step__item" />
                        <div className="process-step__item" />
                      </div>
                      <div className="lutie-13 mt-2">
                        <span className="text-gray">Bước tiếp theo : </span>{" "}
                        Liên kết tài khoản ngân hàng Ví MoMo
                      </div>
                    </div>
                    <div className="contact__item shadow-1">
                      <div className="contact__grid">
                        <div className="contact__avatar">R</div>
                        <div className="contact__info">
                          <div className="contact__name text-truncate">
                            Soju Gatta
                          </div>
                          <div className="contact__phone">0468 455 234</div>
                        </div>
                        <div className="contact__function">
                          <a href className="message">
                            <img src="/static/images/referral/chat.svg" alt="" />
                          </a>
                          <a href className="phone">
                            <img src="/static/images/referral/telephone.svg" alt="" />
                          </a>
                        </div>
                      </div>
                      <div className="text-primary mt-2 mb-1 lutie-14">
                        Đã liên kết thành công, còn 1 bước
                      </div>
                      <div className="process-step">
                        <div className="process-step__item is-active" />
                        <div className="process-step__item is-active" />
                        <div className="process-step__item" />
                      </div>
                      <div className="lutie-13 mt-2">
                        <span className="text-gray">Bước tiếp theo : </span>{" "}
                        Liên kết tài khoản ngân hàng Ví MoMo
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="accordion-item-toggle">
                <div className="text-right">
                  <button className="col button color-pink button-fill font-weight-bold w-auto px-3 open">
                    Xem gợi ý
                  </button>
                  <button className="col button color-pink button-fill font-weight-bold w-auto px-3 close">
                    Thu gọn
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="refFeature-card">
          <div className="refFeature-content">
            <div className="pron-row mb-3 align-items-center">
              <div className="pron-col">
                <h2 className="mt-0 mb-0">Con người công chúng</h2>
              </div>
              <div className="pron-col auto">
                <div className="refFeature-step success">
                  <i className="f7-icons">checkmark_alt</i>
                  3/3
                </div>
              </div>
            </div>
            <ul className="refFeature-list">
              <li>
                <div className="refFeature-list-icon">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={24}
                    height={24}
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-star"
                  >
                    <polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2" />
                  </svg>
                </div>
                <span>Mục tiêu: </span>
                Hướng dẫn 02 người bạn liên kết ngân hàng
              </li>
              <li>
                <div className="refFeature-list-icon">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={24}
                    height={24}
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    strokeWidth={2}
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    className="feather feather-gift"
                  >
                    <polyline points="20 12 20 22 4 22 4 12" />
                    <rect x={2} y={7} width={20} height={5} />
                    <line x1={12} y1={22} x2={12} y2={7} />
                    <path d="M12 7H7.5a2.5 2.5 0 0 1 0-5C11 2 12 7 12 7z" />
                    <path d="M12 7h4.5a2.5 2.5 0 0 0 0-5C13 2 12 7 12 7z" />
                  </svg>
                </div>
                <span>Phần thưởng: </span>
                01 mã giảm 20.000đ mua thẻ điện thoại
              </li>
            </ul>
            <div className="accordion-item">
              <div className="accordion-item-content">
                <div className="mt-1 mb-4">
                  <h4 className="mt-0">Gợi ý nhiệm vụ :</h4>
                  <img
                    src="https://static.mservice.io/img/s500x240/momo-upload-api-201105135734-637401814546973263.jpg"
                    className="img-fluid d-block mx-auto mb-3 rounded"
                    alt=""
                  />
                  <div className="mm-space-title">
                    <h3 className="h5 mb-1 mt-0 mm-space-name">
                      Tiến trình giới thiệu bạn bè
                    </h3>
                  </div>
                  <div className="lutie-14 text-gray mb-3">
                    Hiện đang có <strong className="text-momo">3</strong> người
                    đang nhập mã giới thiệu của bạn
                  </div>
                  <div className="contact__list">
                    <div className="contact__item shadow-1">
                      <div className="contact__grid">
                        <div className="contact__avatar">K</div>
                        <div className="contact__info">
                          <div className="contact__name text-truncate">
                            Soju Gatta
                          </div>
                          <div className="contact__phone">0468 455 234</div>
                        </div>
                        <div className="contact__function">
                          <a href>
                            <i className="f7-icons size-14">chat_bubble</i>
                          </a>
                          <a href>
                            <i className="f7-icons size-14">phone</i>
                          </a>
                        </div>
                      </div>
                      <div className="text-primary mt-2 mb-1 lutie-14">
                        Đã nhập mã thành công, còn 2 bước
                      </div>
                      <div className="process-step">
                        <div className="process-step__item is-active" />
                        <div className="process-step__item" />
                        <div className="process-step__item" />
                      </div>
                      <div className="lutie-13 mt-2">
                        <span className="text-gray">Bước tiếp theo : </span>{" "}
                        Liên kết tài khoản ngân hàng Ví MoMo
                      </div>
                    </div>
                    <div className="contact__item shadow-1">
                      <div className="contact__grid">
                        <div className="contact__avatar">MD</div>
                        <div className="contact__info">
                          <div className="contact__name text-truncate">
                            Soju Gatta
                          </div>
                          <div className="contact__phone">0468 455 234</div>
                        </div>
                        <div className="contact__function">
                          <a href>
                            <i className="f7-icons size-14">chat_bubble</i>
                          </a>
                          <a href>
                            <i className="f7-icons size-14">phone</i>
                          </a>
                        </div>
                      </div>
                      <div className="text-primary mt-2 mb-1 lutie-14">
                        Đã nhập mã thành công, còn 2 bước
                      </div>
                      <div className="process-step">
                        <div className="process-step__item is-active" />
                        <div className="process-step__item" />
                        <div className="process-step__item" />
                      </div>
                      <div className="lutie-13 mt-2">
                        <span className="text-gray">Bước tiếp theo : </span>{" "}
                        Liên kết tài khoản ngân hàng Ví MoMo
                      </div>
                    </div>
                    <div className="contact__item shadow-1">
                      <div className="contact__grid">
                        <div className="contact__avatar">R</div>
                        <div className="contact__info">
                          <div className="contact__name text-truncate">
                            Soju Gatta
                          </div>
                          <div className="contact__phone">0468 455 234</div>
                        </div>
                        <div className="contact__function">
                          <a href className="message">
                            <img src="/static/images/referral/chat.svg" alt="" />
                          </a>
                          <a href className="phone">
                            <img src="/static/images/referral/telephone.svg" alt="" />
                          </a>
                        </div>
                      </div>
                      <div className="text-primary mt-2 mb-1 lutie-14">
                        Đã liên kết thành công, còn 1 bước
                      </div>
                      <div className="process-step">
                        <div className="process-step__item is-active" />
                        <div className="process-step__item is-active" />
                        <div className="process-step__item" />
                      </div>
                      <div className="lutie-13 mt-2">
                        <span className="text-gray">Bước tiếp theo : </span>{" "}
                        Liên kết tài khoản ngân hàng Ví MoMo
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="accordion-item-toggle">
                <div className="text-right">
                  <button className="col button color-pink button-fill font-weight-bold w-auto px-3 open">
                    Xem gợi ý
                  </button>
                  <button className="col button color-pink button-fill font-weight-bold w-auto px-3 close">
                    Thu gọn
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Page>

      <style jsx>{``}</style>
    </>
  );
};

export default ReferralFriendPage;

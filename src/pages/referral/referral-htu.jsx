import React, { useState, useEffect, useRef } from "react";
import {
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  Swiper,
  SwiperSlide,
  NavRight,
  Link,
  List,
  Sheet,
  Icon,
  ListInput,
  ListItem,
  Toggle,
  BlockTitle,
  Row,
  Button,
  Range,
  Block,
  Popup,
} from "framework7-react";

import { ApiResponseErrorCodeMessage, System } from "@/libs/constants";

import Image from "@/components/image";


const ReferralPageHtu = (props) => {
  const [popupOpened, setPopupOpened] = useState(false);

  const [swiperHtu, setSwiperHtu] = useState(null);

  useEffect(() => {
    if (popupOpened === true) {
      swiperHtu.update();
    }
    return function cleanup() {};
  }, [popupOpened]);

  return (
    <>
      <Popup
        className="popup-swipe-htu soju-popup soju-popup-htu"
        onPopupClosed={() => setPopupOpened(false)}
        onPopupOpened={() => setPopupOpened(true)}
      >
        <Page>
          <Navbar>
            <NavLeft>
              <Link popupClose>
                <Icon f7="xmark" className="text-dark"></Icon>
              </Link>
            </NavLeft>
            <NavTitle sliding>Hướng dẫn Baskin Robbins</NavTitle>
          </Navbar>
          <div
            style={{ height: "100%" }}
            className="display-flex justify-content-center align-items-center"
          >
            <Swiper
              speed={500}
              slidesPerView={1}
              spaceBetween={20}
              pagination={{
                // el: ".swiper-pagination",
                type: "fraction",
                hideOnClick: true,
              }}
              className="htu-swiper"
              onSwiper={setSwiperHtu}
            >
              {[1, 2, 3, 4].map((item, index) => (
                <SwiperSlide key={index}>
                  <div className="htu-content">
                    <div className="htu-mockup">
                      <div
                        className="htu-mockup-slider"
                        style={{
                          backgroundImage:
                            'url("/static/images/phone-mockup.png")',
                        }}
                      >
                        <img
                          src="https://static.mservice.io/img/momo-upload-api-tai-vi-1-190103160624.jpg"
                          loading="lazy"
                          className="img-fluid d-block mx-auto swiper-lazy"
                          alt=""
                        />
                      </div>
                      <div className="htu-mockup-txt">
                        <h3 className="htu-mockup-title mt-0 mb-2">
                          Tải ứng dụng Ví MoMo
                        </h3>
                        <div className="htu-mockup-body">
                          Tải ứng dụng miễn phí bằng cách tìm kiếm từ khóa “vi
                          momo” trên App Store hoặc Google Play Store.
                        </div>
                      </div>
                    </div>
                  </div>
                </SwiperSlide>
              ))}
            </Swiper>
          </div>
        </Page>
      </Popup>

      <div className="popup cas-htu-popup popup-swipe-round">
        <div className="view">
          <div className="page">
            <div className="page-content">
              <div
                data-lazy='{"enabled": true}'
                data-pagination='{"el": ".swiper-pagination", "type" : "fraction", "hideOnClick": true}'
                className="swiper-container swiper-init htu-swiper"
              >
                <div className="swiper-pagination" />
                <div className="swiper-wrapper">
                  {"{"}
                  {"{"}#each @root.htu{"}"}
                  {"}"}
                  <div className="swiper-slide">
                    <div className="htu-content">
                      <div className="htu-mockup">
                        <div
                          className="htu-mockup-slider"
                          style={{
                            backgroundImage: 'url("images/phone-mockup.png")',
                          }}
                        >
                          <img
                            data-src="{{image}}"
                            className="img-fluid d-block mx-auto swiper-lazy"
                            alt=""
                          />
                        </div>
                        <div className="htu-mockup-txt">
                          <h3 className="htu-mockup-title mt-0 mb-2">
                            {"{"}
                            {"{"}title{"}"}
                            {"}"}
                          </h3>
                          <div className="htu-mockup-body">
                            {"{"}
                            {"{"}content{"}"}
                            {"}"}
                          </div>
                          <div className="mt-3  font-italic">
                            {"{"}
                            {"{"}note{"}"}
                            {"}"}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  {"{"}
                  {"{"}/each{"}"}
                  {"}"}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <style jsx>{``}</style>
    </>
  );
};

export default ReferralPageHtu;

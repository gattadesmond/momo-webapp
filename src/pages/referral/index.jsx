import React, { useState, useEffect } from "react";
import {
  f7,
  f7ready,
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  Swiper,
  SwiperSlide,
  NavRight,
  Link,
  List,
  Sheet,
  Icon,
  ListInput,
  ListItem,
  Toggle,
  BlockTitle,
  Row,
  Button,
  Toolbar,
  Range,
  Popup,
} from "framework7-react";
import $ from "dom7";
import anime from "animejs/lib/anime.es.js";
import QrCodeWithLogo from "qr-code-with-logo";

import events from "@/components/events";
import { ApiResponseErrorCodeMessage,ProcessAppTracking,ProcessAppShareFacebook,ProcessAppShareOther, System ,ShortName} from "@/libs/constants";

import axios from "@/libs/axios";
import MaxApi from "@momo-platform/max-api";

import ReferralPageHtu from "./referral-htu";

import "./referral.less";

const ReferralPage = (props) => {
  var idx = 0;
  const [data, setData] = useState({
    IsIos: 0,
    CloudToken: "",
    UserInfo: {},
    BodyShareSms: {},
    ReferalDetail: {
      ListBanner: [],
      LinkReferralInfo: {
        linkShare: "",
        codeShare: "",
        partner: {
          title: "",
          promotion_partner: []
        }
      }
    },
    ReferalGift: [],
    ReferalGuide: {},
    ReferalProgress: {
      title: ""
    },
    ReferalProgressStep1: {
      title: ""
    },
    ReferalProgressStep2: {
      title: ""
    },
    IsHaveMission: false,
    ReferalFriendNotMapbank: {
      user_id: null,
      contacts: []
    },
    ReferalFriendNonMoMoData: {
      user_id: null,
      contacts: []
    },
    ListVoucherModel: [],
    CustomAxInfo: {
      status: false
    }

  });

  const getData = (fnc) => {
    if (data.IsLoading)
      return;
    var url = `${System.apihost}/__get/Referral/GetDetail?userPhone=${data.UserInfo.phone}`;
    axios
      .get(url)
      .then((res) => {
        data.IsLoading = false;
        f7.preloader.hide();
        if (!res.Result) {
          f7.dialog.alert(ApiResponseErrorCodeMessage["system_error"]);
          return;
        }

        fnc && fnc(res.Data);
      }).catch((error) => {
        data.IsLoading = false;
        f7.preloader.hide();
        return;
      });
  };

  const bindData = (res) => {
    data.ReferalDetail.ListBanner = res.ListBanner;
    data.ReferalDetail.LinkReferralInfo = res.LinkReferralInfo;

    setTimeout(function () {
      QrCodeWithLogo && QrCodeWithLogo.toCanvas({
        canvas: document.getElementById('qrcodearea'),
        content: res.LinkReferralInfo.linkShare.trim(),
        width: 150,
        logo: {
          src: 'https://static.mservice.io/pwa/images/logoMomox50.png',
        }
      });
      copyAction();

    }, 2000);

    f7.preloader.hide();
    setData({ ...data });

    PreInitSlider();

    getProgressData(function (res2) {
      data.ReferalProgress = res2;

      //Update
      data.ReferalProgressStep1.title = res2.title;
      data.ReferalProgressStep1.description = res2.description;
      data.ReferalProgressStep1.list_user = [];
      data.ReferalProgressStep2.title = res2.title;
      data.ReferalProgressStep2.description = res2.description;
      data.ReferalProgressStep2.list_user = [];
      for (var i = 0; i < res2.list_user.length; i++) {
        if (res2.list_user[i].current_step == 'map_bank') {
          data.ReferalProgressStep2.list_user.push(res2.list_user[i]);
        }
        else {
          data.ReferalProgressStep1.list_user.push(res2.list_user[i]);
        }
      }
      //EndUpdate

      setData({ ...data });
      PreInitSlider();

      try {
        window.ReactNativeWebView.postMessage(JSON.stringify({ action: "getCloudToken" }));
      } catch (e) {
        console.log(e);
      }
    });

    getGiftData((res) => {
      data.ReferalGift = res;
      setData({ ...data });
      PreInitSlider();
    });

    getDataGroupGuide(595, (dataGroupGuide) => {
      data.ReferalGuide = dataGroupGuide;
      setData({ ...data });
      PreInitSlider();
    });

  };

  const getGiftData = (fnc) => {
    var url = `${System.apihost}/__get/Referral/GetGiftData`;
    axios
      .get(url)
      .then((res) => {
        data.IsLoading = false;
        f7.preloader.hide();
        if (!res.Result) {
          f7.dialog.alert(ApiResponseErrorCodeMessage[res.ErrorCode]);
          return;
        }

        fnc && fnc(res.Data);
      }).catch((error) => {
        return;
      });
  }
  const getDataGroupGuide = (id, fnc) => {
    var url = `${System.apihost}/__get/Guide/DetailGroup?id=${id}`;
    axios
      .get(url)
      .then((res) => {
        f7.preloader.hide();
        if (res.Error) {
          f7.dialog.alert(ApiResponseErrorCodeMessage[res.Error.Code]);
          if (res.Error.Code === 'notfound') {
            f7.views.main.router.navigate({
              name: '404'
            });
          }
          return;
        }

        fnc && fnc(res.Data);
      }).catch((error) => {
        return;
      });
  };
  const PreInitSlider = () => {
    setTimeout(function () {

      var swiperBanner = f7.swiper.get(".referral-swiper");
      if (swiperBanner == undefined) {
        swiperBanner = f7.swiper.create('.referral-swiper', {
          pagination: {
            el: '.swiper-pagination',
          },
          autoplay: {
            delay: 5000,
          },
          spaceBetween: 0,
          loop: true,
          speed: 1000,
          slidesPerView: 'auto'
        });
      }
      else {
        swiperBanner.update();
      }

      var swiperHotEvent = f7.swiper.get(".swiper-hot-event");
      if (swiperHotEvent == undefined) {
        f7.swiper.create('.swiper-hot-event', {
          spaceBetween: 15,
          slidesPerView: 'auto',
          lazy: {
            enabled: true,
            loadPrevNext: true,
            loadPrevNextAmount: 2
          }
        });
      }
      else {
        swiperHotEvent.update();
      }

      for (var i = 0; i < data.ReferalGift.length; i++) {
        var swiperRef = f7.swiper.get('.swiper-gift-' + data.ReferalGift[i].Id);
        if (swiperRef == undefined) {
          f7.swiper.create('.swiper-gift-' + data.ReferalGift[i].Id, {
            spaceBetween: 15,
            slidesPerView: 'auto',
            lazy: {
              enabled: true,
              loadPrevNext: true,
              loadPrevNextAmount: 2
            }
          });
        }
        else {
          swiperRef.update();
        }
      }

      if (data.ReferalGuide != undefined && data.ReferalGuide.Groups != undefined) {
        for (var j = 0; j < data.ReferalGuide.Groups.length; j++) {
          if (data.ReferalGuide.Groups[j].Items.length <= 1) {
            var Id = data.ReferalGuide.Groups[j].Id;
            // f7.popup.destroy('.gtbb-htu-popup-' + Id + '0');
            // var casHtuGroup = f7.popup.create({
            //   el: '.gtbb-htu-popup-' + Id + '0',
            //   swipeToClose: "to-bottom",
            //   swipeHandler: '.close-handler',
            //   dataId: Id.toString() + '0'
            // });
            var swiperGroup = f7.swiper.get('.gtbb-htu-sw-' + Id + '0');
            if (swiperGroup == undefined) {
              f7.swiper.create('.gtbb-htu-sw-' + Id + '0', {
                pagination: {
                  el: '.gtbb-pagination-' + Id + '0',
                  type: 'fraction',
                  hideOnClick: true
                },
                navigation: {
                  nextEl: '.gtbb-navigation-next-btn-' + Id + '0',
                  prevEl: '.gtbb-navigation-pre-btn-' + Id + '0'
                },
                lazy: {
                  enabled: true
                }
              });
            }
            else {
              swiperGroup.update();
            }
            // casHtuGroup.on('popup:opened', function (popup) {
            //   debugger;
            //   var swiperGroup = f7.swiper.get('.gtbb-htu-sw-' + popup.params.dataId);
            //   swiperGroup.update();
            // });
          }
          else {
            for (var c = 0; c < data.ReferalGuide.Groups[j].Items.length; c++) {
              var Id = data.ReferalGuide.Groups[j].Id;
              var ItemId = data.ReferalGuide.Groups[j].Items[c].Id;
              // f7.popup.destroy('.gtbb-htu-popup-' + Id.toString() + ItemId.toString());
              // var casHtuGroup = f7.popup.create({
              //   el: '.gtbb-htu-popup-' + Id.toString() + ItemId.toString(),
              //   swipeToClose: "to-bottom",
              //   swipeHandler: '.close-handler',
              //   dataId: Id.toString() + ItemId.toString()
              // });
              var swiperGroup = f7.swiper.get('.gtbb-htu-sw-' + Id.toString() + ItemId.toString());
              if (swiperGroup == undefined) {
                f7.swiper.create('.gtbb-htu-sw-' + Id.toString() + ItemId.toString(), {
                  pagination: {
                    el: '.gtbb-pagination-' + Id.toString() + ItemId.toString(),
                    type: 'fraction',
                    hideOnClick: true
                  },
                  navigation: {
                    nextEl: '.gtbb-navigation-next-btn-' + Id.toString() + ItemId.toString(),
                    prevEl: '.gtbb-navigation-pre-btn-' + Id.toString() + ItemId.toString()
                  },
                  lazy: {
                    enabled: true
                  }
                });
              }
              else {
                swiperGroup.update();
              }

              // casHtuGroup.on('popup:opened', function (popup) {
              //   var swiperGroup = f7.swiper.get('.gtbb-htu-sw-' + popup.params.dataId);
              //   swiperGroup.update();
              // });
            }
          }
        }

      }
    }, 1500);
  }

  const HtuGroupOpen = (dataId) => {
    var swiperGroup = f7.swiper.get('.gtbb-htu-sw-' + dataId);
    swiperGroup.update();
  }

  const CustomAxInfoAccept = (customize_id) => {
    f7.preloader.show();
    postRefferalInfoCustomizeAx(customize_id, function (res) {
      f7.preloader.hide();
      f7.popup.close(".referral-feature-popup-" + customize_id);
      data.CustomAxInfo.customize_id = res.customize_id;
      setData({ ...data });
    });
  }

  const GetDataRef = () => {

    getData(function (res) {
      bindData(res);
    });

    getBodyShareSms((res) => {
      data.BodyShareSms = res;
      setData({ ...data });
    });
  }

  const SetColorContact = () => {
    $(".contact__avatar").each(function () {
      var charFirst = $(this)[0]
        .innerText.toString()
        .charAt(0)
        .toUpperCase();
      var code = charFirst.charCodeAt(0);

      console.log(code);

      if (code > 64 && code < 70) {
        $(this).addClass("is-pink");
      }
      if (code >= 70 && code < 75) {
        $(this).addClass("is-purple");
      }
      if (code >= 75 && code < 80) {
        $(this).addClass("is-orange");
      }
      if (code >= 80 && code < 85) {
        $(this).addClass("is-blue");
      }
      if (code >= 85 && code < 91) {
        $(this).addClass("is-teal");
      }
    });
  };
  const getRefferalMission = (fnc) => {

    var url = `${System.apiAppHost}/referral-dashboard/v1/mission/1`;
    axios
      .get(url, {
        headers: {
          Authorization: "Bearer " + data.CloudToken,
        },
        crossDomain: true,
        contentType: 'application/json',
        dataType: 'json'
      })
      .then((res) => {
        if (res.response_info.error_code == 0 && res.response_info.error_message == "success") {
          res = res.items;
          fnc && fnc(res);
        }

      }).catch((error) => {
        return;
      });
  };
  const getFriendNonMoMoData = (fnc) => {
    var url = `${System.apiAppHost}/friend-rec/non-momo-rec`;

    axios
      .get(url, {
        headers: {
          Authorization: "Bearer " + data.CloudToken,
        },
        crossDomain: true,
        contentType: 'application/json',
        dataType: 'json'
      })
      .then((res) => {
        if (res.response_info.error_code == 0 && res.response_info.error_message == "success") {
          res = res.item;
          fnc && fnc(res);
        }
      }).catch((error) => {
        return;
      });

  };
  const getRefFriendNotMapbank = (fnc) => {

    var url = `${System.apiAppHost}/friend-rec/not-mapbank-rec`;
    axios
      .get(url, {
        headers: {
          Authorization: "Bearer " + data.CloudToken,
        },
        crossDomain: true,
        contentType: 'application/json',
        dataType: 'json'
      })
      .then((res) => {
        if (res.response_info.error_code == 0 && res.response_info.error_message == "success") {
          res = res.item;
          fnc && fnc(res);
        }
      }).catch((error) => {
        return;
      });


  }

  const getProgressData = (fnc) => {
    var url = `${System.apihost}/__get/Referral/GetProgressReferral?userPhone=${data.UserInfo.phone}`;
    axios
      .get(url)
      .then((res) => {
        if (res.Result) {
          fnc && fnc(res.Data);
        }

      }).catch((error) => {
        return;
      });
  }

  const getBodyShareSms = (fnc) => {
    var url = `${System.apihost}/__get/Referral/GetBodyShareSms?userPhone=${data.UserInfo.phone}`;

    axios
      .get(url)
      .then((res) => {
        fnc && fnc(res.Data);
      }).catch((error) => {
        return;
      });
  };
  const CopyShareUrl = () => {
    /* Get the text field */
    var copyText = document.getElementById("inputCode");

    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /*For mobile devices*/

    /* Copy the text inside the text field */
    document.execCommand("copy");
    copyText.blur();

    const noti = document.querySelector(".copy-popover");
    // document.activeElement.blur();

    noti.classList.add("is-active");
    setTimeout(function () {
      noti.classList.remove("is-active");
    }, 3000);
    /* Alert the copied text */
    // alert("Copied the text: " + copyText.value);
    //ProcessAppTracking('referral_dashboard_2020', 'referral_copy');
  }
  const CopyShareUrlBottom = () => {
    var toastCopy = f7.toast.create({
      text: 'Sao chép liên kết thành công',
      closeTimeout: 2000,
    });

    const textInput = document.createElement("input");
    textInput.setAttribute('readonly', true);
    textInput.value = data.ReferalDetail.LinkReferralInfo.linkShare.trim();

    document.body.appendChild(textInput);

    textInput.select();
    document.execCommand("Copy");
    toastCopy.open();

    //ProcessAppTracking('referral_dashboard_2020', 'referral_copy');
  }

  const copyAction = () => {
    var copyBtn = Array.from(document.querySelectorAll(".copy-button"));
    copyBtn.forEach(function (e) {
      e.addEventListener("click", function () {
        const text = e.parentElement.querySelector(".code").textContent;
        const noti = e.parentElement.querySelector(".copy-popover");

        const textInput = document.createElement("input");
        textInput.setAttribute('readonly', true);
        textInput.value = text.trim();

        document.body.appendChild(textInput);

        textInput.select();
        document.execCommand("Copy");

        // alert("Bạn đã copy mã khuyến mãi: " + textInput.value);

        noti.classList.add("is-active");
        setTimeout(function () {
          noti.classList.remove("is-active");
        }, 2000);
      });
    });
  }

  const ButtonShareFBAppClick = () => {
    ProcessAppTracking('referral_dashboard_2020', 'referral_share_facebook');
    ProcessAppShareFacebook(data.ReferalDetail.LinkReferralInfo.linkShare.trim());
  }
  const ButtonShareOtherAppClick = () => {
    var contentShare = data.BodyShareSms.sms_body['share_link'];
    ProcessAppTracking('referral_dashboard_2020', 'referral_share');
    ProcessAppShareOther(contentShare);
  }
  const ListVoucherInfo = () => {
    try {
      var param = {
        msgType: "GET_PROMOTIONS_VOUCHER_RANDOM",
        momoMsg: {
          programmeType: 1,
          _class: "mservice.backend.entity.msg.PromotionVoucherRandomMsg"
        },
        extra: "",
        resFunc: "CallBackListVoucherInfo"
      };
      window.ReactNativeWebView.postMessage(JSON.stringify({ action: "requestBackend", value: param }));
    } catch (e) {
      console.log(e);
    }
  }
  const CallBackListVoucherInfo = (res) => {
    var jsonString = res.replace(/\n/g, "").replace(/"{/g, "{").replace(/}"/g, "}").replace(/]"/g, "]").replace(/"\[/g, "[");
    var jsonModel = JSON.parse(jsonString);

    data.ListVoucherModel = jsonModel.momoMsg.promotionList;
    setData({ ...data });
    PreInitSlider();
  }

  const handleScroll = () => {
    let currentTop = document.querySelector(".page-referral .page-content")
      .scrollTop;
    if (currentTop > 400) {
      f7.toolbar.show(".toolbar");
    } else {
      f7.toolbar.hide(".toolbar");
    }
  }

  /////////////////////////////--TODO--////////////////
  const getRefferalCustomizeAx = (fnc) => {
    var url = `${System.apiAppHost}/referral-dashboard/v1/customize-ax/list/${data.UserInfo.phone}`;
    axios
      .get(url, {
        headers: {
          Authorization: "Bearer " + data.CloudToken,
        },
        crossDomain: true,
        contentType: 'application/json',
        dataType: 'json'
      })
      .then((res) => {
        f7.preloader.hide();
        if (res.response_info.error_code == 0 && res.response_info.error_message == "success") {
          res = res.item;
          fnc && fnc(res);
        }

      }).catch((error) => {
        return;
      });

  };

  const getRefferalInfoCustomizeAx = (fnc) => {
    var url = `${System.apiAppHost}/referral-dashboard/v1/customize-ax/get-info-customize/${data.UserInfo.phone}`;
    axios
      .get(url, {
        headers: {
          Authorization: "Bearer " + data.CloudToken,
        },
        crossDomain: true,
        contentType: 'application/json',
        dataType: 'json'
      })
      .then((res) => {
        f7.preloader.hide();

        if (res.response_info.error_code == 0 && res.response_info.error_message == "success") {
          res = res.item;
          fnc && fnc(res);
        }

      }).catch((error) => {
        return;
      });
  };

  const postRefferalInfoCustomizeAx = (CustomizeAxId, fnc) => {
    var url = `${System.apiAppHost}/referral-dashboard/v1/customize-ax/insert-data/${data.UserInfo.phone}`;
    var param = {
      customize_id: CustomizeAxId
    };

    axios
      .post(url, {
        data: param,
        headers: {
          Authorization: "Bearer " + data.CloudToken,
        },
        crossDomain: true,
        contentType: 'application/json',
        dataType: 'json'
      })
      .then((res) => {
        if (res.response_info.error_code == 0 && res.response_info.error_message == "success") {
          res = res.item;
          fnc(res);
        }
        else {
          app.preloader.hide();
          app.dialog.alert(ApiResponseErrorCodeMessage["system_error"]);
          return;
        }
      });
  };


  useEffect(() => {
    f7ready((f7) => {
      f7.toolbar.hide(".toolbar");
      document
        .querySelector(".page-referral .page-content")
        .addEventListener("scroll", handleScroll);


      $(".accordion-item").on("accordion:opened", function (el) {
        const offset = $(el.currentTarget).offset().top;
        const navHeight = 56;
        const scrollTopValue =
          offset -
          navHeight +
          $(el.currentTarget).parents(".page-referral .page-content").scrollTop();
        const animeTar = document.querySelector(".page-referral .page-content");
        anime({
          targets: animeTar,
          scrollTop: scrollTopValue,
          duration: 500,
          easing: "easeInOutQuad",
        });
        // $(openedItem).parents('.page-content').scrollTop((offset - navHeight - toolbarHeight + scrollTop), 300);
      });


    });
  }, []);

  const pageInit = () => {

    events.on("CallbackGetCloudToken", (res) => {
      data.CloudToken = res;
      setData({
        ...data
      });

      getRefferalMission((res) => {
        if (res.length > 0) {
          data.IsHaveMission = true;
          data.MissionInfo = res;
        }
        getFriendNonMoMoData((res2) => {
          data.ReferalFriendNonMoMoData = res2;
          setData({ ...data });
          PreInitSlider();

          getRefFriendNotMapbank((res3) => {
            data.ReferalFriendNotMapbank = res3;
            setData({ ...data });
            PreInitSlider();

            SetColorContact();

            ///////////////////////////--TODO--////////////
            // $('.referral-notmapbank').on('popup:open', function (e, popup) {
            //   SetColorContact();
            // });
            // $('.referral-nonmomo').on('popup:opened', function (e, popup) {
            //   SetColorContact();
            // });
          });
        });
      });
      getRefferalCustomizeAx(function (res) {
        data.CustomAxInfo = res;
        setData({
          ...data
        });
        getRefferalInfoCustomizeAx(function (res) {
          data.CustomAxInfo.customize_id = res.customize_id;
          setData({
            ...data
          });
        });
      });
    });

    events.on("initPageData", (res) => {
      res = JSON.parse(res);
      data.UserInfo = res;
      setData({
        ...data
      });

      GetDataRef();
    });

    events.on("CallBackListVoucherInfo", (res) => {
      CallBackListVoucherInfo(res);
    });

    // //Open Popup
    // if (props.f7route.query.webToken) {
    //   //Di truc tiep
    //   if (props.f7route.query.webToken) {
    //     data.IsFromNoti = true;
    //     setData({
    //       ...data
    //     });
    //   }

       try {
    window.ReactNativeWebView.postMessage("GetUserInfo");
    //     window.ReactNativeWebView.postMessage(JSON.stringify({ action: "getCloudToken" }));
       } catch (e) {
       }

    //   //ValidateAccessToken(props.f7route.query.webToken, function (res) {

    // } else {
    //   //di bang Miniapp
    //   MaxApi.getProfile(function (res) {
    //     data.UserInfo = {
    //       phone: res.user,
    //       userName: res.userName,
    //     };
    //     MaxApi.getDeviceInfo(function (res) {
    //       data.CloudToken = res.firebaseToken;
    //       setData({ ...data });

    //       BindHeoDiHocData();
    //     });

    //   });
    // }


  };

  const pageDestroy = () => {
    document
      .querySelector(".page-referral .page-content")
      .removeEventListener("scroll", handleScroll);
  };
  const LinkClick = (url) => {
    if (url != "") {
      location.href = "momo://?refId=" + url;
    }
    return false;
  };

  const ImgErrorLoad = (evt) => {
    evt.currentTarget.src = 'https://static.mservice.io/blogscontents/momo-upload-api-200615151838-637278311186392175.png';
  };
  return (
    <>
      <Page
        onPageInit={pageInit}
        onPageBeforeRemove={pageDestroy}
        className="bg-light page-referral "
      >
        {/* <Navbar className="momo-navbar">
          <NavLeft backLink><Link iconIos="f7:chevron_left" /></NavLeft>

          <NavTitle sliding> Referral</NavTitle>
        </Navbar> */}

        <Toolbar
          position="bottom"
          className="toolbar-large toolbar-referral  bg-white"
        >
          <div className=" w-100">
            <div className="pron-row align-items-center">
              <div className="pron-col">
                <div className="referral-action">
                  <div className="mb-1 font-weight-bold lutie-12">
                    Mã giới thiệu của bạn
                  </div>
                </div>
                <div className="referral-code  code">
                  <div className="referral-code-txt text-center">
                    <div className="referral-code-phone text-momo">
                      {data.ReferalDetail.LinkReferralInfo.codeShare}
                    </div>
                  </div>
                </div>
              </div>
              <div className="pron-col auto pl-3">
                <div className="referral-action">
                  <div className="mb-1 font-weight-bold lutie-12">Chia sẽ</div>
                </div>
                <div className="referral-social">
                  <a onClick={() => ButtonShareFBAppClick()} className="referral-social-item">
                    <img
                      src="/static/images/social/facebook.svg"
                      width={30}
                      alt=""
                    />
                  </a>
                  <a onClick={() => ButtonShareOtherAppClick()} className="referral-social-item">
                    <img
                      src="/static/images/social/share.svg"
                      width={30}
                      alt=""
                    />
                  </a>
                  <a onClick={() => CopyShareUrlBottom()} className="referral-social-item">
                    <img
                      src="/static/images/social/copy.svg"
                      width={30}
                      alt=""
                    />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </Toolbar>

        <Swiper pagination className="referral-swiper">
          {data.ReferalDetail.ListBanner.map((item, index) => (

            <SwiperSlide key={`sld-${index}`} >
              <div className="embed-responsive embed-responsive-referral ">
                <a onClick={() => LinkClick(item.banner_ref)} className="external" >
                  <img src={item.banner_link} className="embed-responsive-img" />
                </a>
              </div>
            </SwiperSlide>
          ))}
        </Swiper>

        <div className="mm-space pt-2  ">
          <div className="pron-container balance overflow-hidden">
            <div className="referral-action ">
              <div className="mt-4 mb-2  lutie-14 font-weight-bold">Link giới thiệu bạn bè</div>
            </div>
            <div className="referral-code">
              <div className="referral-code-txt">
                {data.ReferalDetail.LinkReferralInfo.linkShare}
              </div>
              <div className="referral-code-btn">
                <a onClick={() => CopyShareUrl()}>
                  Sao chép
                </a>
              </div>
              <input
                type="text"
                defaultValue={data.ReferalDetail.LinkReferralInfo.linkShare}
                id="inputCode"
                readOnly
              />
              <div className="copy-popover">
                <img
                  src="/static/images/sale/checked.svg"
                  className="copy-popover-img"
                  width={18}
                  alt=""
                />{" "}
                Sao chép đường dẫn thành công
              </div>
            </div>
            <div className="pron-row">
              <div className="pron-col">
                <div className="referral-action">
                  <div className="mt-4 mb-2  lutie-14 font-weight-bold">
                    Mã giới thiệu của bạn
                  </div>
                </div>
                <div className="referral-code  code">
                  <div className="referral-code-txt text-center">
                    <div className="referral-code-phone text-momo">
                      {data.ReferalDetail.LinkReferralInfo.codeShare}
                    </div>
                  </div>
                </div>
                <div className="referral-action">
                  <div className="mt-3 mb-2 lutie-14 font-weight-bold">Chia sẻ</div>
                </div>
                <div className="referral-social">
                  <a onClick={() => ButtonShareFBAppClick()} className="referral-social-item">
                    <img
                      src="/static/images/social/facebook.svg"
                      width={34}
                      alt=""
                    />
                  </a>
                  <a onClick={() => ButtonShareOtherAppClick()} className="referral-social-item">
                    <img
                      src="/static/images/social/share.svg"
                      width={34}
                      alt=""
                    />
                  </a>
                </div>
              </div>
              <div className="pron-col auto pl-3" style={{ zIndex: 1 }}>
                <div className="referral-action ">
                  <div className="mt-4 mb-2  lutie-14">Quét mã QR code</div>
                </div>
                <div className="referral-qr">
                  <canvas id="qrcodearea" className="img-fluid d-block" width="150"></canvas>
                </div>
              </div>
            </div>
          </div>
        </div>

        {data.CustomAxInfo.status && (
          <>
            <div className="mm-space mt-2">
              <div className="pron-container balance">
                <div className="mm-space-title">
                  <h3 className="h5 mb-3 mt-0 mm-space-name">
                    {data.CustomAxInfo.title}
                  </h3>
                </div>
              </div>
              <div className="pron-container balance">
                {data.CustomAxInfo.block_list.map((item, index) => (
                  <div className="referral-feature-item" key={index} >
                    <div className="referral-feature-img" style={{ backgroundImage: `url(${item.image})` }}></div>
                    <div className="referral-feature-meta">
                      <div className="referral-feature-name">{item.title}</div>
                      <div className="referral-feature-btn">
                        {item.id == data.CustomAxInfo.customize_id ? (
                          <div className="check">
                            <i className="f7-icons">checkmark_alt</i>
                          </div>
                        ) : (
                            <button className="button button-round button-fill popup-open" data-popup={`.referral-feature-popup-${item.id}`} >
                              CHỌN
                            </button>
                          )}
                      </div>
                    </div>
                  </div>

                ))}</div>
            </div>
          </>
        )}
        <div className={`mm-space mt-2 ${!(data.ReferalDetail.LinkReferralInfo.partner != null && data.ReferalDetail.LinkReferralInfo.partner.promotion_partner.length > 0) ? 'd-none' : ''}`}  >
          <div className="pron-container balance">
            <div className="mm-space-title ">
              <h3 className="h5 mb-1 mt-0 mm-section-name">
                {data.ReferalDetail.LinkReferralInfo.partner.title}
              </h3>
            </div>
          </div>
          <div className="pron-container balance">
            <div className="contact__list">
              {data.ReferalDetail.LinkReferralInfo.partner.promotion_partner.map((item, index) => (
                <div className="contact__item shadow-1" key={`rdpn-${index}`}>
                  <div >
                    <div className="contact__info">
                      <div className="contact__name">
                        {item.promotion_name}
                      </div>
                    </div>
                    {item.referral_promotion_code.map((item2, index2) => (
                      <div className="sale-code py-3" key={`rdpnit-${index2}`} >
                        <div className="sale-code-item" style={{ background: "#eee" }}>
                          <div className="code lutie-14" >
                            {item2}
                          </div>
                          <div className="copy copy-button">
                            <svg width="20" height="24" viewBox="0 0 20 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                              <path d="M14.7368 0H2.10526C0.947368 0 0 0.981818 0 2.18182V17.4545H2.10526V2.18182H14.7368V0ZM17.8947 4.36364H6.31579C5.1579 4.36364 4.21053 5.34545 4.21053 6.54545V21.8182C4.21053 23.0182 5.1579 24 6.31579 24H17.8947C19.0526 24 20 23.0182 20 21.8182V6.54545C20 5.34545 19.0526 4.36364 17.8947 4.36364ZM17.8947 21.8182H6.31579V6.54545H17.8947V21.8182Z" fill="#7C054F"></path>
                            </svg>
                            <div className="copy-popover">
                              <img src="https://static.mservice.io/pwa/images/sale/checked.svg" className="copy-popover-img" width="18" alt="" /> Sao chép mã thành công
                                </div>
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
        {data.IsHaveMission ? (
          <>
            <div className="pron-container balance">
              <h2 className="text-left mt-3 mb-2">Nhiệm vụ giới thiệu MoMo</h2>
              <p>Bạn ơi! Cùng làm nhiệm vụ nhận quà cực cool nào</p>
            </div>
            {data.MissionInfo.map((item, index) => (
              <div className="refFeature-card" key={`ms-${index}`}>
                <div className="refFeature-content">
                  <div className="pron-row mb-3 align-items-center">
                    <div className="pron-col">
                      <h2 className="mt-0 mb-0">{item.title}</h2>
                    </div>
                    <div className="pron-col auto">
                      <div className={`refFeature-step ${this.missionSuccess >= this.missionTotal ? 'success' : ''}`}>
                        {item.missionSuccess >= item.missionTotal && (
                          <i className="f7-icons">checkmark_alt</i>
                        )}
                        {item.missionSuccess}/{item.missionTotal}</div>
                    </div>
                  </div>
                  <ul className="refFeature-list">
                    <li>
                      <div className="refFeature-list-icon green">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width={24}
                          height={24}
                          viewBox="0 0 24 24"
                          fill="none"
                          stroke="currentColor"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          className="feather feather-star"
                        >
                          <polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2" />
                        </svg>
                      </div>
                      <span>Mục tiêu: </span>
                      {item.target}
                    </li>
                    <li>
                      <div className="refFeature-list-icon red">
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          width={24}
                          height={24}
                          viewBox="0 0 24 24"
                          fill="none"
                          stroke="currentColor"
                          strokeWidth={2}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          className="feather feather-gift"
                        >
                          <polyline points="20 12 20 22 4 22 4 12" />
                          <rect x={2} y={7} width={20} height={5} />
                          <line x1={12} y1={22} x2={12} y2={7} />
                          <path d="M12 7H7.5a2.5 2.5 0 0 1 0-5C11 2 12 7 12 7z" />
                          <path d="M12 7h4.5a2.5 2.5 0 0 0 0-5C13 2 12 7 12 7z" />
                        </svg>
                      </div>
                      <span>Phần thưởng: </span>
                      {item.reward}
                    </li>
                  </ul>
                  <div className="accordion-item">
                    <div className="accordion-item-content">
                      <div className="mt-1 mb-4">
                        <h4 className="mt-0">Gợi ý nhiệm vụ :</h4>
                        dangerouslySetInnerHTML={{ __html: item.suggest }}

                        {item.name == 'entercode' && (
                          <>
                            {data.ReferalFriendNonMoMoData && data.ReferalFriendNonMoMoData.contacts && data.ReferalFriendNonMoMoData.contacts.contacts != null && data.ReferalFriendNonMoMoData.contacts.length > 0 && (
                              <>
                                <div className="mm-space   mt-2 ">
                                  <div className="mm-space-title ">
                                    <h3 className="h6 mb-1 mt-0 mm-section-name">
                                      Hội chưa từng dùng MoMo
                                            </h3>
                                  </div>
                                  <div className="lutie-14 text-gray mb-3 ">
                                    “Rủ rê” hội này đăng ký sử dụng MoMo để cùng nhận quà nào!
                                        </div>
                                  <div className="contact__list is-less shadow-1 ">
                                    {data.ReferalFriendNonMoMoData && data.ReferalFriendNonMoMoData.contacts.map((item2, index2) => {
                                      return index2 < 3 && (
                                        <div className="contact__item " key={`nmmd-${index2}`}>
                                          <div className="contact__grid">
                                            <div className="contact__avatar">
                                              {ShortName(item2.contact_name)}
                                            </div>
                                            <div className="contact__info">
                                              <div className="contact__name text-truncate">
                                                {item2.contact_name}
                                              </div>

                                              <div className="contact__phone">
                                                {item2.phone}
                                              </div>
                                            </div>
                                            <div className="contact__function">
                                              <a onClick={() => SendSms('entercode', 'referral_link', item2.phone)} className="message">
                                                <img src="https://static.mservice.io/pwa/images/referral/message.svg" />
                                              </a>
                                              <a onClick={() => CallPhone('entercode', item2.phone)} className="phone">
                                                <img src="https://static.mservice.io/pwa/images/referral/phone.svg" />
                                              </a>
                                            </div>
                                          </div>
                                        </div>
                                      )
                                    })}
                                  </div>
                                  <div className={`mt-0 ${!(data.ReferalFriendNonMoMoData != null && data.ReferalFriendNonMoMoData.user_id != null && data.ReferalFriendNonMoMoData.contacts.length > 3) ? 'd-none' : ''}`} >
                                    <button className="button col popup-open text-right" data-popup=".referral-nonmomo" >
                                      Xem tất cả
                                    </button>
                                  </div>
                                </div>
                              </>
                            )}

                            <div className={`mm-space   mt-2  ${!(data.ReferalFriendNotMapbank.contacts != null && data.ReferalFriendNotMapbank.contacts.length > 0) ? 'd-none' : ''}`} >
                              <div className="mm-space-title ">
                                <h3 className="h6 mb-1 mt-0 mm-section-name ">
                                  Hội đã có MoMo, chưa liên kết ngân hàng
                                            </h3>
                              </div>
                              <div className="lutie-14 text-gray mb-3">
                                Hãy hướng dẫn hội này Nhập mã giới thiệu của bạn rồi Liên kết Ví MoMo với ngân hàng và Nạp tiền để cùng nhận quà nhé!
                                        </div>
                              <div className="contact__list  is-less shadow-1">
                                {data.ReferalFriendNotMapbank && data.ReferalFriendNotMapbank.contacts.map((item2, index2) => {
                                  return index2 < 3 && (
                                    <div className="contact__item" key={`nmb-${index2}`}>
                                      <div className="contact__grid">
                                        <div className="contact__avatar">
                                          {ShortName(item2.contact_name)}
                                        </div>
                                        <div className="contact__info">
                                          <div className="contact__name text-truncate">
                                            {item2.contact_name}
                                          </div>

                                          <div className="contact__phone">
                                            {item2.phone}
                                          </div>
                                        </div>
                                        <div className="contact__function">
                                          <a onClick={() => SendSms('entercode', 'referral_code', item2.phone)} className="message">
                                            <img src="https://static.mservice.io/pwa/images/referral/message.svg" />
                                          </a>
                                          <a onClick={() => CallPhone('entercode', item2.phone)} className="phone">
                                            <img src="https://static.mservice.io/pwa/images/referral/phone.svg" alt="" />
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                                  )
                                })}
                              </div>
                              <div className={`mt-0 ${!(data.ReferalFriendNotMapbank.contacts != null && data.ReferalFriendNotMapbank.contacts.length > 3) ? 'd-none' : ''}`}>
                                <button className="button col popup-open text-right" data-popup=".referral-notmapbank" >
                                  Xem tất cả
                                            </button>
                              </div>
                            </div>
                          </>
                        )}

                        {item.name == 'mapbank' && (
                          <>
                            <div className={`mm-space   mt-0  ${!(data.ReferalProgressStep1 != null && data.ReferalProgressStep1.title != '' && data.ReferalProgressStep1.list_user.length > 0) ? 'd-none' : ''}`}>
                              <div className="mm-space-title ">
                                <h3 className="h6 mb-1 mt-0 mm-section-name">
                                  {data.ReferalProgressStep1.title}
                                </h3>
                              </div>
                              <div className="lutie-14 text-gray mb-3">
                                {data.ReferalProgressStep1.description}
                              </div>
                              <div className="contact__list">
                                {data.ReferalProgressStep1.list_user.map((item2, index2) => {
                                  return index2 < 3 && (
                                    <div className="contact__item shadow-1" key={`ps1-${index2}`}>
                                      <div className="contact__grid">
                                        <div className="contact__avatar">
                                          <img src={`https://avatars.mservice.io.s3-ap-southeast-1.amazonaws.com/${item2.phone}.png`} onError={x => ImgErrorLoad(x)}
                                          />
                                        </div>

                                        <div className="contact__info">
                                          <div className="contact__name text-truncate">
                                            {item2.username}
                                          </div>

                                          <div className="contact__phone">
                                            {item2.phone}
                                          </div>
                                        </div>

                                        <div className="contact__function">
                                          <a onClick={() => SendSms('mapbank', item2.current_step, item2.phone)} className="message">
                                            <img src="https://static.mservice.io/pwa/images/referral/message.svg" alt="" />
                                          </a>
                                          <a onClick={() => CallPhone('mapbank', item2.phone)} className="phone">
                                            <img src="https://static.mservice.io/pwa/images/referral/phone.svg" alt="" />
                                          </a>
                                        </div>
                                      </div>

                                      <div className="text-primary mt-2 mb-1 lutie-14">
                                        {item2.message}, còn <span className="text-momo font-weight-bold">{item2.current_step == 'map_bank' ? '1' : '2'}</span> bước
                                                </div>

                                      <div className="process-step">

                                        <div className="process-step__item is-active">

                                        </div>

                                        <div className={`process-step__item  ${item2.current_step == 'map_bank' ? 'is-active' : ''}`}></div>
                                        <div className="process-step__item"></div>

                                      </div>

                                      <div className="lutie-13  mt-2">
                                        <span className="text-gray">Bước tiếp theo : </span> {item2.next_message}
                                      </div>
                                    </div>
                                  )
                                })}
                              </div>
                              <div className={`mt-2 ${!(data.ReferalProgressStep1 != null && data.ReferalProgressStep1.title != '' && data.ReferalProgressStep1.list_user.length > 3) ? 'd-none' : ''}`} >
                                <button className="button col popup-open text-right" data-popup=".referral-progress-step1"  >
                                  Xem tất cả
                                            </button>
                              </div>
                            </div>
                          </>
                        )}

                        {item.name == 'cashin' && (
                          <>
                            <div className={`mm-space   mt-2  ${!(data.ReferalProgressStep2 != null && data.ReferalProgressStep2.title != '' && data.ReferalProgressStep2.list_user.length > 0) ? 'd-none' : ''}`} >
                              <div className="mm-space-title ">
                                <h3 className="h6 mb-1 mt-0 mm-section-name">
                                  {data.ReferalProgressStep2.title}
                                </h3>
                              </div>
                              <div className="lutie-14 text-gray mb-3">
                                {data.ReferalProgressStep2.description}
                              </div>
                              <div className="contact__list">
                                {data.ReferalProgressStep2.list_user.map((item2, index2) => {
                                  return index2 < 3 && (
                                    <div className="contact__item shadow-1" key={`ps2-${index2}`} >
                                      <div className="contact__grid">
                                        <div className="contact__avatar">
                                          <img src={`https://avatars.mservice.io.s3-ap-southeast-1.amazonaws.com/${item2.phone}.png`} onError={x => ImgErrorLoad(x)}
                                            alt="" />
                                        </div>

                                        <div className="contact__info">
                                          <div className="contact__name text-truncate">
                                            {item2.username}
                                          </div>

                                          <div className="contact__phone">
                                            {item2.phone}
                                          </div>
                                        </div>

                                        <div className="contact__function">
                                          <a onClick={() => SendSms('cashin', item2.current_step, item2.phone)} className="message">
                                            <img src="https://static.mservice.io/pwa/images/referral/message.svg" alt="" />
                                          </a>
                                          <a onClick={() => CallPhone('cashin', item2.phone)} className="phone">
                                            <img src="https://static.mservice.io/pwa/images/referral/phone.svg" alt="" />
                                          </a>
                                        </div>
                                      </div>

                                      <div className="text-primary mt-2 mb-1 lutie-14">
                                        {item2.message}, còn <span className="text-momo font-weight-bold">{item2.current_step == 'map_bank' ? '1' : '2'}</span> bước
                                                </div>

                                      <div className="process-step">

                                        <div className="process-step__item is-active">

                                        </div>

                                        <div className={`process-step__item ${item2.current_step == 'map_bank' ? 'is-active' : ''}`}></div>
                                        <div className="process-step__item"></div>

                                      </div>

                                      <div className="lutie-13  mt-2">
                                        <span className="text-gray">Bước tiếp theo : </span> {item2.next_message}
                                      </div>
                                    </div>
                                  )
                                })}
                              </div>
                              <div className={`mt-2 ${!(data.ReferalProgressStep2 != null && data.ReferalProgressStep2.title != '' && data.ReferalProgressStep2.list_user.length > 3) ? 'd-none' : ''}`}>
                                <button className="button col popup-open text-right" data-popup=".referral-progress-step2">
                                  Xem tất cả
                                            </button>
                              </div>
                            </div>
                          </>
                        )}
                      </div>
                    </div>
                    <div className="accordion-item-toggle">
                      <div className="text-right">
                        <button className="col button color-pink button-fill font-weight-bold w-auto px-3 open" onClick={() => MissionSuggest(item.name, item.phone)} >
                          {item.cta}
                        </button>
                        <button className="col button color-pink button-fill font-weight-bold w-auto px-3 close">
                          Thu gọn
                  </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </>
        )
          : (
            <>

              <div className={`mm-space   mt-2  ${!(data.ReferalProgress != null && data.ReferalProgress.title != '' && data.ReferalProgress.list_user.length > 0) ? 'd-none' : ''}`}>
                <div className="pron-container balance">
                  <div className="mm-space-title ">
                    <h3 className="h5 mb-1 mt-0 mm-section-name">
                      {data.ReferalProgress.title}
                    </h3>
                  </div>
                  <div className="lutie-14 text-gray mb-3">
                    {data.ReferalProgress.description}
                  </div>
                </div>
                <div className="pron-container balance">
                  <div className="contact__list">
                    {data.ReferalProgress.list_user && data.ReferalProgress.list_user.map((item2, index2) => {
                      return index2 < 3 && (
                        <div className="contact__item shadow-1" key={`rpls-${index2}`} >
                          <div className="contact__grid">
                            <div className="contact__avatar">
                              <img src={`https://avatars.mservice.io.s3-ap-southeast-1.amazonaws.com/${item2.phone}.png`} onError={x => ImgErrorLoad(x)}
                                alt="" />
                            </div>

                            <div className="contact__info">
                              <div className="contact__name text-truncate">
                                {item2.username}
                              </div>

                              <div className="contact__phone">
                                {item2.phone}
                              </div>
                            </div>

                            <div className="contact__function">
                              <a onClick={() => SendSms('progress', item2.current_step, item2.phone)} className="message">
                                <img src="https://static.mservice.io/pwa/images/referral/message.svg" alt="" />
                              </a>
                              <a onClick={() => CallPhone('progress', item2.phone)} className="phone">
                                <img src="https://static.mservice.io/pwa/images/referral/phone.svg" alt="" />
                              </a>
                            </div>
                          </div>

                          <div className="text-primary mt-2 mb-1 lutie-14">
                            {item2.message}, còn <span className="text-momo font-weight-bold">{item2.current_step == 'map_bank' ? '1' : '2'}</span> bước
                            </div>

                          <div className="process-step">

                            <div className="process-step__item is-active">

                            </div>

                            <div className={`process-step__item  ${item2.current_step == 'map_bank' ? 'is-active' : ''}`}></div>
                            <div className="process-step__item"></div>

                          </div>

                          <div className="lutie-13  mt-2">
                            <span className="text-gray">Bước tiếp theo : </span> {item2.next_message}
                          </div>
                        </div>
                      )
                    })}
                  </div>
                  <div className={`mt-2 ${!(data.ReferalProgress != null && data.ReferalProgress.title != '' && data.ReferalProgress.list_user.length > 3) ? 'd-none' : ''}`}>
                    <button className="button col popup-open text-right" data-popup=".referral-progress" >
                      Xem tất cả
                        </button>
                  </div>
                </div>
              </div>

              {!(data.ReferalFriendNonMoMoData) ? 'd-none' : ''}
              <div className={`mm-space   mt-2  ${!(data.ReferalFriendNonMoMoData != undefined && data.ReferalFriendNonMoMoData.contacts != null && data.ReferalFriendNonMoMoData.contacts.length > 0) ? 'd-none' : ''}`} >
                <div className="pron-container balance">
                  <div className="mm-space-title ">
                    <h3 className="h5 mb-1 mt-0 mm-section-name">
                      Hội chưa từng dùng MoMo
                        </h3>
                  </div>
                  <div className="lutie-14 text-gray mb-3 ">
                    “Rủ rê” hội này đăng ký sử dụng MoMo để cùng nhận quà nào!
                    </div>
                </div>
                <div className="pron-container balance">
                  <div className="contact__list is-less shadow-1 ">
                    {data.ReferalFriendNonMoMoData && data.ReferalFriendNonMoMoData.contacts.map((item2, index2) => {
                      return index2 < 3 && (
                        <div className="contact__item " key={`nmmd2-${index2}`} >
                          <div className="contact__grid">
                            <div className="contact__avatar">
                              {ShortName(item2.contact_name)}
                            </div>
                            <div className="contact__info">
                              <div className="contact__name text-truncate">
                                {item2.contact_name}
                              </div>

                              <div className="contact__phone">
                                {item2.phone}
                              </div>
                            </div>
                            <div className="contact__function">
                              <a onClick={() => SendSms('nomomo', 'referral_link', item2this.phone)} className="message">
                                <img src="https://static.mservice.io/pwa/images/referral/message.svg" alt="" />
                              </a>
                              <a onClick={() => CallPhone('nomomo', item2.phone)} className="phone">
                                <img src="https://static.mservice.io/pwa/images/referral/phone.svg" alt="" />
                              </a>
                            </div>
                          </div>
                        </div>
                      )
                    })}
                  </div>
                  <div className={`mt-0 ${!(data.ReferalFriendNonMoMoData != null && data.ReferalFriendNonMoMoData.user_id != null && data.ReferalFriendNonMoMoData.contacts.length > 3) ? 'd-none' : ''}`}>
                    <button className="button col popup-open text-right" data-popup=".referral-nonmomo" >
                      Xem tất cả
                        </button>
                  </div>
                </div>
              </div>



              <div className={`mm-space   mt-2  ${!(data.ReferalFriendNotMapbank != null && data.ReferalFriendNotMapbank.user_id != null && data.ReferalFriendNotMapbank.contacts.length > 0) ? 'd-none' : ''}`}>
                <div className="pron-container balance">
                  <div className="mm-space-title ">
                    <h3 className="h5 mb-1 mt-0 mm-section-name ">
                      Hội đã có MoMo, chưa liên kết ngân hàng
                        </h3>
                  </div>
                  <div className="lutie-14 text-gray mb-3">
                    Hãy hướng dẫn hội này Nhập mã giới thiệu của bạn rồi Liên kết Ví MoMo với ngân hàng và Nạp tiền để cùng nhận quà nhé!
                    </div>
                </div>
                <div className="pron-container balance">
                  <div className="contact__list  is-less shadow-1">
                    {data.ReferalFriendNotMapbank && data.ReferalFriendNotMapbank.contacts.map((item2, index2) => {
                      return index2 < 3 && (
                        <div className="contact__item" key={`nmb-${index2}`} >
                          <div className="contact__grid">
                            <div className="contact__avatar">
                              {ShortName(item2.contact_name)}
                            </div>
                            <div className="contact__info">
                              <div className="contact__name text-truncate">
                                {item2.contact_name}
                              </div>

                              <div className="contact__phone">
                                {item2.phone}
                              </div>
                            </div>
                            <div className="contact__function">
                              <a onClick={() => SendSms('nobank', 'referral_code', item2.phone)} className="message">
                                <img src="https://static.mservice.io/pwa/images/referral/message.svg" alt="" />
                              </a>
                              <a onClick={() => CallPhone('nobank', item2.phone)} className="phone">
                                <img src="https://static.mservice.io/pwa/images/referral/phone.svg" alt="" />
                              </a>
                            </div>
                          </div>
                        </div>
                      )
                    })}
                  </div>
                  <div className={`mt-0 ${!(data.ReferalFriendNotMapbank != null && data.ReferalFriendNotMapbank.user_id != null && data.ReferalFriendNotMapbank.contacts.length > 3) ? 'd-none' : ''}`}>
                    <button className="button col popup-open text-right" data-popup=".referral-notmapbank" >
                      Xem tất cả
                        </button>
                  </div>
                </div>
              </div>

            </>
          )
        }

        {data.ReferalGift.map((item, index) => (
          <div className="mm-space  mt-2 " key={index} >
            <div className="pron-container balance">
              <div className="mm-space-title">
                <h3 className="h5 mb-1 mt-0 mm-section-name">
                  {item.Title}
                </h3>

              </div>
              <div className="lutie-14 text-gray mb-3">{item.Description}</div>
            </div>

            <div className={`swiper-container swiper-item-referral swiper-gift-${item.Id}`}>
              <div className="swiper-wrapper">
                {item.ListItem.map((item2, index2) => (
                  <div className="swiper-slide" key={idx++} >
                    <div className="referral-gift ">
                      <a data-popup={`.referral-giftpopup-${item2.Id}`} className="popup-open link-absolute"></a>
                      <div className="referral-gift-img">
                        <div className="embed-responsive embed-responsive-referral2">
                          <img src={`https://static.mservice.io/img/${item2.Avatar}`}
                            className="swiper-lazy embed-responsive-img" alt="" />
                        </div>
                      </div>

                      <div className="referral-gift-name">
                        {item2.Title}
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>

        ))}

        <div className="mm-space  mt-2 ">
          <div className="pron-container balance">
            <div className="mm-space-title">
              <h3 className="h5 mb-1 mt-0 mm-section-name">
                {data.ReferalGuide.Name}
              </h3>
            </div>
            <div className="list accordion-list accordion-referral mb-4 mt-3 p-0">
              <ul>
                {data.ReferalGuide.Groups && data.ReferalGuide.Groups.map((item, index) => (
                  <li className={`accordion-item ${index == 0 ? 'accordion-item-opened' : ''}`} key={index}>
                    <a className="item-content item-link">
                      <div className="item-inner">
                        <div className="item-title  text-uppercase">{item.Name}</div>
                      </div>
                    </a>
                    <div className="accordion-item-content  bg-light">
                      {item.Items.length <= 1 ? (
                        <>
                          <ul className="steps steps-vertical">
                            {item.Items.map((item2, index2) => {
                              return (
                                item2.Blocks.map((item3, index3) => (
                                  <li className="step-item " key={index3} >
                                    <div className="step-link">
                                      <span className="step-number">{index3 + 1}</span>
                                      {/* <span className="step-title">
                                        dangerouslySetInnerHTML={{ __html: item3.Title }}
                                      </span> */}
                                      <span className="step-title"
                                        dangerouslySetInnerHTML={{ __html: item3.Title }}
                                      ></span>
                                    </div>
                                  </li>

                                )))
                            })}
                          </ul>

                          <div className=" mb-3 text-center ">
                            <a data-popup={`.gtbb-htu-popup-${item.Id}0`} className="d-inline-block button button-fill button-round popup-open color-pink px-3 ">
                              Xem
                              hình hướng
                              dẫn
                                        </a>
                          </div>

                        </>
                      ) :
                        (
                          <>
                            <div className="">
                              <div className=" htu-bank bg-light mt-0 px-2">
                                {item.Items.map((item2, index2) => (
                                  <div className="htu-bank-col popup-button" key={index2} >
                                    <a data-popup={`.gtbb-htu-popup-${item.Id}${item2.Id}`} className=" popup-open">
                                      <div className="htu-bank-item">
                                        <img className="d-block mx-auto img-fluid" alt=""
                                          src={item2.IconUrl} />
                                        <div>{item2.Title}</div>
                                      </div>
                                    </a>
                                  </div>
                                ))}
                              </div>
                            </div>
                          </>

                        )}
                    </div>
                  </li>
                ))}
              </ul>
            </div>
          </div>
        </div>


        {data.ReferalProgress != null && data.ReferalProgress.title != '' && data.ReferalProgress.list_user.length > 3 && (
          <>
            <Popup
              className="popup referral-popup popup-swipe-round referral-progress"
            >
              <Page>
                <Navbar>
                  <NavLeft>
                    <Link popupClose>
                      <Icon f7="xmark" className="text-dark"></Icon>
                    </Link>
                  </NavLeft>
                  <NavTitle sliding>{data.ReferalProgress.title}</NavTitle>
                </Navbar>

                <div className="contact__list p-3 pt-5 ">
                  {data.ReferalProgress.list_user && data.ReferalProgress.list_user.map((item2, index2) => (
                    <div className="contact__item shadow-1" key={`rpls-pu-${index2}`} >
                      <div className="contact__grid">
                        <div className="contact__avatar">
                          <img src={`https://avatars.mservice.io.s3-ap-southeast-1.amazonaws.com/${item2.phone}.png`} onError={x => ImgErrorLoad(x)}
                            alt="" />
                        </div>

                        <div className="contact__info">
                          <div className="contact__name text-truncate">
                            {item2.username}
                          </div>

                          <div className="contact__phone">
                            {item2.phone}
                          </div>
                        </div>

                        <div className="contact__function">
                          <a onClick={() => SendSms('progress', item2.current_step, item2.phone)} className="message">
                            <img src="https://static.mservice.io/pwa/images/referral/message.svg" alt="" />
                          </a>
                          <a onClick={() => CallPhone('progress', item2.phone)} className="phone">
                            <img src="https://static.mservice.io/pwa/images/referral/phone.svg" alt="" />
                          </a>
                        </div>
                      </div>

                      <div className="text-primary mt-2 mb-1 lutie-14">
                        {item2.message}, còn <span className="text-momo font-weight-bold">{item2.current_step == 'map_bank' ? '1' : '2'}</span> bước
                            </div>

                      <div className="process-step">

                        <div className="process-step__item is-active">

                        </div>

                        <div className={`process-step__item  ${item2.current_step == 'map_bank' ? 'is-active' : ''}`}></div>
                        <div className="process-step__item"></div>

                      </div>

                      <div className="lutie-13  mt-2">
                        <span className="text-gray">Bước tiếp theo : </span> {item2.next_message}
                      </div>
                    </div>

                  ))}
                </div>
              </Page>
            </Popup>

            <Popup
              className="popup referral-popup popup-swipe-round referral-progress-step1"
            >
              <Page>
                <Navbar>
                  <NavLeft>
                    <Link popupClose>
                      <Icon f7="xmark" className="text-dark"></Icon>
                    </Link>
                  </NavLeft>
                  <NavTitle sliding>{data.ReferalProgressStep1.title}</NavTitle>
                </Navbar>

                <div className="contact__list p-3 pt-5 ">
                  {data.ReferalProgressStep1.list_user && data.ReferalProgressStep1.list_user.map((item2, index2) => (
                    <div className="contact__item shadow-1" key={`rpls-pu-${index2}`} >
                      <div className="contact__grid">
                        <div className="contact__avatar">
                          <img src={`https://avatars.mservice.io.s3-ap-southeast-1.amazonaws.com/${item2.phone}.png`} onError={x => ImgErrorLoad(x)}
                            alt="" />
                        </div>

                        <div className="contact__info">
                          <div className="contact__name text-truncate">
                            {item2.username}
                          </div>

                          <div className="contact__phone">
                            {item2.phone}
                          </div>
                        </div>

                        <div className="contact__function">
                          <a onClick={() => SendSms('progress', item2.current_step, item2.phone)} className="message">
                            <img src="https://static.mservice.io/pwa/images/referral/message.svg" alt="" />
                          </a>
                          <a onClick={() => CallPhone('progress', item2.phone)} className="phone">
                            <img src="https://static.mservice.io/pwa/images/referral/phone.svg" alt="" />
                          </a>
                        </div>
                      </div>

                      <div className="text-primary mt-2 mb-1 lutie-14">
                        {item2.message}, còn <span className="text-momo font-weight-bold">{item2.current_step == 'map_bank' ? '1' : '2'}</span> bước
                        </div>

                      <div className="process-step">

                        <div className="process-step__item is-active">

                        </div>

                        <div className={`process-step__item  ${item2.current_step == 'map_bank' ? 'is-active' : ''}`}></div>
                        <div className="process-step__item"></div>

                      </div>

                      <div className="lutie-13  mt-2">
                        <span className="text-gray">Bước tiếp theo : </span> {item2.next_message}
                      </div>
                    </div>

                  ))}
                </div>
              </Page>
            </Popup>

            <Popup
              className="popup referral-popup popup-swipe-round referral-progress-step2"
            >
              <Page>
                <Navbar>
                  <NavLeft>
                    <Link popupClose>
                      <Icon f7="xmark" className="text-dark"></Icon>
                    </Link>
                  </NavLeft>
                  <NavTitle sliding>{data.ReferalProgressStep2.title}</NavTitle>
                </Navbar>

                <div className="contact__list p-3 pt-5 ">
                  {data.ReferalProgressStep2.list_user && data.ReferalProgressStep2.list_user.map((item2, index2) => (
                    <div className="contact__item shadow-1" key={`rpls-pu-${index2}`} >
                      <div className="contact__grid">
                        <div className="contact__avatar">
                          <img src={`https://avatars.mservice.io.s3-ap-southeast-1.amazonaws.com/${item2.phone}.png`} onError={x => ImgErrorLoad(x)}
                            alt="" />
                        </div>

                        <div className="contact__info">
                          <div className="contact__name text-truncate">
                            {item2.username}
                          </div>

                          <div className="contact__phone">
                            {item2.phone}
                          </div>
                        </div>

                        <div className="contact__function">
                          <a onClick={() => SendSms('progress', item2.current_step, item2.phone)} className="message">
                            <img src="https://static.mservice.io/pwa/images/referral/message.svg" alt="" />
                          </a>
                          <a onClick={() => CallPhone('progress', item2.phone)} className="phone">
                            <img src="https://static.mservice.io/pwa/images/referral/phone.svg" alt="" />
                          </a>
                        </div>
                      </div>

                      <div className="text-primary mt-2 mb-1 lutie-14">
                        {item2.message}, còn <span className="text-momo font-weight-bold">{item2.current_step == 'map_bank' ? '1' : '2'}</span> bước
                    </div>

                      <div className="process-step">

                        <div className="process-step__item is-active">

                        </div>

                        <div className={`process-step__item  ${item2.current_step == 'map_bank' ? 'is-active' : ''}`}></div>
                        <div className="process-step__item"></div>

                      </div>

                      <div className="lutie-13  mt-2">
                        <span className="text-gray">Bước tiếp theo : </span> {item2.next_message}
                      </div>
                    </div>

                  ))}
                </div>
              </Page>
            </Popup>
          </>
        )}

        {data.ReferalFriendNonMoMoData != null && data.ReferalFriendNonMoMoData.user_id != null && data.ReferalFriendNonMoMoData.contacts != null && data.ReferalFriendNonMoMoData.contacts.length > 3 && (
          <>

            <Popup
              className="popup referral-popup popup-swipe-round referral-nonmomo"
            >
              <Page>
                <Navbar>
                  <NavLeft>
                    <Link popupClose>
                      <Icon f7="xmark" className="text-dark"></Icon>
                    </Link>
                  </NavLeft>
                  <NavTitle sliding>Hội chưa từng dùng MoMo</NavTitle>
                </Navbar>

                <div className="contact__list p-3 pt-5 ">
                  {data.ReferalFriendNonMoMoData && data.ReferalFriendNonMoMoData.contacts.map((item2, index2) => (
                    <div className="contact__item shadow-1" key={`nmmd2-pu-${index2}`} >
                      <div className="contact__grid">
                        <div className="contact__avatar">
                          {ShortName(item2.contact_name)}
                        </div>
                        <div className="contact__info">
                          <div className="contact__name text-truncate">
                            {item2.contact_name}
                          </div>

                          <div className="contact__phone">
                            {item2.phone}
                          </div>
                        </div>
                        <div className="contact__function">
                          <a onClick={() => SendSms('nomomo', 'referral_link', item2this.phone)} className="message">
                            <img src="https://static.mservice.io/pwa/images/referral/message.svg" alt="" />
                          </a>
                          <a onClick={() => CallPhone('nomomo', item2.phone)} className="phone">
                            <img src="https://static.mservice.io/pwa/images/referral/phone.svg" alt="" />
                          </a>
                        </div>
                      </div>
                    </div>

                  ))}
                </div>
              </Page>
            </Popup>

          </>
        )}


        {data.ReferalFriendNotMapbank != null && data.ReferalFriendNotMapbank.user_id != null && data.ReferalFriendNotMapbank.contacts != null && data.ReferalFriendNotMapbank.contacts.length > 3 && (
          <>

            <Popup
              className="popup referral-popup popup-swipe-round referral-notmapbank"
            >
              <Page>
                <Navbar>
                  <NavLeft>
                    <Link popupClose>
                      <Icon f7="xmark" className="text-dark"></Icon>
                    </Link>
                  </NavLeft>
                  <NavTitle sliding>Hội đã có MoMo, chưa liên kết ngân hàng</NavTitle>
                </Navbar>

                <div className="contact__list p-3 pt-5 ">
                  {data.ReferalFriendNotMapbank && data.ReferalFriendNotMapbank.contacts.map((item2, index2) => (
                    <div className="contact__item shadow-1" key={`nmb-pu-${index2}`} >
                      <div className="contact__grid">
                        <div className="contact__avatar">
                          {ShortName(item2.contact_name)}
                        </div>
                        <div className="contact__info">
                          <div className="contact__name text-truncate">
                            {item2.contact_name}
                          </div>

                          <div className="contact__phone">
                            {item2.phone}
                          </div>
                        </div>
                        <div className="contact__function">
                          <a onClick={() => SendSms('nobank', 'referral_code', item2.phone)} className="message">
                            <img src="https://static.mservice.io/pwa/images/referral/message.svg" alt="" />
                          </a>
                          <a onClick={() => CallPhone('nobank', item2.phone)} className="phone">
                            <img src="https://static.mservice.io/pwa/images/referral/phone.svg" alt="" />
                          </a>
                        </div>
                      </div>
                    </div>

                  ))}
                </div>
              </Page>
            </Popup>

          </>
        )}

        {data.ReferalGift.map((item, index) => {
          return (
            item.ListItem.map((item2, index2) => (
              <Popup
                key={index2}
                className={`popup referral-popup popup-swipe-round referral-giftpopup-${item2.Id}`}
              >
                <Page>
                  <Navbar>
                    <NavLeft>
                      <Link popupClose>
                        <Icon f7="xmark" className="text-dark"></Icon>
                      </Link>
                    </NavLeft>
                    <NavTitle sliding>{item2.Title}</NavTitle>
                  </Navbar>

                  <div>
                    <div className="p-3 pt-5 referral-gift-detail">
                      <p className="font-weight-bold"><span className=" text-momo">Số lượng thẻ quà nhận được: </span>{item2.Quantity}</p>
                      <p className="font-weight-bold text-momo">Nơi sử dụng:</p>
                      <p dangerouslySetInnerHTML={{ __html: item2.ApplyPlace }}
                      ></p>
                      <p className="font-weight-bold text-momo">Điều kiện sử dụng:</p>
                      <p dangerouslySetInnerHTML={{ __html: item2.ApplyRule }}
                      ></p>
                    </div>
                  </div>
                </Page>
              </Popup>
            )))
        })}


        {data.ReferalGuide.Groups && data.ReferalGuide.Groups.map((item, index) => {
          return (item.Items.length <= 1 ?
            (
              item.Items.map((item2, index2) => (
                <Popup
                  key={index2}
                  className={`popup cas-htu-popup gtbb-htu-popup-${item.Id}0 popup-swipe-round`}
                  onPopupOpen={() => HtuGroupOpen(`${item.Id}0`)}
                >
                  <Page>
                    <Navbar>
                      <NavLeft>
                        <Link popupClose>
                          <Icon f7="xmark" className="text-dark"></Icon>
                        </Link>
                      </NavLeft>
                      <NavTitle sliding>{item.Name}</NavTitle>
                    </Navbar>
                    <div className={`swiper-container gtbb-htu-sw-${item.Id}0 htu-swiper`} >
                      <div className={`swiper-pagination gtbb-pagination-${item.Id}0`}></div>
                      <div className={`swiper-button-next gtbb-navigation-next-btn-${item.Id}0 htu-navigation-btn`}></div>
                      <div className={`swiper-button-prev gtbb-navigation-pre-btn-${item.Id}0 htu-navigation-btn`}></div>
                      <div className="swiper-wrapper">
                        {item2.Blocks.map((item3, index3) => (
                          <div className="swiper-slide" key={index3} >
                            <div className="htu-content">
                              <div className="htu-mockup">
                                <div className="htu-mockup-slider" style={{ backgroundimage: `url('https://static.mservice.io/pwa/images/phone-mockup.png')` }}>
                                  <img loading="lazy" src={item3.Avatar} className="img-fluid d-block mx-auto swiper-lazy" alt="" />
                                </div>

                                <div className="htu-mockup-txt">
                                  <h3 className="htu-mockup-title mt-0 mb-2"
                                    dangerouslySetInnerHTML={{ __html: item3.Title }}></h3>
                                  <div className="htu-mockup-body"
                                    dangerouslySetInnerHTML={{ __html: item3.Content }}>
                                  </div>
                                </div>

                              </div>
                            </div>

                          </div>
                        ))}
                      </div>
                    </div>
                  </Page>
                </Popup>
              ))
            )
            :
            (
              item.Items.map((item2, index2) => (
                <Popup
                  key={index2}
                  className={`popup cas-htu-popup gtbb-htu-popup-${item.Id}${item2.Id} popup-swipe-round`}
                  onPopupOpen={() => HtuGroupOpen(`${item.Id}${item2.Id}`)}
                >
                  <Page>
                    <Navbar>
                      <NavLeft>
                        <Link popupClose>
                          <Icon f7="xmark" className="text-dark"></Icon>
                        </Link>
                      </NavLeft>
                      <NavTitle sliding>{item.Name}</NavTitle>
                    </Navbar>
                    <div className={`swiper-container gtbb-htu-sw-${item.Id}${item2.Id} htu-swiper`}>
                      <div className={`swiper-pagination gtbb-pagination-${item.Id}${item2.Id}`}></div>
                      <div className={`swiper-button-next gtbb-navigation-next-btn-${item.Id}${item2.Id} htu-navigation-btn`}></div>
                      <div className={`swiper-button-prev gtbb-navigation-pre-btn-${item.Id}${item2.Id} htu-navigation-btn`}></div>
                      <div className="swiper-wrapper">
                        {item2.Blocks.map((item3, index3) => (
                          <div className="swiper-slide" key={index3} >
                            <div className="htu-content">
                              <div className="htu-mockup">
                                <div className="htu-mockup-slider" style={{ backgroundimage: `url('https://static.mservice.io/pwa/images/phone-mockup.png')` }}>
                                  <img loading="lazy" src={item3.Avatar} className="img-fluid d-block mx-auto swiper-lazy" alt="" />
                                </div>

                                <div className="htu-mockup-txt">
                                  <h3 className="htu-mockup-title mt-0 mb-2"
                                    dangerouslySetInnerHTML={{ __html: item3.Title }}></h3>
                                  <div className="htu-mockup-body"
                                    dangerouslySetInnerHTML={{ __html: item3.Content }}>
                                  </div>
                                </div>

                              </div>
                            </div>

                          </div>
                        ))}
                      </div>
                    </div>
                  </Page>
                </Popup>
              ))
            )
          )
        })}

        {data.CustomAxInfo.status &&
          data.CustomAxInfo.block_list.map((item, index) => (
            <Popup
              key={index}
              className={`popup referral-feature-popup-${item.id} popup-swipe-round`}
            >
              <Page>
                <Navbar>
                  <NavLeft>
                    <Link popupClose>
                      <Icon f7="xmark" className="text-dark"></Icon>
                    </Link>
                  </NavLeft>
                  <NavTitle sliding>{item.title}</NavTitle>
                </Navbar>
                <div className="referral-feature-change">
                  <div className="inner">
                    <div className="w-100">
                      <div className="tt">
                        {item.note}
                      </div>
                      <div className="row justify-content-center">
                        <button className="col button button-fill color-pink font-weight-bold mx-2" onClick={() => CustomAxInfoAccept(item.id)} >{item.ctaLeft}</button>
                        <button className="col button button-fill color-gray font-weight-bold mx-2 popup-close">
                          {item.ctaRight}
                        </button>
                      </div>
                    </div>
                  </div>
                </div>

                <div className="page-content">
                  <div className="px-3 pt-4 referral-feature-detail"
                                dangerouslySetInnerHTML={{ __html: item.description }}>
                  </div>
                </div>
              </Page>
            </Popup>
          ))
        }

        <ReferralPageHtu />
      </Page>
      <style jsx>{``}</style>
    </>
  );
};

export default ReferralPage;

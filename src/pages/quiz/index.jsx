import React, { useState, useEffect } from "react";
import {
  f7,
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  Sheet,
  Icon,
  Toolbar,
  Button,
  Block,
} from "framework7-react";
import $ from "dom7";
import format from "number-format.js";
import events from "@/components/events";
import axios from "@/libs/axios";
import MaxApi from "@momo-platform/max-api";
import {
  ApiResponseErrorCodeMessage,
  System,
  ValidateAccessToken,
  FormatNumber,
} from "@/libs/constants";
import "./quiz.less";

const Quiz = (props) => {
  const [SurveyModel, setSurveyModel] = useState({
    CampaignCode: "",
    UserInfo: {},
    SurveyInfo: {
      quiz: {
        questions: []
      },
      user_record: {
        answers: []
      }
    },
    SliderActive: 0,
    IsFinish: false,
    SliderDesc: "",
    IsError: false,
    IsOnline: true,
    IsHaveSurvey: true,
    IsTimeOut: false
  });

  const quizProcessActive = (index) => {
    var quizProcessItems = $('.quiz__process__swiper .swiper-slide');
    var swiper1 = f7.swiper.get(".quiz__swiper");
    var swiper2 = f7.swiper.get(".quiz__process__swiper");
    SurveyModel.SliderActive = index;

    var currentQuiz = SurveyModel.SurveyInfo.quiz.questions[index];
    if (currentQuiz != undefined) {
      if (currentQuiz.description != "") {
        SurveyModel.SliderDesc = currentQuiz.description;
        currentQuiz.description = "";
        var dynamicPopover = f7.popover.create({
          el: '.popover-about',
          targetEl: '.quiz__intro',
          on: {
            open: function (d) {
              d.$el.find(".pron-popup-close").on('click', function (e) {
                f7.popover.close();
              });
            }
          }
        });

        setTimeout(function () {
          dynamicPopover.open();
        }, 200);
      }
      setSurveyModel({
        ...SurveyModel,
      });
      quizProcessItems.removeClass("is-active");
      swiper2.slideTo(index - 3);
      swiper1.slideTo(index);
      quizProcessItems[index].classList.add("is-active");

      if (!currentQuiz.isAnswer) {
        swiper1.allowSlideNext = false;
      }
      else {
        swiper1.allowSlideNext = true;
      }
    }

  }


  const GetQuizDetail = (fnc) => {
    var url = `${System.apihost}/__get/Ajax/SurveyGetData?campaignCode=${SurveyModel.CampaignCode}&userPhone=${SurveyModel.UserInfo.phone}`;
    axios
      .get(url)
      .then((res) => {
        if (!res.Result && res.Data == null) {
          SurveyModel.IsError = true;

          if (res.ErrorCode == "survey_timeout") {
            SurveyModel.IsTimeOut = true;
          }
          if (res.ErrorCode == "no_survey") {
            SurveyModel.IsHaveSurvey = false;
          }
          f7.preloader.hide();
          setSurveyModel({
            ...SurveyModel,
          });
          return;
        }

        res = res.Data;
        fnc && fnc(res);
      })
      .catch((error) => {
        debugger;

        SurveyModel.IsError = true;
        SurveyModel.IsOnline = window.navigator.onLine;
        f7.preloader.hide();
        setSurveyModel({
          ...SurveyModel,
        });
        return;
      });
  };

  const SubmitSurveyData = (answerParam, fnc) => {
    debugger;
    var url = `${System.apihost}/__post/Ajax/SurveySubmitDataV2`;
    var param = {
      user_id: SurveyModel.UserInfo.phone,
      campaign_code: SurveyModel.CampaignCode,
      event_time_submit: 0,
      user_record: {
        answers: answerParam
      }
    };
    axios
      .post(url, {
        data: param,
      })
      .then((res) => {
        debugger;
        if (!res.Result && res.Data == null) {
          f7.preloader.hide();
          if (res.ErrorCode == "survey_timeout") {
            SurveyModel.IsError = true;
            SurveyModel.IsTimeOut = true;
            self.$setState(SurveyModel);
          }
          else {
            f7.dialog.alert("", ApiResponseErrorCodeMessage["request_error"], function () {
              window.ReactNativeWebView.postMessage('FinishActivity');
            });
          }
          return;
        }

        res = res.Data;
        fnc && fnc(res);
      })
      .catch((error) => {
        debugger;

        SurveyModel.IsError = true;
        SurveyModel.IsOnline = window.navigator.onLine;
        f7.preloader.hide();
        setSurveyModel({
          ...SurveyModel,
        });
        return;
      });
  };
  const BindQuiz = () => {
    GetQuizDetail((data) => {
      if (data == null) {
        return;
      }

      for (var i = 0; i < data.quiz.questions.length; i++) {
        var answerRecord = data.user_record.answers.find(x => x.question_id === data.quiz.questions[i].id);
        if (answerRecord != undefined) {
          data.quiz.questions[i].isAnswer = true;
          data.quiz.questions[i].hasSkip = true;
          data.quiz.questions[i].isChange = false;

          for (var j = 0; j < data.quiz.questions[i].options.length; j++) {
            if (answerRecord.selections.includes(data.quiz.questions[i].options[j].answer_id)) {
              data.quiz.questions[i].options[j].isSelected = true;
              data.quiz.questions[i].hasSkip = false;
            }
          }
        }
        else {
          data.quiz.questions[i].isAnswer = false;
          data.quiz.questions[i].hasSkip = true;
        }

      }

      SurveyModel.SliderActive = data.quiz.questions.indexOf(data.quiz.questions.find(x => !x.isAnswer));
      if (SurveyModel.SliderActive < 0) {
        SurveyModel.IsFinish = true;
      }

      SurveyModel.SurveyInfo = data;
      setSurveyModel({
        ...SurveyModel,
      });
      var swiper1 = f7.swiper.create('.quiz__swiper', {
        speed: 400,
        spaceBetween: 5,
        slidesPerView: 'auto',
        centeredSlides: true,
        //allowTouchMove: false,
        on: {
          slideChangeTransitionEnd: function () {
            quizProcessActive(this.activeIndex)
          },
          //slideNextTransitionStart: function (sw) {
          //    if ((sw.activeIndex - 1) >= (SurveyModel.SliderActive + 1)) {
          //        return false;
          //    }
          //}
        }

      });
      var swiper2 = f7.swiper.create('.quiz__process__swiper', {
        speed: 400,
        spaceBetween: 3,
        slidesPerView: 'auto',
        centerInsufficientSlides: true
      });

      var quizProcessItems = $('.quiz__process__swiper .swiper-slide');
      // console.log(quizProcessItems);
      quizProcessItems.each(function (i) {
        var $i = i;
        $(this).on("click", function (e) {
          //var index = SurveyModel.SurveyInfo.quiz.questions.indexOf(SurveyModel.SurveyInfo.quiz.questions.find(x => x.id === $$(this).attr("data-id")));
          var indexReply = SurveyModel.SurveyInfo.quiz.questions.indexOf(SurveyModel.SurveyInfo.quiz.questions.find(x => !x.isAnswer));
          //if (index > (indexReply + 1)) {
          //    return false;
          //}
          if ($i > (indexReply)) {
            return false;
          }

          var quest = SurveyModel.SurveyInfo.quiz.questions[SurveyModel.SliderActive];
          if (quest != undefined && quest.isChange) {
            return false;
          }

          quizProcessActive($i);
          swiper1.slideTo($i);
        })
      });
debugger;


      //Active first use
      quizProcessActive(SurveyModel.SliderActive);

      $('.quiz__answer__item').on('click', function (e) {
        var quizId = parseInt($(this).attr("data-quizid"));
        var dataSelected = parseInt($(this).attr("data-selected"));
        var answer = SurveyModel.SurveyInfo.user_record.answers.find(x => x.question_id === quizId);
        var quest = SurveyModel.SurveyInfo.quiz.questions.find(x => x.id === quizId);
        if (answer == undefined) {
          answer = {
            question_id: quizId,
            reply_data: '',
            selections: []
          };
          SurveyModel.SurveyInfo.user_record.answers.push(answer);
        }

        var indexAnswer = answer.selections.indexOf(dataSelected);
        if (answer.selections.length > 0 && indexAnswer < 0) {
          if (quest.isSkipped && answer.selections.indexOf(0) >= 0) {
            answer.selections.splice(answer.selections.indexOf(0), 1);
          }
          else if (quest.questionType == "single") {
            $('.quiz__answer__item[data-quizid=' + quizId + ']').removeClass('is-active');
            answer.selections = [];
          }
        }

        if (quest.isAnswer && quest.questionType == "single" && indexAnswer >= 0 && !quest.isSkipped) {
          return;
        }

        if ($(this).hasClass("is-active")) {
          answer.selections.splice(indexAnswer, 1);
          $(this).removeClass('is-active');
        }
        else {
          answer.selections.push(dataSelected);
          $(this).addClass('is-active');
        }

        if (answer.selections.length > 0) {
          if (quest.isSkipped) {
            $("#btnSkipAnswer" + quizId).addClass("d-none");
          }
          else {
            $("#btnSubmitAnswerGray" + quizId).addClass("d-none");
          }
          $("#btnSubmitAnswer" + quizId).removeClass("d-none");
        } else {
          if (quest.isSkipped) {
            $("#btnSkipAnswer" + quizId).removeClass("d-none");
          }
          else {
            $("#btnSubmitAnswerGray" + quizId).removeClass("d-none");
          }
          $("#btnSubmitAnswer" + quizId).addClass("d-none");
        }

        if (quest.isAnswer) {
          quest.isChange = true;
          var swiper1 = f7.swiper.get(".quiz__swiper");
          swiper1.allowSlideNext = false;
          swiper1.allowSlidePrev = false;
        }

        try {
          var param = {
            action: 'click_reply'
          };
          ProcessAppTrackingCustomParam('data_collection', param);
        } catch (e) {

        }

        setSurveyModel({
          ...SurveyModel,
        });
      });

      f7.preloader.hide();

    });
  }

  const SkipQuiz = (quizId) => {
    f7.preloader.show();
    var quest = SurveyModel.SurveyInfo.quiz.questions.find(x => x.id === quizId);
    var answer = SurveyModel.SurveyInfo.user_record.answers.find(x => x.question_id === quizId);
    if (answer == undefined) {
      answer = {
        question_id: quizId,
        reply_data: 'Bỏ qua',
        selections: [0]
      };
      SurveyModel.SurveyInfo.user_record.answers.push(answer);
    }
    else {
      answer = {
        question_id: quizId,
        reply_data: 'Bỏ qua',
        selections: [0]
      };
    }
    if (quest.isChange != undefined && !quest.isChange) {
      SurveyModel.SliderActive = SurveyModel.SliderActive + 1;
      quizProcessActive(app, self, SurveyModel.SliderActive);
      f7.preloader.hide();
      setSurveyModel({
        ...SurveyModel,
      });
      return;
    }
    try {
      var param = {
        action: 'click_skip'
      };
      ProcessAppTrackingCustomParam('data_collection', param);
    } catch (e) {

    }

    var param = [answer];
    SubmitSurveyData(param, function (data) {
      f7.preloader.hide();

      if ((SurveyModel.SliderActive + 1) >= SurveyModel.SurveyInfo.quiz.questions.length) {
        SurveyModel.IsFinish = true;
        SubmitSurveyData(SurveyModel.SurveyInfo.user_record.answers, function (data) {
          //console.log(data);
        });
      }
      else {
        SurveyModel.SliderActive = SurveyModel.SliderActive + 1;
        var swiper1 = f7.swiper.get(".quiz__swiper");
        swiper1.allowSlideNext = true;
        swiper1.allowSlidePrev = true;

        quest.isAnswer = true;
        quest.hasSkip = true;
        quest.isChange = false;
        quizProcessActive(SurveyModel.SliderActive);
      }
      setSurveyModel({
        ...SurveyModel,
      });
    });
  }

  const AnswerQuiz = (quizId) => {
    var quest = SurveyModel.SurveyInfo.quiz.questions.find(x => x.id === quizId);
    var answer = SurveyModel.SurveyInfo.user_record.answers.find(x => x.question_id === quizId);

    if (answer == undefined) {
      f7.dialog.alert("Có lỗi xảy ra", "Vui lòng thử lại");
      return;
    }


    //reply_data
    answer.reply_data = "";
    for (var i = 0; i < answer.selections.length; i++) {
      answer.reply_data = answer.reply_data + quest.options.find(x => x.answer_id === answer.selections[i]).answer_content + ", ";
    }

    if (quest.isChange != undefined && !quest.isChange) {
      SurveyModel.SliderActive = SurveyModel.SliderActive + 1;
      quizProcessActive(app, self, SurveyModel.SliderActive);
      f7.preloader.hide();
      setSurveyModel({
        ...SurveyModel,
      });
      return;
    }
    try {
      var param = {
        action: 'click_next'
      };
      ProcessAppTrackingCustomParam('data_collection', param);
    } catch (e) {

    }

    var param = [answer];
    SubmitSurveyData(param, function (data) {
      f7.preloader.hide();
      if ((SurveyModel.SliderActive + 1) >= SurveyModel.SurveyInfo.quiz.questions.length) {
        SurveyModel.IsFinish = true;
        SubmitSurveyData(SurveyModel.SurveyInfo.user_record.answers, function (data) {
          try {
            var param = {
              action: 'click_submit'
            };
            ProcessAppTrackingCustomParam('data_collection', param);
          } catch (e) {

          }
        });
      }
      else {
        SurveyModel.SliderActive = SurveyModel.SliderActive + 1;
        var swiper1 = f7.swiper.get(".quiz__swiper");
        swiper1.allowSlideNext = true;
        swiper1.allowSlidePrev = true;

        quest.isAnswer = true;
        quest.hasSkip = false;
        quest.isChange = false;
        quizProcessActive(SurveyModel.SliderActive);
      }
      setSurveyModel({
        ...SurveyModel,
      });
    });
  }

  const FinisSurveyClick = (quizId) => {
    SubmitSurveyData(SurveyModel.SurveyInfo.user_record.answers, function (data) {
      try {
        var param = {
          action: 'click_submit'
        };
        ProcessAppTrackingCustomParam('data_collection', param);

      } catch (e) {

      }
      location.href = "momo://?refId=" + SurveyModel.SurveyInfo.quiz.cta;
    });
  }


  const pageInit = () => {
    SurveyModel.CampaignCode = props.campaignCode;
    setSurveyModel({
      ...SurveyModel,
    });
    f7.preloader.show();

    events.on("initPageData", (res) => {
      res = JSON.parse(res);
      SurveyModel.UserInfo = res;
      setSurveyModel({
        ...SurveyModel
      });

      BindQuiz();
    });

    try {
      MaxApi.getProfile(function (res) {
        alert(JSON.stringify(res));
        SurveyModel.UserInfo = {
          phone: res.user,
          userName: res.displayName,
        };
        setSurveyModel({
          ...SurveyModel
        });

        BindQuiz();
      });

      window.ReactNativeWebView.postMessage("GetUserInfo");

    } catch (e) {
      //alert('1');
    }


  };

  const pageDestroy = () => { };

  return (
    <>
      <Page
        onPageInit={pageInit}
        onPageBeforeRemove={pageDestroy}
      >
        <div className="quiz__intro"></div>

        {!SurveyModel.IsError ?
          (
            SurveyModel.SurveyInfo.quiz.id && (!SurveyModel.IsFinish ? (
              <div className="quiz__wrapper" style={{ backgroundimage: `url(${SurveyModel.SurveyInfo.quiz.background_img})` }}>
                <div className="quiz__heading">
                  <div className="swiper-container quiz__process quiz__process__swiper">
                    <div className="swiper-wrapper">
                      {SurveyModel.SurveyInfo.quiz.questions.map((item, index) => (
                        <div className="swiper-slide" data-id={item.id} key={index}>
                          <div className={`quiz__process__item ${index} ${item.isAnswer ? "is-select" : ""}`} >
                            <span> {index + 1}</span>
                            <i className="f7-icons">checkmark_alt</i>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>

                <div className="quiz__slider quiz__swiper swiper-container">
                  <div className="swiper-wrapper">
                    {SurveyModel.SurveyInfo.quiz.questions.map((item, index) => (
                      <div className="swiper-slide" key={index}>
                        <div className="quiz__body">
                          <div className="quiz__body__scroll">
                            <div className="quiz__question">
                              <div className="mb-1 quiz__question__label">
                                Câu {index + 1}/{SurveyModel.SurveyInfo.quiz.questions.length}
                              </div>
                              <div
                                dangerouslySetInnerHTML={{ __html: item.question }}
                              ></div>
                            </div>

                            <div className={`quiz__answer ${item.displayType == 'list' ? (item.questionType == 'multiple' ? 'multiple' : 'single') : item.displayType}`} >
                              {item.options.map((item2, index) => (
                                <div key={index} className={`quiz__answer__item  ${item2.isSelected ? 'is-active' : ''}`} data-quizid={item.id} data-selected={item2.answer_id} >

                                  {item.displayType == 'grid_image' && (
                                    <>
                                      <div className="quiz__answer__image__check">
                                        <i className="f7-icons">checkmark_alt_circle_fill</i>
                                      </div>
                                      <div className="quiz__answer__image">
                                        <div className="embed-responsive embed-responsive-16by9 ">
                                          <img src={item2.answer_img} className="img-fluid embed-responsive-img" alt={item2.answer_content} />
                                        </div>
                                      </div>
                                    </>
                                  )}
                                  <div className="quiz__answer__check ">
                                    <i className="f7-icons">check</i>
                                  </div>
                                  <span className="noselect">
                                    {item2.answer_content}
                                  </span>
                                </div>
                              ))}


                              
                            </div>
                          </div>

                          <div className="quiz__function text-center">
                          <a id={`btnSubmitAnswer${item.id}`} onClick={x => AnswerQuiz(item.id)} className={`${(item.isAnswer && item.hasSkip) || !item.isAnswer  ? 'd-none' : ''} quiz__btn is-green have-image`} >
                                        <img className="quiz__function__hmm noselect" src="https://static.mservice.io/blogscontents/momo-upload-api-200727102208-637314421287599333.png" alt="" />
                                        {SurveyModel.SliderActive >= (SurveyModel.SurveyInfo.quiz.questions.length - 1) ? 'Xong rồi nè' : 'Câu tiếp theo >>'}
                                    </a>
                                    <a id={`btnSubmitAnswerGray${item.id}`}  className={`quiz__btn is-gray have-image ${!item.isSkipped ? (!item.isAnswer  ? '' : 'd-none') : 'd-none'}`}>
                                        <img className="quiz__function__hmm noselect" src="https://static.mservice.io/blogscontents/momo-upload-api-200727102208-637314421287599333.png" alt="" />
                                        {'Câu tiếp theo >>'}
                                    </a>
                                    <a id={`btnSkipAnswer${item.id}`} onClick={x => SkipQuiz(item.id)} className={`quiz__btn  is-pink  ${item.isSkipped ? ((item.isAnswer && item.hasSkip) || !item.isAnswer  ? '' : 'd-none') : 'd-none'}`} >
                                        Bỏ qua
                                    </a>
                          </div>
                        </div>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            ) : (
                <div className="text-center errInfo">
                  <img src={SurveyInfo.quiz.campaign_img} />
                  <h3 className="text-gray">
                    {SurveyModel.SurveyInfo.quiz.display_text}
                  </h3>
                  {SurveyModel.SurveyInfo.quiz.btn_text != '' && (
                    <div>
                      <button onClick={x => FinisSurveyClick()} className="external button button-fill button-large color-pink  font-weight-bold">{SurveyModel.SurveyInfo.quiz.btn_text}</button>
                    </div>
                  )
                  }
                </div>
              ))
          )
          : (
            <>
              {!SurveyModel.IsOnline && (
                <div className="text-center errInfo">
                  <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHgAAAB4CAYAAAA5ZDbSAAAAAXNSR0IArs4c6QAAAERlWElmTU0AKgAAAAgAAYdpAAQAAAABAAAAGgAAAAAAA6ABAAMAAAABAAEAAKACAAQAAAABAAAAeKADAAQAAAABAAAAeAAAAAAI4lXuAAAYZklEQVR4Ae1dCZQcxXmunmPn2N05VsfugoTuA7AMgUXGAgmsOCEgmRBEMBg/BwiPPAgvcYDYHCEQmxdCAOMXEZz3IMEGIV4IBHGEEEMCCAlJGBAKh2wjghBIFitp59jduacr3z9S93b39GwfMz3bO0y9tztd1X/9VfV/dfxV9Vc1Yy3mOOfH4+8N/Dnp9oD5+ZNBdMJkyKSVPELwm0G/zEocm7SHEK9XEISyzfhNidZSAAPcfkhtX1Mkx1gC4PY0KS3byXhsx3RnxLObmK2Hm5iW7aR8tmO6M6IK4E8+G2Kf7B2qO6fTpnSxY+f3KfkU4blHGeDW55ZpweieqbL+jlLQQ8mM0mv7eWZ/XBv3UXTPn2oD3eif9C0YwHoSudyMfF48PxDwRCUhF0tlNpLJS17bvz2xMOvqDCjjc3juVAa4+XlSAQwwvalU8QTOxRVMEE/nXDg2kcrNZZwHeZCU2bEOKYHWC/q6ZT/zqKrW+yxa7y/rZtwkBq4GmLrddLo4wHnpDJEJKxLJ3OmM8UhFNhXs8O8Ihj6/VyWyRnTPka4gi3aHVHyHRwqrUHneZILnVSYIGz2847VoVKh/oFel0jiP66ZJAFVIjuSW8zL7lsD4BWiEU4yKK6AUsagaiC1vfcyom67HHb+wn02Jd8osikV0+6MF2U8PNA9GHXtJ8LDHYt3Bp+BPqwgm2OMagJPJwkkiEy9mXLwIMplhRS4dHV7WGe6Qo6RHcuyd9z+T/XYeiN/JS45RRR0eybNSSVSFqTyCkEOl/A/GvetjsY7nAXZO9X4CPBMKMFprMJXKXyoy/ucYMBfbLT+BQSBLbjemR3vqnB4tntfLpk/tllhWgCWAzTshDbAfBsg/isVCH5uP11jKMa2ksXzH5ZZM8vhQMntzMpX9ROTiT+oBlxLy+9XFIAWrHhcM+FXgEq9cnqa+VhyPoOu+Bn8foqzrE4nCiVZiN4pWLZlGca3BJ5PhM6D13iPy3B6AejvG1+k1SE0H+7weGgdl+kZMj2b0x2R+9FAui6xYHKdrVlGrPeilvCjrxZyVtgPoF5LJ7Eo1hbO+pgBMLTaRyKzNF3L/x0XxWqi+XY0qlrb1kvYModpm3wFtvG/6YUVdYpLLl6TH+n45P0vk/L8B9OvQOQbqY2YutqMAQ9Ae1NgrOMv+utJdce43ly3zVP4GT4+O7osxj6JHEEWRFQr1aeNVpeH8q5yVtyWS2QeGh/m0qvcNDHAM4FSq8BWMsdtQYx9Ag5rawDzLrAgHL7popUum7I+/1N0f1SsvhlXY5nINar3KTOKZKj/+riiVs78eSuWuwfOYlqihrcerlk49nI7EpRqZSGUfEnl5C4B1tBvStl6aHtUz9+0HuMoKI4qc5RvdejUyhoxiTBTXYhFn+1A6u1zzum5vQwEmBaJYyu7gIr8UNXJM86k7m9UM0HoHoe2qJrv1rF55PAKbge5Z6YDvkyjEWixnvAtFzv7ArmRa85kvEUT2CmQI5bNxrbkhIFCGUqnc30ACN+K5oZVGkgcAJQuKl7ngednvEV7p6ur4EP6D+JM1orff+xQrTVbmqhJ3Vuma589WDYfDeDsLwCaIKp3mU0WxcAbn5TM5E1aikz1uLHajn4TNwQC/OBwO171jVTfAmUxmZi4vPIYCn9boYqLlZJjAnsE8aF08EvgvCFseEFGRzkR6L0tp0jLi1u27bWnQNM065YRZLBhQLc3fjfC/lPhrf4eH88dh+nQJuthLOOOztO/r9SNLCayIXR6PBzbUw0tVIquMEon8eRij/gXgVm25WOUl0UOo1BW+BGDXxSLBf4d/RHqn+T1H6R+CcgXQlUGmn2lDXwMuxY2C3w8MmJB6/XBZFGeWy2wJNG4y+AtSHBEr1AVUOrsORYFMy09henkfVsKuhxxsdU22WzAUqVsw1hoJwHT5UIAidobW+3yeO7u7AzuNIkKQ74LmSxLdzl372YFDteqCRKX/S2vOyrVsfSrroYVCiY1mrK6A6aQjCFu8QnC1nV0ry+MlBIu5beYfGweukCVFhgfY/Hg8dKlJcGdCDDK4JBJUOB3JGAeRcuUEuJSy3++z34SVWce8ucxzm2g4VAabebYEMMANYG77OLTLq80wH48GLbaELbZ7/b7grHg8/Gc9odCe8eg171S2V+nhHDYD7MmSpkKJOubOmnypvBhHN2DV5NsIVGn7KiKzHs6PzeXZFoz9x5uNQnSmu+ghzqNCKrcBIJ9pJQE9WoD7ChYVrkFrfV/vvVEY8vAUaM6T6OrdPaK577SeLhbosKeSUC+gY/lBWv+XUdZ9yG9nIpW/BbtL1+K5rtU8Ur64Rzi3JxLaJJV/vF9TAI+O8v5CIfufUGFOGI+ZiXf7IMvro9EwtG577oiASHjyXl490yN7uVDHWjBnOuvXrF+D4nyASxVRdul0blFZ5GtRBpVxoExg9oH2nT2eizCzeNooiqkuulDI3VUvuKjk98djoUX1gHukMMvxK4NLmqrdua+RcMy8J4sPHXAf1IJLvCKR4K8gg9+FLL5ZmQaZSUCPhmzQyuI6VBTDTRtTAHNB2KuXjpkwFCQpMO/5sVj4T1Foe2quOiHV+Fvv3q+atTVfh9/HFs6t2vHcBS7fHY8TZPE4DwgnYn6/ZTw6g3dkB2ZoMWIK4Hg0gPGDPWeQYPVrgW1lKAgm66quqprQOAS1NZBM5tdAKbpSST2UGlV6m/pM4Pp9qj0CWoi5BBXZMFOkVMajQViHeu4EvaUJPBrNAb/Pswrx5IWfWgU3BTAYFTDZXmMW5EqGBc9d8WhoeTwU+qRW4mbCsSt1KrbVHsRi/OdYPngCCo28NEnx7U6PzKQ9Hg1tK5LNtMbdhrK/oQmr6SWAemLBG7A9eTZAG6xJqHhB4Pq83pVQUN9TBNd8NKVkSbHRijqwGP4kqttqKUz7i0wXsc1weU80tE77zoofa9tnobXeiGXAM6R4WuO6FKZHOz6ofwYi8Tf7Gw51sJOWzFTtGyPu6/hbgfLbmq8lEtnZmNP8HDJeUCsfVsElPqZasJQgMm/QkoVR1MZz6wEX3fAFGFffwvLfC0pwKQ/a7cFE0rAnlLLesF+aEtE5JaVRAEBhaaxNJ5O5WxIJrt6SMpkyFnl2+7zB0wDim3pR7IBLfCwBTBFqgYwMHPR6vCuj0eALRGfVDY3kl6AWb8Q+8r9BXifpxce4owqm9edmu9kzplStfGWwHImNhxgq5K1MyP4K5bC1XdrdLRyAfffXIOSfK8tlF1zioZaYkus4zwqQnyUy+Hd7PZ7To9EO0+OPxB61P4KVpHuFkvg2BERTIF2HNWpKR343EdOjWDTMtAZ5tN6s3FRA5ZyOcjwE48LNsKT8LTnDJh9QxhEoX6sFJjxKUVDkQStjrjaZMYlp35jwAxwBZ4VOjkb97yFjhiq7liVMVVbB0P1BbDL0ad9p/aGgjwWDfjmYhJpKW19/LsFCcv9gmg2PWssuacu0KaG0vyZ7LXTN2MWSs6V6gEwwHgs/jkUDN+LZ8q4DmdrGYv6PEJf2pm25ugC2lSIioWL4sXT3t1i6u44qiRk+ke6AypzGTJxaNLT+/MY7u1UtrxatFH7cgj42FcuZkkO+2chIgVGFMXJohb9Ai/zmRBjA2+qijQo03nvSFjG1eQ0t93qz4BI/Um4a5YhXKDh21MWIb9+0iApcoidTWjPgEi3qwimoU9tJgSR/M11TASabLSbw7eiSv2K1kPm8rdlHzWQyOfUhslqEIQwL89SmPJVjLNatLXmUFEjM5++2UrFr5ctsuL3tE7PcFXRUezkTH0VtNt90FPGzuSJOF5SrWjLGpwNQFXf4vZ6dXq/3lx4PSyqiSY+0SfI9yUPHUIiXkQNvRmeUvIreg7rm0Yy5yqHHH2ebr4MC1g8+l9oZl/V4jhfWFICHkrmrAO59KFRdPUalSyS1hVZ9OHvc62WPRSJhWmAY1yHdGUoC2j824445Os66cUZY6TLZIqxca2hVSsLxnjn/FkCegnytAciOTuYdBxjHNG7AeHtHnSI5Ii7hfcHL7sI53PUWa/9XlfIm+2kjF+kOsllHq29JQuN9FS1/EGmvqbeyYmA+C8YT/wM+K50Eua4WZSQkZN6HlnaLEZ3Re7TYt9BPfiMeCy6JR0I/swgusT9VmYYRwGQAsHhenzIKPe9HPv4wHgtfiFWsRcjDP+PPWIXWclH4UWGWplLZVYqghj86CjAEUML66ga7uYZAD0KYV2J1Z2lPJPgc+FnuCFDJZiN9GS3qXo3GULKP1rGyvAzpH6CyYLVuF/Z1r4BZ6wAGjM0UZsehfAd8vtCrduKajeMowJQJrMpchgmOja1G4Z8wmVmIueMDEGw9LUXVPdMCB0CvKR8yoe1VHPw+QrgWeXhBGyke79iOXmW54BG+A7Bof9a0q4CLXaGuLgG7ZM45xwGGYAw2KNSFI0EJXu95PbHQVbHY4VMFagrLPk33XNu8mGyyyPxG496HX9bANe9o+ZRjW/SRQAc7Ea15k/a9nl8C1+yWnx4Ps2GOA0wZMQ+ysIkEZcbWyGwBQadqwbU0aOSRLcKUiE4YKhzVBtrAzynCdB/pmAla85lYsbod9DV7nGaCSxlVlUY35w0KNAIZglkHAa1sxHkcKcvoimmOg5Y15oZraNC0iRCLhMYIDz/dhHzv0AbW8oO2jG0/WL94LsRzVVfRbHApn00DmBKrBbIgeO6JxYIYx6wvyBNfyeHo6nQY5V+Iqdn9OPLxDi5NoUUPeYeClhcLxWorF7rJjrYBNe4l+O/VhJnyxmKBJ5mXnYUSp6QIEwEupe34PFgqoPRLIKNlrcHm+ANYtlwFm6QfQBH7B+m91V/w6gavNdDWLymVcyuhP6HSYsMOjJRnfYmv3vQI25yVKREAkB14lEdGiv+LVaf5CKRTjJZdvDv0Kva4V7CiuA6RPej6L2rGmKvNqKJY2lfu9tNB83I5911AeTUAienlVnu90kefHGR791OjHnM0JdKe6iczXLp0BZUR7PmzHo/vDux1bx2LNXmemtpFN0IsNK5iR+q2YimHK5j4TbXApbTISEDptApWT6yzCtw8unHpRh2kRRPvc8tiaQu6/edhJ7ZAyW8yPKsl4PIcQ8C/l0zlPkCzuhXdcJVGpMw+Wp9qYwKVQXX7LN2ms2ieekpE1yXRWrOu4/xs8HgXleuHAN7WhokuX4cDJwXAEKifttkg4OfxPMeMTLStdwS31CGuHHXh3F6VTTO9M1rhAk0AleuvsIa8GeP+XJmZix9cDzBpxmQgQNtsELBpnUEzn1UpWDTmam2aaX+3TNeKmnCoCwOobG+jRznHBPmEkrgaYBgIzIFmvBkqsWUDAW0LljRosmmee8xUldDp6Kn1y85oA58/jcr3HRUzl3lcCzDZF+OS0tfRammqYtl5verGTgscZKqzeH6vamw+3DXXGHcNUkVc2i376USY4hhkTX7tWoCZL3cChCfvAsk5NvFA4JKSJbk8TFvpjxYzusIBKbjySzbN9Wzg07ABYwYsarjTuRbgWFfwNUBkfRcKctbrnmMRY5tmOxDRChVMeu62E7cZcVwLMFqgaOXAm1JYWgUri6mPdkoERek3uVJ5NZr6zVgF21ZZ1FAyMfFM4MIo/Wt07tcE+YSQjPVjE5K8caLoAg0PvElcqFKgW38E69rUZcrdOxnZ0R3QCke7Pb8N+leksJER3lcs5i/Gxz5wmRubJoXX+pXAtXsNRS2+jQ53PcBUYDMgoyDP+Hzem3ADXgJR9hoIis7k3qBHg7Robfs6TJiuQ8pjlu4K4iPgmj7CqYja9MdJATBJpTbIdKLR80eVHZzDdGvw8wTFqeHeRvipAHhc1ZmuLMoX2NNozarzRZMJ3Brld28wgYxtwGeHEhlOf1g23J0YKZDNs+xAcxf+arkMXiyWiQ0eQBvGGvS/yuklM4O4xuhLBtHar+uRAIROY/LtdMUf7ShpeeH9JvzVcldp6c340WX/CSrTI+l03nTlMMO3TWNRAkDVj79sDXSftciuTe42CQDYU2qAux/hVa3dbfl3Ij+unQfbLKzKwE7B43IoVRWbZkVY+3GySQCtdL1OC75vspWjkfmdNNMkM4UGuGQ/pdyc+AD+gWSyuIgL5XNQ2DlYCAniYrd9sNzaFO8OvIiWbWgSayZtt9K0GsA7IWhJ0y3kcuXLcOz0asyiT9MDgOa0+ETAbbDD/gmANrcZrMfIxWGtBvDJkPVP8deDA+NPA9w/Rqs2NK8B0E/i/BMZuFfZMrsYO1NZaymApRLD0oLsp54DuKaVSGwwPgKjdVdv3kvls/LbcgADVNzNnN1lZy9Z8Hp+Px4JPmNFgG6nNV3D3V4QKX+wrrjSDriV+KJ4q8SnVX5bDmBYPdJmgy2HjYWTsCQ521Zkl0ZqOYAhZ9Xmg3W5e1SH1azHd1eMlgIY42+w1h6uWbHDZqClljRbCuDDixZC1iyY+nQeSyf19Xm4J7SlACaxYr2CFjtsO1zNRKtfLeNaDmAgvMEuOugBPoSNVV0VxG7aTsVrOYDxCbj7lQevLQmOszss0U8C4pYDOBIRDmH15i+syh6t90VYY/7Majy307ccwCRwLDk+hKuN/tq08MkumgXxLaPal6eY5uUywpZbqlTKF5+//QN8qeVeLH7MUoZLzwAUt4oKa3Fh9814brmNBipnSwNMBcTcGN9bKuAuEJGOes7BalUQmvY+3A2yKeAPPBEO2//oF/Fvu7YE2hJoS6AtgbYE2hJoS6AtgS+gBJquRb+2ddfJsGg8jYm8HwZvhvZSrYAJNHncvyccFERxZ9jX+eLAwFGZZpWraQC/tu2j1SIX/x4FO7ZZhXNlOgIbFbjwI9/RgTuWzZxZ586XcQmbAvCr2z78O5jRfN84O18cChj5vRX0eFcvXTpnv5OldnypcuO2Xd9rg1sNIVbXTs7y8jMff/wxjBScc44CvPnNPfOwlnS7c9mf5JxxWO7TwdK1TpbC0S5647YP74d6UXUml67wDYc7VB+ccrKQE80bMsBFa8XKVU7avKCrToR983oHBsa/cUAbz6zfZ5bQDh0Kdq42XhjfDJw9cwrAdbTz0CbrCv/gwWH2+aG0Ki/oquO50kenI/Bl1YsGeRyT8o4d+zuRx6O1+eybHvlCgktymI6vudAtt1pXFthCbVij/I4BPFwcnaKXSfqyyRfZdfiry4/P7OrKqhFyqk6tEVzH5WE87B84NMySw9nKF1B68WlX6tb1XKlcZvsPpBl9uJK+Eto/LVp1jb8UbzRbYIMH05VPwtKtd9MU3wKWaKTfQ4kRhuMvlTstqdVprz+U6ET62DR4Em/6kFYf0qcPSY/ncNCtytHFi1WBDQqYAIDHz/lBCJeEJrnsZ4fYQnzLCDfKSUHy7569CQj38D49KTF0a+zsGeqbZIm4iPDdnx2U76TcfyBVUfDoxnetS6YybN+g/C0NxCtU0tdreZ99nmQpVERyFSUKn8ClTwS4yTnWRdstpPbafbqFXe+ibrrbWQJXSmsY31rAqULJK/9SfO2Fo9L1wjLRkYe05tPvtMpI33DQc+kR9UJUFhdrUWVyk3MdwP6O6paq1+3RjbJaTZzuqMS3Dqvkqxffr/megxRJn7Y6T0SvpaW09XoaifdE/LoO4N4pEeZXaJo90U5Gl3jruaP6YvLoBZsqRn49R19fiWPclRxpstORjp6jbxcqFcFoV6jqG8JSvKN6Y/Ld01Sv+nGTvE79ksgn5Nd1YzAJf+HsXihOhYrCpLlEVCWkWHeIdYb6Kre1h6Dk4K5K1XulZ0Z/nE2FYkVdfggVRq+lEz21wAVIPwPFyQN2IXxrr5br7gyyRXP7KkoeVQq9KVCtuM0Kdwxgwc9zrPojYxUBa6/71RaWbmbv1FzcraWR/NRNartK6Z32d7zKoqSlVkit3oyjsnTjy2lmXUn3uxBcPZibZWaCzrEuetmX5x2AoKo+dp9KO1YWE8WdWBK6dT4HRUzrRME5y07nWjBsUzdu3bURmw1fVxZocGiYl0VRoE+o1+omlfSt8Ex6PU2jaKlSq+RDd4AxgA9ycsY5BjBlFwA+VOZqgDHtEGiuS39tR3bb7IUzBmb/xilZONZFU4ZPWzr3MZjWb3Uq85Oer8Dyfp/wfSfL4SjA1P34A8EL8Pupk4WYjLwhEwy93iuWDcx718n8OwowZXzZiTP3BgXvUkwQHdkOc1I4zvEWPsdM4Rsrls5d51wahzlXL/s4mOKmX3x0DpYMvw0TnmX4dnA/xh9zcxEH89QM1hVFivGDSGsn456npnR3P3j88dPbSkgzhN9Ooy2BtgTaEmhLoC2BtgTaEmhLoC0ByxL4fx1C+wuiLfY2AAAAAElFTkSuQmCC" />
                  <h3 class="text-gray">
                    Mạng đi phượt, chưa kết nối được. Vui lòng kiểm tra đường truyền bạn nhé!
                </h3>
                  <div>
                    <button onClick={x => window.location.reload()} className="button button-fill button-large color-pink  font-weight-bold">Thử lại</button>
                  </div>
                </div>
              )}

              {!SurveyModel.IsHaveSurvey && (
                <div className="text-center errInfo">
                  <img src="https://static.mservice.io/blogscontents/momo-upload-api-200729221537-637316577373774990.png" />
                  <h3 className="text-gray">
                    Khảo sát này đang không dành cho bạn. Vui lòng quay lại sau!!!
                      </h3>
                  <div>
                    <button onClick={x => window.ReactNativeWebView.postMessage('FinishActivity')} className="button button-fill button-large color-pink  font-weight-bold">Tìm lại</button>
                  </div>
                </div>
              )}

              {!SurveyModel.IsTimeOut && (
                <div className="text-center errInfo">
                  <img src="https://static.mservice.io/blogscontents/momo-upload-api-200730132112-637317120720464698.png" />
                  <h3 className="text-gray">
                    Chờ một tí nhé, đang nhiều người ghé.
                        </h3>
                  <div>
                    <button onClick={x => window.location.reload()} className="button button-fill button-large color-pink  font-weight-bold">Thử lại</button>
                  </div>
                </div>
              )}
            </>

          )
        }
      </Page>

      <style jsx>{``}</style>
    </>
  );
};

export default Quiz;

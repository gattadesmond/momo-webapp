import React from "react";
import {
  f7,
  Page,
  Navbar,
  List,
  ListInput,
  ListItem,
  Toggle,
  BlockTitle,
  Row,
  Button,
  Range,
  Block,
} from "framework7-react";

function DonationSuccess() {
  var dialog = f7.dialog
    .create({
      title: "",
      text: "",
      cssClass: "pron-popup-container",
      content:
        '<div class="pron-popup">' +
        '   <div class="pron-popup-image">' +
        '     <img src="/static/images/donation/img-popup-banner.svg" class="img-fluid">' +
        "   </div>" +
        '  <div class="pron-popup-content text-left">' +
        '     <h3 class="mt-0 mb-4 lutie-22 " >Thành công ! </h3> ' +
        "     <div >Chân thành cảm ơn Quý khách đã quyên góp cho chương trình Ước mơ của Thuý </div> " +
        "   </div>" +
        "</div>",
      buttons: [{ text: "ĐỒNG Ý" }],

      on: {
        open: function () {
          console.log("OPEN");
        },
      },
    })
    .open();
}

function DonationThanks() {
  var donationThanksInfo =
    '<div class="dona-thanks-info">' +
    '        <div class="dona-thanks-col">' +
    '          <div class="dona-thanks-val">' +
    "           <span>+02</span>" +
    '            <img src="/static/images/donation/heo-v-ng.svg" width="20" alt="">' +
    "          </div>" +
    '          <div class="dona-thanks-la">' +
    "            Đã quyên góp" +
    "          </div>" +
    "        </div>" +
    '        <div class="dona-thanks-col">' +
    '          <div class="dona-thanks-val">' +
    "            1056" +
    '            <img src="/static/images/donation/thanks-heart.svg" width="20" alt="">' +
    "          </div>" +
    '          <div class="dona-thanks-la">' +
    "            Lượt quyên góp" +
    "          </div>" +
    "        </div>" +
    "      </div>";

  var dialog = f7.dialog
    .create({
      title: "",
      closeByBackdropClick: true,
      text: "",
      content:
        '<div class="pron-popup">' +
        '   <div class="pron-popup-image">' +
        '     <img src="/static/images/donation/img-popup-banner.svg" class="img-fluid">' +
        "   </div>" +
        '  <div class="pron-popup-content text-left">' +
        '     <h3 class="mt-0 mb-2 lutie-18 " >Cám ơn Bạn </h3> ' +
        "     <div >Vì hành động cao đẹp. Hãy chia sẻ hoàn cảnh này đến bạn bè để điều kỳ diệu sớm trở thành hiện thực.</div> " +
        donationThanksInfo +
        '<div class="py-3"> <a href="" id="share-hmm" class="button button-fill color-pink py-1 h-auto">Chia sẻ</a> </div>' +
        "   </div>" +
        "</div>",
      cssClass: "pron-popup-container",

      on: {
        open: function (d) {
          console.log("OPEN");

          d.$el.find("#share-hmm").on("click", function (e) {
            console.log("dwdw");
          });
        },
      },
    })
    .open();
}

function DonationThanks2() {
  var donationThanksInfo2 =
    '<div class="dona-thanks-info pb-4">' +
    '        <div class="dona-thanks-col">' +
    '          <div class="dona-thanks-val">' +
    "            1056" +
    '            <img src="images/donation/thanks-heart.svg" width="20" alt="">' +
    "          </div>" +
    '          <div class="dona-thanks-la">' +
    "            Lượt quyên góp" +
    "          </div>" +
    "        </div>" +
    "      </div>";

  var dialog = f7.dialog
    .create({
      title: "",
      closeByBackdropClick: true,
      text: "",
      content:
        '<div class="pron-popup">' +
        ' <div class="pron-popup-close" ><img src="/static/images/donation/popup-close.svg"/></div> ' +
        '   <div class="pron-popup-image">' +
        '     <img src="/static/images/donation/thanks-many.png" class="img-fluid">' +
        "   </div>" +
        '  <div class="pron-popup-content text-center">' +
        '     <h3 class="mt-0 mb-2 lutie-18 " >Cảm ơn LE DUC TUAN </h3> ' +
        "     <div >Thay mặt bé và gia đình, MoMo cảm ơn bạn vì đã cùng chung tay để biến điều kỳ diệu trở thành hiện thực.</div> " +
        donationThanksInfo2 +
        "   </div>" +
        "</div>",
      cssClass: "pron-popup-container",

      on: {
        open: function (d) {
          console.log("OPEN");

          d.$el.find(".pron-popup-close").on("click", function (e) {
            app.dialog.close();
          });
        },
      },
    })
    .open();
}

function DonationNotEnought() {
  var dialog = f7.dialog
    .create({
      title: "",
      closeByBackdropClick: true,
      text: "",
      content:
        '<div class="pron-popup">' +
        ' <div class="pron-popup-close" ><img src="/static/images/donation/popup-close.svg"/></div> ' +
        '   <div class="pron-popup-image">' +
        '     <img src="/static/images/donation/z-data-no-use-popup-banner.svg" class="img-fluid">' +
        "   </div>" +
        '  <div class="pron-popup-content text-left">' +
        '     <h3 class="mt-0 mb-2 lutie-18 " >Số dư trong ví không đủ </h3> ' +
        "     <div >Tài khoản của bạn không đủ để tiếp tục ủng hộ. Bạn vui lòng nạp thêm vào ví MôM tối thiểu từ 1000đ để duy trì tài khoản đăng ký nhé!</div> " +
        '<div class="py-3"> <a href="" id="share-hmm" class="button button-fill color-pink py-1 h-auto">Nạp tiền vào ví</a> </div>' +
        "   </div>" +
        "</div>",
      cssClass: "pron-popup-container",

      on: {
        open: function (d) {
          console.log("OPEN");
          d.$el.find(".pron-popup-close").on("click", function (e) {
            app.dialog.close();
          });
        },
      },
    })
    .open();
}

function DonationNotPig() {
  var dialog = f7.dialog
    .create({
      title: "",
      closeByBackdropClick: true,
      text: "",
      content:
        '<div class="pron-popup">' +
        '   <div class="pron-popup-image">' +
        '     <img src="/static/images/donation/banner-heo-vang.jpg" class="img-fluid">' +
        "   </div>" +
        '  <div class="pron-popup-content text-left">' +
        '     <h3 class="mt-0 mb-2 lutie-18 " >Bạn chưa có Heo Vàng</h3> ' +
        "     <div >Hãy vào tính năng Hoàn tiền và cho Heo Đất ăn ngay để thu thập Heo Vàng.</div> " +
        '<div class="py-3 position-relative"> <a href="" id="share-hmm" class="button button-fill color-green py-1 h-auto">Nuôi heo</a> <img src="/static/images/donation/t-i-th-c-n.svg" class="button-coin-icon"/>  </div>' +
        "   </div>" +
        "</div>",
      cssClass: "pron-popup-container",

      on: {
        open: function (d) {
          console.log("OPEN");

          d.$el.find("#share-hmm").on("click", function (e) {
            console.log("dwdw");
          });
        },
      },
    })
    .open();
}

function DonationXacNhan() {
  var dialog = f7.dialog
    .create({
      title: "",
      text: "",
      cssClass: "pron-popup-container",
      content:
        '<div class="pron-popup">' +
        '  <div class="pron-popup-content text-left">' +
        '     <h3 class="mt-0 mb-4 lutie-22 " >Xác nhận</h3> ' +
        "     <div >Xác nhận quyên góp 5 Heo Vàng cho chương trình Ước mơ của Thuý </div> " +
        "   </div>" +
        "</div>",
      buttons: [
        {
          text: "HỦY",
          close: true,
          color: "gray",
        },
        { text: "XÁC NHẬN" },
      ],

      on: {
        open: function () {
          console.log("OPEN");
        },
      },
    })
    .open();
}

function DonationFail() {
  var dialog = f7.dialog
    .create({
      title: "",
      text: "",
      cssClass: "pron-popup-container",
      content:
        '<div class="pron-popup">' +
        '  <div class="pron-popup-content text-left">' +
        "     <div >Đã xảy ra lỗi trong quá trình xử lý. Quý khách vui lòng thực hiện lại sau.</div> " +
        "   </div>" +
        "</div>",
      buttons: [{ text: "ĐÓNG" }],

      on: {
        open: function () {
          console.log("OPEN");
        },
      },
    })
    .open();
}

export default () => (
  <Page name="form">
    <Navbar title="Form" backLink="Back"></Navbar>

    <BlockTitle>Buttons</BlockTitle>
    <Block strong>
      <Row tag="p">
        <Button className="col" onClick={() => DonationSuccess()}>
          Donation Success
        </Button>

        <Button className="col" fill onClick={() => DonationThanks()}>
          Donation Thanks
        </Button>
      </Row>
      <Row tag="p">
        <Button className="col" raised onClick={() => DonationNotEnought()}>
          Donation Not Enough
        </Button>
        <Button className="col" raised fill onClick={() => DonationNotPig()}>
          Donation Not Pig
        </Button>
      </Row>
      <Row tag="p">
        <Button className="col" round onClick={() => DonationXacNhan()}>
          Donation Xac nhan
        </Button>
        <Button className="col" round fill onClick={() => DonationFail()}>
          Donation Fail
        </Button>
      </Row>
      <Row tag="p">
        <Button className="col" outline onClick={() => DonationThanks2()}>
          Donation Thanks 2
        </Button>
        <Button className="col" round outline>
          Outline Round
        </Button>
      </Row>
      <Row tag="p">
        <Button className="col" small outline>
          Small
        </Button>
        <Button className="col" small round outline>
          Small Round
        </Button>
      </Row>
      <Row tag="p">
        <Button className="col" small fill>
          Small
        </Button>
        <Button className="col" small round fill>
          Small Round
        </Button>
      </Row>
      <Row tag="p">
        <Button className="col" large raised>
          Large
        </Button>
        <Button className="col" large fill raised>
          Large Fill
        </Button>
      </Row>
      <Row tag="p">
        <Button className="col" large fill raised color="red">
          Large Red
        </Button>
        <Button className="col" large fill raised color="green">
          Large Green
        </Button>
      </Row>
    </Block>

    <BlockTitle>Form Example</BlockTitle>
    <List noHairlinesMd>
      <ListInput label="Name" type="text" placeholder="Your name"></ListInput>

      <ListInput label="E-mail" type="email" placeholder="E-mail"></ListInput>

      <ListInput label="URL" type="url" placeholder="URL"></ListInput>

      <ListInput
        label="Password"
        type="password"
        placeholder="Password"
      ></ListInput>

      <ListInput label="Phone" type="tel" placeholder="Phone"></ListInput>

      <ListInput label="Gender" type="select">
        <option>Male</option>
        <option>Female</option>
      </ListInput>

      <ListInput
        label="Birth date"
        type="date"
        placeholder="Birth day"
        defaultValue="2014-04-30"
      ></ListInput>

      <ListItem title="Toggle">
        <Toggle slot="after" />
      </ListItem>

      <ListInput label="Range" input={false}>
        <Range slot="input" value={50} min={0} max={100} step={1} />
      </ListInput>

      <ListInput type="textarea" label="Textarea" placeholder="Bio"></ListInput>
      <ListInput
        type="textarea"
        label="Resizable"
        placeholder="Bio"
        resizable
      ></ListInput>
    </List>

    <BlockTitle>Checkbox group</BlockTitle>
    <List>
      <ListItem
        checkbox
        name="my-checkbox"
        value="Books"
        title="Books"
      ></ListItem>
      <ListItem
        checkbox
        name="my-checkbox"
        value="Movies"
        title="Movies"
      ></ListItem>
      <ListItem
        checkbox
        name="my-checkbox"
        value="Food"
        title="Food"
      ></ListItem>
    </List>

    <BlockTitle>Radio buttons group</BlockTitle>
    <List>
      <ListItem radio name="radio" value="Books" title="Books"></ListItem>
      <ListItem radio name="radio" value="Movies" title="Movies"></ListItem>
      <ListItem radio name="radio" value="Food" title="Food"></ListItem>
    </List>
  </Page>
);

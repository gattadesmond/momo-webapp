import React, { useState, useEffect } from "react";
import { Swiper, SwiperSlide, Link, Page, f7 } from "framework7-react";
import { ApiResponseErrorCodeMessage, System } from "@/libs/constants";

import axios from "@/libs/axios";
import Image from "@/components/image";

const Single = (props) => {

  const [Model, setModel] = useState({
    Blocks: []
  });

  const getData = (id, fnc) => {
    var url = `${System.apihost}/__get/Guide/DetailSingle?id=${id}`;
    axios
      .get(url)
      .then((res) => {
        if (res.Error) {
          f7.preloader.hide();
          f7.dialog.alert(ApiResponseErrorCodeMessage[res.Error.Code]);
          if (res.Error.Code === 'notfound') {
            f7.views.main.router.navigate({
              name: '404'
            });
          }
          return;
        }
        f7.preloader.hide();
        fnc && fnc(res.Data);
      })
      .catch((error) => {
        f7.preloader.hide();
        f7.dialog.alert(ApiResponseErrorCodeMessage["system_error"]);
        return;
      });

  };

  const bindData = (data) => {
    setModel({ ...data });
  }

  const pageInit = () => {
    f7.preloader.show();
    getData(props.id, (data) => {
      bindData(data);
      f7.preloader.hide();
    })
  };

  const pageDestroy = () => { };

  return (
    <Page onPageInit={pageInit} onPageBeforeRemove={pageDestroy}>

      <Swiper className=" htu-swiper"
        pagination={{ type: "fraction" }}
        spaceBetween={0}>
        {Model.Blocks.map((item, index) => (
          <SwiperSlide key={index} className="mainSlider-item">
            <div className="htu-content">
              <div className="htu-mockup">
                <div
                  className="htu-mockup-slider"
                  style={{
                    backgroundImage:
                      'url("https://static.mservice.io/pwa/images/phone-mockup.png")',
                  }}
                >
                  <img
                    src={item.Avatar}
                    loading="lazy"
                    className="img-fluid d-block mx-auto swiper-lazy"
                    alt=""
                  />
                </div>
              </div>
              <div className="htu-mockup-txt">
                <h3 className="htu-mockup-title mt-0 mb-2" dangerouslySetInnerHTML={{
                  __html: item.Title,
                }}></h3>
                <div className="htu-mockup-body" dangerouslySetInnerHTML={{
                  __html: item.Content,
                }}></div>
              </div>
            </div>
          </SwiperSlide>
        ))}
      </Swiper>

      <style jsx>{``}</style>
    </Page>
  );
};

export default Single;

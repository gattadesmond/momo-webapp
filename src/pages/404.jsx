import React from "react";
import { Page, Navbar, Block } from "framework7-react";

export default () => (
  <Page pageContent={false}>
    <Navbar title="Not found" backLink="Back" />
    <div className=" page-content d-flex align-items-center justify-content-center">
      <div style={{ textAlign: "center" }}>
        <h1
          style={{
            margin: 0,
            fontSize: "80px",
            lineHeight: "80px",
            fontWeight: "bold",
            opacity:"0.4"
          }}
          className="text-momo"
        >
          404
        </h1>
        <h2
          style={{ marginTop: "0px", fontSize: "22px" }}
          className="mb-2 text-secondary"
        >
        Page  Not Found
        </h2>
        <p className="px-5 text-secondary mt-4 mb-5">
          The resource requested could not be found on this server!
        </p>
      </div>
    </div>
  </Page>
);

import React, { useState, useEffect } from "react";
import {
  f7,
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  Sheet,
  Icon
} from "framework7-react";
import $ from 'dom7';
import anime from "animejs/lib/anime.es.js";

import events from "@/components/events";
import { ApiResponseErrorCodeMessage, System } from "@/libs/constants";

import axios from "@/libs/axios";
import MaxApi from "@momo-platform/max-api";
import format from "number-format.js";

import "./heodihoc.less";

const HeoDiHoc = (props) => {
  const [sheetMessageOpen, setSheetMessageOpen] = useState(false);

  const [data, setData] = useState({
    HeoDiHocItemModel: {},
    IsCompletedQuiz: false,
    IsCompletedQuizInDetail: false,
    CurrentQuiz: null,
    AnswerParam: null,
    AnswerResult: null,
    CountDownNext: 3,
    TotalFood: 0,
    CurrentQuestion: 1,
    UserInfo: {},
    IsNext: true,
    FirebaseToken: "",
  });


  const getData = (fnc) => {
    var url = `${System.apiQuiz}`;
    axios
      .get(url, {
        headers: {
          Authorization: "Bearer " + data.FirebaseToken,
        },
      })
      .then((res) => {
        f7.preloader.hide();
        if (!res.quiz || !res.user_record) {
          f7.dialog.alert(ApiResponseErrorCodeMessage["system_error"]);
          return;
        }

        fnc && fnc(res);
      }).catch((error) => {
        f7.preloader.hide();
        f7.dialog.alert(ApiResponseErrorCodeMessage["system_error"]);

        return;
      });
  };

  const bindData = (res, isSubmit) => {

    data.HeoDiHocItemModel = res;
    data.TotalFood = 0;
    for (var i = 0; i < res.user_record.answers.length; i++) {
      data.TotalFood = data.TotalFood + res.user_record.answers[i].prize;
    }
    if (res.quiz.questions.length <= res.user_record.answers.length) {
      if (!isSubmit) {
        data.IsCompletedQuiz = true;
      }
      else {
        data.IsCompletedQuizInDetail = true;
      }
    }

    data.ListQuizNoAnswer = [];
    for (var j = 0; j < res.quiz.questions.length; j++) {
      var isAdd = true;
      for (var i = 0; i < res.user_record.answers.length; i++) {
        if (res.quiz.questions[j].id == res.user_record.answers[i].question_id) {
          isAdd = false;
        }
      }
      if (isAdd) {
        data.ListQuizNoAnswer.push(res.quiz.questions[j]);
      }
    }

    if (data.CurrentQuiz == null && data.ListQuizNoAnswer.length > 0) {
      data.CurrentQuestion = data.HeoDiHocItemModel.user_record.answers.length + 1;
      data.CurrentQuiz = data.ListQuizNoAnswer[0];
    }
    else if (data.AnswerParam != null) {
      for (var i = 0; i < res.user_record.answers.length; i++) {
        if (data.AnswerParam.questionId == res.user_record.answers[i].question_id) {
          ///New
          $(".hdh-choice-item").removeClass("is-focus");
          data.AnswerResult = res.user_record.answers[i];
          if (res.user_record.answers[i].selection != res.user_record.answers[i].correct_selection) {
            $("#quiz-option-" + res.user_record.answers[i].selection).addClass("is-error");
          }
          ///EndNew
          $("#quiz-option-" + res.user_record.answers[i].correct_selection).addClass("is-active");
          setSheetMessageOpen(true);

          if (!data.IsCompletedQuizInDetail) {
            data.CountDownNext = 3;
            CountDownNextQuestion();
          }
          else {
            setData({ ...data });
          }
        }
      }
    }

    setData({ ...data });

  };

  const BindHeoDiHocData = () => {
    getData((res) => {
      bindData(res, false);

      var pigstudyMat = document.querySelectorAll("#pigstudy-mat");
      var pigstudyMui = document.querySelectorAll("#pigstudy-mui");
      var eyebrowLeft = document.querySelectorAll("#eyebrow-left");
      var eyebrowRight = document.querySelectorAll("#eyebrow-right");

      var heoAnimation = anime
        .timeline({
          easing: "easeInOutQuad",
          loop: true,
        })
        .add({
          targets: pigstudyMat,
          keyframes: [
            { translateX: -4, delay: 3000 },
            { translateX: 4, delay: 800 },
            { translateX: 0, delay: 800 },
          ],
          duration: 1000,
        }) //Liec mat het 1s
        .add(
          {
            targets: eyebrowLeft,
            keyframes: [
              { translateY: -2, delay: 0 },
              { translateY: 0, delay: 1000 },
            ],
            easing: "easeInOutCubic",
            duration: 500,
          },
          "-=2600"
        )

        .add(
          {
            targets: eyebrowRight,
            keyframes: [
              { translateY: -2, delay: 1000 },
              { translateY: 0, delay: 1000 },
            ],
            easing: "easeInOutCubic",
            duration: 400,
          },
          "-=2400"
        )

        .add({
          targets: pigstudyMui,
          keyframes: [
            { translateY: -2 },
            { translateY: 0 },
            { translateY: -2 },
            { translateY: 0 },
          ],
          delay: 800,
        });

    });
  }

  const NextQuestion = (currentQuiz) => {
    data.IsNext = true;

    data.CurrentQuestion = data.HeoDiHocItemModel.user_record.answers.length + 1;
    data.CurrentQuiz = currentQuiz;

    setSheetMessageOpen(false);

    $(".hdh-choice-item").removeClass("is-focus");
    $(".hdh-choice-item").removeClass("is-active");
    $(".hdh-choice-item").removeClass("is-error");

    setData({ ...data });
  }

  const CountDownNextQuestion = () => {
    window.clearTimeout(window._timeoutCountDown);

    window._timeoutCountDown = window.setTimeout(() => {
      if (!data.IsNext) {
        data.CountDownNext = data.CountDownNext - 1;
        setData({ ...data });
        if (data.CountDownNext - 1 >= 0) {
          CountDownNextQuestion();
        }
        else {
          NextQuestion(data.ListQuizNoAnswer[0]);
        }
      }
    }, 1000);
  }

  const QuizAnswerSelect = (idx) => {

    if (data.IsCompletedQuizInDetail || !data.IsNext) {
      return;
    }

    $("#quiz-option-" + idx).addClass("is-focus");
    f7.preloader.show();
    data.IsNext = false;

    data.AnswerParam = {
      quizId: data.HeoDiHocItemModel.quiz.id,
      questionId: data.CurrentQuiz.id,
      selection: idx,
      userPhone: data.UserInfo.phone
    };

    SubmitAnswer(res => {
      bindData(res, true);
      f7.preloader.hide();
    });
  }

  const SubmitAnswer = (fnc) => {

    var url = `${System.apiQuiz}/submit`;

    var param = {
      quiz_id: data.AnswerParam.quizId,
      question_id: data.AnswerParam.questionId,
      selection: data.AnswerParam.selection,
      user_id: data.AnswerParam.userPhone,
    };
    axios
      .post(url, {
        headers: {
          Authorization: "Bearer " + data.FirebaseToken,
        },
        data: param,
      })
      .then((res) => {
        fnc && fnc(res);
      })
      .catch((error) => {
        alert(error);
        f7.preloader.hide();
        f7.dialog.alert(ApiResponseErrorCodeMessage["system_error"]);

        return;
      });
  };

  const NextQuestionClick = () => {

    NextQuestion(data.ListQuizNoAnswer[0]);
  }
  const BtnFinishClick = () => {

    try {
      window.ReactNativeWebView.postMessage('FinishActivity');
    } catch (e) {

    }

    window.setTimeout(function () {
      f7.dialog.alert("Vui lòng cập nhật phiên bản mới nhất để có trải nghiệm tốt hơn.<br> Đóng Thông báo và nhấn Quay về để thoát nhiệm vụ.");
    }, 2000);
  }

  const pageInit = () => {

    events.on("CallbackGetCloudToken", (res) => {
      data.FirebaseToken = res;
      setData({
        ...data
      });

      BindHeoDiHocData();
    });

    events.on("initPageData", (res) => {
      res = JSON.parse(res);
      data.UserInfo = res;
      setData({
        ...data
      });


    });

    //Open Popup
    if (props.f7route.query.webToken) {
      //Di truc tiep
      if (props.f7route.query.webToken) {
        data.IsFromNoti = true;
        setData({
          ...data
        });
      }

      try {
        window.ReactNativeWebView.postMessage("GetUserInfo");
        window.ReactNativeWebView.postMessage(JSON.stringify({ action: "getCloudToken" }));
      } catch (e) {
      }

      //ValidateAccessToken(props.f7route.query.webToken, function (res) {

    } else {
      //di bang Miniapp
      MaxApi.getProfile(function (res) {
        data.UserInfo = {
          phone: res.user,
          userName: res.userName,
        };
        MaxApi.getDeviceInfo(function (res) {
          data.FirebaseToken = res.firebaseToken;
          setData({ ...data });

          BindHeoDiHocData();
        });

      });
    }


  };

  const pageDestroy = () => { };

  return (
    <>
      <Page onPageInit={pageInit} onPageBeforeRemove={pageDestroy}>
        {/* <Navbar className="momo-navbar">
          <NavLeft backLink></NavLeft>

          <NavTitle sliding> Cho Heo đi học</NavTitle>
        </Navbar> */}
        {data.IsCompletedQuiz ?
          <>
            <div
              className="embed-responsive embed-responsive-hdh"
              style={{ backgroundImage: 'url("/static/images/heo/bg-answer.png")' }}
            >
              <div className="hdh-question">
                <div className="hdh-question-content">
                  Lớp học hôm nay đã kết thúc, vui lòng quay lại vào ngày mai nhé!
               </div>
              </div>
              <div className="pigstudy-container">
                <div className="pigstudy">
                  <div
                    className="pigstudy-component pigstudy-mat"
                    id="pigstudy-mat"
                  >
                    <img
                      src="/static/images/heo/body/heo-mat.svg"
                      className="img-fluid"
                      alt=""
                    />
                  </div>
                  <div className="pigstudy-component pigstudy-bg-mui">
                    <img
                      src="/static/images/heo/body/heo-bg-mui.svg"
                      className="img-fluid"
                      alt=""
                    />
                  </div>
                  <div
                    className="pigstudy-component pigstudy-mui"
                    id="pigstudy-mui"
                  >
                    <img
                      src="/static/images/heo/body/heo-mui.svg"
                      className="img-fluid"
                      alt=""
                    />
                  </div>
                  <div
                    className="pigstudy-component pigstudy-eyebrow-left"
                    id="eyebrow-left"
                  >
                    <img
                      src="/static/images/heo/body/eyebrow-left.svg"
                      className="img-fluid"
                      alt=""
                    />
                  </div>
                  <div
                    className="pigstudy-component pigstudy-eyebrow-right"
                    id="eyebrow-right"
                  >
                    <img
                      src="/static/images/heo/body/eyebrow-right.svg"
                      className="img-fluid"
                      alt=""
                    />
                  </div>
                  <img
                    src="/static/images/heo/body/heo-body.png"
                    className="img-fluid"
                    alt=""
                  />
                </div>
              </div>
            </div>
            <div className="hdh-box">
              <img
                src="/static/images/heo/heo-hand.png"
                className="heoAnimate-hand"
                alt=""
              />
              <div className="block">
                <div className="text-center">
                  <img
                    src="/static/images/heo/chuc-mung.svg"
                    width={110}
                    className="img-fluid mx-auto"
                    alt=""
                  />
                  <div className="lutie-16">Hôm nay, bạn đã nhận được</div>
                  <div className="text-center mt-4">
                    <div className="hdh-gram d-inline-block text-left">
                      <img
                        src="/static/images/heo/bag-gram.svg"
                        className="icon"
                        width={28}
                        alt=""
                      />
                      {data.TotalFood} gram
                    </div>
                  </div>
                </div>
              </div>

            </div>
          </>
          :
          (data.CurrentQuiz &&
            <>
              <div
                className="embed-responsive embed-responsive-hdh"
                style={{ backgroundImage: 'url("/static/images/heo/bg-answer.png")' }}
              >
                <div className="hdh-question">
                  <div className="hdh-question-number">Câu hỏi {data.CurrentQuestion}/{data.HeoDiHocItemModel.quiz.questions.length}</div>
                  <div className="hdh-question-content">
                    {data.CurrentQuiz.question}
                  </div>
                </div>
                <div className="pigstudy-container">
                  <div className="pigstudy">
                    <div
                      className="pigstudy-component pigstudy-mat"
                      id="pigstudy-mat"
                    >
                      <img
                        src="/static/images/heo/body/heo-mat.svg"
                        className="img-fluid"
                        alt=""
                      />
                    </div>
                    <div className="pigstudy-component pigstudy-bg-mui">
                      <img
                        src="/static/images/heo/body/heo-bg-mui.svg"
                        className="img-fluid"
                        alt=""
                      />
                    </div>
                    <div
                      className="pigstudy-component pigstudy-mui"
                      id="pigstudy-mui"
                    >
                      <img
                        src="/static/images/heo/body/heo-mui.svg"
                        className="img-fluid"
                        alt=""
                      />
                    </div>
                    <div
                      className="pigstudy-component pigstudy-eyebrow-left"
                      id="eyebrow-left"
                    >
                      <img
                        src="/static/images/heo/body/eyebrow-left.svg"
                        className="img-fluid"
                        alt=""
                      />
                    </div>
                    <div
                      className="pigstudy-component pigstudy-eyebrow-right"
                      id="eyebrow-right"
                    >
                      <img
                        src="/static/images/heo/body/eyebrow-right.svg"
                        className="img-fluid"
                        alt=""
                      />
                    </div>
                    <img
                      src="/static/images/heo/body/heo-body.png"
                      className="img-fluid"
                      alt=""
                    />
                  </div>
                </div>
              </div>
              <div className="hdh-box">
                <img
                  src="/static/images/heo/heo-hand.png"
                  className="heoAnimate-hand"
                  alt=""
                />
                <div className="pron-row no-gutters align-items-center">
                  <div className="pron-col ">
                    <div className="font-weight-bold">Trả lời</div>
                    <div className="lutie-12 mt-1">Đúng: {data.CurrentQuiz.prize.correct ? format("#,###.", data.CurrentQuiz.prize.correct) : 0} gram | Sai: {data.CurrentQuiz.prize.incorrect ? format("#,###.", data.CurrentQuiz.prize.incorrect) : 0} gram</div>
                  </div>
                  <div className="pron-col auto">
                    <div className="hdh-gram">
                      <img
                        src="/static/images/heo/bag-gram.svg"
                        className="icon"
                        width={28}
                        alt=""
                      />
                      {data.TotalFood} gram
                    </div>
                  </div>
                </div>
                <div className="hdh-choice">
                  {data.CurrentQuiz.options.map((item, idx) => {

                    return (
                      <div
                        key={idx} id={`quiz-option-${idx + 1}`}
                        onClick={x => QuizAnswerSelect(idx + 1)}
                        className="hdh-choice-item"
                      >{item}</div>
                    );
                  })}

                  {/* <div className="hdh-choice-item is-focus">1.000đ</div>
              <div className="hdh-choice-item is-active is-focus">5.000đ</div>
              <div
                className="hdh-choice-item is-focus"
                style={{
                  color: "#58151c",
                  backgroundColor: "#f8d7da",
                  borderColor: "#f1909b",
                }}
              >
                10.000đ
              </div> */}
                </div>
              </div>
            </>
          )
        }

      </Page>

      <Sheet
        className=" sheet-modal-round  hdh-chinhxac-sheet border-0"
        style={{ height: "auto" }}
        opened={sheetMessageOpen}
        onSheetClosed={() => {
          setSheetMessageOpen(false);
        }}
      >
        <div className="p-4  bg-white">
          {data.AnswerResult &&
            (data.IsCompletedQuizInDetail ?
              <div className="pron-row no-gutters align-items-center">
                <div className="pron-col text-center">
                  <div className="lutie-16 ">
                    {data.AnswerResult.is_correct ?
                      "Wow, chính xác rồi!" :
                      "Sai rồi, tiếc quá!"}
                  </div>
                  <div className="font-weight-bold lutie-18">+{data.AnswerResult.prize} gram</div>
                  <div className="text-success mt-3">Bạn đã hoàn thành lớp học hôm nay!</div>

                  <div className="mt-3">
                    <button onClick={x => BtnFinishClick()}
                      className="button button-fill px-5 font-weight-bold color-pink d-inline-block">Hoàn tất</button>
                  </div>
                </div>
              </div>
              :
              <div className="pron-row no-gutters align-items-center">
                <div className="pron-col ">
                  <div className="lutie-16 ">
                    {data.AnswerResult.is_correct ?
                      "Wow, chính xác rồi!" :
                      "Sai rồi, tiếc quá!"}
                  </div>
                  <div className="font-weight-bold lutie-18">+{data.AnswerResult.prize} gram</div>
                </div>
                <div className="pron-col auto">
                  <button
                    onClick={x => NextQuestionClick()}
                    className="col button  sheet-close button-fill  color-pink font-weight-bold lutie-16 py-2 ">
                    Tiếp theo ({data.CountDownNext}s){" "}
                    <Icon f7="chevron_right" style={{ fontSize: "15px" }} />
                  </button>
                </div>
              </div>
            )
          }

        </div>
      </Sheet>

      <style jsx>{`
        .is-error {
           color: #58151c;
           background-color: #f8d7da;
           border-color: #f1909b;
        }
      `}</style>
    </>
  );
};

export default HeoDiHoc;

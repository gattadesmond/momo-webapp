import React, { useState } from "react";
import {
  FormatNumber,
} from "@/libs/constants";
const Step5 = ({ model }) => {
  const [Model, setModel] = useState(model);
  return (
    <>
      <div className="dona-status ">
        <div className="container">
          <div className="dona-status-icon">
            <img
              src="/static/images/donation/success.svg"
              width={55}
              className="img-fluid mx-auto d-block"
              alt=""
            />
          </div>
          <div className="dona-status-content text-center">
            <div className="lutie-16 mb-0">Cảm ơn bạn vì hành động cao đẹp!</div>
          </div>
        </div>
      </div>
      {Model.DonationSubcriberInfo.Type != 1 ?
        <div className="block block-strong mt-0">
          <div className="text-center">
            <div className="lutie-16 mb-0 lh-reset">
              Cảm ơn bạn đã góp phần vào sự thành công của chương trình Sức Mạnh 2.000!
              <br />
              Tiền ủng hộ định kỳ sẽ được trừ vào lúc 18h theo lịch đã đăng ký. Bạn có thể theo dõi, hủy gói ủng hộ tại mục Quản lý gói ủng hộ trong tính năng sức mạnh 2,000
          </div>
          </div>
        </div>
        :
        <>
          <div className="block block-strong mt-0">
            <div className="text-center">
              <div className="lutie-16 mb-0 lh-reset">
                Cảm ơn bạn đã góp phần vào sự thành công của chương trình Sức Mạnh 2.000!
              <br />
              Tiền ủng hộ định kỳ sẽ được trừ vào lúc 18h theo lịch đã đăng ký. Bạn có thể theo dõi, hủy gói ủng hộ tại mục Quản lý gói ủng hộ trong tính năng sức mạnh 2,000
          </div>
            </div>
          </div>

          <div className="list list-style1">
            <ul>
              <li>
                <div className="item-content">
                  <div className="item-inner">
                    <div className="item-title">Số tiền quyên góp</div>
                    <div className="item-after">{FormatNumber(Model.DonationSubcriberInfo.Amount)}đ</div>
                  </div>
                </div>
              </li>
            </ul>
          </div>

        </>
      }

    </>
  );

};

export default Step5;

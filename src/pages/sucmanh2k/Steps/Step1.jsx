import React, { useState, useEffect, Fragment } from "react";

import {
  Progressbar,
  Button,
  Link,
  Preloader,
  Popup,
  Page,
  Swiper,
  SwiperSlide,
  Navbar,
  NavLeft,
  NavTitle,
  Toolbar,
  Block,
} from "framework7-react";

import ShareBtn from "@/components/share-btn";
import { numberWithCommas } from "../../../../utils/utils";
import { Gallery } from "./components";

const Step1 = ({ model }) => {
  const [Model, setModel] = useState(model);
  const [popupArticle, setPopupArticle] = useState(false);

  return (
    <>
      {!Model || !Model.DonationInfo || Model.IsLoading ? (
        <div className="text-center py-5 mt-5">
          <Preloader size={24}></Preloader>
        </div>
      ) : (
        <>
          {/* <ShareBtn data={dona} /> */}
          <div className="embed-responsive embed-responsive-apd ">
            <div className="embed-responsive-img">
              <img
                className="img-fluid "
                alt="Sức mạnh 2,000 mùa 2: Xây dựng 5 điểm trường tiếp theo tại Huyện Mường Nhé"
                src="https://static.mservice.io/blogscontents/momo-upload-api-200416150617-637226463779722442.png"
              />
            </div>
          </div>

          <div className="block mt-4 mb-4 no-select">
            <div className="dona-title">
              <h1>
                Sức mạnh 2,000 mùa 2: Xây dựng 5 điểm trường tiếp theo tại Huyện
                Mường Nhé
              </h1>
            </div>

            <div className="soju_sapo">
              Đồng hành cùng Hoàng Hoa Trung - Forbes 30Under30 Việt Nam, trên
              chặng đường dựng xây 4,000 điểm trường, xóa sổ điểm trường tạm.
            </div>

            <div className="soju_progress">
              <div className="soju_progress_title">
                {Model.DonationInfo.Type == 1 ? (
                  <>
                    {" "}
                    <strong className="soju_progress_num">
                      {numberWithCommas(parseInt(Model.DonationInfo.amount))}đ{" "}
                    </strong>{" "}
                    / {numberWithCommas(parseInt(Model.DonationInfo.total))}đ
                  </>
                ) : (
                  <>
                    Đã quyên góp{" "}
                    <strong className="soju_progress_num">
                      {numberWithCommas(parseInt(Model.DonationInfo.amount))}
                    </strong>{" "}
                    / {numberWithCommas(parseInt(Model.DonationInfo.total))}{" "}
                    <b>Heo Vàng</b>
                  </>
                )}
              </div>
              <div className="soju_progress_bar">
                <Progressbar
                  color={
                    Model.DonationInfo.amount >= Model.DonationInfo.total
                      ? "green"
                      : Model.DonationInfo.IsEndCampaign
                      ? "gray"
                      : "pink"
                  }
                  progress={
                    Model.DonationInfo.FinishPercent > 100
                      ? 100
                      : Model.DonationInfo.FinishPercent
                  }
                ></Progressbar>
              </div>
            </div>

            <div className="soju_info">
              <div className="soju_info_item">
                <div className="soju_info_label">Lượt quyên góp</div>
                <div className="soju_info_value">
                  {numberWithCommas(
                    parseInt(Model.DonationInfo.numberDonate ?? 0)
                  )}
                </div>
              </div>

              <div className="soju_info_item">
                <div className="soju_info_label">Đạt được</div>
                <div className="soju_info_value">
                  {Model.DonationInfo.FinishPercent ?? 0}%
                </div>
              </div>

              {/* <div className="soju_info_item btn_soju">

                  {Model.DonationInfo.total != null &&
                    Model.DonationInfo.amount >= Model.DonationInfo.total 
                    &&(!Model.DonationInfo.IsContDonate || Model.DonationInfo.IsEndCampaign)
                    ? <div className="soju_info_label">Đạt mục tiêu</div>
                    : Model.DonationInfo.IsEndCampaign
                      ? <div className="soju_info_label">Hết thời hạn</div>
                      : <>
                        <div className="soju_info_label">Thời hạn còn</div>
                        <div className="soju_info_value">{Model.DonationInfo.TimeToEndDate}</div>
                      </>}


                </div> */}
            </div>

            <div className="splash no-select"></div>

            <div className="section_title no-select">Đồng hành cùng MoMo</div>

            <div className="soju_sponser no-select">
              <div className="soju_sponser_avatar">
                <img
                  src="https://static.mservice.io/default/s/no-image.png"
                  className="img-fluid"
                  loading="lazy"
                  alt="MoMo"
                />
              </div>
              <div className="soju_sponser_txt">
                <div className="soju-sponser-name">MoMo</div>
                <div>Đồng hành cùng MoMo</div>
              </div>
            </div>

            <div className="splash no-select"></div>

            <div className="section_title no-select">
              Thông tin chương trình
            </div>

            <div className="article_content dona_article_more no-select">
              <p>
                <strong>
                  &quot;Sức mạnh 2000&quot; v&agrave; tấm l&ograve;ng thiện
                  nguyện của một người b&igrave;nh thường
                </strong>
              </p>

              <p>
                Dự &aacute;n g&acirc;y quỹ chỉ từ 2,000đ lẻ mỗi ng&agrave;y được
                s&aacute;ng lập v&agrave; điều h&agrave;nh bởi Ho&agrave;ng Hoa
                Trung, đồng h&agrave;nh bởi Trung t&acirc;m T&igrave;nh nguyện
                Quốc gia v&agrave; MoMo nhằm mục ti&ecirc;u x&acirc;y dựng hơn
                4,000 điểm trường khang trang, x&oacute;a sổ nạn điểm trường tạm
                tại c&aacute;c v&ugrave;ng kh&oacute; khăn tr&ecirc;n
                to&agrave;n quốc. Với h&agrave;nh tr&igrave;nh hoạt động
                x&atilde; hội 12 năm, Hoa Trung đ&atilde; x&acirc;y dựng
                th&agrave;nh c&ocirc;ng 35 điểm trường v&agrave; hiện đang điều
                h&agrave;nh dự &aacute;n Nu&ocirc;i Em với 12,000 em nhỏ được
                nu&ocirc;i cơm với gần 4 triệu bữa ăn.&nbsp;
              </p>

              <p>
                <img
                  alt=""
                  src="https://static.mservice.io/blogscontents/momo-upload-api-200416141843-637226435238013630.png"
                />
              </p>

              <p>
                <em>Ho&agrave;ng Hoa Trung v&agrave; c&aacute;c em nhỏ</em>
              </p>

              <p>
                Trong giai đoạn hai, dự &aacute;n đặt mục ti&ecirc;u g&acirc;y
                quỹ với tổng gi&aacute; trị 1,375,000,000 VND để x&acirc;y cất 5
                ng&ocirc;i trường tại c&aacute;c bản Mường Toong, Tả K&ocirc;
                Ki, Trạm P&uacute;ng, Huổi Pết, Ng&atilde; Ba thuộc huyện Mường
                Nh&eacute; tỉnh Điện Bi&ecirc;n.
              </p>
            </div>
            <div className="mt-3">
              <Button
                outline
                className="soju-btn-gray"
                onClick={(x) => setPopupArticle(true)}
              >
                Xem chi tiết
              </Button>
            </div>

            <Popup
              className="demo-popup"
              opened={popupArticle}
              onPopupClosed={() => setPopupArticle(false)}
            >
              <Page className="no-select">
                <Navbar>
                  <NavLeft>
                    <Link
                      popupClose
                      className="text-dark"
                      iconIos="f7:chevron_left"
                    />
                  </NavLeft>
                  <NavTitle>Thông tin chương trình</NavTitle>
                </Navbar>

                <Toolbar position="bottom" className="soju-popup-toolbar ">
                  <div className="px-1 w-100">
                    <Button
                      outline
                      className="soju-btn-gray bg-white"
                      popupClose
                    >
                      Đóng
                    </Button>
                  </div>
                </Toolbar>

                <Block>
                  <div className="article_content no-select">
                    <p>
                      <strong>
                        &quot;Sức mạnh 2000&quot; v&agrave; tấm l&ograve;ng
                        thiện nguyện của một người b&igrave;nh thường
                      </strong>
                    </p>

                    <p>
                      Dự &aacute;n g&acirc;y quỹ chỉ từ 2,000đ lẻ mỗi
                      ng&agrave;y được s&aacute;ng lập v&agrave; điều
                      h&agrave;nh bởi Ho&agrave;ng Hoa Trung, đồng h&agrave;nh
                      bởi Trung t&acirc;m T&igrave;nh nguyện Quốc gia v&agrave;
                      MoMo nhằm mục ti&ecirc;u x&acirc;y dựng hơn 4,000 điểm
                      trường khang trang, x&oacute;a sổ nạn điểm trường tạm tại
                      c&aacute;c v&ugrave;ng kh&oacute; khăn tr&ecirc;n
                      to&agrave;n quốc. Với h&agrave;nh tr&igrave;nh hoạt động
                      x&atilde; hội 12 năm, Hoa Trung đ&atilde; x&acirc;y dựng
                      th&agrave;nh c&ocirc;ng 35 điểm trường v&agrave; hiện đang
                      điều h&agrave;nh dự &aacute;n Nu&ocirc;i Em với 12,000 em
                      nhỏ được nu&ocirc;i cơm với gần 4 triệu bữa ăn.&nbsp;
                    </p>

                    <p>
                      <img
                        alt=""
                        src="https://static.mservice.io/blogscontents/momo-upload-api-200416141843-637226435238013630.png"
                      />
                    </p>

                    <p style={{ textAlign: "center" }}>
                      <em>
                        Ho&agrave;ng Hoa Trung v&agrave; c&aacute;c em nhỏ
                      </em>
                    </p>

                    <p>
                      Trong giai đoạn hai, dự &aacute;n đặt mục ti&ecirc;u
                      g&acirc;y quỹ với tổng gi&aacute; trị 1,375,000,000 VND để
                      x&acirc;y cất 5 ng&ocirc;i trường tại c&aacute;c bản Mường
                      Toong, Tả K&ocirc; Ki, Trạm P&uacute;ng, Huổi Pết,
                      Ng&atilde; Ba thuộc huyện Mường Nh&eacute; tỉnh Điện
                      Bi&ecirc;n.
                    </p>

                    <p>
                      <img
                        alt=""
                        src="https://static.mservice.io/blogscontents/momo-upload-api-200416141925-637226435651609286.jpg"
                      />
                    </p>

                    <p style={{ textAlign: "center" }}>
                      <em>
                        M&ocirc; h&igrave;nh x&acirc;y dựng trường học
                        m&ugrave;a 2&nbsp;
                      </em>
                    </p>

                    <p>
                      <strong>Điểm trường Mường Toong 10</strong>
                    </p>

                    <p>
                      Ng&ocirc;i trường x&acirc;y dựng bằng vật liệu th&ocirc;
                      sơ, n&ecirc;n sau 7 năm x&acirc;y dựng, trường đ&atilde;
                      xuống cấp trầm trọng. V&aacute;ch tường được x&acirc;y
                      bằng tấm gỗ gh&eacute;p, c&ograve;n trần nh&agrave;
                      th&igrave; lợp bằng t&ocirc;n, do đ&oacute; m&ugrave;a
                      đ&ocirc;ng c&aacute;c em học dễ bị gi&oacute; lạnh
                      l&ugrave;a c&ograve;n m&ugrave;a h&egrave; th&igrave;
                      qu&aacute; n&oacute;ng. Dự &aacute;n l&ecirc;n kế hoạch
                      x&acirc;y ki&ecirc;n cố 01 ph&ograve;ng mầm non, 01
                      ph&ograve;ng c&ocirc;ng vụ.
                    </p>

                    <p>
                      <img
                        alt=""
                        src="https://static.mservice.io/blogscontents/momo-upload-api-200416141956-637226435966331171.png"
                      />
                    </p>

                    <p style={{ textAlign: "center" }}>
                      <em>Điểm trường m&ugrave;a 2: Trường bản Mường Toong</em>
                    </p>

                    <p>
                      <strong>Điểm trường Tả Kố Ki</strong>
                    </p>

                    <p>
                      Ở bản Tả Kố Ki, 100% người d&acirc;n l&agrave; d&acirc;n
                      tộc H&agrave; Nh&igrave;, cuộc sống phụ thuộc v&agrave;o
                      l&agrave;m nương rẫy n&ecirc;n đời sống c&ograve;n gặp
                      nhiều kh&oacute; khăn. Theo th&ocirc;ng tin từ hiệu trưởng
                      trường, ph&ograve;ng học tạm của c&aacute;c em đ&atilde;
                      xuống cấp trầm trọng, kh&ocirc;ng c&ograve;n đủ đ&aacute;p
                      ứng điều kiện giảng dạy, chăm s&oacute;c của c&aacute;c
                      thầy c&ocirc;. Do vậy, Dự &aacute;n sẽ g&acirc;y quỹ để
                      t&agrave;i trợ cho việc x&acirc;y dựng 1 ph&ograve;ng học
                      mầm non v&agrave; 1 ph&ograve;ng c&ocirc;ng
                      vụ.&nbsp;&nbsp;
                    </p>

                    <p>
                      <img
                        alt=""
                        src="https://static.mservice.io/blogscontents/momo-upload-api-200416142031-637226436315290823.png"
                      />
                    </p>

                    <p style={{ textAlign: "center" }}>
                      <em>Điểm trường m&ugrave;a 2: Trường bản Tả Kố Ki</em>
                    </p>

                    <p>
                      <strong>Điểm trường Bản Trạm P&uacute;ng</strong>
                    </p>

                    <p>
                      Sau 15 năm x&acirc;y dựng, t&igrave;nh trạng mối mọt khiến
                      cơ sở vật chất kh&ocirc;ng c&ograve;n đảm bảo cho
                      c&aacute;c hoạt động giảng dạy v&agrave; học tập của
                      gi&aacute;o vi&ecirc;n v&agrave; học sinh. Với số lượng
                      học sinh l&ecirc;n đến 48 em, nh&agrave; trường cần hỗ trợ
                      02 ph&ograve;ng học v&agrave; 01 ph&ograve;ng c&ocirc;ng
                      vụ.&nbsp;&nbsp;&nbsp;
                    </p>

                    <p>
                      <img
                        alt=""
                        src="https://static.mservice.io/blogscontents/momo-upload-api-200416142059-637226436599316680.png"
                      />
                    </p>

                    <p style={{ textAlign: "center" }}>
                      <em>
                        Điểm trường m&ugrave;a 2: Trường bản Trạm P&uacute;ng
                      </em>
                    </p>

                    <p>
                      <strong>Điểm trường Bản Huổi Pết 2&nbsp;</strong>
                    </p>

                    <p>
                      Được x&acirc;y dựng từ 10 năm về trước, nay lớp học
                      đ&atilde; trở n&ecirc;n qu&aacute; chật chội so với số
                      lượng học sinh nay đ&atilde; tăng l&ecirc;n đến 15 em.
                      Trong m&ugrave;a h&egrave; n&oacute;ng bức sắp tới,
                      c&aacute;c em mong ước được lắp đặt bộ năng lượng
                      gi&oacute; mặt trời, nhờ đ&oacute; m&agrave; xua đi
                      c&aacute;i n&oacute;ng bức trong mỗi buổi học.&nbsp;
                    </p>

                    <p>
                      <img
                        alt=""
                        src="https://static.mservice.io/blogscontents/momo-upload-api-200416142134-637226436941247775.png"
                      />
                    </p>

                    <p style={{ textAlign: "center" }}>
                      <em>Điểm trường m&ugrave;a 2: Trường bản Huổi Pết 2</em>
                    </p>

                    <p>
                      <strong>Điểm trường Bản Ng&atilde; Ba</strong>
                    </p>

                    <p>
                      Điểm trường c&aacute;ch trung t&acirc;m x&atilde; 4km với
                      3 lớp c&ugrave;ng khoảng 55 em học sinh. C&aacute;c lớp
                      học tại đ&acirc;y đ&atilde; được x&acirc;y dựng tr&ecirc;n
                      20 năm n&ecirc;n bị xuống cấp nặng nề, nhiều mối mọt. Do
                      vậy, Dự &aacute;n c&ugrave;ng Ph&ograve;ng Gi&aacute;o Dục
                      sẽ hỗ trợ diện t&iacute;ch, tiền san ủi đất, to&agrave;n
                      bộ b&agrave;n ghế cho c&aacute;c em nhỏ c&ugrave;ng với 2
                      ph&ograve;ng học ki&ecirc;n cố tại trường.
                    </p>

                    <p>
                      <img
                        alt=""
                        src="https://static.mservice.io/blogscontents/momo-upload-api-200416142202-637226437223860962.png"
                      />
                    </p>

                    <p style={{ textAlign: "center" }}>
                      <em>
                        Điểm trường m&ugrave;a 2: Trường bản Ng&atilde; Ba
                      </em>
                    </p>

                    <p>
                      Dự &aacute;n rất cần sự đồng h&agrave;nh của tất cả
                      c&aacute;c nh&agrave; hảo t&acirc;m, bạn đọc gần xa. Mỗi
                      đ&oacute;ng g&oacute;p nhỏ b&eacute; mỗi ng&agrave;y của
                      c&aacute;c bạn đều l&agrave; những vi&ecirc;n gạch tạo
                      n&ecirc;n k&igrave; t&iacute;ch, điều phi thường vĩ
                      đại.&nbsp;
                    </p>

                    <div
                      style={{
                        background: "#eee",
                        border: "1px solid #ccc",
                        padding: "5px 10px",
                      }}
                    >
                      <p>
                        To&agrave;n bộ số tiền của người d&ugrave;ng MoMo
                        quy&ecirc;n g&oacute;p mỗi ng&agrave;y sẽ được chuyển
                        trực tiếp cho Trung t&acirc;m T&igrave;nh nguyện Quốc
                        gia để triển khai việc x&acirc;y dựng c&aacute;c điểm
                        trường. To&agrave;n bộ th&ocirc;ng tin được update
                        tr&ecirc;n website:{" "}
                        <a
                          href="http://www.sucmanh2000.com/"
                          className="external"
                        >
                          http://www.sucmanh2000.com/
                        </a>
                      </p>
                    </div>
                  </div>
                </Block>
              </Page>
            </Popup>

            <Gallery gallerys={Model.ListAlbum} />
            {/* <DonationTimeline caseId={Model.DonationInfo.Id} /> */}
          </div>

          <style jsx>{`
            .dona-slider {
              position: relative;
              background-color: var(--gray-200);
            }

            .dona-slider:after {
              content: "";
              position: relative;
              padding-top: 53.3333%;
              display: block;
            }

            .dona-slider :global(.swiper-container) {
              position: absolute;
              top: 0;
              left: 0;
              width: 100%;
              height: 100%;
            }

            .dona-slider :global(.swiper-pagination-fraction) {
              width: auto;
              padding: 3px 11px;
              background-color: rgba(0, 0, 0, 0.6);
              border-radius: 5px;
              color: white;
              font-size: 12px;
              font-weight: 600;
              right: 10px;
              left: auto;
            }

            .dona-title h1 {
              font-size: 21px;
              line-height: 26px;
              font-weight: 600;
              margin-top: 0;
              margin-bottom: 8px;
            }

            .dona_article_more {
              max-height: 250px;
              overflow: hidden;
            }

            .soju_sponser {
              display: flex;
              flex-flow: row nowrap;
              align-items: center;
            }

            .soju_sponser_avatar {
            }
            .soju_sponser_avatar img {
              max-height: 34px;
              width: auto;
            }

            .soju-sponser-name {
              font-size: 15px;
              margin-bottom: 4px;
              font-weight: 600;
              color: var(--gray-800);
            }

            .soju_sponser_txt {
              padding-left: 10px;
              flex: 1;
              font-size: 13px;
              color: var(--gray-600);
            }

            .soju_sapo {
              margin-top: 10px;
              color: var(--gray-600);
              font-size: 14px;
              line-height: 20px;
            }

            .soju-tab :global(.mainSlider-item) {
            }

            .soju_info {
              margin-top: 15px;
              display: flex;
              flex-flow: row nowrap;
              align-items: center;
            }
            .soju_info_item {
              flex: 1 0 auto;
            }
            .soju_info_label {
              font-size: 15px;
              margin-bottom: 4px;
              line-height: 20px;
              color: var(--gray-600);
            }
            .soju_info_value {
            font-size: 15px;
            line-height: 20px;
            font-weight: bold;
          }

            .soju_progress {
              padding-top: 24px;
            }
            .soju_progress_title {
              margin-bottom: 8px;
              font-size: 14px;
              line-height: 18px;
              vertical-align: bottom;
              color: var(--gray-700);
            }

            .soju_endtime {
              padding-left: 5px;
            }
            .soju_endtime span {
              font-size: 11px;
              display: inline-block;
              padding: 3px 7px;
              border-radius: 20px;
              color: var(--orange);
              background-color: rgba(252, 100, 45, 0.15);
            }
            .soju_progress_num {
              color: var(--gray-900);
              font-size: 18px;
              line-height: 22px;
              font-weight: bold;
            }
            .soju_progress_bar :global(.progressbar) {
              height: 8px;
              border-radius: 10px;
            }
          `}</style>
        </>
      )}
    </>
  );
};

export default Step1;

import React, { useState } from "react";
import {
  FormatNumber,
} from "@/libs/constants";

import { Navbar, Toolbar, Button, NavLeft, NavRight, NavTitle } from "framework7-react";

const Step4 = ({ model }) => {
  const [Model, setModel] = useState(model);
  return (
    <>
      <Navbar className="momo-navbar">
        <NavLeft>
          <div className="logo-momo link back ml-2">
            <img
              src="https://static.mservice.io/img/logo-momo.png"
              className="img-fluid"
              width={40}
              alt=""
            />
          </div>
        </NavLeft>
        <NavTitle sliding>Xác nhận thanh toán</NavTitle>
        <NavRight>&nbsp;</NavRight>
      </Navbar>

      <div className="block-title">NGUỒN TIỀN</div>
      <div className="block block-strong ">
        <div className="pron-row no-gutters align-items-center">
          <div className="pron-col auto">
            <div className="dona-momo-icon mr-4">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="svg-inline svg"
                viewBox="0 0 29.29 27.32"
                fill="#fff"
              >
                <g data-name="Layer 2">
                  <g data-name="Layer 1">
                    <path d="M22.94 12.68a6.34 6.34 0 1 0-6.35-6.34 6.34 6.34 0 0 0 6.35 6.34zm0-9.1a2.76 2.76 0 1 1-2.77 2.76 2.77 2.77 0 0 1 2.77-2.76zM10.57 0a5.1 5.1 0 0 0-2.88.89A5.1 5.1 0 0 0 4.81 0 4.65 4.65 0 0 0 0 4.46v8.22h3.86V4.46a.93.93 0 0 1 .95-.88.93.93 0 0 1 1 .84v8.22h3.82V4.46a1 1 0 0 1 1.26-.63 1 1 0 0 1 .64.63v8.22h3.86V4.46A4.65 4.65 0 0 0 10.57 0zM22.94 27.32A6.35 6.35 0 1 0 16.59 21a6.32 6.32 0 0 0 6.34 6.32zm0-9.1a2.75 2.75 0 1 1-.05 0zM10.57 14.64a5.1 5.1 0 0 0-2.88.89 5.1 5.1 0 0 0-2.88-.89A4.65 4.65 0 0 0 0 19.1v8.22h3.86V19.1a.93.93 0 0 1 .95-.88.93.93 0 0 1 1 .84v8.22h3.82V19.1a1 1 0 0 1 1.26-.63 1 1 0 0 1 .64.63v8.22h3.86V19.1a4.65 4.65 0 0 0-4.82-4.46z" />
                  </g>
                </g>
              </svg>
            </div>
          </div>

          <div className="pron-col">
            <div className="lutie-16 font-weight-normal mb-1">Ví MoMo</div>
            <div className="opacity-5 lutie-12">{FormatNumber(Model.UserInfo.balance)}đ</div>
          </div>

        </div>
      </div>
      <div className="block-title">Chi tiết giao dịch</div>
      <div className="list list-style1">
        <ul>
          <li>
            <div className="item-content">
              <div className="item-inner">
                <div className="item-title">Tên chương trình</div>
                <div className="item-after">Sức mạnh 2000</div>
              </div>
            </div>
          </li>
          <li>
            <div className="item-content">
              <div className="item-inner">
                <div className="item-title">Số tiền</div>
                <div className="item-after">{FormatNumber(Model.DonationSubcriberInfo.Amount)}đ</div>
              </div>
            </div>
          </li>
          <li>
            <div className="item-content">
              <div className="item-inner">
                <div className="item-title">Phí giao dịch</div>
                <div className="item-after">{FormatNumber(Model.DonationSubcriberInfo.Fee)}đ</div>
              </div>
            </div>
          </li>
          <li>
            <div className="item-content">
              <div className="item-inner">
                <div className="item-title">Tổng tiền</div>
                <div className="item-after">
                  <strong>{FormatNumber(Model.DonationSubcriberInfo.TotalAmount)}đ</strong>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <div className="block">


        <div className="pron-row no-gutters">
          <div className="pron-col auto">
            <img
              src="https://static.mservice.io/pwa/images/donation/dv-image-seal.png"
              width={80}
              className="d-block mr-4"
              alt=""
            />
          </div>
          <div className="pron-col">
            <div className="lutie-12 text-gray">
              Bảo mật chuẩn SSL/TLS, mọi thông tin giao dịch đều được mã hoá an
              toàn.
            </div>
          </div>
        </div>
      </div>

    </>
  );

};

export default Step4;

import React, { useState } from "react";
import events from "@/components/events";
import { f7, Page, Toolbar, Button, Tabs, Tab } from "framework7-react";

//Lib import
import {
  ApiResponseErrorCodeMessage,
  Track
} from "@/libs/constants";
import $ from "dom7";
import { Step1, Step2, Step3, Step4, Step5, Step6 } from "./Steps";


const SucManh2kDetail = (props) => {

  const [Model, setModel] = useState({
    DonationInfo: {
      IsEndCampaign: false,
      FinishPercent: 0.0,
    },
    DonationSubcriberInfo: {
      Type: 2,
      Amount: 2000,
    },
    DateEndCampaign: '2050-04-30 00:00',
    UserInfo: {
      phone: ''
    },
    ListAlbum: [
      {
        Avatar: "https://static.mservice.io/blogscontents/momo-upload-api-200416141843-637226435238013630.png",
        Description: "Hoàng Hoa Trung và các em nhỏ"
      },
      {
        Avatar: "https://static.mservice.io/blogscontents/momo-upload-api-200416141925-637226435651609286.jpg",
        Description: "Mô hình xây dựng trường học mùa 2 "
      },
      {
        Avatar: "https://static.mservice.io/blogscontents/momo-upload-api-200416141956-637226435966331171.png",
        Description: "Điểm trường mùa 2: Trường bản Mường Toong"
      },
      {
        Avatar: "https://static.mservice.io/blogscontents/momo-upload-api-200416142031-637226436315290823.png",
        Description: "Điểm trường mùa 2: Trường bản Tả Kố Ki"
      },
      {
        Avatar: "https://static.mservice.io/blogscontents/momo-upload-api-200416142059-637226436599316680.png",
        Description: "Điểm trường mùa 2: Trường bản Trạm Púng"
      },
      {
        Avatar: "https://static.mservice.io/blogscontents/momo-upload-api-200416142134-637226436941247775.png",
        Description: "Điểm trường mùa 2: Trường bản Huổi Pết 2"
      },
      {
        Avatar: "https://static.mservice.io/blogscontents/momo-upload-api-200416142202-637226437223860962.png",
        Description: "Điểm trường mùa 2: Trường bản Ngã Ba"
      },
    ],
    Step: 1,
    LinkShare: 'https://momoapp.page.link/BsTbQ6xsyBo92Ehs5',
    IsLoading: true
  });

  const GetUserInfo = () => {
    try {
      window.ReactNativeWebView.postMessage('GetUserInfo');
    } catch (ex) {
      Track({ Name: '2kManage-GetUserInfo-ex', Info: ex.message });
    }
  };

  const GetDonationInfo = (fnc) => {

    var param = {
      msgType: "GET_INFO_EVENT_POWER",
      momoMsg: {
        user: Model.UserInfo.phone,
        service: "power_2000",
        _class: "mservice.backend.entity.msg.InfoEventPowerMsg"
      },
      extra: "",
      resFunc: "CallBackDonationInfo"
    };

    // test
    // CallBackDonationInfo('{"momoMsg":{"numberDonate":425,"amount":167500,"total":1000000,"user":"0775232512","service":"power_2000","_class":"mservice.backend.entity.msg.InfoEventPowerMsg"},"time":1585038442925,"user":"01222532512","pass":"48197ede0a33f9fc4a06bb6c3444d937c3668a6efdb985ef64106cb8c84510c8","cmdId":"1585038441224000000","lang":"vi","msgType":"GET_INFO_EVENT_POWER","result":true,"appCode":"2.1.39","appVer":21390,"channel":"APP","deviceOS":"ANDROID","ip":"172.16.1.66","localAddress":"172.16.9.119","session":"5d7fb07b-34e1-4551-af70-9bd8a82c872a","extra":{"agent":"donationspm_song01","originalPhone":"0772532512","checkSum":"iTGIa3cnfK5d7HOY5CzKIsV+Fx/L4NhW2vmubjVEQDITKy9QT6kDoVunhq+enp/1ddVisZXzJM+tPXrSedbELb/U6i7pQyeIqOcTN0zfN4U="}}');

    try {
      window.ReactNativeWebView.postMessage(JSON.stringify({ action: "requestBackend", value: param }));
    } catch (ex) {

    }



  };
  const CallbackPasswordInfo = (data) => {

    var param = {
      msgType: "REGISTER_EVENT_POWER",
      momoMsg: {
        user: Model.UserInfo.phone,
        service: "power_2000",
        amount: Model.DonationSubcriberInfo.Amount,
        type: Model.DonationSubcriberInfo.Type,
        _class: "mservice.backend.entity.msg.UserEventPowerMsg"
      },
      extra: "",
      resFunc: "CallBackDonationSubcriber"
    };
    //Track(app, JSON.stringify({ action: "requestBackend", value: param }));
    try {
      window.ReactNativeWebView.postMessage(JSON.stringify({ action: "requestBackend", value: param }));
    } catch (error) {

    }
    f7.preloader.show();
  };

  const CallBackDonationInfo = (data) => {
    try {
      var jsonString = data.replace(/\n/g, "").replace(/"{/g, "{").replace(/}"/g, "}").replace(/]"/g, "]").replace(/"\[/g, "[");
      var rs = JSON.parse(jsonString);

      if (!rs.result) {
        f7.preloader.hide();
        f7.dialog.alert(ApiResponseErrorCodeMessage["system_error"]);
        return;
      }

      if (!rs.momoMsg.amount) {
        rs.momoMsg.amount = 0;
      }
      Model.DonationInfo = rs.momoMsg;
      Model.DonationInfo.Type = 1;
      Model.DonationInfo.FinishPercent = parseFloat((Model.DonationInfo.amount / Model.DonationInfo.total) * 100).toFixed(2);
      //var time = new Date(Model.DonationInfo.endDate) - new Date();
      var time = new Date(Model.DateEndCampaign) - new Date();
      if (time <= 0) {
        Model.DonationInfo.IsEndCampaign = true;
      }
      else {
        Model.DonationInfo.TimeToEndDate = Math.ceil(Math.abs(time) / (1000 * 60 * 60 * 24));
      }
      Model.IsLoading = false;

      setModel({ ...Model });

      f7.preloader.hide();

    } catch (ex) {
      Track({ Name: '2k-CallBackDonationInfo-ex', Info: ex.message });
    }
  };

  const CallBackDonationSubcriber = (data) => {

    try {
      var jsonString = data.replace(/\n/g, "").replace(/"{/g, "{").replace(/}"/g, "}").replace(/]"/g, "]").replace(/"\[/g, "[");
      var rs = JSON.parse(jsonString);
      if (rs.result) {
        Model.Step = 5;
        f7.tab.show("#step5", true);
        var date = new Date();
        if (date.getHours() >= 18)
          date.setDate(date.getDate() + 1);

        $("#dateMoney").text(date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear());

        f7.tab.show("#step5", true);
      }
      else {
        Model.Step = 6;
        if (rs.errorDesc == undefined || rs.errorDesc == "") {
          $("#errorDesc").text("Giao dịch gặp sự cố, vui lòng thử lại");
        }
        else {
          $("#errorDesc").text(rs.errorDesc);
        }
        f7.tab.show("#step6", true);
      }

      setModel({ ...Model });

      f7.preloader.hide();
    } catch (ex) {
      Track({ Name: '2k-CallBackDonationSubcriber-ex', Info: ex.message });
    }


  };

  const InitPageData = (res) => {
    res = JSON.parse(res);
    Model.UserInfo = res;
    setModel({
      ...Model
    });
    GetDonationInfo();
  }

  const BtnDonationClick = () => {
    Model.Step = 2;
    f7.tab.show("#step2", true);
    setModel({ ...Model });
  }

  const BtnManageDonationClick = () => {
    // var self = this;
    // var app = self.$app;
    location.href = "/sucmanh2k-quanly";
    //location.href = 'momo://?refId=WebInApp&featureCode=sucmanh2k_quanly&extra=%22%7B%5C%22useNavigator%5C%22%3Atrue%2C%5C%22title%5C%22%3A%5C%22S%E1%BB%A9c%20M%E1%BA%A1nh%202000%5C%22%2C%5C%22disableToken%5C%22%3Atrue%2C%5C%22uri%5C%22%3A%5C%22https%3A%2F%2Fm.momo.vn%2Fsucmanh2k-quanly%5C%22%7D%22';
    //window.ReactNativeWebView.postMessage('FinishActivity');
  };
  const BtnSubcriberClick = () => {
    try {
      if (Model.DonationSubcriberInfo.Type == 1)//1 lan
      {
        if (Model.DonationInfo.TotalMoneyFormat == undefined || Model.DonationInfo.TotalMoneyFormat.trim() == '') {
          $("#TotalMoneyError").text("Vui lòng nhập số tiền muốn ủng hộ.");
          $("#TotalMoneyError").show();
          return;
        }
        var value = Model.DonationInfo.TotalMoneyFormat.replace(/\./g, '').replace(/\-/g, '').replace(/\ /g, '');
        if (isNaN(value)) {
          $("#TotalMoneyError").text("Số tiền muốn ủng hộ phải là số.");
          $("#TotalMoneyError").show();
          return;
        }
        Model.DonationSubcriberInfo.Amount = parseInt(value);
        Model.DonationSubcriberInfo.Fee = (Model.DonationSubcriberInfo.Amount * 0.5) / 100;
        Model.DonationSubcriberInfo.TotalAmount = Model.DonationSubcriberInfo.Amount + Model.DonationSubcriberInfo.Fee;

        if (Model.DonationSubcriberInfo.Amount < 1000) {
          $("#TotalMoneyError").text("Số tiền tối thiểu là 1.000đ.");
          $("#TotalMoneyError").show();
          return;
        }
        if (Model.DonationSubcriberInfo.Amount > 20000000) {
          $("#TotalMoneyError").text("Số tiền tối đa là 20.000.000đ");
          $("#TotalMoneyError").show();
          return;
        }
      }

      if (parseInt(Model.DonationSubcriberInfo.Amount) > parseInt(Model.UserInfo.balance)) {
        var dialog = f7.dialog.create({
          title: '',
          text: '',
          closeByBackdropClick: true,
          cssClass: "pron-popup-container",
          content: '<div class="pron-popup">' +
            ' <div class="pron-popup-close" ><img src="https://static.mservice.io/pwa/images/donation/popup-close.svg"/></div> ' +
            '   <div class="pron-popup-image">' +
            '     <img src="https://static.mservice.io/pwa/images/donation/z-data-no-use-popup-banner.svg" class="img-fluid">' +
            '   </div>' +
            '  <div class="pron-popup-content text-left">' +
            '     <h3 class="mt-0 mb-4 lutie-22 " >Số dư trong ví không đủ</h3> ' +
            '     <div >Tài khoản của bạn không đủ để tiếp tục ủng hộ. Bạn vui lòng nạp thêm tiền để chọn gói ủng hộ nhé!</div> ' +
            '     <div class="py-3 position-relative"> <a href="momo://?refId=CashInOutMomo" class="external button button-fill color-pink py-1 h-auto">Nạp tiền vào ví</a></div>' +
            '   </div>' +
            '</div>',
          on: {
            open: function (d) {
              d.$el.find(".pron-popup-close").on('click', function (e) {
                f7.dialog.close();
              });
            }
          }
        }).open();

        return;
      }

      if (Model.DonationSubcriberInfo.Type != 1) {
        Model.Step = 3;
        f7.tab.show("#step3", true);
      }
      else {
        Model.Step = 4;
        f7.tab.show("#step4", true);
      }

      setModel({ ...Model });

    } catch (ex) {
      Track({ Name: '2k-BtnSubcriberClick-ex', Info: ex.message });
    }
  };
  const BtnSubcriberConfirmClick = () => {
    if (!$('#chkAgree').prop("checked")) {
      return;
    }
    try {
      window.ReactNativeWebView.postMessage(JSON.stringify({ action: "requirePassword" }));
    } catch (ex) {

    }

  }
  const BtnSubcriberConfirmPassClick = () => {
    try {
      window.ReactNativeWebView.postMessage(JSON.stringify({ action: "requirePassword" }));
    } catch (ex) {

    }
  };

  const pageInit = () => {

    events.on("initPageData", InitPageData);
    events.on("CallBackDonationInfo", CallBackDonationInfo);
    events.on("CallbackPasswordInfo", CallbackPasswordInfo);
    events.on("CallBackDonationSubcriber", CallBackDonationSubcriber);

    GetUserInfo();
    /*InitPageData(`{
      "momoMsg": {
        "amount": 116456145,
        "total": 550000000,
        "user": "01678964081",
        "isSubscribe": true,
        "service": "power_2000",
        "numberDonate": 13,
        "endDate": 1617987600000,
        "_class": "mservice.backend.entity.msg.InfoEventPowerMsg"
      },
      "time": 1614327154347,
      "user": "01678964081",
      "pass": "48197ede0a33f9fc4a06bb6c3444d937c3668a6efdb985ef64106cb8c84510c8",
      "cmdId": "1614327154140000000",
      "lang": "vi",
      "msgType": "GET_INFO_EVENT_POWER",
      "result": true,
      "appCode": "3.0.10",
      "appVer": 30100,
      "channel": "APP",
      "deviceOS": "IOS",
      "ip": "172.16.1.66",
      "path": "/api/GET_INFO_EVENT_POWER",
      "localAddress": "172.16.9.120",
      "session": "nosessionid",
      "extra": {
        "agent": "donationspm_eusucmanh2000",
        "originalClass": "mservice.backend.entity.msg.InfoEventPowerMsg",
        "originalPhone": "01678964081",
        "checkSum": "r/ZotJVfLkJOyGE5asr28vZgd6zm/QXVMud23CLfrEfyKnK9Nq79rcZ1JBQSs096b5UHpYcHVQIuxnJHKmqiuYQIGmqqkKI3E4eDQXpd93I=",
        "TOKEN": "a8e462f6-445d-42c3-9695-89327ed4bf92"
      }
    }`);
*/
    //changeStep(6);

  };

  const changeStep = (step) => {
    Model.Step = step;

    f7.tab.show(`#step${step}`, true);

    setModel({ ...Model })
  }

  const pageOut = () => {
    f7.sheet.close();
  };

  const pageDestroy = () => {
    if (f7.sheet) f7.sheet.destroy();
  };

  return (
    <Page
      name="home"
      onPageInit={pageInit}
      onPageBeforeRemove={pageDestroy}
      onPageBeforeOut={pageOut}
    >
      {/* Top Navbar */}

      <Toolbar tabbar bottom className=" soju-toolbar ">
        {Model.Step == 1 && (
          <>
            {!Model.DonationInfo.isSubscribe ? (
              <>
                <div className="px-1 w-50">
                  <Button
                    className="external"
                    large
                    color="gray"
                    href="momo://?refId=app_all_service_top"
                  >
                    Quay lại
                  </Button>
                </div>
                {!Model.DonationInfo.isPartnerOff && (
                  <div className="px-1 w-100">
                    <Button
                      large
                      fill
                      className="soju-btn-cta"
                      onClick={(x) => BtnDonationClick()}
                    >
                      Ủng hộ ngay
                    </Button>
                  </div>
                )}
              </>
            ) : (
                <div className="px-1 w-100">
                  <Button
                    large
                    fill
                    className="soju-btn-cta"
                    onClick={(x) => BtnManageDonationClick()}
                  >
                    Quản lý gói ủng hộ
                </Button>
                </div>
              )}
          </>
        )}

        {Model.Step == 2 && (
          <div className="px-1 w-100">
            <Button
              large
              fill
              className="soju-btn-cta"
              onClick={(x) => BtnSubcriberClick()}
            >
              Tiếp tục
            </Button>
          </div>
        )}

        {Model.Step == 3 && (
          <div className="px-1 w-100">
            <Button
              id="btnDonationStep3"
              large
              fill
              className="col button button-fill button-large color-gray"
              onClick={(x) => BtnSubcriberConfirmClick()}
            >
              Tiếp tục
            </Button>
          </div>
        )}

        {Model.Step == 4 && (
          <div className="px-1 w-100">
            <Button
              large
              fill
              className="soju-btn-cta"
              onClick={(x) => BtnSubcriberConfirmPassClick()}
            >
              <i className="f7-icons lutie-18">lock_fill</i>
              Xác nhận
            </Button>
          </div>
        )}

        {Model.Step == 5 && (
          <>
            <div className={`px-1 w-${Model.DonationSubcriberInfo.Type != 1 ? '50' : '100'}`}>
              <Button
                className="external soju-btn-cta"
                href="momo://?refId=TAB0"
                large
                fill
              >
                Màn hình chính
            </Button>

            </div>
            {Model.DonationSubcriberInfo.Type != 1 && (
              <div className="px-1 w-50">
                <Button
                  large
                  fill
                  className="soju-btn-cta"
                  onClick={(x) => BtnManageDonationClick()}
                >
                  Quản lý gói ủng hộ
              </Button>
              </div>
            )}
          </>
        )}

        {Model.Step == 6 && (
          <div className="px-1 w-100">
            <Button
              className="external soju-btn-cta"
              href="momo://?refId=TAB0"
              large
              fill
            >
              Màn hình chính
            </Button>
          </div>
        )}
      </Toolbar>

      <Tabs animated>
        <Tab id="step1" className="page-content pb-0" tabActive>
          <Step1 model={Model} />
        </Tab>

        <Tab id="step2" className="page-content pb-0" tabActive>
          <Step2 model={Model} />
        </Tab>

        <Tab id="step3" className="page-content pb-0 d-flex flex-column flex-nowrap" tabActive>
          <Step3 model={Model} />
        </Tab>

        <Tab id="step4" className="page-content pb-0" tabActive>
          <Step4 model={Model} />
        </Tab>

        <Tab id="step5" className="page-content pb-0" tabActive>
          <Step5 model={Model} />
        </Tab>

        <Tab id="step6" className="page-content pb-0" tabActive>
          <Step6 />
        </Tab>
      </Tabs>

      <style jsx>{`
        .soju-tab {
        }
        .soju-tab :global(.mainSlider-item) {
        }
      `}</style>
    </Page>
  );
};

export default SucManh2kDetail;

import React, { useState, useEffect } from "react";
import {
  f7,
  Page,
  Navbar,
  NavLeft,
  NavTitle,
  Toolbar,
  useStore
} from "framework7-react";
import $ from "dom7";
import {
  ApiResponseErrorCodeMessage,
  System
} from "@/libs/constants";
import axios from "@/libs/axios";
import MaxApi from "@momo-platform/max-api";
import events from "@/components/events";
import { Style1, Style2, Style3, Style4 } from "./Template";
import GuideArticle from "@/components/guide/GuideArticle.jsx"
import "./noti.less";

const Noti = (props) => {

  const WebToken = useStore('WebToken');

  const [Model, setModel] = useState({
    Article: {
      ArticleCategoryId: 0
    },
    ListImage: [],
    ListTracking: [892],
    UserInfo: {
      phone: ''
    }
  });

  const pageInit = () => {

    bindData();

  };


  const getData = (fnc) => {

    // var slug = getSlugFromSlug(props.slug);
    // var id = getIdFromSlug(props.slug);
    var url = `${System.apihost}/__get/Article/GetPreview?slug=${props.slug}&id=${props.id}&category=${props.category}`;
    axios
      .get(url)
      .then((res) => {

        if (res.Error) {
          f7.preloader.hide();
          if (res.Error.Code === 'redirect_url') {


            f7.views.main.router.navigate({
              name: 'article-detail',
              params: {
                category: res.Data.category,
                slug: res.Data.slug,
                id: res.Data.id
              }
            });
            return;
          }
          if (res.Error.Code === 'notfound') {
            f7.views.main.router.navigate({
              name: '404'
            });
          }

          f7.dialog.alert(ApiResponseErrorCodeMessage[res.Error.Code]);

          return;
        }

        f7.preloader.hide();
        fnc && fnc(res.Data);
      })
      .catch((error) => {
        debugger
        f7.preloader.hide();
        f7.dialog.alert(ApiResponseErrorCodeMessage["system_error"]);
        return;
      });
  };
  const bindData = (fnc) => {
    getData(data => {

      Model.Article = data.Article;
      Model.ListArticleRelated = data.ListArticleRelated;
      Model.ListCategoryInGroup = data.ListCategoryInGroup;
      Model.MainCategory = data.MainCategory;
      Model.UrlRewrite = data.UrlRewrite;

      //Loại template có lấy Thông tin User
      if (Model.Article.TemplateType == 4) {
        try {
          window.ReactNativeWebView.postMessage("GetUserInfo");
        } catch (e) {
        }
      }
      getAlbum((album) => {
        bindAlbum(album);
      })
      setModel({ ...Model });

      setTimeout(() => {
        var link = $('.page-content a[href]');
        link.each(x => {
          var href = $(x).attr('href');
          if (href && href != '#' && href.indexOf('javascript') != 0) {
            $(x).addClass('external');
          }
        });

      }, 1000);
    })
  };
  const getAlbum = (fnc) => {
    var url = `${System.apihost}/__get/Article/ListArticleDetailAlbum?id=${props.id}`;
    axios
      .get(url)
      .then((res) => {
        if (res.Error) {
          f7.preloader.hide();
          f7.dialog.alert(ApiResponseErrorCodeMessage[res.Error.Code]);
          return;
        }

        f7.preloader.hide();
        fnc && fnc(res.Data);
      })
      .catch((error) => {
        debugger
        f7.preloader.hide();
        f7.dialog.alert(ApiResponseErrorCodeMessage["system_error"]);
        return;
      });
  }
  const bindAlbum = (res) => {
    Model.ListImage = res;
    setModel({ ...Model });

    if (res.length > 0) {

      var swiper = f7.swiper.create('.swiper-container', {
        pagination: {
          el: '.swiper-pagination',
        },
        spaceBetween: 15,
        slidesPerView: 'auto'
      });

      var galeryList = [];

      for (var i = 0; i < res.length; i++) {
        galeryList.push({ url: res[i].AvatarUrl, caption: res[i].Description })
      }

      var myPhotoBrowserPopupDark = f7.photoBrowser.create({
        photos: galeryList,
        theme: "dark",
        type: "standalone",
        backLinkText: ""
      });

      $(".js-noti-gallery").on("click", function () {
        debugger
        var thumb = $(this).attr('data-thumb') - 1;
        myPhotoBrowserPopupDark.open(thumb);
      });


    }
  }
  const CtaClick = (btnNumber) => {
    if (Model.ListTracking.indexOf(Model.Article.Id) >= 0) {
      trackingCtaClick(Model.Article.Id, btnNumber);
    }
  };
  const trackingCtaClick = (articleId, btnNumber) => {

    var url = `${System.apihost}/__get/Article/TrackingCtaClick?articleId=${articleId}&btnNumber=${btnNumber}`;
    axios
      .get(url)
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {

      });
  };
  const getIdFromSlug = (slug) => {
    if (!slug)
      return slug;

    var idx = slug.lastIndexOf('-');
    if (idx > -1)
      return slug.substr(idx + 1);
  }
  const getSlugFromSlug = (slug) => {
    if (!slug)
      return slug;

    var idx = slug.lastIndexOf('-');
    if (idx > -1)
      return slug.substr(0, idx);
  }
  const pageDestroy = () => { };

  return (
    <>
      <Page onPageInit={pageInit} onPageBeforeRemove={pageDestroy}>
        {/* <Navbar className="momo-navbar">
          <NavLeft backLink></NavLeft>

          <NavTitle sliding> noti</NavTitle>
        </Navbar> */}

        {Model.Article && (Model.Article.Button1Text || Model.Article.Button2Text) &&
          <Toolbar position="bottom" className=" soju-toolbar ">
            <div className=" w-100">
              <p className="row mb-0 ">
                {Model.Article.Button1Text &&
                  <a href={Model.Article.Button1Link} onClick={x => CtaClick(1)} className="col button button-fill button-large color-pink external">
                    {Model.Article.Button1Text}
                  </a>
                }
                {Model.Article.Button2Text &&
                  <a href={Model.Article.Button2Link} onClick={x => CtaClick(2)} className="col button button-fill button-large color-pink external">
                    {Model.Article.Button2Text}
                  </a>
                }
              </p>
            </div>
          </Toolbar>}

        {Model.Article &&
          Model.Article.TemplateType == 1 && <Style1 Data={Model} />
        }
        {Model.Article &&
          Model.Article.TemplateType == 2 && <Style2 Data={Model} />
        }
        {Model.Article &&
          Model.Article.TemplateType == 3 && <Style3 Data={Model} />
        }
        {Model.Article &&
          Model.Article.TemplateType == 4 && <Style4 Data={Model} />
        }
        {Model.Article &&
          <GuideArticle Html={Model.Article.Content} />
        }
      </Page>

      <style jsx>{``}</style>
    </>
  );
};

export default Noti;

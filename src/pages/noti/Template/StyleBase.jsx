import React from "react";

const StyleBase = (props) => {
  const { Data } = props;
  return (
    <>

      {Data.Article.Content && (Data.Article.IsViewMoreContent ?
        <div className="sale-cluster">
          <div className="js-sale-more">
            <div className="sale-cluster-content  noti-content" dangerouslySetInnerHTML={{
              __html: Data.Article.Content,
            }}>
            </div>
            <div className="js-sale-more-btn">+ XEM THÊM</div>
          </div>
        </div>
        :
        <div className="sale-cluster">
          <div className="sale-cluster-content  noti-content" dangerouslySetInnerHTML={{
            __html: Data.Article.Content,
          }}>
          </div>
        </div>
      )}

      {Data.Article.TermsConditionsContent && (Data.Article.IsViewMoreTermsConditionsContent ?
        <div className="sale-cluster">
          <div className="js-sale-more">
            <div className="sale-cluster-content  noti-content" dangerouslySetInnerHTML={{
              __html: Data.Article.TermsConditionsContent,
            }}>
            </div>
            <div className="js-sale-more-btn">+ XEM THÊM</div>
          </div>
        </div>
        :
        <div className="sale-cluster">
          <div className="sale-cluster-content  noti-content" dangerouslySetInnerHTML={{
            __html: Data.Article.TermsConditionsContent,
          }}>
          </div>
        </div>
      )}

      {Data.ListImage && Data.ListImage.length > 0 &&
        <div className="noti-gallery">
          <div className="swiper-container swiper-container-horizontal">
            <div className="swiper-wrapper">
              {Data.ListImage.map((item, idx) => {
                return (
                  <div key={idx} className="swiper-slide">
                    <div className="simple-slide js-noti-gallery" data-thumb={idx + 1}>
                      <div className="img-wrapper">
                        <img src={item.AvatarUrl} alt={item.Description} />
                      </div>
                      <div className="image-caption">{item.Description}</div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      }

    </>
  );
};

export default StyleBase;

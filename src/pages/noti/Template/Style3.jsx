import React from "react";
import StyleBase from './StyleBase'

const Style3 = (props) => {
  const { Data } = props;
  return (
    <>

      <div className="pron-container balance bg-white">
        {Data.Article.Avatar && Data.Article.Avatar != 'https://static.mservice.io/default/s/no-image.png' &&
          <div className="pron-row balance">
            <div >
              <img src={Data.Article.Avatar} className="img-fluid" alt="" />
            </div>
          </div>}

        <StyleBase Data={Data} />
      </div>

      <style jsx>{``}</style>
    </>
  );
};

export default Style3;

import React from "react";
import StyleBase from './StyleBase'

const Style1 = (props) => {
  const { Data } = props;
  return (
    <>

      <div className="pron-container balance bg-white">

        {Data.Article.IsShowTitle &&
          <div class="sale-header">
            <h1 class="sale-title mt-0 mb-2">
              {Data.Article.Title}
            </h1>
            <div class="sale-time">
              {Data.Article.ShortContent}
            </div>
          </div>
        }

        {Data.Article.Avatar && Data.Article.Avatar != 'https://static.mservice.io/default/s/no-image.png' &&
          <div className="pron-row balance">
            <div class="embed-responsive embed-responsive-sale">
              <img src={Data.Article.Avatar} className="embed-responsive-img" alt="" />
            </div>
          </div>

        }

        {Data.Article.MerchantLogo && Data.Article.MerchantLogo != 'https://static.mservice.io/default/s/no-image.png' &&
          <div className="sale-avatar-container">
            <div className="sale-avatar">
              <img src={Data.Article.MerchantLogo} alt="" />
            </div>
          </div>
        }

        {Data.Article.PromotionCode &&
          <div className="sale-code">
            <div className="sale-code-item">
              <div className="code">
                {Data.Article.PromotionCode}
              </div>
              <div className="copy copy-button">
                <svg width="20" height="24" viewBox="0 0 20 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M14.7368 0H2.10526C0.947368 0 0 0.981818 0 2.18182V17.4545H2.10526V2.18182H14.7368V0ZM17.8947 4.36364H6.31579C5.1579 4.36364 4.21053 5.34545 4.21053 6.54545V21.8182C4.21053 23.0182 5.1579 24 6.31579 24H17.8947C19.0526 24 20 23.0182 20 21.8182V6.54545C20 5.34545 19.0526 4.36364 17.8947 4.36364ZM17.8947 21.8182H6.31579V6.54545H17.8947V21.8182Z"
                    fill="#7C054F" />
                </svg>

                <div className="copy-popover">
                  <img src="/images/sale/checked.svg" className="copy-popover-img" width="18" alt="" /> Sao chép mã thành công
                        </div>

              </div>
            </div>
          </div>
        }

        <StyleBase Data={Data} />
      </div>

      <style jsx>{``}</style>
    </>
  );
};

export default Style1;
import React from "react";
import {
  Page,
  Navbar,
  Toolbar,
  Link,
  Tabs,
  Tab,
  Block,
  Row,
  Col,
} from "framework7-react";

const AboutPage = () => (
  <Page pageContent={false}>
    {/* <Navbar title="Swipeable Tabs" backLink="Back"></Navbar> */}
    <Toolbar inner={false} bottom className="toolbar-topup">
      <Link tabLink="#tab-1" tabLinkActive>
        <div className="">
          <div className="">
            <img src="https://static.mservice.io/fileuploads/svg/momo-file-210920160758.svg" />
          </div>
          Lợi ích Nạp Tiền Tự Động
        </div>
      </Link>
      <Link tabLink="#tab-2">
        <div className="">
          <div className="">
            <img src="https://static.mservice.io/fileuploads/svg/momo-file-210920160935.svg" />
          </div>
          Định nghĩa và cơ chế hoạt độ
        </div>
      </Link>
      <Link tabLink="#tab-3">
        <div className="">
          <div className="">
            <img src="https://static.mservice.io/fileuploads/svg/momo-file-210920161002.svg" />
          </div>
          An toàn - Bảo mật
        </div>
      </Link>
    </Toolbar>
    <Tabs swipeable>
      <Tab id="tab-1" className="page-content page-content-topup " tabActive>
        <div className="pron-container balance bg-white">
          <div className="sale-cluster">
            <div className="sale-cluster-content  noti-content">
              <h1 style={{fontSize: '24px', color: '#303233'}}>Lợi ích Nạp Tiền Tự Động</h1>

              <div className="d-flex  align-items-center p-1 mb-3">
                <div className="flex-shrink-0" style={{width: "34px"}}>
                  <img src="https://static.mservice.io/fileuploads/svg/momo-file-210920160935.svg" width="100" />
                </div>
                <div className="pl-2 flex-grow-1" style={{fontSize: '14px', lineHeight: '18px'}}>
                  Nạp tiền tự động giúp quá trình thanh toán của bạn trở nên
                  nhanh chóng và không gián đoạn{" "}
                </div>
              </div>


              <div className="d-flex  align-items-center p-1 mb-3">
                <div className="flex-shrink-0" style={{width: "34px"}}>
                  <img src="https://static.mservice.io/fileuploads/svg/momo-file-210920160935.svg" width="100" />
                </div>
                <div className="pl-2 flex-grow-1" style={{fontSize: '14px', lineHeight: '18px'}}>
                Khi cài đặt tính năng này bạn sẽ luôn có sẵn tiền trong Ví để chi tiêu mà không cần tiền mặt
                </div>
              </div>
            </div>
          </div>
        </div>
      </Tab>
      <Tab id="tab-2" className="page-content page-content-topup">
        <Block>
          <p>Tab 2 content</p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam enim
            quia molestiae facilis laudantium voluptates obcaecati officia cum,
            sit libero commodi. Ratione illo suscipit temporibus sequi iure ad
            laboriosam accusamus?
          </p>
          <p>
            Saepe explicabo voluptas ducimus provident, doloremque quo totam
            molestias! Suscipit blanditiis eaque exercitationem praesentium
            reprehenderit, fuga accusamus possimus sed, sint facilis ratione
            quod, qui dignissimos voluptas! Aliquam rerum consequuntur deleniti.
          </p>
          <p>
            Totam reprehenderit amet commodi ipsum nam provident doloremque
            possimus odio itaque, est animi culpa modi consequatur reiciendis
            corporis libero laudantium sed eveniet unde delectus a maiores nihil
            dolores? Natus, perferendis.
          </p>
          <p>
            Atque quis totam repellendus omnis alias magnam corrupti, possimus
            aspernatur perspiciatis quae provident consequatur minima doloremque
            blanditiis nihil maxime ducimus earum autem. Magni animi blanditiis
            similique iusto, repellat sed quisquam!
          </p>
          <p>
            Suscipit, facere quasi atque totam. Repudiandae facilis at optio
            atque, rem nam, natus ratione cum enim voluptatem suscipit veniam!
            Repellat, est debitis. Modi nam mollitia explicabo, unde aliquid
            impedit! Adipisci!
          </p>
          <p>
            Deserunt adipisci tempora asperiores, quo, nisi ex delectus vitae
            consectetur iste fugiat iusto dolorem autem. Itaque, ipsa voluptas,
            a assumenda rem, dolorum porro accusantium, officiis veniam nostrum
            cum cumque impedit.
          </p>
          <p>
            Laborum illum ipsa voluptatibus possimus nesciunt ex consequatur
            rem, natus ad praesentium rerum libero consectetur temporibus
            cupiditate atque aspernatur, eaque provident eligendi quaerat ea
            soluta doloremque. Iure fugit, minima facere.
          </p>
        </Block>
      </Tab>
      <Tab id="tab-3" className="page-content page-content-topup">
        <Block>
          <p>Tab 3 content</p>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam enim
            quia molestiae facilis laudantium voluptates obcaecati officia cum,
            sit libero commodi. Ratione illo suscipit temporibus sequi iure ad
            laboriosam accusamus?
          </p>
          <p>
            Saepe explicabo voluptas ducimus provident, doloremque quo totam
            molestias! Suscipit blanditiis eaque exercitationem praesentium
            reprehenderit, fuga accusamus possimus sed, sint facilis ratione
            quod, qui dignissimos voluptas! Aliquam rerum consequuntur deleniti.
          </p>
          <p>
            Totam reprehenderit amet commodi ipsum nam provident doloremque
            possimus odio itaque, est animi culpa modi consequatur reiciendis
            corporis libero laudantium sed eveniet unde delectus a maiores nihil
            dolores? Natus, perferendis.
          </p>
          <p>
            Atque quis totam repellendus omnis alias magnam corrupti, possimus
            aspernatur perspiciatis quae provident consequatur minima doloremque
            blanditiis nihil maxime ducimus earum autem. Magni animi blanditiis
            similique iusto, repellat sed quisquam!
          </p>
          <p>
            Suscipit, facere quasi atque totam. Repudiandae facilis at optio
            atque, rem nam, natus ratione cum enim voluptatem suscipit veniam!
            Repellat, est debitis. Modi nam mollitia explicabo, unde aliquid
            impedit! Adipisci!
          </p>
          <p>
            Deserunt adipisci tempora asperiores, quo, nisi ex delectus vitae
            consectetur iste fugiat iusto dolorem autem. Itaque, ipsa voluptas,
            a assumenda rem, dolorum porro accusantium, officiis veniam nostrum
            cum cumque impedit.
          </p>
          <p>
            Laborum illum ipsa voluptatibus possimus nesciunt ex consequatur
            rem, natus ad praesentium rerum libero consectetur temporibus
            cupiditate atque aspernatur, eaque provident eligendi quaerat ea
            soluta doloremque. Iure fugit, minima facere.
          </p>
        </Block>
      </Tab>
    </Tabs>

    <style jsx global>{`
      .toolbar-topup {
        height: auto;
        display: flex;
        align-content: center;
        justify-content: center;
        flex-wrap: nowrap;
        padding: 10px 6px;
        background-color: white;
      }
      .toolbar-topup .tab-link {
        flex: 1 1 0px;
        height: auto;
        display: block;
        white-space: normal;
        line-height: inherit;
        font-size: 11px;
        border: 2px solid #e8e8e8;
        border-radius: 10px;
        padding: 8px;
        margin: 0 6px;
        color: #303233;
      }
      .toolbar-topup .tab-link-active {
        border: 2px solid #ff85c0;
        background-color: #fff7fa;
      }
      .toolbar-topup .tab-link {
      }

      .page-content-topup {
        padding-bottom: 120px;
        padding-top: 10px;
      }
    `}</style>
  </Page>
);

export default AboutPage;

export function getSlug(params) {
  // Handle optional catch all route for `/docs`
  const slug = params.slug;

  if (slug[0] === "tag") {
    return {
      tag: slug[1],
      slug: `/docs/${getDocsSlug(slug.slice(2)).join("/")}`,
    };
  }

  return { slug: `/docs/${slug.join("/")}` };
}

export function isEmpty(value) {
  return value === undefined || value === null || value === "";
}

export function isEmptyObject(obj) {
  if (obj !== null && obj !== undefined) return Object.keys(obj).length === 0;
  return true;
}

export function isInteger(value) {
  if (undefined === value || null === value) {
    return false;
  }
  return value % 1 === 0;
}

export function isNumeric(str) {
  if (typeof str != "string") return false; // we only process strings!
  return (
    !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
    !isNaN(parseFloat(str))
  ); // ...and ensure strings of whitespace fail
}

export function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}
